// File : star_io_mmdb.h

%module star_io_mmdb
// Suppress compile warning: 
// "Warning 451: Setting a const char * variable may leak memory."
// No adverse effects noted
#pragma SWIG nowarn=451

%include "std_vector.i"
%include "std_string.i"

namespace mmdb {
  typedef  char*             pstr;
  typedef  const char*       cpstr;
  typedef  realtype*         rvector;
}

%{
  // the resulting C file should be built as a python extension
  #define SWIG_FILE_WITH_INIT
  //  Includes the header in the wrapper code
  #include "star_io_mmdb.h"
%}

// std::vector support
%include "std_vector.i"
namespace std {
    %template(DoubleVector) vector<double>;
    %template(IntVector) vector<int>;
    %template(StringVector) vector<std::string>;
}
// End std::vector support

%include star_io_mmdb.h

