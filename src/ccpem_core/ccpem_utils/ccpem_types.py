#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import sys
import re

'''
CCPEM types.  For CCP-EM argument parser help string should be tagged with
type where necessary.  Tags take the form:
    #CCPEMType#
'''

def get_ccpem_type(help_str):
    '''
    Parse string for ccpem_type.  N.B. finds first occurrence of
        #CCPEMType#
    in help string and returns corresponding type object of same name.
    '''
    type_regex = re.compile("(#)(?P<ccpem_type>.*?)(#)")
    match = type_regex.search(help_str)
    ct = sys.modules[__name__]
    if match is not None:
        ccpem_type = match.group('ccpem_type')
        if hasattr(ct, ccpem_type):
            ccpem_type = getattr(ct, ccpem_type)
    else:
        ccpem_type = None
    return ccpem_type


class CCPEMType(object):
    '''
    Base class for CCPEM types
    '''
    def __init__(self, *args, **kwargs):
        pass


class CCPEMTypeFile(CCPEMType):
    '''
    Generic file type.
    '''
    def __init__(self, *args, **kwargs):
        super(CCPEMTypeFile, self).__init__(*args, **kwargs)
        self.path = kwargs.pop('path', None)
        self.children = kwargs.pop('children', None)
        parents = kwargs.pop('parents', None)
        self.display = kwargs.pop('display', True)
        #
        self.parents = []
        self.children = []
        if parents is not None:
            self.set_parents(parents=parents)

    def set_parents(self, parents):
        for parent in parents:
            self.parents.append(parent)
            if hasattr(parent, '_set_children'):
                parent._set_children([self])

    def _set_children(self, children):
        for child in children:
            self.children.append(child)


class CCPEMTypeFileMap(CCPEMTypeFile):
    '''
    Map file type.
    '''
    def __init__(self, *args, **kwargs):
        super(CCPEMTypeFileMap, self).__init__(*args, **kwargs)


class CCPEMTypeFilePDB(CCPEMTypeFile):
    '''
    Map file type.
    '''
    def __init__(self, *args, **kwargs):
        super(CCPEMTypeFilePDB, self).__init__(*args, **kwargs)


def is_sub_class(i_class, sub_class):
    '''
    Check returns true if sub_class is a sub class of class  i
    '''
    return issubclass(i_class, sub_class)


def main():
    '''
    For testing.
    '''
    #
    map_in = CCPEMTypeFileMap(path='./in.map')
    print is_sub_class(map_in.__class__, CCPEMType)
    map_out = CCPEMTypeFileMap(path='./out.map',
                               parents=[map_in])
    #
    print 'Map in parents : ', map_in.parents
    print 'Map in children: ', map_in.children
    print 'Map out parents : ', map_out.parents
    print 'Map out children: ', map_out.children


if __name__ == '__main__':
    main()
