#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
'''
Task window for MRC2TIF program.
'''
from ccpem_core.tasks.mrc_mrc2tif import mrc_mrc2tif_task
from ccpem_gui.utils import window_utils

class Mrc2TifWindow(window_utils.CCPEMTaskWindow):
    '''
    Inherits from CCPEMTaskWindow.
    '''
    def __init__(self,
                 task,
                 parent=None):
        super(Mrc2TifWindow, self).__init__(parent=parent,
                                            task=task)

    def set_args(self):
        '''
        Set input arguments.
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        # MRC header display
        mrc_header_frame = window_utils.CCPEMExtensionFrame(
            button_name='Image details',
            button_tooltip='Show image details')
        mrc_header_frame.button.click()
        self.mrc_header = window_utils.MRCMapHeaderInfo(
            parent=self,
            filename=self.args.mrc_in.value)
        mrc_header_frame.add_extension_widget(self.mrc_header)
        # Input mrc
        mrc_input = window_utils.FileArgInput(parent=self,
                                              required=True,
                                              arg_name='mrc_in',
                                              args=self.args)
        self.args_widget.args_layout.addWidget(mrc_input)
        mrc_input.value_line.textChanged.connect(
            lambda: self.mrc_header.set_filename(
                str(mrc_input.value_line.text())))
        self.args_widget.args_layout.addLayout(mrc_header_frame)


    def set_on_job_finish_custom(self):
        if hasattr(self.task, 'tif_out_path'):
            self.launcher_dock.setVisible(True)
            self.launcher.add_file(
                arg_name=None,
                file_type='standard',
                path=self.task.tif_out_path,
                description='Output TIF',
                display_from='finished',
                selected=True)

def main():
    '''
    For testing
    '''
    window_utils.standalone_window_launch(
        task=mrc_mrc2tif_task.Mrc2Tif,
        window=Mrc2TifWindow)

#         args_json='./test_data/unittest_args.json')

if __name__ == '__main__':
    main()
