#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Test buccaneer task
'''

import unittest
import os
import sys
import shutil
import time
import tempfile
from PyQt4 import QtGui, QtCore
from PyQt4.QtTest import QTest
from ccpem_core.tasks.buccaneer import buccaneer_task
from ccpem_gui.tasks.buccaneer import buccaneer_window
from ccpem_core.test_data.tasks import buccaneer as buccaneer_test
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from lxml import etree

app = QtGui.QApplication(sys.argv)

class BuccaneerTest(unittest.TestCase):
    '''
    Unit test for Buccaneer (invokes GUI).
    '''
    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_data = os.path.dirname(buccaneer_test.__file__)
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(self.test_output):
            shutil.rmtree(self.test_output)

    def test_buccaneer_window_integration(self):
        '''
        Test Buccaneer auto building pipeline via GUI.
        '''
        ccpem_utils.print_header(message='Unit test - Buccaneer')
        # Unit test args contain relative paths, must change to this directory
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        args_path = os.path.join(self.test_data, 'unittest_args.json')
        run_task = buccaneer_task.Buccaneer(
            job_location=self.test_output,
            args_json=args_path)
        print run_task.args.output_args_as_text()
        # Run w/out gui
#         run_task.run_task()
        # Run w/ gui
        window = buccaneer_window.BuccaneerWindow(task=run_task)
        # Mouse click run
        QTest.mouseClick(
            window.tool_bar.widgetForAction(window.tb_run_button),
            QtCore.Qt.LeftButton)
        # Wait for run to complete
        job_completed = False
        xmlout_found = False
        # Global refine stdout (i.e. last job in pipeline)
        stdout_ref = run_task.pipeline.pipeline[-1][-1].stdout
        stdout_buc = run_task.pipeline.pipeline[-2][-1].stdout
        assert os.path.basename(stdout_ref) == 'refmacrefineglobal1_stdout.txt'
        assert os.path.basename(stdout_buc) == 'buccaneerbuild1_stdout.txt'
        # Wait for job to finish
        run_time = 0
        timeout = 500
        delay = 5.0
        while not job_completed and run_time < timeout:
            print 'Buccaneer running for {0} secs (timeout = {1})'.format(
                run_time,
                timeout)
            time.sleep(delay)
            run_time += delay
            status =\
                process_manager.get_process_status(run_task.pipeline.json)
            # Check job is finished
            if status == 'finished':
                if os.path.isfile(stdout_buc) and os.path.isfile(stdout_ref):
                    tail = ccpem_utils.tail(stdout_ref, maxlen=10)
                    if tail.find('CCP-EM process finished') != -1:
                        # Check Buccaneer output exists 
                        buccaneer_pdb = \
                            run_task.process_buccaneer_pipeline.pdbout
                        if os.path.exists(buccaneer_pdb):
                            job_completed = True
                        # Get result summary
                        result_print = getResultLines(stdout_buc)
                        for line in result_print:
                            print line
                        if os.path.isfile('program.xml'):
                            xmlout_found = True
                            print 'Found program.xml'
                            get_final_data('program.xml')

        # Check timeout
        assert run_time < timeout
        # Check job completed
        assert job_completed
        # Check program.xml produced
        assert xmlout_found, 'xml output not found!'

    def xxx_performance(self):
        '''
        Test Buccaneer auto building pipeline's performance via GUI.
        '''
        ccpem_utils.print_header(message='Unit test - Buccaneer (Performance)')
        # Unit test args contain relative paths, must change to this directory
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        args_path = os.path.join(self.test_data, 'Performancetest_args.json')
        run_task = buccaneer_task.Buccaneer(
            job_location=self.test_output,
            args_json=args_path)
        print run_task.args.output_args_as_text()
        # Run w/out gui
#         run_task.run_task()
        # Run w/ gui
        window = run_task.task_window(task=run_task)
        # Mouse click run
        QTest.mouseClick(
            window.tool_bar.widgetForAction(window.tb_run_button),
            QtCore.Qt.LeftButton)
        # Wait for run to complete
        job_completed = False
        timeout = 0
        # Global refine stdout (i.e. last job in pipeline)
        stdout_ref = run_task.pipeline.pipeline[-1][-1].stdout
        stdout_buc = run_task.pipeline.pipeline[-2][-1].stdout
        assert os.path.basename(stdout_ref) == 'refmacrefineglobal_stdout.txt'
        assert os.path.basename(stdout_buc) == 'buccaneerbuild_stdout.txt'
        delay = 60.0
        while not job_completed and timeout < 2000:
            print 'Buccaneer running for {0} secs (timeout = 2000)'.format(timeout)
            time.sleep(delay)
            timeout += delay
            status =\
                process_manager.get_process_status(run_task.pipeline.json)
            # Check job is finished
            if status == 'finished':
                if os.path.isfile(stdout_ref) and os.path.isfile(stdout_buc):
                    tail = ccpem_utils.tail(stdout_ref, maxlen=10)
                    if tail.find('CCP-EM process finished') != -1:
                        # Check Buccaneer output exists 
                        buccaneer_pdb = \
                            run_task.process_buccaneer_pipeline.pdbout
                        if os.path.exists(buccaneer_pdb):
                            job_completed = True
                        # Get result summary
                        result_print = getResultLines(stdout_buc)
                        for line in result_print:
                            print line
                        if os.path.isfile('program.xml'):
                            xmlout_found = True
                            print 'Found program.xml'
                            get_final_data('program.xml')

        # Check timeout
        assert timeout < 2000
        # Check job completed
        assert job_completed
        # Check program.xml produced
        assert xmlout_found, 'xml output not found!' 

'''
Get result summary from buccaneer stdout
'''
def getResultLines(filename):
    tail_str = []
    found_result = False
    found_summary = False
    found_bucstart = False
    count=0
    with open(filename, 'r') as openfile:
        for line in openfile:
            if ' cbuccaneer' in line:
                found_bucstart = True
                tail_str = []
            if found_bucstart:
                if '--SUMMARY_END--' in line:
                    found_summary = True
                if found_summary:
                    if 'TEXT:Result:' in line:
                        found_result = True
            if found_result and count<10:
                temp_line=line.strip('\n')
                tail_str.append(temp_line)
                count = count + 1;
            if 'cbuccaneer: Normal termination' in line:
                found_summary = False
                found_result = False
                found_bucstart = False
                count = 0
    return tail_str

'''
Get result summary from buccaneer program.xml
'''
def get_final_data(filename):
    '''
    Get the data from XML output
    '''
    tree = etree.parse(filename)
    for child in tree.findall('Final'):
        CompResBuilt = child.find('CompletenessByResiduesBuilt')
        CompChainBuilt = child.find('CompletenessByChainsBuilt')
        ChainsBuilt = child.find('ChainsBuilt')
        FragsBuilt = child.find('FragmentsBuilt')
        ResUniq = child.find('ResiduesUnique')
        ResBuilt = child.find('ResiduesBuilt')
        ResSeq = child.find('ResiduesSequenced')
        ResLongFrag = child.find('ResiduesLongestFragment')

    print ""
    print "{0} : {1:.1f}%" .format(CompResBuilt.tag, float(CompResBuilt.text)*100)
    print "{0} : {1:.1f}%" .format(CompChainBuilt.tag, float(CompChainBuilt.text)*100)
    print "{0} : {1}" .format(ChainsBuilt.tag, ChainsBuilt.text)
    print "{0} : {1}" .format(FragsBuilt.tag, FragsBuilt.text)
    print "{0} : {1}" .format(ResUniq.tag, ResUniq.text)
    print "{0} : {1}" .format(ResBuilt.tag, ResBuilt.text)
    print "{0} : {1}" .format(ResSeq.tag, ResSeq.text)
    print "{0} : {1}" .format(ResLongFrag.tag, ResLongFrag.text)
    print ""
    
    
if __name__ == '__main__':
    unittest.main()
