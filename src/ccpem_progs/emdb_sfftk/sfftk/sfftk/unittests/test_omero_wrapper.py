#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division

"""
test_omero_wrapper.py

Unit tests for OMERO handlers
"""

__author__  = 'Paul K. Korir, PhD'
__email__   = 'pkorir@ebi.ac.uk'
__date__    = '2016-06-13'

import sys
import os
import unittest
import omero_wrapper.handlers

user = 'test_user'
password = 'test'
host = 'localhost'
port = '4064'


class TestOMEROConnectionHandler(unittest.TestCase):
    """Tests on setting and closing the connection"""
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.connection_handler = omero_wrapper.handlers.OMEROConnectionHandler(user=user, password=password, host=host, port=port)
        
    def test_connect(self):
        """Test that entering the context makes it connected"""
        with self.connection_handler as conn_handle:
            self.assertTrue(self.connection_handler.connected)
    
    def test_disconnect(self):
        """Test that exiting the context handler disconnects""" 
        with self.connection_handler as conn_handle:
            pass
        self.assertFalse(self.connection_handler.connected)
    

class TestConnection(unittest.TestCase):
    """Test on using the opened connection"""
    def test_user(self):
        pass