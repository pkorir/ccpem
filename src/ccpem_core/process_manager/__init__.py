#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

"""
Objects which should be used as the process API by the rest of the system.
"""
from .ccpem_pipeline import (CCPEMPipeline,
                             CCPEMPipelineCustomFinish)
from .ccpem_process import (CCPEMProcess,
                            CCPEMProcessCustomFinish)
from .process_utils import (get_process_status,
                            set_status,
                            statuses,
                            task_filename)
