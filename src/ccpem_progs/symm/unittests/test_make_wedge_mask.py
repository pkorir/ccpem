#!/usr/bin/env python
# coding: utf-8

import unittest
import numpy as np
import make_wedge_mask


class TestAnalysisMethods(unittest.TestCase):

    def setUp(self):
        pass

    def test_make_mask(self):
        mask = make_wedge_mask.get_missing_wedge_volume(-55, 55, 20)
        exp = np.load("unittests/testdata/unrot_mask.npy")
        np.testing.assert_array_equal(mask, exp, "Unrotated mask is different from the expected one.")


if __name__ == "__main__":
        unittest.main()
