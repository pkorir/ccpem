# coding: utf-8

"""
Helper functions for process_one.py and process_all.py
"""
import os
import numpy as np
from helper_functions.nptomo import open_tom

try:
    from scipy.stats.mstats import zscore as sci_zscore
except ImportError:
    pass


###
### Normalisation functions
###

def scipy_zscore(data):
    return sci_zscore(data, axis=1)

def zscore(data, scaling_factor=1):
    """
    For each column in a slice, calculate the mean and standard deviation,
    then based on those values convert each pixel to:
    (new pixel value) = ((old pixel value) - (mean pixel value of 148 pixels in this column)) / (standard deviation of pixel values for the 148 pixels in this column).
    This value should probably also be multiplied by a constant
    --maybe 50 or 100? -- to scale it to a reasonable value between 0 and 255
    """
    # calculate mean for each column
    mean    = np.nanmean(data, 1)

    # calculate standard deviation for each column
    std_dev = np.nanstd(data, 1)

    # for each pixel value
    # new = (old - mean_of_column) / std_dev_of_column
    iter_data = np.nditer(data, flags=["multi_index"], op_flags=['readwrite'])
    data = np.zeros(data.shape, dtype=np.float)
    for el in iter_data:
        column   = iter_data.multi_index[2]
        depth    = iter_data.multi_index[0]
        col_mean = mean[depth, column]
        col_std  = std_dev[depth, column]
        data[iter_data.multi_index] = ((el - col_mean) / col_std) * scaling_factor

    return data


def minmax(data, scaling_factor=1):
    """"""
    max_value = np.nanmax(data)
    min_value = np.nanmin(data)

    print max_value, min_value
    data = (data-min_value)/(max_value-min_value) * scaling_factor
    return data


###
###
###

def get_filepaths(folderpath, extension):
    """
    """
    filenames = os.listdir(folderpath)
    filenames = [fn for fn in filenames if fn[-3:] == extension]
    filepaths = [os.path.join(folderpath, fn) for fn in filenames]
    return filepaths


def get_subtoms(filepaths, do_something=None):
    """
    """
    for filepath in filepaths:

        with open_tom(filepath) as t:

            data = t.get_all_tom_density_values()
            if do_something:
                data = do_something(data)

            yield data
        
        del data


def write_row(row, delimiter):

    with open("mut_inf.csv", "a") as ofile:

        writer = csv.writer(ofile, delimiter=delimiter)
        writer.writerow(row)

    return

###
### Bit of a hack of a convolution function for different window sizes
### in the case of scipy not being installed
###

def convolve2d_hack(matrix_name, convolved_matrix_name, window_width):
    ww = window_width
    
    op = "%s[%d:%d, %d:%d] = (" % (convolved_matrix_name, ww/2, -(ww/2), ww/2, -(ww/2))

    for ii in range(ww):
        for jj in range(ww):
            op += "%s[%d:%d, %d:%d] + " % (matrix_name, ii, ii-ww+1, jj, jj-ww+1)
    
    op = op[:-3] + ')'
    op = op.replace('0]', ' ]')
    op = op.replace('0,', ' ,')
        
    # op += "; %s = %s/%d" % (convolved_matrix_name, convolved_matrix_name, ww**2)
    return op
