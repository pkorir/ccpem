#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

from collections import OrderedDict
import numpy
from scipy import stats
from pandas import DataFrame
from clipper_tools.em import structure_factors

def get_mean_amplitudes(blur_array,
                        sharp_array,
                        mtz_in_path,
                        results_path,
                        n_bins=None):
    '''
    Get mean amplitudes from mtz file.  Return dateframe indexed by
    resolution.
    '''
    # Import mtz as numpy array
    # Get data
    column_labels = OrderedDict()
    for b in sorted(sharp_array, reverse=True):
        label = '[FoutSharp_{0:.2f}, Pout0]'.format(b)
        column_labels[label] = (r'S +{0:.0f} $\AA^2$'.format(b))
    column_labels['[Fout0, Pout0]'] = (r'In 0 $\AA^2$')
    for b in sorted(blur_array):
        label = '[FoutBlur_{0:.2f}, Pout0]'.format(b)
        column_labels[label] = (r'B -{0:.0f} $\AA^2$'.format(b))
    mtz = None
    reso = None
    for column_label in column_labels.keys():
        try:
            mtz = structure_factors.ClipperMTZ(
                mtz_in_path=mtz_in_path)
        except RuntimeError:
            pass
        if reso is None:
            mtz.import_column_data(column_label=column_label,
                                   get_resolution=True)
            reso = mtz.column_data['resolution_1/Ang^2']
            data = mtz.column_data[column_label]['F']
            # Get binned stats (mean / shell)

            if n_bins is None:
                n_bins = len(reso) / 1000
                n_bins = max(20, n_bins)
            bin_means, bin_edges, bin_num = stats.binned_statistic(
                reso,
                data,
                'mean',
                bins=n_bins)

            # Get bin centres
            bin_edge_centres = []
            for n, x in enumerate(bin_edges):
                if n > 0:
                    c = 0.5 * (bin_edges[n]+bin_edges[-1])
                    bin_edge_centres.append(c)
            mean_amps = DataFrame(
                data=bin_means,
                index=bin_edge_centres,
                columns=[column_labels[column_label]])
            mean_amps_log = DataFrame(
                data=numpy.log(bin_means),
                index=bin_edge_centres,
                columns=[column_labels[column_label]])

        else:
            mtz.import_column_data(column_label=column_label,
                                   get_resolution=False)
            data = mtz.column_data[column_label]['F']
            bin_means, bin_edges, bin_num = stats.binned_statistic(
                reso,
                data,
                'mean',
                bins=n_bins)
            mean_amps[column_labels[column_label]] = bin_means
            mean_amps_log[column_labels[column_label]] = numpy.log(bin_means)
    # Convert from 1/Ang**2 to 1/Ang
    index_ang = numpy.power(mean_amps.index, 0.5)
    index_ang_log = numpy.power(mean_amps_log.index, 0.5)
    # Convert from 1/Ang to Ang
    ones = numpy.ones(index_ang.shape[0])
    index_ang = numpy.divide(ones, index_ang)
    index_ang_log = numpy.divide(ones, index_ang_log)
    mean_amps.set_index(index_ang, inplace=True)
    mean_amps.to_csv(results_path)
    mean_amps_log.set_index(index_ang_log, inplace=True)
    mean_amps_log.to_csv(results_path+'log')
    return mean_amps, mean_amps_log
