#!/bin/tcsh
#image.login
#
set MRC_IMAGE_COM=/home/tom/Code/ccpem_svn/ccpem/ccpem/lib/MRCImageLibraries/image2010/com
if ($?MRC_IMAGE_COM == 0) then
  echo "\nPath not set in image.login"
  echo "\nPlease edit ../ccpem/lib/MRCImageLibraries/image2010/test/image.login\n"
  # Uncomment and edit first line above to include full path to this directory:
  # e.g.:
  # set MRC_IMAGE_COM=home/ccpem/lib/MRCImageLibraries/image2010/test/
else
  set IMAGECOM=$MRC_IMAGE_COM
  setenv IMAGECOM $IMAGECOM
  setenv IMAGE_PUBLIC $IMAGECOM/..
  setenv IMAGE_LASER $IMAGECOM/../laser/postscript
  setenv IMAGELIB $IMAGE_PUBLIC/lib
  setenv IMAGETEST $IMAGE_PUBLIC/test
  setenv IMAGEBIN $IMAGECOM/../../../../../build/bin/
  setenv PATH ${PATH}:$IMAGECOM/../../../../../build/bin/
  setenv PATH ${PATH}:$IMAGE_PUBLIC/local
endif
