C	PREPARE LATLINE OUTPUT DATA FOR MKLCF
C	VX 1.0		18.11.88	RH
C	VX 1.1		22.09.93	RH
C
C	CONTROL CARDS
C
C	1.   RESOLUTION,REDUCAC	(*)  resolution in Angstroms
C				     factor to reduce estim. phase accuracy.
C	2.   a,b,gamma,c        (*)  cell dimensions
C
C	3.   SCALE		(*)  scale factor to ensure data >1 and <32000
C					for LCF file.
C
      DEGTORAD=3.1415962/180.0
      NSIGZERO=0
      NAZERO=0
      NFOMLOW=0
      NRESHI=0
      NGOOD=0
      NIN=0
      WRITE(6,51)
51    FORMAT('   PREPMKLCF VX 1.1(22.9.93) -'
     .	 ' prepares LATLINE data for MKLCF')
      READ(5,*)RESOLUTION,REDUCAC
      READ(5,*)CELLA,CELLB,GAMMA,CAXIS
      READ(5,*)SCALE
      IF(SCALE.EQ.0.0) SCALE=1.0
      WRITE(6,50)REDUCAC,CELLA,CELLB,GAMMA,CAXIS,RESOLUTION,SCALE
50    FORMAT('            Reduce phase accuracy by factor ===',F6.2/
     .       '            a-axis cell dimension =============',F6.2/
     .       '            b-axis cell dimension =============',F6.2/
     .       '            gamma angle =======================',F6.2/
     .	     '            c-axis cell dimension =============',F6.2/
     .       '            Resolution ========================',F6.2/
     .       '            Amplitudes scaled by===============',F6.2)
      IF(GAMMA.GT.90.0)GAMMA=180.0-GAMMA
      ASTAR=1.0/(CELLA*SIN(GAMMA*DEGTORAD))
      BSTAR=1.0/(CELLB*SIN(GAMMA*DEGTORAD))
      CSTAR=1.0/CAXIS
      CALL CCPDPN(1,'IN','READONLY','F',0,0)
      CALL CCPDPN(2,'OUT','UNKNOWN','F',0,0)
C      CALL DOPEN(1,'IN','RO','F')
C      CALL DOPEN(2,'OUT','NEW','F')
      WRITE(6,60)
60    FORMAT('   H   K   L      A      P     FOM*100         REJECTS')
61    FORMAT(35X,2I4,F7.3,F8.1,F7.1,F8.1,F6.1,F10.1)
100   READ(1,*,END=400)IH,IK,ZSTAR,A,P,SIGA,SIGP,FOM
      	A=A*SCALE ! too big for LCF file
      	NIN=NIN+1
      	IF(SIGA.EQ.0.0.OR.SIGP.EQ.0.0) THEN
      	 NSIGZERO=NSIGZERO+1
      	 WRITE(6,61)IH,IK,ZSTAR,A,P,SIGA,SIGP
      	 GO TO 100
      	ENDIF
      	IF(A.LE.0.0) THEN
      	 NAZERO=NAZERO+1
      	 WRITE(6,61)IH,IK,ZSTAR,A,P,SIGA,SIGP
      	 GO TO 100
      	ENDIF
      	SIGPR=SIGP*REDUCAC
      	IF(SIGPR.GT.90.0)SIGPR=90.0
      	FOMCALC=100.0*COS(SIGPR*DEGTORAD)
      	IF(FOMCALC.LT.1.0) THEN
      	 NFOMLOW=NFOMLOW+1
      	 WRITE(6,61)IH,IK,ZSTAR,A,P,SIGA,SIGP
      	 GO TO 100 ! skip this one
      	ENDIF
      IL=NINT(ZSTAR*CAXIS)
      RSQ=IH**2*ASTAR**2+IK**2*BSTAR**2+ZSTAR**2+
     .	 2.0*IH*IK*ASTAR*BSTAR*COS(GAMMA*DEGTORAD)
      IF(RSQ.GT.1.0/RESOLUTION**2) THEN
      	 NRESHI=NRESHI+1
      	 RES=SQRT(1.0/RSQ)
      	 WRITE(6,61)IH,IK,ZSTAR,A,P,SIGA,SIGP,RES
      	 GO TO 100
      ENDIF
      NGOOD=NGOOD+1
      WRITE(2,55)IH,IK,IL,A,P,FOMCALC
      WRITE(6,55)IH,IK,IL,A,P,FOMCALC
55    FORMAT(3I4,F8.1,F7.1,F6.1)
      GO TO 100
400   WRITE(6,56)NGOOD,NIN,NSIGZERO,NAZERO,NFOMLOW,NRESHI,RESOLUTION
56    FORMAT(' MKLCF FILE COMPLETED'//
     .	I10,' good phases written out',
     .	', from a total read in of',I10,/
     .	I10,' of these had zero sigma(s)'/
     .	I10,' of them had zero amplitude'/
     .	I10,' of them had figure of merit <0.01, and'/
     .	I10,' were beyond resolution limit of',F8.1)
      END
