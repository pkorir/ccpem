#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

from ccpem_core.ccpem_utils.ccp4_log_parser import smartie


def get_start_finish_lines(stdout, start_string, end_string):
    '''
    Find start and end lines for concatenated stdout files.
    '''
    start_line = end_line = None
    with open(stdout) as f:
        for n, line in enumerate(f):
            if line.find(start_string) != -1:
                start_line = n
            if start_line is not None:
                if line.find(end_string) != -1:
                    end_line = n
                    break
    return (start_line, end_line)


def main():
    '''
    For testing.
    '''
    stdout = 'stdout.txt'
    start, end = get_start_finish_lines(
        stdout=stdout,
        start_string='CCP-EM pipeline job 2 of 10',
        end_string='CCP-EM job finished')
    print start
    print end
    log = smartie.parselog(stdout)
    

if __name__ == '__main__':
    main()
