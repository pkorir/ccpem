#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Generic widgets for CCPEM GUI
'''

import sys
import os
import math
from PyQt4 import QtCore, QtGui
from ccpem_gui import icons


class CCPEMWelcomeDialog(QtGui.QMessageBox):
    '''
    Welcome dialog
    '''
    def __init__(self, parent=None):
        super(CCPEMWelcomeDialog, self).__init__(parent)
        self.setIconPixmap(QtGui.QPixmap(icons.icon_utils.get_ccpem_icon()))
        text = ('Welcome to CCP-EM.'
                '\n\nPlease set directory for CCP-EM settings')
        self.path = None
        self.setText(text)
        self.setStandardButtons(QtGui.QMessageBox.Open |
                                QtGui.QMessageBox.Cancel)
        retval = self.exec_()
        if retval == QtGui.QMessageBox.Open:
            self.get_path()

    def get_path(self):
        self.path = QtGui.QFileDialog.getExistingDirectory(
            self,
            'Select directory',
            QtCore.QDir.currentPath(),
            QtGui.QFileDialog.ShowDirsOnly)

        if self.path == '':
            message='Path not selected'
            self.path = None
        elif not os.path.exists(self.path):
            message='Path does not exist'
            self.path = None
        elif not os.access(self.path, os.W_OK):
            message='Path does not have write permission'
            self.path = None
        QtGui.QMessageBox.critical(self,
                                   'Error',
                                   message)


class CCPEMTextBrowser(QtGui.QTextBrowser):
    '''
    Simple text browser for log, pdb, txt, etc. files.  Can automatically
    update as file size increases (i.e. for running output).
    '''
    def __init__(self, filepath=None, auto_update=True, parent=None):
        super(CCPEMTextBrowser, self).__init__(parent)
        self.filepath = filepath
        self.auto_update = auto_update
        self.setFont(QtGui.QFont('Courier', 8))
        self.mouseDoubleClickEvent = self.on_tb_double_click
        self.setToolTip(
            'Double click to open in default text editor')
        self.setOpenExternalLinks(True)
        self.scroll_at_max = True
        # Auto update
        if self.filepath is not None:
            self.load_file()
        # Set no line wrap
        self.setLineWrapMode(self.NoWrap);

    def on_tb_double_click(self, event):
        '''
        On text browser double click open viewed file in native application.
        '''
        if self.filepath is not None:
            url = QtCore.QUrl.fromLocalFile(QtCore.QString(self.filepath))
            QtGui.QDesktopServices.openUrl(url)

    def set_filepath(self, filepath, clear_text=True):
        '''
        Set path for displayed file.
        '''
        self.filepath = filepath
        if clear_text:
            self.setText('')
        self.auto_update = True
        self.load_file()

    def load_file(self):
        '''
        Load file using buffer.
        '''
        self.file_size = -1
        self.current_file_pos = 0
        self.screenout = ''
        self.qfile = QtCore.QFile(self.filepath)
        self.qfile.open(QtCore.QIODevice.ReadOnly)
        if self.auto_update:
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.check_file_size)
            self.timer.start(1)
        else:
            self.update_editor_text()

    def check_file_size(self):
        '''
        Change in file triggers update of displayed text.
        '''
        # Check if file has opened ok (log file may not have been created when
        # widget is initiated
        if self.qfile.error() == QtCore.QFile.NoError:
            if self.qfile.size() != self.file_size:
                self.update_editor_text()
                self.timer.setInterval(500)
            else:
                interval = min(10000, self.timer.interval() * 2)
                self.timer.setInterval(interval)
            self.file_size = self.qfile.size()
        else:
            self.qfile = QtCore.QFile(self.filepath)
            self.qfile.open(QtCore.QIODevice.ReadOnly)

    def closeEvent(self, event):
        '''
        Stop timer on close.
        '''
        self.timer.stop()

    def update_editor_text(self):
        '''
        Text stream loaded from previous position to minimise i/o time.
        '''
        # Only update position if at end of page scroll.
        scroll_max = self.verticalScrollBar().maximum()
        if self.verticalScrollBar().value() == scroll_max:
            self.scroll_at_max = True
        else:
            self.scroll_at_max = False

        stream = QtCore.QTextStream(self.qfile)
        stream.setCodec(QtCore.QTextCodec.codecForLocale())
        line = stream.readAll()
        self.current_file_pos += line.length()
        self.insertPlainText(line)

        if self.scroll_at_max:
            self.moveCursor(QtGui.QTextCursor.End)
            self.verticalScrollBar().setValue(
                self.verticalScrollBar().maximum())


class CCPEMMenuSeparator(QtGui.QFrame):
    '''
    Draws simple horizontal line to act as a separator for menu items.
    '''
    def __init__(self, parent=None):
        super(CCPEMMenuSeparator, self).__init__(parent)
        self.setFrameShape(self.HLine)
        self.setFrameShadow(self.Sunken)


class CCPEMFileDialog(QtGui.QDialog):
    '''
    Usage:
        path = None
        fd = CCPEMFileDialog()
        if fd.exec_():
            path = fd.path
    '''
    def __init__(self,
                 message=None,
                 title='CCPEM File Dialog',
                 tooltip='CCP-EM directory selection',
                 parent=None):
        super(CCPEMFileDialog, self).__init__(parent)
        self.path = None
        self.setWindowTitle(title)
        self.setToolTip(tooltip)

        path_widget = QtGui.QWidget()
        path_layout = QtGui.QGridLayout()
        path_label = QtGui.QLabel('Path')
        path_layout.addWidget(path_label, 0, 0)
        select_input_path = QtGui.QPushButton('Select')
        path_layout.addWidget(select_input_path, 0, 1)
        self.path_edit = QtGui.QLineEdit()
        path_layout.addWidget(self.path_edit, 0, 2)
        path_widget.setLayout(path_layout)
        self.button_box = QtGui.QDialogButtonBox(
            QtGui.QDialogButtonBox.Ok |
            QtGui.QDialogButtonBox.Cancel)
        self.button_box.button(QtGui.QDialogButtonBox.Ok).setEnabled(False)
        grid = QtGui.QGridLayout()
        grid.addWidget(path_widget, 0, 0)
        grid.addWidget(self.button_box, 1, 0)
        self.setLayout(grid)
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)
        select_input_path.clicked.connect(self.select_file)
        self.path_edit.editingFinished.connect(self.path_edit_finished)

    def select_file(self):
        path = QtGui.QFileDialog.getExistingDirectory(
            self,
            'Select directory',
            QtCore.QDir.currentPath(),
            QtGui.QFileDialog.ShowDirsOnly)
        self.set_path(path=path)

    def path_edit_finished(self):
        # Block signals to prevent double warning bug
        self.path_edit.blockSignals(True)
        self.set_path(path=self.path_edit.text())
        self.path_edit.blockSignals(False)

    def set_path(self, path):
        if path != '':
            self.button_box.button(QtGui.QDialogButtonBox.Ok).setEnabled(
                True)
            #
            self.path = path
            self.path_edit.setText(self.path)

    def accept(self):
        QtGui.QDialog.accept(self)


class CCPEMTextDisplay(QtGui.QWidget):
    def __init__(self,
                 label_text,
                 line_text,
                 label_width=100,
                 parent=None):
        super(CCPEMTextDisplay, self).__init__(parent=parent)
        layout = QtGui.QHBoxLayout()
        self.setLayout(layout)
        self.label = QtGui.QLabel(label_text)
        self.label.setFixedWidth(label_width)
        self.line = QtGui.QLineEdit(line_text)
        self.line.setReadOnly(True)
        layout.addWidget(self.label)
        layout.addWidget(self.line)

    def mouseDoubleClickEvent(self, event):
        self.launch_contents()

    def launch_contents(self):
        launch_desktop_services(self.line.text())


class CCPEMPairValueView(QtGui.QGroupBox):
    def __init__(self,
                 title=None,
                 labels_values=None,
                 edit=False,
                 parent=None):
        super(CCPEMPairValueView, self).__init__(parent=parent)
        if title is not None:
            self.setTitle(title)
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)
        for label, value in labels_values.iteritems():
            self.add_item(label=label, value=value, edit=edit)

    def add_item(self, label, value, edit=False, label_width=100):
        pair_layout = QtGui.QHBoxLayout()
        label = QtGui.QLabel(str(label))
        if label_width is not None:
            label.setFixedWidth(label_width)
        value = QtGui.QLineEdit(str(value))
        value.setReadOnly(not edit)
        pair_layout.addWidget(label)
        pair_layout.addWidget(value)
        self.layout.addLayout(pair_layout)


class CCPEMStatusWidget(QtGui.QWidget):
    mouseHover = QtCore.pyqtSignal(bool)

    def __init__(self, parent=None):
        super(CCPEMStatusWidget, self).__init__(parent)
        palette = QtGui.QPalette(self.palette())
        palette.setColor(palette.Background, QtCore.Qt.transparent)
        self.setPalette(palette)
        self.status = 'ready'
        self.timer = self.startTimer(100)
        self.counter = 0
        self.setFixedWidth(40)
        self.scale = 8
        self.mouseHover.connect(self.show_status)

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        painter.setPen(QtGui.QPen(QtCore.Qt.NoPen))
        for i in range(6):
            if self.status == 'running':
                if (self.counter // 5) % 6 == i:
                    painter.setBrush(QtGui.QBrush(QtGui.QColor(0, 170, 0)))
                else:
                    painter.setBrush(QtGui.QBrush(QtGui.QColor(127, 127, 127)))
            elif self.status == 'finished':
                painter.setBrush(QtGui.QBrush(QtGui.QColor(0, 170, 0)))
            elif self.status == 'failed':
                painter.setBrush(QtGui.QBrush(QtGui.QColor(255, 0, 0)))
            elif self.status == 'ready':
                painter.setBrush(QtGui.QBrush(QtGui.QColor(127, 127, 127)))
            width = self.width()/2\
                + (self.scale * 1.5)\
                * math.cos(2 * math.pi * i / 6.0)\
                - (self.scale * 0.5)
            height = self.height()/2\
                + (self.scale * 1.5)\
                * math.sin(2 * math.pi * i / 6.0)\
                - (self.scale * 0.5)
            painter.drawEllipse(
                width,
                height,
                self.scale,
                self.scale)
        painter.end()

    def timerEvent(self, event):
        '''
        Update paint event and add one to drive animation.
        '''
        self.update()
        if self.status is 'running':
            self.counter += 1

    def set_ready(self):
        '''
        Set status to ready (set to grey).
        '''
        self.status = 'ready'

    def set_running(self):
        '''
        Set status to running (activates animation).
        '''
        self.status = 'running'

    def set_finished(self):
        '''
        Set status to finished (set to green).
        '''
        self.status = 'finished'

    def set_failed(self):
        '''
        Set status to finished (set to red).
        '''
        self.status = 'failed'

    def enterEvent(self, event):
        self.mouseHover.emit(True)

    def leaveEvent(self, event):
        self.mouseHover.emit(False)

    def show_status(self, event):
        if event:
            text = 'Job status: {0}'.format(self.status)
            self.setToolTip(text)


def launch_desktop_services(path):
    '''
    Uses desktop services to open file with host system's default application. 
    '''
    if os.path.exists(path):
        qfile = QtCore.QUrl.fromLocalFile(QtCore.QString(
            path))
        QtGui.QDesktopServices.openUrl(qfile)


def clear_layout(layout):
    '''
    Removes all widgets from given layout.
    '''
    while layout.count():
        child = layout.takeAt(0)
        if child.widget() is not None:
            child.widget().deleteLater()
        elif child.layout() is not None:
            clear_layout(child.layout())


def main():
    '''
    For testing.
    '''
    app = QtGui.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon(icons.icon_utils.get_ccpem_icon()))
    mw = QtGui.QMainWindow()
    #
    pv_dict = {'one':'1', 2: 2, 3: 3.000}
    pw = CCPEMPairValueView(
        title='Test pair values',
        labels_values=pv_dict)
    mw.setCentralWidget(pw)
    mw.show()
    

    filepath = '/Users/tom/test.txt'
    assert os.path.exists(filepath)
    path = QtCore.QUrl.fromLocalFile(QtCore.QString(filepath))

    QtGui.QDesktopServices.openUrl(QtCore.QUrl(path))

    
    #
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
