Brief description of the icosahedral virus reconstruction
programs on the Alliant as at 10 Feb 93.

List of programs

refine  -  finds orientations and origins by common lines
x37corr -  cross-correlates two particles of known orientation
           to find relative hand and scaling
racmat  -  extracts data from transforms needed for solving for big G's
racbg   -  solves for big G's
raclg   -  converts big G's to little g's
racfb   -  does Fourier summation from little g's to make 3-D map
mapconv -  converts 3-D map to MRC format

**********************************************************************
refine
------
    This works from an MRC format transform of the image and gives
three options - a whole asymmetric unit search in angle
              - an omega search for fixed theta, phi
              - an x,y origin search for specified theta,phi,omega

Data
1) MINR, MAXR, NSAMPL, FMIN

       MINR, MAXR   set radial range used in transform steps
       NSAMPL       number of samples taken in specified radial range
       FMIN         fraction of mean amplitude below which points
                    are ignored.  Typically use FMIN=1. to throw away
                    data lower than mean.
2) FNAME            file name for output of results
3) ISWITCH          1 to refine origin
                    2 to search asymmetric unit
                    3 for omega search
Remaining control data depend on ISWITCH value

ISWITCH=1
4) THETA, PHI, OMEGA   - specifies angle of view in degrees

5) XMIN, DELX, NXSTEP, YMIN, YMAX, NYSTEP
                       - sets origin search parameters in pixels,
                         search around current best value of origin

ISWITCH=2
4) IENTER =1           - read current best origin
5) ORIGX, ORIGY        - current best origin in pixels

ISWITCH=3
4) IENTER =1           - read current best origin
5) ORIGX, ORIGY        - current best origin
6) THETA, PHI          - theta, phi in degrees for omega search

Examples of scripts to run different options:  parameters to be
changed from particle to particle are set as parameters and invoked
on command line.

Origin search
#!/bin/csh -f
#
setenv IN v$1.trn1
/ss0/rac1/icos/refine.exe << eot
6,25,20,1.0
v$1.org
1
$2,$3,$4
$5,0.2,11,$6,0.2,11
eot

Asymmetric unit search
#!/bin/csh -f
#
setenv IN v$1.trn1
/ss0/rac1/icos/refine.exe << eot
6,25,20,1.0
v$1.asu
2
1
$2,$3
eot

Omega search
#!/bin/csh -f
#
setenv IN v$1.trn1
/ss0/rac1/icos/refine.exe << eot
6,25,20,1.0
v$1.om
3
1
$2,$3
$4,$5
eot

*******************************************************************
x37corr
-------
    This program cross-correlates two particles of known orientation
and origin (found by refine) using 37 pairs of cross-common lines,
using the transforms of the two particles in MRC format.  The program
finds relative handedness, radial scaling (magnification) and
amplitude weighting of two particles.  The two transform files are
specified as IN1 and IN2 with IN1 being the "reference" particle.

Data

1) THETA1, PHI1, OMEGA1   - orientation of reference particle
2) THETA2, PHI2, OMEGA2   - orientation of second particle
3) FNAME                  - file name for output of scaling info
4) RSCMIN, RSCMAX, DELRSC - range of radial scale factors
5) RMIN, RMAX             - range of transform radii to use
6) NBAND, INCR, NSAMPL    - number of bands for amplitude scaling
                          - width of each band in transform steps
                          - number of samples to be taken in each band
7) IENTER =1              - read origin for reference particle
8) ORIGX1, ORIGY1         - origin for reference particle in pixels
9) IENTER =1              - read origin for second particle
8) ORIGX2, ORIGY2         - origin for second particle in pixels

Example of script to cross correlate general particle against particle
v2 as reference

#!/bin/csh
#
setenv IN1 v2.trn
setenv IN2 v$1.trn
/ss0/rac1/icos/x37corr.exe << eot
87.,-9.,167.
$2,$3,$4
v2$1.sca
0.95,1.05,0.01
1.,25.
5,5,5
1
66.,63.4
1
$5,$6
eot
#Now print the temporary output file
smallprint x37corr.tmp

****************************************************************
racmat
------
     This program extracts data needed to form normal matrices
for solving for big G's from transforms of particles and stores
it in various files, one indexing file which refers to all particles
and one file for each particle which contains the data.
The indexing file is called  emicomat.dat

Data

1) I        -  1  open new emicobg.dat file for first particle
               0  use existing emicobg.dat file for subsequent particles
2) FNAME    -  filename for input transform
3) IENTER  =1    to enter origin
4) ORIGX, ORIGY  x,y origin in pixels
5) TITLE         title for this data set
6) FNAME         filename for big G data for this image
7) LMAX, NSAMP, ICHECK  - LMAX  number of radial annuli to be used
                          NSAMP sets spacing of annuli as 1./(NSAMP*DIAM)
                                Usually take NSAMP=2 
                          ICHECK print control, usually 0
8) DIAM          diameter of particle in pixels
9) THETA, PHI, OMEGA, NSYM   angles of view.
                             NSYM=5 for 5-fold view, 0 otherwise
10) RSCAL       radial scale factor from x37corr
11) NBAND, INCR number and size of bands for amplitude scaling from
                x37corr.   If 0,0 then no amplitude scaling.
                If specified, then amplitude scale factors follow.
12) AMPFAC      NBAND records containing amplitude scale factors

Example of script for processing two particles, opening new index
emicomat.dat file on first

#!/bin/csh -f
#
/ss0/rac1/icos/racmat.exe << 'eot' > racmat.log
1
v2.trn
1
66.,63.4
v2 hbhiv
cmv2.dat
30,2,0
66
87.,-9.,167.,0
1.
0,0
'eot'
/ss0/rac1/icos/racmat.exe << 'eot' >> racmat.log
0
v3.trn
1
64.7,64.8
v3 hbhiv
cmv3.dat
30,2,0
66
89.,3.,207.,0
1.
0,0
'eot'


******************************************************************
racbg
-----
      This program takes the normal matrix data files set up
by racmat and solves the linear equations on each annulus at
each Z height for the big G's.  Reads indexing information
from  emicomat.dat  and produces file of big G's in  emicobg.dat.

Data

1) IBMAX, NBAND, ILIST, SPREAD  
    -  IBMAX number of radial annuli to solve on.  Usually same as
             LMAX in racmat
    -  NBAND controls whether to read and apply amplitude scaling
             factors.  If NBAND=0, no scaling factors, otherwise
             NBAND scaling factors for each image (see 4 below).
    -  ILIST controls output with 0 least, 1 intermediate, 2 lots
    -  SPREAD sets maximum value allowed for mean inverse eigenvalue
              when solving normal equations on each annulus.   If
              value exceeded, higher order Bessels are dropped until
              equations soluble.   If too many annuli affected, then
              need more views.
                                   
2) NVIEW    -   number of particles to be included
3) NV(I)    -   index numbers of the NVIEW particles to be included
                (used to access the  emicomat.dat indexing file)
4) AMPFAC   -   include only if NBAND.ne.0 !!!!!!!!!!!
                NVIEW records each containing NBAND scaling factors
5) FNAME    -   NVIEW records of names of files containing normal
                matrix data

Example script combining data from 6 particles, which are the first
6 entries in  emicomat.dat  but which come from images 2,3,4,5,6,7.
No amplitude scaling factors.

#!/bin/csh -f
#
/ss0/rac1/icos/racbg.exe << 'eot' > racbg.log
30,0,0,1.
6
1,2,3,4,5,6
cmv2.dat
cmv3.dat
cmv4.dat
cmv5.dat
cmv6.dat
cmv7.dat
'eot'

*******************************************************************
raclg
-----
     Program that converts big G's to little g's.    Takes big G
data from  emicobg.dat  and puts little g's  in  emicolg.dat.

Data

1) TITLE    -  title for this run
2) MAXR, ISMAX, DELSR   -  MAXR  transform cut-off radius in annuli
                           usually IBMAX (as set in racbg) but can be 
                           reduced for lower resolution
                        -  ISMAX  number of radial steps in real space
                        -  DELSR  spacing of points in real space
                           measured in original pixels of densitometered
                           image
3) IDUMP                -  usually 0, if 1 opens file for debugging
Example script

#!/bin/csh -f
#
/ss0/rac1/icos/raclg.exe << 'eot' > raclg.log
hbhiv v2,v3,v4,v5,v6,v7 11Jan93
30,33,1.
0
'eot'

*************************************************************************
racfb
-----
     This program uses the little g's from emicolg.dat in a Fourier
Bessel sum to produce a three dimensional map in the 5-fold setting.
This map is then rotated to produce maps in the 2-fold and 3-fold
settings (ie 2-fold or 3-fold up z)  Output files are   icos2f.map,
icos3f.map, icos5f.map.    AT PRESENT THESE ARE NOT IN MRC FORMAT
(use  mapconv  to convert to MRC format).

Data

1) NHAND      0  handedness unchanged,  1  change handedness

Example script

#!/bin/csh -f
#
/ss0/rac1/icos/racfb.exe << 'eot' > racfb.log
0
'eot'

**********************************************************************
mapconv, mapconv3
-------  --------
   Convert 2-fold or 3-fold maps to MRC format.   Expects input
map on icos2f.map (icos3f.map) and puts output map on OUT

#!/bin/csh -f
#
setenv OUT hbhiv_2fold.map
/ss0/rac1/icos/mapconv.exe
