#!/usr/bin/env wish
#image in a canvas
#FindEM GUI filter
# Alan Roseman 2001
# version 2 ,uses quick get CCC's from the CCCmaxmap. 26/10//01
# testscreen 15/5/02

frame .f -bg red
pack .f

# 2. now params and 3. circle drawing

set codeA 0
set codeB 0
set oldcodeA 0
set oldcodeB 0

set da 320.
set db 360.
set doff 1.
set pw 200.
set tha .4
set thb .4
set sampling 12.5
set scale 5

set maxnuma 10000
set maxnumb 10000

set numa 0
set numb 0
set numca 0
set numcb 0

set projectFolder "~"

# circle radius
set cra [expr $da / $sampling /2]
set crb [expr $db / $sampling /2]

wm  title .  "Parameters for FindEM filter" ; #Where is this line displayed?

frame .f.f2 -bg blue
#pack .f.f2 -side right ; # -expand 1
#; # -expand 1 ; # -fill x -side right

#set sidemenu [canvas .f.f2.sm -yscrollcommand {.f.f2.sm.scv set}]
canvas .f.f2.sm -yscrollcommand ".f.f2.scv set" -yscrollincrement 1 ; # -scrollregion "0 0 2000 500"  -yscrollincrement -0.5
#pack .f.f2.sm -yscrollcommand ".f.f2.scv set" -yscrollincrement 1 ; # -scrollregion "0 0 2000 500" -yscrollincrement  -0.5

scrollbar .f.f2.scv -command ".f.f2.sm yview" ; # scroll 10 units
pack .f.f2.scv -side right -fill y

pack .f.f2.sm -side left -fill both -expand true ; # -fill x ; # -side right
pack .f.f2 -side right ; # -expand 1
#pack propagate .f.f2.sm false

label .f.f2.sm.l20 -text "Current project directory:" -pady 2 -padx 5 -anchor w
entry .f.f2.sm.l21 -disabledforeground darkred -relief flat -textvariable projectFolder -state disabled

label .f.f2.sm.l16 -text "Run code for particle set A :"
entry .f.f2.sm.l17 -textvariable codeA 
label .f.f2.sm.l18 -text "Run code for particle set B :"
entry .f.f2.sm.l19 -textvariable codeB


label .f.f2.sm.l12 -text "Number of A's"
label .f.f2.sm.l13 -textvariable numa
label .f.f2.sm.l14 -text "Number of B's"
label .f.f2.sm.l15 -textvariable numb

label .f.f2.sm.l10 -text "Sampling (A/pixel)"
entry  .f.f2.sm.e1 -textvariable sampling 

label .f.f2.sm.l11 -text "scale factor"
entry  .f.f2.sm.e2 -textvariable scale

label .f.f2.sm.l1 -text "Diameter of particles A (A)"
scale .f.f2.sm.s1 -variable da -from 0 -to 500 -orient horizontal

label .f.f2.sm.l2 -text "Diameter of particles B (A)"
scale .f.f2.sm.s2 -variable db -from 0 -to 500 -orient horizontal

label .f.f2.sm.l8 -text "Scale factor for displayed circles"
scale .f.f2.sm.s8 -variable doff -from 0 -to 2 -orient horizontal -resolution 0.01


label .f.f2.sm.l3 -text "Peak width (A)"
scale .f.f2.sm.s3 -variable pw -from 0 -to 500 -orient horizontal

label .f.f2.sm.l4 -text "CCC threshold for A"
scale .f.f2.sm.s4 -variable tha -from .2 -to 1 -resolution 0.01 -orient horizontal

label .f.f2.sm.l5 -text "CCC threshold for B"
scale .f.f2.sm.s5 -variable thb -from .2 -to 1 -resolution 0.01 -orient horizontal


label .f.f2.sm.l6 -text "Max number of particles A"
scale .f.f2.sm.s6 -variable maxnuma -from 0 -to 10000 -orient horizontal

label .f.f2.sm.l7 -text "Max number of particles B"
scale .f.f2.sm.s7 -variable maxnumb -from 0 -to 10000 -orient horizontal

button .f.f2.sm.update -text "Update particle positions" -command drawcircles

button .f.f2.sm.clear -text "clear" -command clearcircles

button .f.f2.sm.wa -text "Write coordinates A" -command writeA

button .f.f2.sm.wb -text "Write coordinates B" -command writeB

button .f.f2.sm.new -text "New image" -command new_image

button .f.f2.sm.exit -text "Exit" -command exit

pack .f.f2.sm.l20 -fill x
pack .f.f2.sm.l21 -fill x

pack .f.f2.sm.l16 .f.f2.sm.l17 -fill x
pack .f.f2.sm.l18 .f.f2.sm.l19   -fill x

bind .f.f2.sm.l17 <Return> {update_rawcoordsA}
bind .f.f2.sm.l17 <Leave> {update_rawcoordsA}

bind .f.f2.sm.l19 <Return>  {update_rawcoordsB}
bind .f.f2.sm.l19 <Leave>   {update_rawcoordsB}

pack .f.f2.sm.l12   -fill x
pack .f.f2.sm.l13   -fill x

pack .f.f2.sm.l14   -fill x
pack .f.f2.sm.l15  -fill x

pack .f.f2.sm.l10   -fill x
pack .f.f2.sm.e1   -fill x
pack .f.f2.sm.l11  -fill x
pack .f.f2.sm.e2   -fill x
 
pack .f.f2.sm.l1  -fill x
pack .f.f2.sm.s1  -fill x
pack .f.f2.sm.l2  -fill x
pack .f.f2.sm.s2  -fill x
pack .f.f2.sm.l8  -fill x
pack .f.f2.sm.s8  -fill x

pack .f.f2.sm.l3  -fill x
pack .f.f2.sm.s3  -fill x
pack .f.f2.sm.l4  -fill x
pack .f.f2.sm.s4   -fill x
pack .f.f2.sm.l5  -fill x
pack .f.f2.sm.s5   -fill x
pack .f.f2.sm.l6  -fill x
pack .f.f2.sm.s6   -fill x
pack .f.f2.sm.l7  -fill x
pack .f.f2.sm.s7   -fill x
pack .f.f2.sm.update  -fill x
pack .f.f2.sm.clear  -fill x
pack .f.f2.sm.wa  -fill x
pack .f.f2.sm.wb  -fill x
pack .f.f2.sm.new  -fill x

pack .f.f2.sm.exit -side right

set plist(2) 2

# 1. create canvas and draw image
wm title . "FindEM particle finding - filter operation"

# choose a gif file

# read filename widget
proc selectFile {} {
  set fltyp {
    {"gif image" {.gif*} }
    {"All files" * }
  }
  set fname [tk_getOpenFile -filetypes $fltyp -parent .]
  if {$fname == ""} {
    retryEmptyInput{}
  } 
  return $fname
}

# read filename
set filename [selectFile ]
set projectFolder [file dirname $filename]
puts $projectFolder
puts $filename

set im [image create photo  -file $filename -palette 256 ]
set w [image width $im]
set h [image height $im]


set scrw [expr [winfo screenwidth . ] -250 ]
puts $scrw 
puts $w
set scrh [expr [winfo screenheight .] -100 ]
puts $scrh
if  {$scrw > [expr $w + 2]} then { set scrw  [expr $w + 2]}

frame .f.f1 -bg black
pack .f.f1

frame .f.f1.sub -bg blue -bd 2
pack .f.f1.sub -in .f.f1


canvas .f.f1.sub.c -width  $scrw -height $scrh \
 -scrollregion " 0 0 $w $h "   \
			-xscrollcommand ".f.f1.sch set" \
			-yscrollcommand ".f.f1.sub.scv set" \
	-closeenough 5

#Scrollbars for main image			
scrollbar .f.f1.sub.scv -command ".f.f1.sub.c yview"
pack .f.f1.sub.c .f.f1.sub.scv -in .f.f1.sub -side left -fill y

scrollbar .f.f1.sch -orient horizontal -command ".f.f1.sub.c xview"
pack .f.f1.sch -in .f.f1 -side top -fill x

pack .f.f1.sub.c -side left

.f.f1.sub.c create image 0 0  -image $im -anchor nw -tags HBV

#scrollbars for sidemenu 	 
#canvas .f.f2.c  -height $scrh -scrollregion " $w 0 0 $scrh" ; # -yscrollcommand ".f.f2.scv" -closeenough 5 	 
#pack .f.f2.c -in .f.f2 -side left 	 
	  	 
#scrollbar .f.f2.sm.scv -command {sidemenu yview} 	 
#pack .f.f2.sm.scv -side right -fill y

proc drawcircles { } {
  global tha thb pw da db maxnuma maxnumb sampling numa numb numac numcb doff codeA codeB env projectFolder
  puts " $tha $thb $pw $da $db $maxnuma $maxnumb"

  #delete old circles
  clearcircles

#<<<<<<< .mine
#run program to create files
#set proc $env(FINDEMDIR)/goFindEM_filtercoords
#puts $proc
#set error [catch {exec $proc $tha $thb $da $db $pw $sampling $codeA $codeB $maxnuma $maxnumb}]
#=======
  #run program to create files
  set proc $env(CCPEMBIN)/goFindEM_filtercoords.py
  #tk_messageBox -message $proc
  puts $projectFolder

  set error [catch {exec $proc $tha $thb $da $db $pw $sampling $codeA $codeB $maxnuma $maxnumb} tv1 ]
  puts $tv1
#>>>>>>> .r78

  puts $error

  #read files
  set x(1) 1
  set y(1) 1


  set cra [expr ($da * $doff )/ $sampling /2]
  set crb [expr ($db * $doff) / $sampling /2]

  if {$codeA > 0} {
    #set f1 [file join $projectFolder coordsA.xyz]
    #tk_messageBox -message $f1   
    set num [readfile [file join $projectFolder coordsA.xyz]  x y $maxnuma ] ; # join adds path seperators in

    #draw them
    circles x y "green" $num $cra "A"
    puts "$num A's  "
    set numa $num

    set num [readfile [file join $projectFolder coordsCA.xyz] x y $maxnuma ]
    #draw them
    circles x y "red" $num $cra "CA"
    puts "$num rejected A's  "
    set numca $num
  }

  #read files
  if {$codeB > 0} {
    set num [readfile [file join $projectFolder coordsB.xyz] x y $maxnumb ]
    #draw them
    circles x y "blue" $num $crb "B"
    puts "$num B's  "
    set numb $num

    #read files
    set num [readfile [file join $projectFolder coordsCB.xyz] x y $maxnumb]
    #draw them
    circles x y "orange" $num $crb "CB"
    puts "$num rejected B's  "
    set numcb $num
  }
}

proc readfile { filename arrayx arrayy maxentries  } {
  upvar $arrayx x
  upvar $arrayy y
  global val

  set fh4 [open $filename]

  set n 0

  while { [gets $fh4 Line] >= 0 } {
    set x($n)  [ lindex $Line 0 ]
    set y($n)  [ lindex $Line 1]
    set val($n)  [ lindex $Line 2]
    incr n
  }

  close $fh4
  puts " $filename $n "
  return $n
}

proc circles {coordsx coordsy colour num cr tag} {
  upvar $coordsx x
  upvar $coordsy y
  global h da db sampling val plist

  for {set n 0} {$n < $num}  {incr n} {
    #set x1 [ expr $x($n) - $cr -1 ]
    #set x2 [ expr $x($n) + $cr -1 ]
    #set y1 [ expr $y($n) - $cr -1 ]
    #set y2 [ expr $y($n) + $cr -1 ]
    set x1 [ expr $x($n) - $cr  ]
    set x2 [ expr $x($n) + $cr  ]
    set y1 [ expr $y($n) - $cr  ]
    set y2 [ expr $y($n) + $cr  ]

    #set y1 [expr $h - $y1 -1 ]
    #set y2 [expr $h - $y2 -1 ]
    #set y1 [expr  $y1 +1 ]
    #set y2 [expr  $y2 +1 ]

    set item [.f.f1.sub.c create oval $x1 $y1 $x2 $y2 -outline $colour -tags {marker $tag} ]

    .f.f1.sub.c bind $item <Button-1> "kill_particle $item %x %y"
    set plist($item,active) 1
    set plist($item,tag) $tag
    set plist($item,CCC) $val($n)
    set plist($item,colour) $colour
    set plist($item,x) $x($n)
    set plist($item,y) $y($n) 
  }

  # parray x
  # parray y
}

proc clearcircles { } {
  global plist
  unset plist
  set plist(2) 2

  .f.f1.sub.c delete marker
}

proc writeA {} {
  global plist scale codeA
  global da db pw tha thb sampling maxnuma maxnumb numa numb numca numcb projectFolder

  if {[file exists [file join $projectFolder FindWMcoordsA${codeA}.xyz]]} {
    set file [file join $projectFolder FindEMcoordsA${codeA}.xyz]
    set error [catch [exec rm $file] ]
    puts $error
  }

  if {$codeA > 0} {
    #tk_messageBox -message $plist
    #set file ~/ccpem/FindEMcoordsA${codeA}.xyz
    set file [file join $projectFolder FindEMcoordsA${codeA}.xyz]
    #tk_messageBox -message $file
    set fh5 [open $file w ]
#    puts $fh5 " ; File output from FindEM" 
    puts $fh5 "        x         y      density"
    
    foreach i [array names plist *,active] {
      set j  [ string first "," $i ]
      set k  [ expr $j - 1  ]
      set l [string range $i 0 $k]

      if { $plist($i) > 0 } {
	if  { $plist($l,tag) == "A" } {
	  set x [ expr $plist($l,x) * $scale ]
	  set y [ expr $plist($l,y) * $scale ]
	  set val $plist($l,CCC)
	  #Ximdisp writes 2 (seemingly) randomly in the 2nd col...
	  #Ximdisp writes: `particle id` `2` x y 
	  #tk_messageBox -message [format "%12d" $x]
          puts $fh5 [format "%10d%10d%12.1f" $x $y $val]
	  #puts $fh5 " $i 2 $x $y" 
        }
      }
    }
	
    close $fh5	
	
    set fh25 [open [file join $projectFolder FindEMA${codeA}.dat] w ]	
    puts $fh25 "Param: $da $db $pw $tha $thb $scale $sampling $maxnuma $maxnumb "
    puts $fh25 "Hits: $numa $numb $numca $numcb  "	
    puts $fh25
    close $fh25
  }
}

proc writeB {} {
  global plist scale codeB
  global da db pw tha thb sampling maxnuma maxnumb numa numb numca numcb projectFolder

  if {[file exists [file join $projectFolder FindEMcoordsB${codeB}.xyz]]} {
    set file [file join $projectFolder FindEMcoordsB${codeB}.xyz] 
    set error [catch [exec rm $file] ]
    puts $error
  }

  if {$codeB > 0} {
    set file [file join $projectFolder FindEMcoordsB${codeB}.xyz]

    set fh6 [open $file w ]
#    puts $fh6 " x         y      density "
    puts $fh6 "        x         y      density"
    foreach i [array names plist *,active] {
      set j  [ string first "," $i ]
      set k  [ expr $j - 1  ]
      set l [string range $i 0 $k]

      if { $plist($i) > 0 } {
	if  { $plist($l,tag) == "B" } {
	  set x [ expr $plist($l,x) * $scale ]
	  set y [ expr $plist($l,y) * $scale ]
	  set val $plist($l,CCC)
	  #puts $fh6 "$x   $y  $val  "
          puts $fh6 [format "%10d%10d%12.1f" $x $y $val]
	}
      }
    }
	
    close $fh6	
	
    set fh26 [open "FindEMB${codeB}.dat" w ]	
    puts $fh26 "Param: $da $db $pw $tha $thb $scale $sampling $maxnuma $maxnumb "
    puts $fh26 "Hits: $numa $numb $numca $numcb  "	
    puts $fh26
    close $fh26		
  }
}

proc kill_particle {item x y } {
  global plist numa numb

  if { $plist($item,tag) == "A" } {
    if { $plist($item,active) > 0 } {
      .f.f1.sub.c itemconfigure $item -outline black 
      set plist($item,active)  0
      incr numa -1
    } else {
      .f.f1.sub.c itemconfigure $item -outline  "$plist($item,colour)"
      set plist($item,active) 1
      incr numa
    }
  }

  if { $plist($item,tag) == "B" } {
    if { $plist($item,active) > 0 } {
      .f.f1.sub.c itemconfigure $item -outline black 
      set plist($item,active)  0
      incr numb -1
    } else {
      .f.f1.sub.c itemconfigure $item -outline  "$plist($item,colour)"
      set plist($item,active) 1
      incr numb
    }
  }
}

proc new_image {} {

  ##
  # Added by Chris Wood Oct 2012
  ##

  global im da db pw tha thb sampling maxnuma maxnumb numa numb numca numcb projectFolder
  image delete $im

  set answer [tk_messageBox -message "Do you want to reset the settings for the image you're about to select?" -type yesno -icon question]

  if {$answer=="yes"} { 
    #reset settings
  
    set codeA 0
    set codeB 0
    set oldcodeA 0
    set oldcodeB 0

    set da 320.
    set db 360.
    set doff 1.
    set pw 200.
    set tha .4
    set thb .4
    set sampling 12.5
    set scale 5

    set maxnuma 10000
    set maxnumb 10000

    set numa 0
    set numb 0
    set numca 0
    set numcb 0
  }  

  #Clear the circles on the old image
  clearcircles 

  # circle radius
  set cra [expr $da / $sampling /2]
  set crb [expr $db / $sampling /2]

  set filename [selectFile ]
  puts $filename
  set projectFolder [file dirname $filename]

  set im [image create photo  -file $filename -palette 256 ]
  set w [image width $im]
  set h [image height $im]

  set scrw [expr [winfo screenwidth . ] -250 ]
  puts $scrw 
  puts $w
  set scrh [expr [winfo screenheight .] -100 ]
  puts $scrh
  if  {$scrw > [expr $w + 2]} then { set scrw  [expr $w + 2]}

  .f.f1.sub.c create image 0 0  -image $im -anchor nw -tags HBV
  
  # return $projectFolder
}

#set projectFolder [new_image ] 	 
#set filename [new_image ] 	 
#set projectFolder [file dirname $filename]

proc update_rawcoordsA {} {
  global codeA oldcodeA
  if {$codeA != $oldcodeA} {
    #set error [catch {exec goFindEM_getCCCquick $codeA} ] 
    set oldcodeA $codeA
  }
}

proc update_rawcoordsB {} {
  global codeB oldcodeB
  if {$codeB != $oldcodeB} {
    #set error [catch {exec goFindEM_getCCCquick $codeB} ]
    set oldcodeB $codeB
  }
}

