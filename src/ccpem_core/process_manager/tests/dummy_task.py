#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
from __future__ import print_function

import sys
import time

def main():
    prefix = 'a'
    if len(sys.argv) > 1:
        prefix = sys.argv[1]
    print('Hello world')
    # Add delay
    time.sleep(1.3)
    with open(prefix + '_test.log', 'w') as f:
        f.write('TEST')

if __name__ == '__main__':
    main()