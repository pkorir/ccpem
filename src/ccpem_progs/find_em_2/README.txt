FindEM2 is an improved particle picking application.
FindEM2 is for semi-automated particle picking from electron micrographs.

Copyright (C) 2012 The University of Manchester 

FindEM2 is a new correlation program that runs similarly to the previous FindEM program.
See the publications: ROSEMAN, 2003, 2004.
The same scripts and programs from FindEM are used to filter the coordinates, and extract the particles.

For enquiries, contact: Alan.Roseman@manchester.ac.uk

Alan Roseman  							       
Faculty of Life Sciences						       
University of Manchester						       
The Michael Smith Building					       
Oxford Road							       
Manchester. M13 9PT						       
Email:Alan.Roseman@manchester.ac.uk   

Licensing is by GNU GPLv3.

===========================================================================================================

Installing FindEM2
 
Required:
Computer with Linux operating system.
MRC image2010 libraries (Available from: www.ccpem.ac.uk, or email Alan.Roseman@manchester.ac.uk).
fftw packages installed (Available from: www.fftw.org).
The old FindEM package  (Available from: www.ccpem.ac.uk, or email Alan.Roseman@manchester.ac.uk).
The SPIDER package (Available from: www.wadsworth.org/spider_doc).
 
The fftw libraries are used to compute the fast Fourier transforms. 
 
1. Make a target directory for the code on your system:
   e.g. /usr/local/programs/FindEM2
 
2. Unpack the zipped tar file into this directory.
 
3. Run the Makefile to create new exe’s.
   e.g. > make
 
3. Copy across the executable program files and scripts from the main directory of your old FindEM installation:
a. set the current directory to the FindEM2 directory
   e.g. > cd /usr/local/programs/FindEM2
b. copy across old FindEM files.
   e.g.  > cp /oldfindemdirectory/* .
 
4. Edit the FindEM2.login file to have the path to your local FindEM2 installation.
   Edit your .cshrc to source the FindEM2.login 
   e.g. add this line: source /usr/local/programs/FindEM2/FindEM2.login

5. Now your can run the full particle picking application. 'goFindEM2-script.com' is an example script.
 
Note: With the programs in this distribution you can run the correlation of your template with your images. 
The scripts and GUI from the old FindEM distribution are used to process the correlation maps 
and extract the particles from the images.
 
A new feature is use of a customized asymmetric mask. This you can create in WEB/spider, then copy to mrc format using the 
'cp to ccp4' command. See spider documentation for details.
 
Notes: It is set up for the cshell (csh).
It could run in bash with some minor changes to the scripts.
The programs use mrc format images for input and output, unless otherwise stated.


------------------------------------------------------------------------------------------------------------
Alan Roseman (PhD)

Faculty of Life Sciences
University of Manchester
The Michael Smith Building
Oxford Road
Manchester. M13 9PT.

Alan.Roseman@manchester.ac.uk
http://www.ls.manchester.ac.uk/
