#include <vector>
#include <cstdio>
#include <clipper/clipper.h>
#include <clipper/clipper-ccp4.h>
#include <clipper/core/clipper_precision.h>

// nxmap - xtal map
/////
class NXMap{
  float mean;
  float std_dev;
  float min;
  float max;
  int spacegroup_number;
  std::string spacegroup_hm_symbol;
  std::vector<double> unit_cell;
  clipper::Vec3<int> grid;
  clipper::Cell cell;
 public:
  clipper::NXmap<float> nxmap;
  NXMap(const char* map_file_name);
  std::vector<std::vector<float> > GetMapSector(int z_grid = 0);
  void GetMapSectorNumpy(float *invec, int nx, int ny, int z_grid = 0);
  std::vector < std::vector < std::vector < float > > > GetMapArray();
  double GetMin();
  double GetMax();
  double GetMean();
  double GetStdDev();
  clipper::Vec3<int> GetGrid();
  std::vector<double> GetUnitCell();
  int GetSpaceGroupNumber();
  std::string GetSpaceGroupHMSymbol();
  void ResampleGrid(int grid_size);
  void WriteMap(char const* filename_mapout);
};

NXMap::NXMap(const char* map_file_name_cp){
  clipper::CCP4MAPfile file;
  file.open_read(std::string(map_file_name_cp));
  file.import_nxmap( nxmap );
  file.close_read();

  clipper::Spacegroup spacegroup(file.spacegroup());
  spacegroup_number = spacegroup.spacegroup_number();
  spacegroup_hm_symbol = spacegroup.symbol_hm();
  cell = file.cell();

  unit_cell.assign(6,0.0);
  unit_cell[0] = cell.a();
  unit_cell[1] = cell.b();
  unit_cell[2] = cell.c();
  unit_cell[3] = cell.alpha_deg();
  unit_cell[4] = cell.beta_deg();
  unit_cell[5] = cell.gamma_deg();

  clipper::Map_stats stats(nxmap);
  min = stats.min();
  // N.B. multiplicity always 1 for NXmap, therefore get min and max from stats
  max = stats.max();
  mean = stats.mean();
  std_dev = stats.std_dev();
  clipper::NXmap<float>::Map_reference_index im;
  grid = nxmap.grid();
}

// Pass 2d array of size nx, ny for pixel values to be written from map slice 
// z_grid
void NXMap::GetMapSectorNumpy(float *invec, int nx, int ny, int z_grid)
{
  int x_dim = nx;
  int y_dim = ny;
  clipper::NXmap<float>::Map_reference_index im = nxmap.first();
  clipper::ftype64 pix_val;
  grid = nxmap.grid();
  for (int y_grid = 0; y_grid < y_dim; y_grid++) {
    for (int x_grid = 0; x_grid < x_dim; x_grid++) {
      clipper::Coord_grid cg(y_grid, x_grid, z_grid);
      im.set_coord(cg);
      pix_val = clipper::ftype64(nxmap[im]);
      int index = y_grid + (x_grid * ny);
      if (clipper::Util::isnan(pix_val)) {
        invec[index] = 0.0;
      }
      else {
        invec[index] = pix_val;
      }
    }
  }
}

// Return single image from image stack, assumes images stored in z plane
std::vector<std::vector<float> >  NXMap::GetMapSector(int z_grid) {
  // Initiate 2d vector
  int x_dim = grid[1];
  int y_dim = grid[0];
  std::vector<std::vector<float> > return_vec(x_dim, std::vector<float>(y_dim,0.0));
  // Get pixel values from map sector
  clipper::NXmap<float>::Map_reference_index im = nxmap.first();
  clipper::ftype64 pix_val;
  grid = nxmap.grid();
  for (int y_grid = 0; y_grid < y_dim; y_grid++) {
    for (int x_grid = 0; x_grid < x_dim; x_grid++) {
      clipper::Coord_grid cg(y_grid, x_grid, z_grid);
      im.set_coord(cg);
      pix_val = clipper::ftype64(nxmap[im]);
      if (clipper::Util::isnan(pix_val)) {
        return_vec[im.coord()[1]] [im.coord()[0]] = 0.0;
      }
      else {
        return_vec[im.coord()[1]] [im.coord()[0]] = pix_val;
      }
    }
  }
  return return_vec;
}

// Return single image from image stack, assumes images stored in z plane
std::vector<std::vector<std::vector<float> > > NXMap::GetMapArray() {
  // Initiate 2d vector
  int x_dim = grid[2];
  int y_dim = grid[1];
  int z_dim = grid[0];

  std::vector<std::vector<std::vector<float> > > 
      return_vec(x_dim, std::vector<std::vector<float> > (y_dim, std::vector<float> (z_dim, 0.0) ) );

  // Get voxel values from map
  clipper::NXmap<float>::Map_reference_index im;
  clipper::ftype64 pix_val;
  for (im = nxmap.first(); !im.last(); im.next()) {
    pix_val = clipper::ftype64(nxmap[im]);
    return_vec[im.coord()[2]] [im.coord()[1]] [im.coord()[0]] = pix_val;
  }
  return return_vec;
}

double NXMap::GetMin() {
  return min;
}

double NXMap::GetMax() {
  return max;
}

double NXMap::GetMean() {
  return mean;
}

double NXMap::GetStdDev() {
  return std_dev;
}

clipper::Vec3<int> NXMap::GetGrid() {
  return grid;
}

int NXMap::GetSpaceGroupNumber() {
  return spacegroup_number;
}

std::string NXMap::GetSpaceGroupHMSymbol() {
  return spacegroup_hm_symbol;
}

std::vector<double> NXMap::GetUnitCell() {
  return unit_cell;
}

void NXMap::ResampleGrid(int grid_size) {
  // Set new map with new grid sampling
  clipper::Coord_map zero_coord_map = nxmap.coord_map(clipper::Coord_orth(0,0,0));
  int grid_size_h, u_min, v_min, w_min, u_max, v_max, w_max;
  if (zero_coord_map[0] == 0) {
    u_min = -zero_coord_map[0];
    v_min = -zero_coord_map[1];
    w_min = -zero_coord_map[2];
    u_max = u_min + grid_size - 1;
    v_max = v_min + grid_size - 1;
    w_max = w_min + grid_size - 1;
  }
  else {
    if (grid_size % 2 != 0) {
      grid_size += 1;
    }
    grid_size_h = 0.5 * grid_size;
    u_min = 0 - grid_size_h;
    v_min = 0 - grid_size_h;
    w_min = 0 - grid_size_h;
    u_max = -1 + grid_size_h;
    v_max = -1 + grid_size_h;
    w_max = -1 + grid_size_h;
  }
  clipper::Grid_range grid_range_rs(clipper::Coord_grid(u_min,v_min,w_min), clipper::Coord_grid(u_max,v_max,w_max));
  clipper::Grid_sampling grid_sampling_rs(grid_size, grid_size, grid_size);
  clipper::NXmap<float> nxmap_rs(cell, grid_sampling_rs, grid_range_rs);
  float k = (float)nxmap.grid()[2] / grid_size;
  // Rescale 
  clipper::NXmap_base::Map_reference_index ix;
  for (ix = nxmap_rs.first(); !ix.last(); ix.next()){
    nxmap_rs[ix] = nxmap.interp<clipper::Interp_linear>( k * ix.coord().coord_map() );
    if (clipper::Util::isnan(nxmap_rs[ix])){
      nxmap_rs[ix] = 0.0;
    }
  }
  nxmap = nxmap_rs;
}

void NXMap::WriteMap(char const* filename_mapout) {
  clipper::CCP4MAPfile mapout;
  std::cout << "\n  Writing map : " << filename_mapout << std::endl;
  mapout.open_write( filename_mapout );      // write map
  mapout.set_cell( cell );
  mapout.export_nxmap( nxmap );
  mapout.close_write();
}
//End nxmap
/////

// xmap - xtal map
/////
class XMap{
  float mean;
  float std_dev;
  float min;
  float max;
  int spacegroup_number;
  std::string spacegroup_hm_symbol;
  std::vector<double> unit_cell;
 public:
  clipper::Xmap<float> xmap;
  XMap(const char* map_file_name_cp);
  double GetMin();
  double GetMax();
  double GetMean();
  double GetStdDev();
  std::vector<double> GetUnitCell();
  int GetSpaceGroupNumber();
  std::string GetSpaceGroupHMSymbol();
  void WriteMap(char const* filename_mapout);
};

XMap::XMap(const char* map_file_name_cp){
  clipper::CCP4MAPfile file;
  file.open_read(std::string(map_file_name_cp));
  file.import_xmap( xmap );
  file.close_read();

  clipper::Spacegroup spacegroup(file.spacegroup());
  spacegroup_number = spacegroup.spacegroup_number();
  spacegroup_hm_symbol = spacegroup.symbol_hm();
  clipper::Cell cell( file.cell());
  unit_cell.assign(6,0.0);
  unit_cell[0] = cell.a();
  unit_cell[1] = cell.b();
  unit_cell[2] = cell.c();
  unit_cell[3] = cell.alpha_deg();
  unit_cell[4] = cell.beta_deg();
  unit_cell[5] = cell.gamma_deg();

  clipper::Map_stats stats(xmap);
  mean = stats.mean();
  std_dev = stats.std_dev();

  clipper::Xmap<float>::Map_reference_index im;
  clipper::ftype64 w, x, sx;
  sx = 0.0;
  clipper::ftype64 minsx = 1.0e8;
  clipper::ftype64 maxsx = -1.0e8;
  for ( im = xmap.first(); !im.last(); im.next() ) {
    w = 1.0 / clipper::ftype64( xmap.multiplicity( im.coord() ) );
    x = clipper::ftype64( xmap[im] );
    if ( !std::isnan(x) ) {
      sx = w*x;
      if(sx>maxsx) maxsx = sx;
      if(sx<minsx) minsx = sx;
    }
  }
  min = minsx;
  max = maxsx;
}

double XMap::GetMin(){
  return min;
}

double XMap::GetMax(){
  return max;
}

double XMap::GetMean(){
  return mean;
}

double XMap::GetStdDev(){
  return std_dev;
}

int XMap::GetSpaceGroupNumber(){
  return spacegroup_number;
}

std::string XMap::GetSpaceGroupHMSymbol() {
  return spacegroup_hm_symbol;
}

std::vector<double> XMap::GetUnitCell(){
  return unit_cell;
}

void XMap::WriteMap(char const* filename_mapout) {
  clipper::CCP4MAPfile mapout;
  std::cout << "\n  Writing map : " << filename_mapout << std::endl;
  mapout.open_write( filename_mapout );      // write map
  mapout.export_xmap( xmap );
  mapout.close_write();
}

//End xmap
/////

void MtzToXMap(char const* filename_mtzin, char const* filename_mapout = "out.map") {
  clipper::CCP4MTZfile mtzin;
  std::cout << "\n  Reading mtz : " << filename_mtzin << std::endl;
  mtzin.open_read(filename_mtzin);        // open new file
  clipper::Spacegroup   spgr = mtzin.spacegroup();
  clipper::Cell         cell = mtzin.cell();
  clipper::Resolution   reso = mtzin.resolution();
  clipper::HKL_info myhkl( spgr, cell, reso, true );  // init hkls
  clipper::HKL_data<clipper::data32::F_phi> fphidata( myhkl );
  mtzin.import_hkl_data( fphidata, "/*/*/[FCAL,PHIFCAL]" ); // define this from column header
  mtzin.close_read();
  clipper::Grid_sampling mygrid( spgr, cell, reso );  // define grid
  clipper::Xmap<float> mymap( spgr, cell, mygrid );  // define map
  mymap.fft_from( fphidata );                  // generate map
  clipper::CCP4MAPfile mapout;
  std::cout << "\n  Writing map : " << filename_mapout << std::endl;
  mapout.open_write( filename_mapout );      // write map
  mapout.export_xmap( mymap );
  mapout.close_write();
}

// Create nx map, add data from 3d array
void ExportNXMap(char const* filename_mapout,
                  float cell_a,
                  float cell_b,
                  float cell_c,
                  std::vector<std::vector<std::vector<float> > > data,
                  int sampling_rate = 1) {

  // Debug
  if (false) {
    int x = data[0][0].size();
    int y = data[0].size();
    int z = data.size();
    std::cout << "\n x : " << x;
    std::cout << "\n y : " << y;
    std::cout << "\n z : " << z;
  }
  clipper::Coord_grid cgrid_min(0, 0, 0);
  clipper::Coord_grid cgrid_max(data[0][0].size()-1, data[0].size()-1, data.size()-1);
  clipper::Grid_range gmap = clipper::Grid_range(cgrid_min, cgrid_max);
  clipper::Cell cell(clipper::Cell_descr(cell_a, cell_b, cell_c, 90, 90, 90));
  clipper::Coord_grid nu_nv_nw( (cgrid_max - cgrid_min) + clipper::Coord_grid(1,1,1) );
  int n = sampling_rate;
  clipper::Grid_sampling gsm = clipper::Grid_sampling(nu_nv_nw[0] * n, nu_nv_nw[1] * n, nu_nv_nw[2] * n);
  clipper::NXmap<float> nxmap(cell, gsm, gmap);
  clipper::NXmap<float>::Map_reference_index im;

  // Set data in map to 3D vector
  for (im = nxmap.first(); !im.last(); im.next()) {
    nxmap.set_data(im.coord(), data[im.coord()[2]] [im.coord()[1]] [im.coord()[0]]);
  }

  // Export nx map
  clipper::CCP4MAPfile mapout;
  mapout.open_write(filename_mapout);
  mapout.set_cell(cell);
  mapout.export_nxmap(nxmap);
  mapout.close_write();
}
