C******************************************************************
C*** 	SLEUTH
C***       11.07.2006 MARK 1.4 RELEASE.
C***       Medical Research Council,20 Park Crescent, LONDON W1N 4Al
C***       COPYRIGHT 2004.
C***       WRITTEN BY Judith M. Short
C***  
C***	   V1.1  14.03.2006 Correction to Spider O/P coords.
C***	   V1.2  21.03.2006 Removal of default requests
C***       V1.3  11.07.2006 Bug fix to stack output
C***	   V1.4  26.07.2006 Bug fix to incorrect radius
C***	   V1.5  30.05.2013  "   "   " coordinate output
C******************************************************************
C*** Program to search electron micrograph images for particle images
C*** given a stack of reference images.
C******************************************************************
C*** SLEUTH has two modes of operation :
C***
C*** 1) The graphical user interface (GUI) which is used for setting
C***    the parameters and creating a script for searching a set of
C***    micrograph images.  It is run with the -f switch set :
C***    e.g. sleuth.exe -f
C***    It is best to select a small area of the micrograph for testing,
C***    which should include at least part of a label and some carbon,
C***    if present. It offers many default values which can be tailored 
C***    to the images interactively while running of the program, but 
C***    also requires information from the user :
C***
C***    Radius of particle in pixels
C***    Flag to invert the image (particle density must be higher than 
C***                              background)
C***    Flag to test references against each other
C***    File names of reference stack and micrograph image
C***    Output flags for coordinate format etc 
C***    
C***    A log file (sleuth.log) charts the progress of pixel positions
C***    and can be examined while running the GUI.
C***    A script can be written when all the parameter values are set
C***    ready for batch running (command line mode) of the program.
C***    The script can be edited for particular micrograph images.
C***
C*** 2) Command line mode. All these parameter values can have been set
C***    by running the program in interactive mode.
C***
C***
C***
C***    Data input
C***	
C***    INREF (I5)
C***            = 1 input a stack of references
C***            = 2 input a file of parameters created by a previous
C***                run.
C***
C***    if INREF = 1 :
C***
C***        RADIUS, RADFAC, DISTMIN, RATSDEVS   (*) 
C***		RADIUS    external particle radius in pixels.
C***                      This value should include the whole particle.
C***		RADFAC	  Factor multiplied by the radius to set the
C***			  size of the surrounding band.
C***			  - set to 0.0 for default (1.2)
C***            DISTMIN   minimum inter-particle distance in pixels
C***                      - set to 0.0 for default(RADIUS * 2 + 2)
C***		RATSDEVS  controls the number of standard deviations
C***			  of the ratio between central and edge density.
C***			  increase this value to include weak particles
C***			  - set to 0.0 for default (1.0)
C***        INVERT, ICOMPRESS, NPIXLOW, ITESTREFS  (*)
C***            INVERT    = 1 to invert densities (ice). Particles should
C***                        have higher density than the background.
C***                      = 2 do not invert (stain).
C***		ICOMPRESS compression factor
C***			  - set to 0 for default (calculated from the radius)
C***		NPIXLOW   size for averaging window for low pass filter
C***			  and must be an odd number
C***			  - set to 0 for default (5)
C***		ITESTREFS = 0 do not test the references against each other
C***			      set this value to 0 if you have > 1 view
C***			  = 1 test the references, reject outliers.
C***        STACKFILE (A) filename for the input stack of
C***                      references to be used for matching. These
C***                      should be as well centred as possible.
C***        PARAMFILE (A) filename for output of parameter value+
C***                      for future micrograph processing. <CR> if
C***                      not required.
C***    if INREF = 2 :
C***            PARAMFILE (A) filename for input of parameter values.
C***                      for future micrograph processing. <CR> if
C***                      not required.
C***
C***    IMAGEFILE (A) Input file name of digitized image to be proce+
C***
C***
C***    MICTYPE, IADJACENT
C***		MICTYPE micrograph type
C***		    0   no label, no carbon
C***		    1   label, no carbon
C***		    2   label and carbon
C***		IADJACENT test for adjacent particles ?
C***		    0   do not test
C***		    1   do test adjacency
C***
C***
C***	HISTEDGE, HISTCUT, SDEVPAR  (*)
C***		HISTEDGE Percentage of histogram edges to be excluded
C***			from calulating the maximum. This is to exlude
C***			large areas of background from becoming peak 
C***			maximum.
C***			Set to 0.0 for default (5%)
C***		HISTCUT Percentage of maximum peak value to be used
C***			for threshold cutoff. Increase to include
C***			more particles.
C***			Set to 0.0 for default (0.5%)
C***			
C***            SDEVPAR number of standard deviations for parameter
C***                    matching. Increase to include more particles.
C***		        - Set to 0.0 for default (2.5).
C***    IOUT (I5)
C***               1      outputs a stack of images in MRC image format.
C***               2      outputs coords in MRC coordinate format.
C***               3      outputs coords in SPIDER coordinate format
C***               4      outputs coords in IMAGIC coordinate format
C***
C***    OUTFILE (A) Output file name for coordinates.
C***	if IOUT = 1
C***            OUTSTACK (A) Output file name for stack.
C***		NOUTSIZE (*) output box size in pixels
C**
C***	Note : Re-entry for multiple files starts with input of IMAGEFILE
C***
C*******************************************************************
C*******************************************************************
C***    HOW TO SET THE PARAMETER VALUES FOR THIS PROGRAM
C***
C***    Sample starting script :
C***
C*** #!/bin/csh -x -e
C*** time /public/sleuth/sleuth.exe << eof
C*** 1
C*** 12.0,0.0,0.0,0.0   ! RADIUS, RADFAC, DISTMIN, RATSDEVS
C*** 1,0,0,1            ! INVERT, ICOMPRESS, NPIXLOW, ITESTREFS
C*** film.stack
C*** film.params
C*** film1.mrc
C*** 0,1              ! MICTYPE, IADJACENT
C*** 0.0,0.0,0.0        ! HISTEDGE, HISTCUT, SDEVPAR
C*** 2                  ! IOUT=1 output stack of images, 2 coordinates
C*** film1.coords
C*** eof
C************************************************************************
C*** This script takes default values for all possible parameters. 
C*** The first image displayed is the set of denoised reference images.
C*** the first image file displayed is the de-noised image of the micrograph
C*** which has also had histogram tail cutoffs applied. The particles
C*** should stand out clearly from the background. If the background
C*** has high frequency noise, increase the value NPIXLOW which sets
C*** the box size for local pixel averaging. If the particles are of
C*** similar overall density level to the background, increase HISTCUT
C*** which sets the percentage height of the histogram cutoff. This figure
C*** can be as high as 20.0 or more. The second image is only displayed if
C*** the micrograph centains labels, carbon and/or areas of unexposed film. 
C*** It will indicate which areas are to be excluded from
C*** particle searching. Sometimes there are very large background areas
C*** which cause an edge peak on the histogram which overrides the desired
C*** peak maximum of the bulk of the densities. In this case, The value of 
C*** HISTEDGE can be used to adjust the number of bins excluded. HISTCUT 
C*** can also be varied to include/exclude areas from the mask map.
C***   When these files are acceptable, RATSDEVS and SDEVPAR can be used
C*** in conjunction for the final particle selection. It is worth using
C*** a log file to watch the progress of individual particles. If the
C*** coordinates of a 'true' particle do not appear, then it has failed
C*** the ratio test. In this case, increase RATSDEVS, or sometimes it is
C*** better to increase HISTCUT. Too many false positives can sometimes
C*** be corrected by decreasing SDEVPAR and/or RATSDEVS. If coordinate
C*** pairs are too close, try decreasing DISTMIN.
C*** A final script might look like this :
C************************************************************************
C*** #!/bin/csh -x -e
C*** time sleuth.exe << eof
C*** 1				! INREF
C*** 12.0,1.25,25.0,0.85   	! RADIUS, RADFAC, DISTMIN, RATSDEVS
C*** 1,0,12,1   		! INVERT, ICOMPRESS, NPIXLOW, ITESTREFS
C*** film.stack
C*** film.params
C*** film1.mrc
C*** 0,1 	  		! MICTYPE, IADJACENT
C*** 2.0,15.0,1.0   		! HISTEDGE, HISTCUT, SDEVPAR
C*** 1    			! IOUT=1 output stack of images, 2 coordinates
C*** film1.coords
C*** film1.out.stack
C*** 50
C*** eof
C*******************************************************************
C*** Future work :
C*** 1.   Improve carbon masking
C*** 2.   Output stack images close to the micrograph edge
C***      need to have edges set to background, not zero
C***      or stripes
C*******************************************************************
	include 'sleuth.inc'
C***
	byte		mapflag(maxsize)
	byte		mapout(maxsize)
	byte		mapval(maxsize)
        byte          	maskout(maxsize)
C***
	character*512	controlfile
	character*512	datafile
	character*512   imagefile
	character	imode
	character*512   logfile
	character*512   outfile
	character*512   outstack
	character*512   paramfile
	character*512	refsfile
	character*64	message(20)
	character*512	tmpstack
	character*512   stackfile
	character*2	switch
C***
        integer*4       ifoundx(maxcoords)
        integer*4       ifoundy(maxcoords)
        integer*4       index(maxcoords,maxclus)
        integer*4       ireject(maxcoords)
        integer*4       kclus(maxcands)
        integer*4       nclus(maxcands)
C***
	logical		displaymap
	logical 	firstcalc
	logical		firstpass
	logical		modmask
	logical		modcompav
	logical		modsdev
	logical		modparam
C***
        real*4          candc(maxcands)
        real*4          candx(maxcands)
        real*4          candy(maxcands)
        real*4          clusminx(maxcoords)
        real*4          clusmaxx(maxcoords)
        real*4          clusminy(maxcoords)
        real*4          clusmaxy(maxcoords)
        real*4          cvar(maxrefs)
        real*4          boxbuf(maxbox)
        real*4          foundc(maxcoords)
        real*4          mapbuf(maxsize)
        real*4          mapin(maxsize)
	real*4		mapstore(maxsize)
        real*4          maskbuf(maxbox)
        real*4          refd(maxrefs)
        real*4          refm(maxrefs)
        real*4          refr(maxrefs)
        real*4          refa(nmaxrings,maxrefs)
        real*4          refv(nmaxrings,maxrefs)
        real*4          refavg(nmaxrings)
        real*4          refvar(nmaxrings)
        real*4          refsects(maxsectors,maxrefs)
	real*4		sectmin(maxrefs)
	real*4		sectref(maxsectors)
	real*4		sectest(maxsectors)
C***
	data refsfile/'sleuth_refs.stack'/
	data logfile/'sleuth.log'/
	data tmpstack/'sleuth_tmp.stack'/
	data message(1)/'masked by label'/
	data message(2)/'ratio test - increase RATSDEVS to include'/
        data message(3)/
     *  'density touch test - decrease RADFAC to include'/
        data message(4)/'variance test'/
        data message(5)/'mass test - increase SDEVPAR to include'/
        data message(6)/
     *  'radius of gyration test - increase SDEVPAR to include'/
        data message(7)/'ring mean/variance test'/
        data message(8)/'sector mean/variance test'/
	data message(9)/'cluster reject-try decreasing DISTMIN'/
	data message(10)/'member of overlarge cluster'/
	data message(11)
     *  /'member of cluster too close to another - decrease DISTMIN'/
        data message(12)/
     *  'too close to another particle - decrease DISTMIN to include'/
        data message(20)/'too close to edge of image'/
C***
        write(6,'(
     *  ''SLEUTH - v1.5 30.05.13 J. Struct. Biol. 2004'')')
C***
C*** gui if f switch set, command line otherwise
C***
	call getarg(1,switch)
	if(switch .eq. ' ') then
	 gui = .false.
	 iounit = 5
	else
	 gui = .true.
	 iounit = idevdata
	 open(unit=idevlog,file=logfile,status='unknown')
         write(idevlog,'(
     *   ''SLEUTH - v1.5 30.05.2013 J. Struct. Biol. 2004'')')
	end if
	firstpass = .true.
	modcompav = .false.
	displaymap = .false.
	if(gui) then
         max_screen_width = 1
         max_screen_height = 1
         call ximageinit(max_screen_width, max_screen_height)
	 call colour_table
	 nlabels = 1
	 iolabel(nlabels) = 'sleuth v1.5 30.05.2013'
	else
  100    write(6,'(''Input stack of references ...... 1'')')
	 write(6,'(''Input file of parameters ....... 2'')')
	 read(iounit,'(i5)',err=100) inref
  	 if(inref .eq. 1) then
     	  write(6,'(''Processing stack of reference images ...'')')
	 else
	  go to 1800
	 end if
	end if
C**********************************************
C*** read in parameter values
  200	if(firstpass) then
	 if(gui) then
C*** radius
  250	  iolabel(nlabels+1) = 'Type in particle radius in pixels ...'
	  return_string = ' '
	  nlabels = 1
	  call ximageioboxdisplay(iolabel,return_string,nlabels+1)
	  call check_real(rad,return_string)
	  if(ierr .ne. 0) go to 250
	  if(rad .lt. 7.0) then
	   nlabels = 2
	   iolabel(nlabels+1) = 'Radius must be at least 7.0'
	   go to 250
	  end if
C*** calculate compression default
	  icompdef = int(rad * 0.1 + 0.4)
C*** make sure it is an odd number so that compression is on midpoint
	  icompdef = icompdef - mod(icompdef,2) - 1
  	  call ximagechangezoom
     *    (izoomfac,izoomfac,izoomsize,izoomsize,ierr)
	  if(ierr .ne. 0) then
  	   iolabel(nlabels+1) = 
     *     'Warning - error changing zoom box, <cr> to continue'
	   return_string = ' '
	   call ximageioboxdisplay(iolabel,return_string,nlabels+1)
	  end if
C*** radfac 
	  radfac = defradfac
C*** distmin
	  distmin = 2.0 * (rad + 2.0)
C*** number of sdevs for ratio calculation
  	  ratsdevs = defratsdevs
C*** invert densities ?
	  iolabel(nlabels+1) = 
     *    'Invert (particles must have high densities) ?'
	  call ximagelabeldisplay(iolabel,nlabels+1)
	  menulist(1) = '1 Invert (ice)'
	  menulist(2) = '2 Do not invert (stain)'
	  call ximagemenuinit(menulist,2)
	  job = -1
360       call ximagewait(job)
	  if(job .le. 0) then
	   go to 360
	  else
	   invert = job
	   call ximagemenuhide
	   call ximagelabelhide
	  end if
C*** compression  
  	  icompress = icompdef
C*** pixel averaging factor
          npixlow = npixlowdef
C*** test refs ?
	  iolabel(nlabels+1) = 
     *    'Set flag for testing references against each other'
	  iolabel(nlabels+2) = 
     *    'test if all references represent same view'
	  call ximagelabeldisplay(iolabel,nlabels+2)
	  menulist(1) = '1 Test'
	  menulist(2) = '2 Do not test'
	  call ximagemenuinit(menulist,2)
	  job = -1
370       call ximagewait(job)
	  if(job .le. 0) then
	   go to 370
	  else
	   itestrefs = job
	   if(job .eq. 2) itestrefs = 0
	   call ximagemenuhide
	   call ximagelabelhide
	  end if
          write(idevlog,'(/
     *    ''Radius, rad scale fac, min dist, num. ratio sdevs...''/
     *    4f10.3)') rad, radfac, distmin,  ratsdevs
	  write(idevlog,'(/
     *    ''invert, icompress, npixlow, itestrefs...''/
     *    4i10)') invert, icompress, npixlow, itestrefs
C******************************** 
C*** command line mode
C********************************
         else
          write(6,'(
     *    ''Radius, rad scale fac, min dist, num. ratio sdevs... ?'')')
          read(iounit,*) rad, radfac, distmin,  ratsdevs
	  write(6,'(4f10.3)') rad, radfac, distmin,  ratsdevs
C*** calculate compression default
	  icompdef = int(rad * 0.1 + 0.4)
C*** make sure it is an odd number
	  icompdef = icompdef - mod(icompdef,2) - 1
 	  write(6,'(''Invert, icompress, npixlow, itestrefs ..... '')')
  	  read(iounit,*) invert, icompress, npixlow, itestrefs
	  write(6,'(4i10)') invert, icompress, npixlow, itestrefs
	 end if
	end if
C***************************************************************
C*** calculate compression factor from radius if setting default
C*** restart here if modparam/gui/not firstpass
C***************************************************************
	if(icompress .eq. 0) icompress = icompdef
        fcompress = float(icompress)
	write(6,'(
     *  ''Images compressed by factor of '',f5.0)') fcompress
        radius = rad / fcompress
	if(radius .lt. radmin) then
	 if(gui) then
	  if(icompress .eq. 1) then
	   iolabel(nlabels+1) =
     *     'Error - particle radius too small for program'
	   iolabel(nlabels+2) = 'Press <cr> to quit'
	   return_string = ' '
	   call ximageioboxdisplay(iolabel,return_string,nlabels+2)
	   stop
	  else 
	   icompress = icompdef
           fcompress = float(icompress)
           radius = rad / fcompress
	   write(iolabel(nlabels+1),
     *     '(''Particle radius too small, re-setting compression to ''
     *     ,i2)') icompress
	   iolabel(nlabels+2) = 'Press <cr> to continue'
	   return_string = ' '
	   call ximageioboxdisplay(iolabel,return_string,nlabels+2)
	  end if
	 else
	  if(icompress .eq. 1) then
	   write(6,'(
     *     ''Error - particle radius too small for program'')')
	   stop
	  else
	   icompress = icompdef
           fcompress = float(icompress)
           radius = rad / fcompress
	   write(6,'(''Particle radius too small, re-setting''
     *     '' compression to '',i2)') icompress
	  end if
	 end if
	end if
	radsq = radius * radius
	if(radfac .eq. 0.0) radfac = defradfac
	radmax = radius * radfac
C*** set maximum radius allowance
	radmaxsq = radmax * radmax
C*** calculate number of rings excluding those close to the centre
	nrad = int(radius)
C*** set and test number of rings
	nrings = nrad - nexclude 
	if(nrings .gt. nmaxrings) then
	 iolabel(1) = 'Error - radius too large - increase nmaxrings.'
	 call prog_error(1)
	 stop
	end if
C*** set minimum distance between particle centres
	if(distmin .eq. 0.0) distmin = 2.0 * (radius + 2.0)
        write(6,'(
     *  ''Minimum distance between particles = '',f10.1)') distmin
        if(gui) write(idevlog,'(
     *  ''Minimum distance between particles = '',f10.1)') distmin
        distminsq = distmin * distmin
C*** number of sdevs for ratio calculation
	if(ratsdevs .le. 0.0) ratsdevs = defratsdevs
C*** set high pass filter box size, pixhigh = 0.5
	npixhigh = 2 * nint(radius * pixhigh) - 1
        ipixhigh = (npixhigh - 1) / 2
C*** set low pass filter factor if default requested
	if(npixlow .eq. 0) npixlow = npixlowdef
C*** ipixlow is amount to add and subt from centre in box averaging
        ipixlow = (npixlow - 1) / 2
	if(invert .eq. 1) then
	 reverse = .true.
	else 
	 reverse = .false.
        end if
C******************************************************
C*** input reference stack filename and param file name
C******************************************************
	if(firstpass) then
	 if(gui) then
 460	  iolabel(nlabels+1) = 
     *    'Type in filename of stack of reference images :'
	  stackfile = ' '
	  call ximageioboxdisplay(iolabel,stackfile,nlabels+1)
	  there = .false.
          inquire(file=stackfile,exist=there)
	  if(.not. there) go to 460
	  write(idevlog,'(''Input reference stack : '',a)') 
     *    stackfile(1:lnblank(stackfile))
	  iolabel(nlabels+1) = 
     *    'Type in output filename to store parameters :' 
	  paramfile = ' '
	  call ximageioboxdisplay(iolabel,paramfile,nlabels+1)
	  write(idevlog,'(''Output parameter file : '',a)')
     *    paramfile(1:lnblank(paramfile))
	 else
          write(6,'(''File name of stack ?'')')
          read(iounit,'(a)') stackfile
	  there = .false.
          inquire(file=stackfile,exist=there)
	  if(.not. there) then
	   write(6,'(''Error - stack file not found :'',a)')
     *     stackfile(1:lnblank(stackfile))
	   stop
	  end if
	  write(6,'(a)') stackfile(1:lnblank(stackfile))
	  write(6,'(''Output file to store parameters <cr> if none'')')
	  read(iounit,'(a)') paramfile
	  write(6,'(a)') paramfile(1:lnblank(paramfile))
	 end if
C*** open reference stack file, keep open for re-reading
	 call imopen(idevrefin,stackfile,'old')
         call irdhdr(idevrefin,nxyz,mxyz,mode,dmin,dmax,dmean)
	 nrefx = nxyz(1)
	 nrefy = nxyz(2)
	 nrefz = nxyz(3)
	end if
C*****************************************************
C*** calculate input box size required
C*****************************************************
C*** radius already compressed, so ncut is uncompressed box size ...?
	ncut =  2 * icompress * (int(radmax) + max(ipixhigh,ipixlow))
	if(ncut .gt. nrefx) then
	 write(iolabel(1),'(
     *   ''Error - box size for reference stack too small.'')')
         write(iolabel(2),'
     *   (''Must be at least '',i6,'' x '',i6,'' pixels.'')')
     *   ncut, ncut
	 iolabel(3) = 'OR : radius incorrectly set'
         iolabel(4) = 
     *   'OR : decrease minimum distance between particles'
	 iolabel(5) = 'OR : decrease ICOMPRESS'
	 call prog_error(5)
	 stop
	end if
C*** final box size
	nbox = ncut / icompress
	nxybox = nbox * nbox
	half = float(nbox) * 0.5
	ihalfbox = nint(half)
        nzbox = nrefz
	if(nrefx .ne. nrefy) then
	 write(iolabel(1),'(''Error - references must be in a square box.'')')
	 call prog_error(1)
	 stop
	else if(nrefx .lt. nbox) then
	 write(iolabel(1),'(''Error - reference box size too small.'')')
	 call prog_error(1)
	 stop
        else if(nrefx .gt. maxline) then
	 write(iolabel,'(''Error - box size too large, increase maxline.'')')
	 call prog_error(1)
         stop
        else if(nrefz .gt. maxrefs) then
	 write(iolabel,'(''Error - too many references, increase maxrefs.'')')
	 call prog_error(1)
	 stop
	end if
C*************************************************************
C*** open binary format file for output reference stack
C*************************************************************
	close(idevrefout)
	open(unit=idevrefout,file=refsfile,status='unknown',
     *  form='unformatted')
	nsecs = 0
C*** initialize parameter variables
	sumd = 0.
	summ = 0.
c	sums = 0.
	sumr = 0.
	nrejects = 0
	write(6,'(/
     *  ''  ref#'',2x,''ratio mean    '',9x,''mass      rgyr'',
     *  '' central variance'')')
	if(gui) write(idevlog,'(/
     *  ''  ref#'',2x,''ratio mean    '',9x,''mass      rgyr'',
     *  '' central variance'')')
C*** set starting box centre between pixels
	xcentre = half + 0.5
	ycentre = half + 0.5
C*** calculate start and end positions for section reading
	ixystart = nint(float(nrefx - ncut) * 0.5) + 1
	ixyend = ixystart + ncut - 1
C*** set maskout array to zero so template pixels included in filtering
	do ixy=1,nxybox
	 maskout(ixy) = 0
        end do
C****************************************************************
C*** calculate box indicies
C****************************************************************
	indmaxy = 0
        indminy = 0
        do iy=1,nbox
         nxy = nbox * (iy - 1)
         ydist = ycentre - float(iy)
         ydistsq = ydist * ydist
         indmaxx = 1
         indminx = 1
         do ix=1,nbox
          xdist = xcentre - float(ix)
          xdistsq = xdist * xdist
          distsq = xdistsq + ydistsq
C*** index outer radius, and outer part of safety band
          if(distsq .le. radmaxsq .and. indmaxx .eq. 1) then
	   indmaxy = indmaxy + 1
	   indxy = nxy + ix
           indexmax(indmaxy,1) = indxy
C*** set inner band indicies at halfbox in case where radial distance
C*** out of range
	   indexband(indmaxy,1) = nxy + ihalfbox
	   indexband(indmaxy,2) = nxy + ihalfbox + 1
	   indxy = nxy + nbox - ix + 1
           indexmax(indmaxy,2) = indxy
           indmaxx = 0
C*** index inner radius, and inner part of safety band 
          else if(distsq .le. radsq .and. indminx .eq. 1) then
	   indminy = indminy + 1
	   indxy = nxy + ix
           indexmin(indminy,1) = indxy
	   indexband(indmaxy,1) = indxy - 1
	   indxy = nxy + nbox - ix + 1
           indexmin(indminy,2) = indxy
	   indexband(indmaxy,2) = indxy + 1
           indminx = 0
          end if
         end do
        end do
	if(gui) then
	 iolabel(nlabels+1) = 'Processing reference stack ...'
	 call ximagelabeldisplay(iolabel,nlabels+1)
	end if
C****************************************************************
C*** start reference loop
C****************************************************************
	do 1400 iz=1,nrefz
	 ireject(iz) = iz
C*** position map on line to start reading, use ncut to solve
C*** promlem with images to be compressed
	 call imposn(idevrefin,iz - 1,ixystart - 1)
	 do iy=1,ncut
	  nxy = (iy - 1) * ncut
	  call irdlin(idevrefin,mapbuf(nxy+1))
	  ixy = ncut * (iy - 1) - ixystart + 1
C*** cut out part of line to go into boxbuf
    	  do ix=ixystart,ixyend
	   boxbuf(ixy + ix) = mapbuf(nxy + ix)
	  end do
	 end do
C*** if map to be compressed copy back to mapbuf call subroutine
	 if(icompress .gt. 1) then 
	  do ixy = 1,ncut * ncut
	   mapbuf(ixy) = boxbuf(ixy)
	  end do
	  call dencompress(ncut,ncut,mapbuf,boxbuf)
	 end if
C*** invert densities if flag set, make all +ve anyway
	 call deninvert(nbox,nbox,boxbuf)
C*** density cleaning - local averaging and spatial highpass filtering
         call denclean(nbox,nbox,boxbuf)
C*** scale from 0-255
	 call denscale(nbox,nbox,boxbuf)
C***********************************************************
C*** write out cleaned references
C***********************************************************
	 do iy=1,nbox
	  ixy = (iy - 1) * nbox + 1
	  write(idevrefout) (boxbuf(ix),ix=ixy,ixy+nbox-1)
	 end do
	 nsecs = nsecs + 1
C*** calculate parameter values, iterating mask and centre calculation
         call calcparams(nbox,boxbuf,maskbuf)
	 if(ierr .gt. 0) then
	  write(6,'(/''!!!Warning - rejecting ref# '',i3)') iz
          write(6,'(''Reason : centre too far out to converge.'')')
	  if(gui) then
	   write(idevlog,'(/''!!!Warning - rejecting ref# '',i3)') iz
           write(idevlog,'(
     *     ''Reason : centre too far out to converge.'')')
	  end if
	  nrejects = nrejects + 1
	  ireject(iz) = 0
	  go to 1400
	 end if
C*** store sector sums
	 do i=1,maxsectors
	  refsects(i,iz) = sectsum(i) 
	 end do
C*** central mean and variance centred filtered unscaled image
	 call denstats(nbox,nbox,4,maskbuf)
C*** store density sum and variance as these will be overwritten
	 sumcent = centsum
	 varcent = centvar
C*** calculate band mean
	 call denstats(nbox,nbox,5,maskbuf)
C*** bandmean-float the image
	 threshlow = bandmean
	 call denfloat(nbox,nbox,maskbuf)
C*** calc central mean from bandmean thresholded image
	 call denstats(nbox,nbox,3,maskbuf)
C*** calc band mean from bandmean thresholded image
	 call denstats(nbox,nbox,5,maskbuf)
	 if(ierr .gt. 0) then
	  write(6,'(/''!!!Warning - rejecting ref# '',i3)') iz
          write(6,'(''Reason : band mean has zero density'')')
	  if(gui) then
	   write(idevlog,'(/''!!!Warning - rejecting ref# '',i3)') iz
           write(idevlog,'(''Reason : band mean has zero density'')')
	  end if
	  nrejects = nrejects + 1
	  ireject(iz) = 0
	  go to 1400
         end if
	 ratmean = centmean / bandmean
	 refd(iz) = ratmean
C*** store parameter values for that reference
	 refm(iz) = sumcent
	 refr(iz) = radgyr
	 cvar(iz) = varcent
	 sumd = sumd + ratmean
	 summ = summ + sumcent
	 sumr = sumr + radgyr
C*** store ring profile data
	 do n=1,nrings
	  refa(n,iz) = ringmean(n)
	  refv(n,iz) = ringsdev(n)
	 end do
 1400   continue
	nzbox = nzbox - nrejects
C*** test if all references rejected
	if(nzbox .le. 0) then
	 write(iolabel,'(''Error - references failed fitting tests ...'')')
	 call prog_error(1)
	 stop
	end if
C***********************************************************
C*** calculate mean and sdev for each parameter
C***********************************************************
 1500   nrejects = 0
        fz = float(nzbox)
C*** calculate chi values for ring mean and variance
C*** average the ring means and variances
	do n=1,nrings
	 refavg(n) = 0.
	 refvar(n) = 0.
	 do iz=1,nzbox
	  if(ireject(iz) .gt. 0) then
	   refavg(n) = refavg(n) + refa(n,iz)
           refvar(n) = refvar(n) + refv(n,iz)
	  end if
	 end do
	 refavg(n) = refavg(n) / fz
	 refvar(n) = refvar(n) / fz
	end do
C*** calc parameter averages
	refratm = sumd / fz
	refmass = summ / fz
	refradg = sumr / fz
	write(6,'(a)')
	sdevd = 0.
	sdevm = 0.
	sdevr = 0.
C*** calculate standard deviations for density ratio mean, sdev, 
C*** total density mass, radius of gyration.
	do iz=1,nzbox
	 if(ireject(iz) .gt. 0) then
          diff = refd(iz) - refratm
	  sdevd = sdevd + diff * diff
	  diff = refm(iz) - refmass
	  sdevm = sdevm + diff * diff
	  diff = refr(iz) - refradg
C*** write calculated parameters first time only
	  sdevr = sdevr + diff * diff
	 end if
	end do
	sdevd = sqrt(sdevd / fz)
	sdevm = sqrt(sdevm / fz)
	sdevr = sqrt(sdevr / fz)
	write(6,'(/
     *  ''Parameter values calculated for'',i5,
     *  '' reference images...'')') nzbox
	write(6,'(''Mean : ratio mass rgyr'',3f10.2)')
     *  refratm,refmass,refradg
	write(6,'(''Sdev : ratio mass rgyr'',3f10.2)')
     *  sdevd,sdevm,sdevr
	if(gui) then
	 write(idevlog,'(/
     *   ''Parameter values calculated for'',i5,
     *   '' reference images...'')') nzbox
	 write(idevlog,'(''Mean : ratio mass rgyr'',3f10.2)')
     *   refratm,refmass,refradg
	 write(idevlog,'(''Sdev : ratio mass rgyr'',3f10.2)')
     *   sdevd,sdevm,sdevr
	end if
C*** only test references if flag set
	if(itestrefs .eq. 0) go to 1750
C*** reject references with > 2 * sdevs away from mean and iterate
	do 1700 iz=1,nzbox
	 if(ireject(iz) .ne. 0) then
	  diffd = refratm - refd(iz)
C*** use absolute values for mass and radius of gyration
	  diffm = abs(refmass - refm(iz))
	  diffr = abs(refradg - refr(iz)) 
C*** test reference with :
C*** ratio of mean density between centre and edges must be > 1.0
C*** standard deviations for mass and radgyr
	  if(refd(iz) .le. 1.0 .or.
     *     diffd .gt. sdevd * defsdevs .or.
     *     diffm .gt. sdevm * defsdevs .or.
     *     diffr .gt. sdevr * defsdevs) then
	   nrejects = nrejects + 1
	   ireject(iz) = 0
	   write(6,'(/''!!!Warning - rejecting ref# '',i3)') iz
	   if(gui) write(idevlog,'(/
     *     ''!!!Warning - rejecting ref# '',i3)') iz
C*** print reason for rejection
	   if(refd(iz) .le. 1.0) then 
            write(6,'(''Reason : ratio mean < 1 '',f6.2)') refd(iz)
            if(gui) write(idevlog,'(
     *      ''Reason : ratio mean < 1 '',f6.2)') refd(iz)
     	   else if(diffd .gt. sdevd * defsdevs) then
            write(6,'(''Reason : ratio mean         = '',
     *      f10.1)') refd(iz)
	    if(gui)
     *      write(idevlog,'(''Reason : ratio mean         = '',
     *      f10.1)') refd(iz)
     	   else if(diffm .gt. sdevm * defsdevs) then
            write(6,'(''Reason : reference mass     = '',
     *      f10.1)') refm(iz)
            if(gui) write(idevlog,'(''Reason : reference mass     = '',
     *      f10.1)') refm(iz)
           else if(diffr .gt. sdevr * defsdevs) then
            write(6,'(''Reason : radius of gyration ='',
     *      f6.3)') refr(iz)
            if(gui) write(idevlog,'(''Reason : radius of gyration ='',
     *      f6.3)') refr(iz)
	   end if
C*** re-initialize all the totals and recalculate with the remaining
C*** particles
	   sumd = 0.
	   summ = 0.
	   sumr = 0.
	   do jz=1,nzbox
	    sumd = sumd + refd(jz)
	    summ = summ + refm(jz)
	    sumr = sumr + refr(jz)
	   end do
	  end if
	 end if
 1700   continue
C*** recalculate if any references rejected
C*** test if all references rejected
	nzbox = nzbox - nrejects
	if(nzbox .eq. 0) then
	 write(iolabel,'(''Error - references failed fitting tests ...'')')
	 call prog_error(1)
	 stop
	end if
	if(nrejects .gt. 0) go to 1500
C******************************************************************
C*** iteration complete for reference images
C*** close reference stack file
C******************************************************************
 1750   close(idevrefout)
	if(gui) then
	 write(idevlog,'(/''Reference calculations complete.'')')
C*** display reference stack for inspection, only if first time,
C*** or icompress or npixlow modified
	 if(firstpass .or. modcompav) then
	  call display_stack(idevrefout,nbox,nbox,nsecs,refsfile)
	  if(ierr .ne. 0) then
	   write(idevlog,'(''Error displaying reference stack'')')
	   stop
	  end if
	  cmin = 0.
	  cmax = grey
	  call colour_contrast
 	  iolabel(nlabels+1) = 'Reference calculations complete'
	  call ximagelabeldisplay(iolabel,nlabels+1)
	 end if
	end if
C****************************************************************
C*** all references selected, calculate parameter value ranges
C*****************************************************************
	centvarmin = bignum
	centvarmax = -bignum
	chiavgmax = -bignum
	chiavgmin = bignum
	chivarmax = -bignum
	chivarmin = bignum
	firstcalc = .true.
c	ratmin = bignum
	do iz=1,nzbox
	 if(ireject(iz) .ne. 0) then
C*** central variance
	  centvarmax = max(centvarmax,cvar(iz))
	  centvarmin = min(centvarmin,cvar(iz))
c	  ratmin = min(ratmin,refd(iz))
	  chia = 0.
	  chiv = 0.
C*** ring means, variances
	  do n=1,nrings
	   diff = refavg(n) - refa(n,iz)
	   chia = chia + diff * diff / refavg(n)
	   diff = refvar(n) - refv(n,iz)
	   chiv = chiv + diff * diff / refvar(n)
	  end do
	  chiavgmax = max(chiavgmax,chia)
	  chiavgmin = min(chiavgmin,chia)
	  chivarmax = max(chivarmax,chiv)
	  chivarmin = min(chivarmin,chiv)
C*** sector sums, align sums to first 
C*** set reference sector to 1st acceptable reference
	  if(firstcalc) then
	   do i=1,maxsectors
	    sectref(i) = refsects(i,iz)
	    sectsum(i) = sectref(i)
	   end do
	   firstcalc = .false.
	   nsectsum = 1
	   nsectref = iz
	   sectmin(nsectsum) = 1
C*** align sector sums for this reference to the first, add into sectsum
	  else
	   do i=1,maxsectors
	    sectest(i) = refsects(i,iz)
	   end do
	   call sectalign(sectref,sectest)
C*** sum sectors, and align reference sectors
	   sectmin(nsectsum) = minsect
	   j = minsect - 1
	   do i=1,maxsectors
	    j = j + 1
	    if(j .gt. maxsectors) j = 1
	    sectsum(i) = sectsum(i) + sectest(j)
	    refsects(i,iz) = sectest(j)
	   end do
	   nsectsum = nsectsum + 1
	  end if
	 end if
	end do
C*** average the sector sums
	if(nsectsum .lt. 1) then
	  write(iolabel(1),'(
     *   ''Error - sector test failure, check radius is correct.'')')
	 call prog_error(1)
	 stop
	end if
	do i=1,maxsectors
	 sectref(i) = sectsum(i) / float(nsectsum)
	end do
C*** calculate chi-square max for the sectors
	chisectmax = -bignum
	do iz=1,nzbox
	 chisectsum = 0.
	 if(ireject(iz) .ne. 0) then
          do i=1,maxsectors
	   diff = sectref(i) - refsects(i,iz)
	   chisectsum = chisectsum + diff * diff / sectref(i)
	  end do
	  chisectmax = max(chisectmax,chisectsum)
	 end if
	end do
C*** write parameter values to output file
	if(paramfile .ne. ' ') then
	 write(6,'(''Writing values to parameter file '',a)')
     *   paramfile(1:lnblank(paramfile))
	 if(gui) write(idevlog,'(
     *   ''Writing values to parameter file '',a)')
     *   paramfile(1:lnblank(paramfile))
	 open(unit=idevparam,file=paramfile,status='unknown')
	 write(idevparam,*) icompress,nbox,nxybox,
     *   nrad,rad,radsq,radmax,distminsq,
     *   centvarmin,centvarmax,chisectmax,
     *   refratm,sdevd,ratsdevs,refmass,sdevm,refradg,sdevr,
     *   chiavgmin,chiavgmax,chivarmin,chivarmax,
     *   reverse,ipixlow,ipixhigh,nrings
	 do n=1,nrings
	  write(idevparam,*) refavg(n), refvar(n)
	 end do
	 write(idevparam,*) indmaxy, indminy
	 do i=1,indmaxy
	  write(idevparam,*) indexmax(i,1), indexmax(i,2)
	 end do
	 do i=1,indminy
	  write(idevparam,*) indexmin(i,1), indexmin(i,2)
	 end do
	 write(idevparam,*) sectref
	 close(idevparam)
	end if
	go to 1820
C*** stack processed by previous run, read parameter file, but
C*** only if first pass
 1800   write(6,'(''Reading reference data from parameter file...'')')
	if(firstpass) then
	 write(6,'(/''Type in parameter file name ...'')')
	 read(iounit,'(a)',err=9500,end=9000) paramfile
	 write(6,'(a)') paramfile(1:lnblank(paramfile))
 1810    inquire(file=paramfile,exist=there)
         if(.not.there) then
          write(6,'(''File does not exist, please retype....'')')
	  read(iounit,'(a)') paramfile
	  write(6,'(a)') paramfile(1:lnblank(paramfile))
	  go to 1810
	 end if
         open(unit=idevparam,file=paramfile,status='old')
         read(idevparam,*) icompress,nbox,nxybox,
     *   nrad,rad,radsq,radmax,distminsq,
     *   centvarmin,centvarmax,chisectmax,
     *   refratm,sdevd,ratsdevs,refmass,sdevm,refradg,sdevr,
     *   chiavgmin,chiavgmax,chivarmin,chivarmax,
     *   reverse,ipixlow,ipixhigh,nrings
	 do n=1,nrings
	  read(idevparam,*) refavg(n), refvar(n)
	 end do
	 read(idevparam,*) indmaxy, indminy
	 do i=1,indmaxy
	  read(idevparam,*) indexmax(i,1), indexmax(i,2)
	 end do
	 do i=1,indminy
	  read(idevparam,*) indexmin(i,1), indexmin(i,2)
	 end do
	 read(idevparam,*) sectref
         close(idevparam)
	 radius = rad
 	end if
C*******************************************************************
C*** read in image file
C*******************************************************************
 1820   if(firstpass) then	
C********************************
C*** gui entry
C********************************
	 if(gui) then
	  nadd = 1
 1830 	  iolabel(nlabels+nadd) = 'Type in MRC image format file name...'
 	  imagefile = ' '
	  call ximageioboxdisplay(iolabel,imagefile,nlabels+nadd)
          inquire(file=imagefile,exist=there)
          if(.not.there) then
           iolabel(nlabels+1) = 'File does not exist....'
	   nadd = 2
	   go to 1830
	  end if
	  write(idevlog,'(''Image file : '',a)') 
     *    imagefile(1:lnblank(imagefile))
C*** micrograph type 
	  iolabel(nlabels+1) = 'Select micrograph type :'
	  call ximagelabeldisplay(iolabel,nlabels+1)
	  menulist(1) = '1 no label, no carbon'
	  menulist(2) = '2 label, no carbon'
	  menulist(3) = '3 label + carbon' 
	  call ximagemenuinit(menulist,3)
	  job = -1
 1840     call ximagewait(job)
	  if(job .le. 0) then
	   go to 1840
	  else 
	   mictype = job -1
	   micsave = mictype
	  end if
	  call ximagelabelhide
	  call ximagemenuhide
C*** adjaceny test
 	  iolabel(nlabels+1) = 'Test for adjacent particles :'
	  iolabel(nlabels+2) = 'Do test unless particles rod-shaped or'
	  iolabel(nlabels+3) = 'fall inside each others (circular) mask'
	  call ximagelabeldisplay(iolabel,nlabels+3)
	  menulist(1) = '1 do test' 
	  menulist(2) = '2 do not test'
	  call ximagemenuinit(menulist,2)
	  job = -1
 1850     call ximagewait(job)
	  if(job .le. 0) then
	   go to 1850
	  else if(job .eq. 1) then
	   iadjacent = job
	  else
	   iadjacent = 0
	  end if
	  call ximagelabelhide
	  call ximagemenuhide
C*** histedge
 	  histedge = defedge * 100.
C*** histcut
 	  histcut = defcut * 100.
C*** sdevpar
 	  sdevpar = defsdevpar
C***********************
C*** command line entry
C***********************
	 else
 1860	  write(6,'(//''Type in MRC Image format file name .......'')')
	  read(iounit,'(a)',err=9500,end=9000) imagefile
	  write(6,'(a)') imagefile(1:lnblank(imagefile))
          inquire(file=imagefile,exist=there)
          if(.not.there) then
           write(6,'(''File does not exist....'')')
	   go to 1860
	  end if
C*** read number of standard deviations allowed
	  write(6,'(/''Micrograph type, adjacency flag'')')
	  read(iounit,*) mictype, iadjacent
	  micsave = mictype
	  write(6,'(2i10)') mictype, iadjacent
	  write(6,'(/
     *    ''Number of standard deviations for histogram cutoff, '',
     *    ''parameter matches'')')
	  read(iounit,*) histedge, histcut, sdevpar
	 end if
C*** set histedge to a percentage
	 if(histedge .le. 2.0) then
	  histedge = defedge
	 else
	  histedge = histedge * 0.01
	 end if	
	 if(histcut .eq. 0.0) then
	  histcut = defcut
	 else
	  histcut = histcut * 0.01
	 end if
	 if(sdevpar .eq. 0.0) sdevpar = defsdevpar
	 write(6,'(3f10.2)') histedge, histcut, sdevpar
	 write(6,'(
     *   '' Using '',f5.0,'' sdevs about mean for each parameter'')')
     *   sdevpar
C*************************************************
C*** select output mode and file name
C*************************************************
	 noutsize = 0
	 if(gui) then
	  write(idevlog,'(''mictype, iadjacent : '',2i10)') 
     *    mictype, iadjacent
	  write(idevlog,'(/
     *    ''Number of standard deviations for histogram cutoff, '',
     *    ''parameter matches'',3f10.1)') histedge, histcut, sdevpar
	  iolabel(nlabels+1) = 'Select output type'
	  call ximagelabeldisplay(iolabel,nlabels+1)
	  menulist(1) = '1 Output MRC format stack of images'
	  menulist(2) = '2 Output MRC format coordinate list'
	  menulist(3) = '3 Output SPIDER format coordinate list'
	  menulist(4) = '4 Output IMAGIC format coordinate list'
	  call ximagemenuinit(menulist,4)
	  job = -1
1865      call ximagewait(job)
	  if(job .le. 0) then
	   go to 1865
	  else
	   iout = job
	   call ximagemenuhide
	   call ximagelabelhide
	  end if
C*** read coordinate file name
	  iolabel(nlabels+1) = 'Type coordinate file name ...'
	  outfile = ' '
	  call ximageioboxdisplay(iolabel,outfile,nlabels+1)
	  write(idevlog,'(''Output coordinate file : '',a)')
     *    outfile(1:lnblank(outfile))
C*** output stack file name
	  if(iout .eq. 1) then
	   iolabel(nlabels+1) = 'Type output stack file name ...'
	   outstack = ' '
	   call ximageioboxdisplay(iolabel,outstack,nlabels+1)
	   write(idevlog,'(''Output stack file : '',a)')
     *     outstack(1:lnblank(outstack))
C*** output box size  
 1862	   iolabel(nlabels+1) = 'Type output box size in pixels ...'
	   return_string = ' '
	   call ximageioboxdisplay(iolabel,return_string,nlabels+1)
	   call check_integer(noutsize,return_string)
	   if(ierr .ne. 0) go to 1862
	   write(idevlog,'(''Output box size : '',i10)') noutsize
	  end if
C*******************************************
C*** command line mode
C*******************************************
	 else
	  write(6,'(
     *    ''Output stack of images in MRC image format ..... 1''/
     *    ''Output file of coordinates in MRC format ....... 2''/
     *    ''Output file of coordinates in Spider format .... 3''/
     *    ''Output file of coordinates in IMAGIC format .... 4'')')
	  read(iounit,'(i5)') iout
	  write(6,'(''output coordinate file name ..... ?'')')
	  read(iounit,'(a)') outfile
	  write(6,'(a)') outfile(1:lnblank(outfile))
C*** MRC stack of images
  	  if(iout .eq. 1) then
	   write(6,'(''output stack file name ..... ?'')')
	   read(iounit,'(a)') outstack
	   write(6,'(a)') outstack(1:lnblank(outstack))
           inquire(file=outstack,exist=there)
           if(there) then
	     write(6,'(''Warning - overwriting stack '',a)') 
     *       outstack(1:lnblank(outstack))
	   end if
	   write(6,'(
     *     ''Output box size in pixels ... ?'')')
	   read(iounit,*) noutsize
	   write(6,'(i10)') noutsize
	  end if
	 end if
	end if
C***************************************************************
C***************************************************************
C*** halfbox will be between pixels, ihalfbox will be even
	halfbox = float(nbox) * 0.5
	ihalfbox = nint(halfbox)
	xcentre = halfbox + 0.5
	ycentre = halfbox + 0.5
	cenx = xcentre
	ceny = ycentre
	radmaxsq = radmax * radmax
C*** calculate allowance range for mass and radius of gyration
	sdevsm = sdevpar * sdevm
	sdevsr = sdevpar * sdevr
	ratmin = max(1.0,refratm - sdevd * ratsdevs)
	ratmax = refratm + sdevd * ratsdevs
	ixystart = 1
C*** print out mean,sd of all params
	write(6,'(//''min/max density ratio    = '',2f10.3)')
     *  ratmin, ratmax
	write(6,'(''min/max central variances       = '',2f10.3)')
     *  centvarmin, centvarmax
	write(6,'(''mean, sdev total density      = '',2f10.3)')
     *   refmass,sdevm
        write(6,'(''mean, sdev radius gyration    = '',2f10.3)')
     *  refradg,sdevr
     	write(6,'(''min, max chi mean, variance   = '',4f10.3)')
     *  chiavgmin,chiavgmax,chivarmin,chivarmax
	write(6,'(''max sector chi value          = '',f10.3)')
     *  chisectmax
	if(gui) then
	 write(idevlog,'(//''min/max density ratio    = '',2f10.3)')
     *   ratmin, ratmax
	 write(idevlog,'(''min/max central variances       = '',2f10.3)')
     *   centvarmin, centvarmax
	 write(idevlog,'(''mean, sdev total density      = '',2f10.3)')
     *    refmass,sdevm
         write(idevlog,'(''mean, sdev radius gyration    = '',2f10.3)')
     *   refradg,sdevr
     	 write(idevlog,'(''min, max chi mean, variance   = '',4f10.3)')
     *   chiavgmin,chiavgmax,chivarmin,chivarmax
	 write(idevlog,'(''max sector chi value          = '',f10.3)')
     *   chisectmax
	end if
C**************************************************************
C*** close stack file if used, open image input file
C**************************************************************
 1870	call imopen(idevimage,imagefile,'old')
        call irdhdr(idevimage,nxyz,mxyz,mode,dmin,dmax,dmean)
	nx = nxyz(1)
	ny = nxyz(2)
	nz = nxyz(3)
	ncompx = nx / icompress
	ncompy = ny / icompress
	fnxy = icompress * icompress
	if(ncompx * ncompy .gt. maxsize) then
	 write(6,'(
     *   ''Error - map too large for program, increase maxsize'')')
	 if(gui) write(idevlog,'(
     *   ''Error - map too large for program, increase maxsize'')')
	 stop
	end if
	WRITE(6,'(''************icompress :'',i5)')ICOMPRESS
C*** read micrograph image
        do iy=1,ny,icompress
C*** read ncompy lines at a time
	 do jy=1,icompress
	  istarty = iy + jy - 2
	  istartx = (jy - 1) * nx + 1
          call imposn(idevimage,0,istarty)
	  call irdlin(idevimage,mapin(istartx))
	 end do
C*** compress those ncompy lines and store in final map
	 do ix=1,nx,icompress
	  sum = 0.
	  do jy=1,icompress
	   jxy = (jy - 1) * nx + ix - 1
	   do jx=1,icompress
	    sum = sum + mapin(jxy + jx)
	   end do
	  end do
	  ixy = (iy / icompress) * ncompx + ix / icompress + 1
	  mapbuf(ixy) = sum / fnxy
	 end do
	end do
	call imclose(idevimage)
	jxyz(1) = ncompx
	jxyz(2) = ncompy
	jxyz(3) = nz
C*** invert densities if flag set, make all +ve anyway
	call deninvert(ncompx,ncompy,mapbuf)
C*** store compressed map if gui
	if(gui) then
	 iolabel(nlabels+1) =
     *   'Masking/filtering input image, please wait ...'
	 call ximagelabeldisplay(iolabel,nlabels+1)
	 do i=1,ncompx * ncompy
	  den = mapbuf(i)
	  mapstore(i) = den
	 end do
	end if
C*** calculate mask map to remove blank areas and histo scale main map
C*** only if map has labels / areas of carbon
 	modparam = .false.
	modmask = .false.
 1880 	mictype = micsave
	if(mictype .gt. 0) call denmask(ncompx,ncompy,mapbuf,maskout)
C*** reset mictype if 2 (carbon) for density cleaning as cleaning removes
C*** double peak
 1890	mictype = min(1, mictype)
C*** density cleaning
  	call denclean(ncompx,ncompy,mapbuf)
C*** enhance by histogram modification
	call denenhance(ncompx,ncompy,mapbuf)
C**************************************************************
C*** if gui, write out map and modify parameter values
C**************************************************************
	if(gui .and. firstpass) then
C*** display cleaned map - zero mapout first
 	 do i=1,maxsize
	  mapout(i) = 0
	 end do
C*** scale map and write as byte
	 scl = grey/ (tmax - tmin)
	 do iy = 1, ncompy
	  ixy = (iy - 1) * ncompx
	  do ix = 1, ncompx
	   nxy = ixy + ix
	   mapout(nxy) = min(grey, max(1.,(mapbuf(nxy) - tmin) * scl))
	  end do
	 end do
	 call display_image(ncompx,ncompy,mapout)
	 if(ierr .ne. 0) then
	  write(6,'(''Error displaying image'')')
	  stop
	 end if
	 cmin = 0.
	 cmax = grey
	 call colour_contrast
 	 iolabel(nlabels+1) = 
     *   'Reference processing complete, examine cleaned image'
 	 iolabel(nlabels+2) =
     *   'Reset parameter if particles cannot be seen easily :'
         iolabel(nlabels+3) = 
     *   'Decrease compression factor ICOMPRESS if particles'//
     *   ' very small'
	 iolabel(nlabels+4) =
     *   'Increase pixel averaging window NPIXLOW for noisy images'
	 call ximagelabeldisplay(iolabel,nlabels+4)
	 menulist(1) = '1 Reset ICOMPRESS'
	 menulist(2) = '2 Reset NPIXLOW'
	 menulist(3) = '3 Continue processing'
	 menulist(4) = '4 Quit'
	 call ximagemenuinit(menulist,4)
	 job = -1
 1900    call ximagewait(job)
	 if(job .le. 0) then
	  go to 1900
C*** reset ICOMPRESS
	 else if(job .eq. 1) then
	  modcompav = .true.
	  call ximagemenuhide
	  call ximagelabelhide
 1902	  write(iolabel(nlabels+1),'(''Reset value for ICOMPRESS :'',i3)')
     *    icompress
	  return_string = ' '
	  call ximageioboxdisplay(iolabel,return_string,nlabels+1)
	  call check_integer(icompress,return_string)
	  if(ierr .ne. 0) go to 1902
C*** check compression within limits
	  testrad = rad / float(icompress)
	  if(testrad .gt. radius_max) then
	   nlabels = 2
	   iolabel(nlabels) = 
     *     'Compression factor too small for program'
	   go to 1902
	  else if(rad / float(icompress) .lt. radmin) then
	   nlabels = 2
	   iolabel(nlabels) = 
     *     'Compression factor too large for program'
	   go to 1902
	  end if
	  nlabels = 1
	  iolabel(nlabels+1) = 'Recomputing map for display...'
	  call ximagelabeldisplay(iolabel,nlabels+1)
	  write(idevlog,'(''### icompress reset to :'',i10)') icompress
	  go to 1870
C*** reset NPIXLOW
	 else if(job .eq. 2) then
	  modcompav = .true.
	  call ximagemenuhide
	  call ximagelabelhide
 1904	  write(iolabel(nlabels+1),'(''Reset value for NPIXLOW :'',I3)')
     *    npixlow
	  return_string = ' '
	  call ximageioboxdisplay(iolabel,return_string,nlabels+1)
	  call check_integer(npixlow,return_string)
	  if(ierr .ne. 0) go to 1904
	  iolabel(nlabels+1) = 'Recomputing map for display...'
	  call ximagelabeldisplay(iolabel,nlabels+1)
	  do i=1,ncompx * ncompy
	   mapbuf(i) = mapstore(i)
	  end do
C*** set low pass filter factor if default requested
	  if(npixlow .eq. 0) npixlow = npixlowdef
C*** ipixlow is amount to add and subt from centre in box averaging
	  ipixlow = (npixlow - 1) / 2
	  write(idevlog,'(''### npixlow reset to : '',i10)') npixlow
	  go to 1880
C*** continue processing
	 else if(job .eq. 3) then
	  call ximagemenuhide
	  call ximagelabelhide
C*** quit
	 else if(job .eq. 4) then
	  call ximagemenuhide
	  call ximagelabelhide
	  stop
	 end if
C***************************************************************
C*** modify mask map parameters
C***************************************************************
	 if(mictype .gt. 0) then
C*** first display mask map
 1910	  do iy = 1, ncompy
	   ixy = (iy - 1) * ncompx
C*** add mask map set 0 or 1 to 0 or grey
	   do ix = 1,ncompx
	    nxy = ixy + ix
	    mapout(nxy) = grey * maskout(nxy)
	   end do
	  end do
	  call colour_table
 	  call display_image(ncompx,ncompy,mapout)
 	  iolabel(nlabels+1) = 
     *    'Examine mask map to see if appropriate areas are masked out'
	  iolabel(nlabels+2) =
     *    'HISTEDGE sets percentage of density histogram bins '//
     *    'to be excluded from max. calculation'
          iolabel(nlabels+3) =
     *    'Increasing HISTEDGE excludes large areas of background'
	  iolabel(nlabels+4) =
     *    'HISTCUT sets percentage of density histogram peak used '//
     *    'for cutoff' 
	  iolabel(nlabels+5) =
     *    'Increasing HISTCUT masks more carbon '//
     *    'and background areas'
	  call ximagelabeldisplay(iolabel,nlabels+5)
	  menulist(1) = '1 Reset HISTEDGE'
	  menulist(2) = '2 Reset HISTCUT'
	  menulist(3) = '3 Continue processing'
	  menulist(4) = '4 Quit'
	  call ximagemenuinit(menulist,4)
	  job = -1
 1920     call ximagewait(job)
	  if(job .le. 0) then
	   go to 1920
C*** modify HISTEDGE
	  else if(job. eq. 1) then
	   modmask = .true.
	   call ximagemenuhide
	   call ximagelabelhide
 1922	   write(iolabel(nlabels+1),
     *     '(''Reset value for HISTEDGE :'',f6.1)') histedge * 100.
	   return_string = ' '
	   call ximageioboxdisplay(iolabel,return_string,nlabels+1)
	   if(return_string .ne. ' ') then
	    call check_real(histedge,return_string)
	    if(ierr .ne. 0) go to 1922
	    write(idevlog,'(''### histedge reset to : '',f10.1)') histedge
	   end if
C*** set histedge to a percentage
	   if(histedge .le. 2.0) then
	    histedge = defedge
	   else
	    histedge = histedge * 0.01
	   end if	
	   do i=1,ncompx * ncompy
	    mapbuf(i) = mapstore(i)
	   end do
	   mictype = micsave
	   call denmask(ncompx,ncompy,mapbuf,maskout)
	   go to 1910
C*** modify HISTCUT
	  else if(job. eq. 2) then
	   modmask = .true.
	   call ximagemenuhide
	   call ximagelabelhide
 1924	   write(iolabel(nlabels+1),
     *     '(''Reset value for HISTCUT :'',f6.1)') histcut *100.
	   return_string = ' '
	   call ximageioboxdisplay(iolabel,return_string,nlabels+1)
	   if(return_string .ne. ' ') then
	    call check_real(histcut,return_string)
	    if(ierr .ne. 0) go to 1924
	    write(idevlog,'(''### histcut reset to : '',f10.1)') histcut
	    if(histcut .eq. 0.0) then
	     histcut = defcut
	    else
	     histcut = histcut * 0.01
	    end if
	   end if
	   do i=1,ncompx * ncompy
	    mapbuf(i) = mapstore(i)
	   end do
	   mictype = micsave
	   call denmask(ncompx,ncompy,mapbuf,maskout)
	   go to 1910
C*** continue processing
	  else if(job .eq. 3) then
           call ximagemenuhide
	   call ximageclearimage
 	   call ximagelabelhide
C*** quit 
          else if(job .eq. 4) then
           call ximagemenuhide
           call ximagelabelhide
	   stop
	  end if
	  call ximagemenuhide
	 end if
C*** parameters changed, so need to re-process references
	 firstpass = .false.
	 mictype = micsave
	 if(modcompav) then
	  iolabel(nlabels+1) = 
     *    'Reprocessing references and map with modified values...'
	  call ximagelabeldisplay(iolabel,nlabels+1)
	  write(idevlog,'(/
     *    ''### reprocessing references and map'')') 
	  go to 200
C*** recompute mask map ?
	 else if(modmask) then
	  iolabel(nlabels+1) = 
     *    'Reprocessing mask and cleaned map with modified values...'
	  call ximagelabeldisplay(iolabel,nlabels+1)
	  write(idevlog,'(/
     *    ''### reprocessing map with modified values'')') 
	  go to 1890
	 end if
	end if
	modcompav = .false.
	if(gui) then
         call ximagechangecursor(iskullcursor)
	 iolabel(nlabels+1) = 'Searching for particles ...'
	 call ximagelabeldisplay(iolabel,nlabels+1)
C*** set mapflag to 0, map values to 0.
	 do nxy = 1,ncompx * ncompy
	  mapflag(nxy) = 20.
	  mapval(nxy) = 0.
	 end do
	end if
C*** output candidates if itest set
	if(itest) then
         call imopen(idevcands,'cands.stack','unknown')
	 lxyz(1) = nbox
	 lxyz(2) = nbox
	 lxyz(3) = 1
	 mode = 0	  
	 call icrhdr(idevcands,lxyz,lxyz,mode,label,1)
         amin = bignum
         amax = -bignum
	 amean = 0.
	 lxyz(3) = 0
	end if
C************************************************************************
C*** initialize parameter values
C************************************************************************
 1930   ncands = 0
C*** reset centre
	cenx = xcentre
	ceny = ycentre
	reverse = .false.
C************************************************************************
C*** main vertical loop for finding candidate particles
C*** start one whole box up to prevent catching the edge
C*** parts of particles
C************************************************************************
	do icoordy=ihalfbox,ncompy-ihalfbox
C*** icoordx, icoordy have origin at 0,0 first pixel
	 mystart = ncompx * (icoordy - ihalfbox)
C*** -ontal loop
	 do 2000 icoordx=ihalfbox,ncompx-ihalfbox
	  tmin = bignum
	  tmax = -bignum
CCC	  mxystart = mystart + icoordx - ihalfbox + 1
	  mxystart = mystart + icoordx - ihalfbox
	  icoordxy = (icoordy - 1) * ncompx + icoordx
C*** window each box starting at 1
	  do iy=1,nbox
	   ixy = nbox * (iy - 1)
	   mxy = mxystart + ncompx * (iy - 1)
	   do ix=1,nbox
C*** reject all boxes with any pixels flagged by reject mask
	    if(maskout(mxy + ix) .eq. 1) then
	     mapflag(icoordxy) = 1
	     go to 2000
	    end if
	    den = mapbuf(mxy + ix)
	    boxbuf(ixy + ix) = den
	    tmin = min(tmin,den)
	    tmax = max(tmax,den)
	   end do
	  end do
C*** store image
	  do ixy=1,nxybox
	   maskbuf(ixy) = boxbuf(ixy)
	  end do
C*** calculate band mean
	  call denstats(nbox,nbox,5,boxbuf)
C*** threshold the image to the band mean
	  threshlow = bandmean
	  call denfloat(nbox,nbox,boxbuf)
C*** calculate central mean from thresholded image
	  call denstats(nbox,nbox,3,boxbuf)
C*** calculate band mean from thresholded image
	  call denstats(nbox,nbox,5,boxbuf)
	  if(ierr .ne. 0) go to 2000
C*** ratio between centre and safety band
	  ratmean = centmean / bandmean
	  if(itest) write(6,'(''******Ratio for'',2i6,'' = '',f10.4)')
     *    icoordx*icompress,icoordy*icompress,ratmean
C**************************************************************************
C*** first filter out windows with low density distribution
C*** where density sd and mean ratios too low
C**************************************************************************
	  if(ratmean .lt. ratmin .or. ratmean .gt. ratmax) then
	   if(gui) then
	    if(ratmean .lt. ratmin) then
	     mapflag(icoordxy) = -2
	     mapval(icoordxy) = nint(scalefac * (refratm - ratmean) / sdevd)
	    else
	     mapflag(icoordxy) = 2
	     mapval(icoordxy) = nint(scalefac * (ratmean - refratm) / sdevd)
	    end if
	   end if
	   go to 2000
	  end if
C**************************************************************************
C*** write to output file if itest set
C**************************************************************************
	  if(gui)
     *	   write(idevlog,'(2i6,'' passed ratio test :''/6f10.2)')
     *     icoordx*icompress,icoordy*icompress,
     *     ratmean,boxmean,boxsdev,centmean,centvar,bandmean
C**************************************************************************
C*** test particle not adjacent to noise or particle aggregate
C**************************************************************************
	  if(iadjacent .eq. 1) then
	   call dentouch(nbox,boxbuf)
	   if(ierr .ne. 0) then
	    if(gui) write(idevlog,'(''Rejecting window'',2i6,
     *      '' with density in surrounding band'')') 
     *      icoordx*icompress,icoordy*icompress
	    mapflag(icoordxy) = 3
	    go to 2000
	   end if
	  end if
C**************************************************************************
C*** retrieve boxbuf from saved maskbuf
C**************************************************************************
	  do ixy=1,nxybox
	   boxbuf(ixy) = maskbuf(ixy)
	  end do
C*** scale from 0-255
	  call denscale(nbox,nbox,boxbuf)
C*** centre mean and variance ratios
	  call denstats(nbox,nbox,4,boxbuf)
	  if(centvar .lt. centvarmin) then
	   mapflag(icoordxy) = 4
	   go to 2000
	  end if
	  if(gui)
     *    write(idevlog,'(2i6,'' passed variance test :'',f15.2)')
     *    icoordx*icompress,icoordy*icompress,centvar
C**************************************************************************
C*** second filter test for candidates matching mass - filters out weak
C*** areas of density (subtract calculated mass from reference mass)
C**************************************************************************
	  if(refmass - centsum .gt. sdevsm) then
	   if(gui) then
	    mapflag(icoordxy) = 5
	    mapval(icoordxy) = nint(scalefac * (refmass - centsum) / sdevm)
	   end if
	   go to 2000
	  end if
	  if(gui) 
     *    write(idevlog,'(2i6,'' passed mass test :'',2f15.1)')
     *    icoordx*icompress,icoordy*icompress,centsum,radgyr
C*** calculate mean and variance within large radius
          call denstats(nbox,nbox,2,boxbuf)
C*** carry out thresholding from mean +/- 2.5 * sdev
	  call denthresh(nbox,nbox,boxbuf)
C*** taper the scaled image
          call calctaper(nbox,boxbuf,maskbuf)
C*** calculate mean and sdev of tapered box
	  call denstats(nbox,nbox,2,maskbuf)
C*** calculate radius of gyration
	  call calcradg(nbox,maskbuf)
C**************************************************************************
C*** third filter matching radius of gyration. Filters radius of gyration 
C*** too far away from reference value.
C**************************************************************************
          if(abs(refradg - radgyr) .gt. sdevsr) then
	   if(gui) then
C*** radg too small
	    if(refradg - radgyr .gt. sdevsr) then
	     mapflag(icoordxy) = -6
	     mapval(icoordxy) = 
     *       nint(scalefac * abs(refradg - radgyr) / sdevr)
C*** radg too large
	    else
	     mapflag(icoordxy) = 6
	     mapval(icoordxy) = 
     *       nint(scalefac * abs(refradg - radgyr) / sdevr)
	    end if
	   end if
           go to 2000
	  end if
	  if(gui) 
     *    write(idevlog,'(2i6,'' passed radg test :'',f6.2)')
     *    icoordx*icompress,icoordy*icompress,radgyr
C**************************************************************************
C*** third filter test for ring mean and variance chi values
C**************************************************************************
C*** calculate ring means and variances for this particle
	  call ringparams(nbox,maskbuf)
	  if(ierr .eq. 1) then
	   if(gui) mapflag(icoordxy) = 7
	   go to 2000
	  end if
	  chia = 0.
	  chiv = 0.
	  do n=1,nrings
	   diff = refavg(n) - ringmean(n) 
	   chia = chia + diff * diff / refavg(n)
	   diff = refvar(n) - ringsdev(n) 
	   chiv = chiv + diff * diff / refvar(n)
	  end do
C*** check chi mean and variance, check goodness of fit
	  if(chia .gt. chiavgmax .or. chiv .gt. chivarmax) then
	   if(gui) mapflag(icoordxy) = 7
	   go to 2000
C*** passed ring mean, variance test, procced to sector test
	  else 
	   if(gui)
     *     write(idevlog,'(2i6,'' passed ring mean,var test :'',2f6.2)')
     *     icoordx*icompress,icoordy*icompress,chia,chiv
	   if(itest) then
	    do iy=1,nbox
	     ixy = (iy - 1) * nbox + 1
	     call iwrlin(idevcands,maskbuf(ixy))
	    end do
	    do ixy=1,nbox * nbox
	     den = maskbuf(ixy)
	     amin = min(amin,den)
	     amax = max(amax,den)
	     amean = amean + den
	    end do
	    lxyz(3) = lxyz(3) + 1
	   end if
C**************************************************************************
C*** fourth filter test for sectors of density
C**************************************************************************
C*** calculate mean of central area
	   call denstats(nbox,nbox,3,maskbuf)
	   call denclose(nbox,nbox,maskbuf)
	   call sectparams(nbox,maskbuf)
	   call sectalign(sectref,sectsum)
C*** calculate chisq for sector values
	   chisect = 0
	   j = minsect - 1
	   do i=1,maxsectors
	    j = j + 1
	    if(j .gt. maxsectors) j = 1
	    diff = sectref(i) - sectsum(j)
	    chisect = chisect + diff * diff / sectref(i)
	   end do
	   if(chisect .gt. chisectmax) then
	    if(gui) mapflag(icoordxy) = 8
	    go to 2000
	   end if
	   if(gui)
     *     write(idevlog,'(2i6,'' passed sector test :'',f10.2)')
     *     icoordx*icompress,icoordy*icompress,chisect
	   ncands = ncands + 1
	   if(ncands .gt. maxcands) then
	    write(6,'(
     *      ''Warning - too many candidates for program, processing'',
     *      i5)') maxcands
	    if(gui) write(idevlog,'(
     *      ''Warning - too many candidates for program, processing'',
     *      i5)') maxcands
	    ncands = maxcands
	    go to 2500
	   end if
C*** candidate passed test, store coords, radius of gyration,mass
C**  and chi parameter values for ring mean and variance
 	   candx(ncands) = icoordx
	   candy(ncands) = icoordy
	   candc(ncands) = chia + chiv
	  end if
 2000    continue
	end do
C**************************************************************************
C*** write output file if itest set
C**************************************************************************
 2500   if(itest) then
	 call ialsiz(idevcands,lxyz,lxyz)
	 fxyz = float(lxyz(1) * lxyz(2) * lxyz(3))
	 if (fxyz .eq. 0.) then
    	  write(6,'(''!!! No candidate sections written to file.'')')
	  stop
	 end if
	 amean = amean / fxyz
         call iwrhdr(idevcands,title,-1,amin,amax,amean)
	 call imclose(idevcands)
	end if
C*******************************************************************
C*** re-order candidate particles into clusters
C*** kclus contains the cluster pointer for each entry
C*** nclus is the number of entries per cluster
C*** nclusters is the total number of clusters
C*** initialize cluster arrays
C*******************************************************************
	if(ncands .gt. maxcands) then
	  write(iolabel(1),'(
     *   ''Error - too many candidates for program,''/
     *   '' increase maxcands to'',i10)') ncands * 2
	 call prog_error(1)
	 stop
	else
	 write(6,'(i10,'' candidates selected.'')') ncands
	 if(gui) write(idevlog,'(i10,'' candidates selected.'')') ncands	  
	end if
	do n=1,maxcoords
	 nclus(n) = 0
	 kclus(n) = 0
	end do
	nclusters = 0
C*** start main loop which advances each entry
	do 3000 n=1,ncands
C*** start a new cluster, transfer parameter values
	 nclusters = nclusters + 1
	 if(nclusters .gt. maxcoords) then
	  nclusters = nclusters - 1
	  ncands = n - 1
	  write(6,'(''Warning - too many clusters for program'')')
	  if(gui) write(idevlog,'(
     *    ''Warning - too many clusters for program, processing ''
     *    ,i10)') nclusters
	  go to 3100
	 end if
	 kclus(n) = nclusters
	 nclus(nclusters) = 1
	 index(nclusters,1) = n
	 clusminx(nclusters) = candx(n)
	 clusmaxx(nclusters) = candx(n)
	 clusminy(nclusters) = candy(n)
	 clusmaxy(nclusters) = candy(n)
	 if(n .gt. 1) then
	  do i=1,n-1
	   diffx = candx(n) - candx(i)
	   diffy = candy(n) - candy(i)
	   diffsq = diffx * diffx + diffy * diffy
C*** particle n joining cluster for particle i
	   if(diffsq .le. radsq) then
	    nclusters = nclusters - 1
	    kclus(n) = kclus(i)
	    nclus(kclus(i)) = nclus(kclus(i)) + 1
	    iclus = nclus(kclus(i))
	    if(iclus .gt. maxclus) then
	     write(iolabel(1),'(
     *      ''Error - too many entries for cluster'')')
	     call prog_error(1)
	     stop
	    end if
	    jclus = kclus(i)
	    index(jclus,iclus) = n
	    clusminx(jclus) = min(candx(n),clusminx(jclus))
	    clusmaxx(jclus) = max(candx(n),clusmaxx(jclus))
	    clusminy(jclus) = min(candy(n),clusminy(jclus))
	    clusmaxy(jclus) = max(candy(n),clusmaxy(jclus))
	    go to 3000
	   end if
	  end do
	 end if
 3000   continue
 3100   write(6,'(''Found'',i5,'' clusters'')') nclusters
        if(gui) write(idevlog,'(''Found'',i5,'' clusters'')') nclusters
C*****************************************************************
C*** write information and set mapflag to 9 for all non-rejected
C*** cluster members
C*****************************************************************
	do i=1,nclusters
	 write(6,'(''Cluster'',i5,'' : '',i5,'' entries'')')i,nclus(i)
	 write(6,'(''Minx,maxx,miny,maxy : '',4f10.0)')
     *   clusminx(i)*icompress,clusmaxx(i)*icompress,
     *   clusminy(i)*icompress,clusmaxy(i)*icompress
	 if(gui) then
	  write(idevlog,'(
     *    ''Cluster'',i5,'' : '',i5,'' entries'')')i,nclus(i)
	  write(idevlog,'(''Minx,maxx,miny,maxy : '',4f10.0)')
     *    clusminx(i)*icompress,clusmaxx(i)*icompress,
     *    clusminy(i)*icompress,clusmaxy(i)*icompress
	 end if
	 do n=1,nclus(i)
	  indx = index(i,n)
	  write(6,*) indx, candx(indx)*icompress, 
     *               candy(indx)*icompress, candc(indx)
	  if(gui) then
     	   write(idevlog,*) indx, candx(indx)*icompress, 
     *                candy(indx)*icompress, candc(indx)
C*** set flag as particle in cluster, use compressed coords
	   icoordxy = (candy(indx) - 1) * ncompx + candx(indx)
	   mapflag(icoordxy) = 9
	  end if
	 end do
	end do
	minmapflag = ihalfbox * ncompx
	maxmapflag = (ncompy - ihalfbox) * ncompx - ihalfbox
	diameter = radius * 2.0
C*******************************************************************
C*** search each cluster to see if too small or too large - reject if so
C*******************************************************************
	nfound = 0
	iend = 0
	do i=1,nclusters
C*** only select single particle clusters, reject multiple particle clusters
	 if(clusmaxx(i) - clusminx(i) .gt. diameter .or.
     *      clusmaxy(i) - clusminy(i) .gt. diameter) then
	  write(6,'(
     *    ''!!! Cluster'',i5,'' rejected, range too large'',4f8.0)')
     *    i,clusminx(i),clusmaxx(i),clusminy(i),clusmaxy(i)
	  if(gui)
     *    write(idevlog,'(
     *    ''!!! Cluster'',i5,'' rejected, range too large'',4f8.0)')
     *    i,clusminx(i),clusmaxx(i),clusminy(i),clusmaxy(i)
C*** reset all members of cluster to mapflag 10
	  do n=1,nclus(i)
	   indx = index(i,n)
	   icoordxy = (candy(indx) - 1) * ncompx + candx(indx)
	   mapflag(icoordxy) = 10
	  end do
	 else
C**********************************************************************
C*** use ring means and variances to find the best image in the cluster
C**********************************************************************
	  nfound = nfound + 1
	  chimin = bignum
	  ioffset = icompress / 2
	  do k=1,nclus(i)
	   indx = index(i,k)
	   if(candc(indx) .lt. chimin) then
	    chimin = candc(indx)
	    indmin = indx
	   end if
	  end do
C*** apply correction from array origin at (1,1) to map origin at (0,0) 
	  ifoundx(nfound) = candx(indmin) * icompress - ioffset
	  ifoundy(nfound) = candy(indmin) * icompress - ioffset
	  foundc(nfound) = candc(indmin)
	 end if
	end do
C*******************************************************************
C*** check no double particles, reject otherwise
C*******************************************************************
	i = 0
 3200   i = i + 1
	if(i .ge. nfound) go to 3500
	j = i
 3300   j = j + 1
	if(j .gt. nfound) go to 3200
	diffx = float(ifoundx(j) - ifoundx(i))
	diffy = float(ifoundy(j) - ifoundy(i))
	if(diffx * diffx + diffy * diffy .ge. distminsq) go to 3300
C*** found a pair within minimum distance, reject both 
        write(6,'(''Rejecting '',2i5,'' and '',2i5,'' - too close'')')
     *  ifoundx(i),ifoundy(i),ifoundx(j),ifoundy(j)
	if(gui) then
	 idistance = nint(sqrt(diffx * diffx + diffy * diffy))
	 icoordxy = (ifoundy(i)/icompress - 1) * ncompx + 
     *               ifoundx(i)/icompress
	 mapflag(icoordxy) = 12
	 mapval(icoordxy) = idistance
	 icoordxy = (ifoundy(j)/icompress - 1) * ncompx + 
     *               ifoundx(j)/icompress
	 mapflag(icoordxy) = 12
	 mapval(icoordxy) = idistance
         write(idevlog,'(
     *   ''Rejecting '',2i5,'' and '',2i5,'' - too close'')')
     *   ifoundx(i),ifoundy(i),ifoundx(j),ifoundy(j)
	end if
C*** shuffle up coords for partical i
	nfound = nfound - 1
	do n=i,nfound
	 ifoundx(n) = ifoundx(n+1)
	 ifoundy(n) = ifoundy(n+1)
	 foundc(n) = foundc(n+1)
	end do
C*** shuffle up coords for particle j (now j - 1)
	nfound = nfound - 1
	do n=j-1,nfound
	 ifoundx(n) = ifoundx(n+1)
	 ifoundy(n) = ifoundy(n+1)
	 foundc(n) = foundc(n+1)
	end do
	go to 3200
C*******************************************************************
C*** check boxed particles not too close to edge
C*******************************************************************
 3500	if(noutsize .ne. 0) then
	 nouthalf = noutsize / 2
	 i = 0
 3600    i = i + 1
	 if(i .gt. nfound) go to 4000
	 if(ifoundx(i) - nouthalf .lt. 0 .or. 
     *      ifoundx(i) + nouthalf .gt. nx .or.
     *      ifoundy(i) - nouthalf .lt. 0 .or. 
     *      ifoundy(i) + nouthalf .gt. ny) then
C*** shuffle up coords
	  nfound = nfound - 1
	  do n=i,nfound
	   ifoundx(n) = ifoundx(n+1)
	   ifoundy(n) = ifoundy(n+1)
	   foundc(n) = foundc(n+1)
	  end do
	 end if
	 go to 3600
	end if
C*******************************************************************
C*** list of accepted particles complete, print them out
C*******************************************************************
 4000   do i=1,nfound
	 icoordxy = (ifoundy(i) / icompress - 1) * ncompx + 
     *               ifoundx(i) / icompress
	 mapflag(icoordxy) = 0
	end do
	open(unit=idevcoords,file=outfile,status='unknown')
	write(6,'(//''Writing coordinates to file : '',a)')
     *  outfile(1:lnblank(outfile))
	if(gui)
     *	write(idevlog,'(//''Writing coordinates to file : '',a)')
     *  outfile(1:lnblank(outfile))
C*** MRC image format stack
	if(iout .eq. 1) then
	 write(6,'(/''Writing'',2i5,'' x'',i5,
     *   '' sections to output stack file : '',a)') 
     *   nfound,noutsize, noutsize, outstack(1:lnblank(outstack))
	 if(gui) write(idevlog,'(/''Writing'',2i5,'' x'',i5,
     *   '' sections to output stack file : '',a)') 
     *   nfound,noutsize, noutsize, outstack(1:lnblank(outstack))
C*** re-open image file for stack selection
 	 call imopen(idevimage,imagefile,'old')
         call irdhdr(idevimage,nxyz,mxyz,mode,dmin,dmax,dmean)
C*** open temporary output file for stack of selected particles
C*** for display at the end of the program
	 open(unit=idevstack,file=tmpstack,status='unknown',
     *   form='unformatted')
	 lz = -1
	 nouthalf = noutsize / 2
	 dencoord = 0.0
C*** write header for coordinate file
	 write(idevcoords,'(''Coordinate output for '',a)')
     *   imagefile(1:lnblank(imagefile))
	 do i=1,nfound
C*** write coords to coord file
	  write(idevcoords,'(2i10,f12.1)') ifoundx(i),ifoundy(i),dencoord
	  lz = lz + 1
	  iposx = ifoundx(i) - nouthalf
	  iposy = ifoundy(i) - nouthalf
	  iposxend = iposx + noutsize - 1
	  do iy=1,noutsize
C*** position input map, read line of data
	   call imposn(idevimage,0,iposy + iy -1)
	   call irdlin(idevimage,aline)
	   write(idevstack) (aline(ix),ix=iposx,iposxend)
	  end do
	 end do
	 close(idevstack)
	 close(idevcoords)
	 call imclose(idevimage)
C*** MRC format coordinate list 
	else if(iout .eq. 2) then
	 if(gui) write(idevlog,'(
     *   ''Writing '',i5,'' coordinates in MRC format'')') nfound
         write(idevcoords,'(''Coordinate output for '',a)')
     *   imagefile(1:lnblank(imagefile))
 	 den = 0.
	 do i=1,nfound
	  write(idevcoords,'(2i10,f12.1)') ifoundx(i),ifoundy(i),den
	 end do
	 close(idevcoords)
C*** Spider format
	else if(iout .eq. 3) then
	 if(gui) write(idevlog,'(
     *   ''Writing '',i5,'' coordinates in SPIDER format'')') nfound
	 write(idevcoords,'('' ; File output from Sleuth : '',a)')
     *   imagefile(1:lnblank(imagefile))
	 do i=1,nfound
	  fx = float(ifoundx(i))
	  fy = float(ifoundy(i))
	  write(idevcoords,'(i5,i2,2f12.6)') i, 2, fx, fy
	  write(6,'(i5,i2,2f12.6)') i, 2, fx, fy
	 end do
C*** IMAGIC format
        else if(iout .eq. 4) then
	 if(gui) write(idevlog,'(
     *   ''Writing '',i5,'' coordinates in IMAGIC format'')') nfound
	 one = 1.0
	 do i=1,nfound
	  write(idevcoords,*) float(ny - ifoundy(i)), ifoundx(i), one
         end do
        end if
	close(idevcoords)
	if(gui) then
	 write(idevlog,'(//
     *   ''Final list of particle positions ...'')')
	 do i=1,nfound
	  write(idevlog,'(i6,2i10)') i, ifoundx(i), ifoundy(i)
	 end do
	end if
	write(6,'(//''Final list of particle positions ...'')')
	do i=1,nfound
	 write(6,'(i6,2i10)') i, ifoundx(i), ifoundy(i)
	end do
	if(gui) then
         modsdev = .false.
         modparam = .false.
	 firstpass = .false.
 5000	 nlabels = 1
         call ximagechangecursor(idefaultcursor)
	 write(iolabel(nlabels+1),'(
     *   ''Found'',i8,'' particle positions'')') nfound
	 iolabel(nlabels+2) = 
     *   'RADFAC controls size of surrounding background ring'
	 iolabel(nlabels+3) =
     *   '    - increase if weak but well-separated particles missed'
	 iolabel(nlabels+4) = 
     *	 'RATSDEVS controls ratio of particle to background - '
	 iolabel(nlabels+5) = 
     *   '    - increase to include more particles'
	 iolabel(nlabels+6) = 
     *	 'SDEVPAR controls parameter matching'
	 iolabel(nlabels+7) = 
     *   '    - increase to include more particles'
	 iolabel(nlabels+8) = 
     *	 'DISTMIN sets minimum inter-particle distance'
	 iolabel(nlabels+9) = 
     *   '    - increase if particles too close'
	 call ximagelabeldisplay(iolabel,nlabels+9)
	 menulist(1) = '1 Display image with selected coordinates'
	 menulist(2) = '2 Reset RADIUS'
	 menulist(3) = '3 Reset RADFAC'
	 menulist(4) = '4 Reset RATSDEVS'
	 menulist(5) = '5 Reset SDEVPAR'
	 menulist(6) = '6 Reset DISTMIN'
	 menulist(7) = '7 Re-process data'
	 menulist(8) = '8 Save control file'
	 if(iout .eq. 1) then
	  nitems = 10
	  menulist(9) = '9 Display stack of boxed particles'
	  menulist(10) = 'a Quit'
	 else
	  nitems = 9
	  menulist(9) = '9 Quit'
	 end if
	 call ximagemenuinit(menulist,nitems)
	 job = -1
 5500    call ximagewait(job)
	 if(job .le. 0) then
	  go to 5500
C*** display images with particle positions
	 else if(job .eq. 1) then
	  call ximagemenuhide
	  call ximagelabelhide
	  nlabels = nlabels + 1
	  tmin = bignum
	  tmax = -tmin
	  do i=1,ncompx * ncompy
	   den = mapstore(i)
	   tmin = min(tmin,den)
	   tmax = max(tmax,den)
	  end do
	  scl = grey / (tmax - tmin)
	  do i=1,ncompx * ncompy
	   mapout(i) = min(grey, max(1.,(mapstore(i) - tmin) * scl))
	  end do
	  call display_image(ncompx,ncompy,mapout)
	  displaymap = .true.
	  if(ierr .ne. 0) then
	   write(6,'(''Error displaying overlaid image'')')
	   write(idevlog,'(''Error displaying overlaid image'')')
	   stop
	  end if
C*** mark particle positions
	  do i=1,nfound
	   ixpos = ifoundx(i) / icompress
	   iypos = ifoundy(i) / icompress
	   call ximagedrawcircle(ixpos,iypos,icrad)
	  end do
	  call colour_contrast
	  ndisplay = 0
	  nlabs = 0
	  iolabel(nlabels+1) = 'Mark cursor position with left '//
     *    'mouse button to display coordinates'
	  call ximagelabeldisplay(iolabel,nlabels+1)
	  menulist(1) = '1 Quit coordinate display'
	  call ximagemenuinit(menulist,1)
	  iopt = -1
 5600     call ximagewait(iopt)
	  if(iopt .lt. 0) then
	   go to 5600
C*** display coordinates
	  else if(iopt .eq. 0) then
	   call ximagereadmenupointer(ixscreen,iyscreen)
C*** draw a cross on the screen
	   ix1 = ixscreen - icrad
	   iy1 = iyscreen - icrad
	   ix2 = ixscreen + icrad
	   iy2 = iyscreen + icrad
	   call ximagedrawlines(ix1,iy1,ix2,iy2,1)
	   ix1 = ixscreen - icrad
	   iy1 = iyscreen + icrad
	   ix2 = ixscreen + icrad
	   iy2 = iyscreen - icrad
	   call ximagedrawlines(ix1,iy1,ix2,iy2,1)
	   ixpos = ixscreen * icompress
	   iypos = iyscreen * icompress
	   ndisplay = ndisplay + 1
C*** draw text
	   return_string = ' '
	   return_string = intoch(ndisplay,nchars)
	   call ximagedrawtext(ixscreen+icrad,iyscreen+icrad,return_string)
	   nlabs = nlabs + 1
	   if(nlabs .gt. 24) nlabs = 1
	   icoordxy = (iyscreen - 1) * ncompx + ixscreen
C*** check not too near edge
	   if(icoordxy .lt. minmapflag .or. 
     *        icoordxy .gt. maxmapflag) then
	    write(iolabel(nlabels+nlabs),'(
     *     ''Particle : '',2i6,'' too close to edge'')') ixpos,iypos
C*** write message to iobox
	   else if(mapflag(icoordxy) .eq. 0) then
	    write(iolabel(nlabels+nlabs),'(
     *     ''Particle : '',2i6,'' selected'')') ixpos,iypos
	   else
	    imess = abs(mapflag(icoordxy))
	    imesslen = lnblank(message(imess))
	    write(iolabel(nlabels+nlabs),'(''Pixel : '',2i6,
     *      '' failed : '',a)') ixpos, iypos
	    nlabs = nlabs + 1
	    iolabel(nlabels+nlabs) = '*** '//message(imess)(1:imesslen)
C*** value for RATSDEVS
	    if(imess .eq. 2) then
	     nlabs = nlabs + 1
	     resetvalue = float(mapval(icoordxy)) / scalefac
	     write(iolabel(nlabels+nlabs),
     *       '(''RATSDEVS must exceed'',f5.1)')resetvalue
C*** value for SDEVPAR
	    else if(imess .eq. 5 .or. imess .eq. 6) then
	     nlabs = nlabs + 1
	     resetvalue = float(mapval(icoordxy)) / scalefac
	     write(iolabel(nlabels+nlabs),
     *       '(''SDEVPAR must exceed'',f5.1)')resetvalue
C*** value for DISTMIN
	    else if(imess .eq. 11 .or. imess .eq. 12) then
	     nlabs = nlabs + 1
	     resetvalue = float(mapval(icoordxy)) - 1.0
	     write(iolabel(nlabels+nlabs),
     *       '(''DISTMIN must not exceed'',f5.1)')resetvalue
	    end if
	   end if
	   call ximagelabeldisplay(iolabel,nlabels+nlabs)
	   go to 5600
C*** stop displaying coordinates
	  else
	   call ximagelabelhide
	   call ximagemenuhide
	  end if
	  go to 5000
C*** reset radius
	 else if(job .eq. 2) then
	  call ximagemenuhide
	  call ximagelabelhide
	  modparam = .true.
 5610	  write(iolabel(nlabels+1),'(''Reset value for RADIUS :'',f5.1)')
     *    rad
	  return_string = ' '
	  call ximageioboxdisplay(iolabel,return_string,nlabels+1)
	  if(return_string .ne. ' ') then 
	   call check_real(rad,return_string)
	   if(ierr .ne. 0) go to 5610
	  end if
	  write(idevlog,'(''### radius reset to :'',f10.1)') rad
	  radius = rad / fcompress
C*** check radius within limits with the compression
	  if(radius .gt. radius_max) then
	   nlabels = 2
	   iolabel(nlabels) = 'Radius too large for program'
	   go to 5610
	  else if(radius .lt. radmin) then
	   nlabels = 2
	   iolabel(nlabels) = 'Radius too small for program'
	   go to 5610
	  end if
	  nlabels = 1
	  go to 5000
C*** reset radfac
	 else if(job .eq. 3) then
	  call ximagemenuhide
	  call ximagelabelhide
	  modparam = .true.
 5620	  write(iolabel(nlabels+1),'(''Reset value for RADFAC :'',f5.1)')
     *    radfac
	  return_string = ' '
	  call ximageioboxdisplay(iolabel,return_string,nlabels+1)
	  if(return_string .ne. ' ') then 
	   call check_real(radfac,return_string)
	   if(ierr .ne. 0) go to 5620
	  end if
	  write(idevlog,'(''### radfac reset to :'',f10.1)') radfac
	  go to 5000
C*** reset ratsdevs
	 else if(job .eq. 4) then
	  call ximagemenuhide
	  call ximagelabelhide
	  modparam = .true.
 5630	  write(iolabel(nlabels+1),
     *    '(''Reset value for RATSDEVS :'',f5.1)') ratsdevs
	  return_string = ' '
	  call ximageioboxdisplay(iolabel,return_string,nlabels+1)
	  if(return_string .ne. ' ') then 
	   call check_real(ratsdevs,return_string)
	   if(ierr .ne. 0) go to 5630
	  end if
	  write(idevlog,'(''### ratsdevs reset to :'',f10.1)') ratsdevs
	  go to 5000
C*** reset sdevpar
	 else if(job .eq. 5) then
	  call ximagemenuhide
	  call ximagelabelhide
	  modsdev = .true.
 5640	  write(iolabel(nlabels+1),'(''Reset value for SDEVPAR :'',f5.1)')
     *    sdevpar
	  return_string = ' '
	  call ximageioboxdisplay(iolabel,return_string,nlabels+1)
	  if(return_string .ne. ' ') then 
	   call check_real(sdevpar,return_string)
	   if(ierr .ne. 0) go to 5640
	  end if
	  write(idevlog,'(''### sdevpar reset to :'',f10.1)') sdevpar
	  go to 5000
C*** reset distmin
	 else if(job .eq. 6) then
	  call ximagemenuhide
	  call ximagelabelhide
	  modparam = .true.
 5650	  write(iolabel(nlabels+1),'(''Reset value for DISTMIN :'',f5.1)')
     *    distmin
	  return_string = ' '
	  call ximageioboxdisplay(iolabel,return_string,nlabels+1)
	  if(return_string .ne. ' ') then
	   call check_real(distmin,return_string)
	   if(ierr .ne. 0) go to 5650
	  end if
	  write(idevlog,'(''### distmin reset to :'',f10.1)') distmin
	  go to 5000
C*** reprocess data
	 else if(job .eq. 7) then
	  nlabels = 1
	  call ximagemenuhide
	  call ximagelabelhide
	  if(displaymap) then
	   call ximageremovevectors
	   displaymap = .false.
	  end if
	  if(modparam) then
	   iolabel(nlabels+1) = 
     *     'Reprocessing references and map with modified values...'
	   call ximagelabeldisplay(iolabel,nlabels+1)
	   write(idevlog,'(''### reprocessing references and map'')')
	   go to 200
	  else if(modsdev) then
	   sdevsm = sdevpar * sdevm
	   sdevsr = sdevpar * sdevr
	   iolabel(nlabels+1) = 
     *     'Searching for particles with modified sdevpar ...'
	   call ximagelabeldisplay(iolabel,nlabels+1)
	   write(idevlog,'(
     *     ''### searching for particles with modified sdevpar'')')
	   go to 1930
	  end if
	  go to 5000
C*** save control file
	 else if(job .eq. 8) then
	  nlabels = 1
	  call ximagemenuhide
	  call ximagelabelhide
	  iolabel(nlabels+1) = 'Type in file name ...'
	  controlfile = ' '
	  call ximageioboxdisplay(iolabel,controlfile,nlabels+1)
	  open(unit=idevscript,file=controlfile,status='unknown')
	  write(idevlog,'(''### saving control file : '',a)')
     *    controlfile(1:lnblank(controlfile))
	  write(idevscript,'(
     *    ''#!/bin/csh''/''#''/''sleuth.exe << eot'')')
	  if(paramfile .ne. ' ') then
	   ioutref = 2 
	   write(idevscript,'(i5)') ioutref
	   write(idevscript,'(a)') paramfile(1:lnblank(paramfile))
	  else
	   ioutref = 1
	   write(idevscript,*) radius,radfac,distmin,ratsdevs
	   write(idevscript,*) invert,icompress,npixlow,itestrefs
	   write(idevscript,'(a)') stackfile(1:lnblank(stackfile))  
	   write(idevscript,'(a)')
	  end if 
	  write(idevscript,'(a)') imagefile(1:lnblank(imagefile))
	  write(idevscript,*) micsave,iadjacent
	  write(idevscript,*) histedge*100.,histcut*100.,sdevpar
C*** provide only coordinate file 
	  write(idevscript,'(i5)') max(2,iout)
	  write(idevscript,'(a)') outfile(1:lnblank(outfile))
	  write(idevscript,'(''eot'')')
	  close(idevscript)
	  go to 5000
C*** Display stack
	 else if(job .eq. 9) then
	  if(nitems .eq. 9) then
	   call ximagemenuhide
	   call ximagelabelhide
	   call ximageremovevectors
	   stop
	  end if
C*** display mrc stack
	  call ximagemenuhide
	  call ximagelabelhide
	  call display_stack(idevstack,noutsize,noutsize,nfound,tmpstack)
	  if(ierr .ne. 0) then
	   iolabel(1) = 'Error displaying output stack'
	   call prog_error(1)
	   stop
	  end if
	  cmin = 0.
	  cmax = grey
	  call colour_contrast
	  go to 5000
C*** must be quit
	 else
C*** write output stack of selected particles
	  if(iout .eq. 1) then
C*** re-open image file for stack selection
 	   call imopen(idevimage,imagefile,'old')
           call irdhdr(idevimage,nxyz,mxyz,mode,dmin,dmax,dmean)
	   call imopen(idevstackout,outstack,'unknown')
	   lxyz(1) = noutsize
	   lxyz(2) = noutsize
	   lxyz(3) = nfound
	   call icrhdr(idevstackout,lxyz,lxyz,mode,label,-1)
	   amin = bignum
	   amax = -bignum
	   amean = 0.
	   do i=1,nfound
	    iposx = ifoundx(i) - nouthalf
	    iposy = ifoundy(i) - nouthalf
	    do iy=1,noutsize
C*** position input map, read line of data
	     call imposn(idevimage,0,iposy + iy -1)
	     call irdlin(idevimage,aline)
 	     do ix = 1,noutsize
	      den = aline(iposx + ix)
	      amin = min(amin,den)
	      amax = max(amax,den)
	      amean = amean + den
	     end do
C*** position output map, write line of data
	     call imposn(idevstackout,i-1,iy - 1)
	     call iwrlin(idevstackout,aline(iposx + 1))
	    end do
	   end do
	   amean = amean / float(noutsize * noutsize)
	   write(label,'(''Particle stack selected by SLEUTH from '',a)')
     *     imagefile(1:lnblank(imagefile))
           call iwrhdr(idevstackout,label,0,amin,amax,amean)
	   close(idevstack)
	   close(idevrefout)
	   call imclose(idevrefin)
	   call imclose(idevstackout)
	   call imclose(idevimage)
	  end if
	  call ximagemenuhide
	  call ximagelabelhide
	  write(idevlog,'(''Normal termination'')')
	  stop
	 end if
C*************************************************************
C*** Command line mode, return to see if more files to process
C*************************************************************
	else
C*** re-open param file and read values for next file
         open(unit=idevparam,file=paramfile,status='old')
         read(idevparam,*) icompress,nbox,nxybox,
     *   nrad,rad,radsq,radmax,distminsq,
     *   centvarmin,centvarmax,chisectmax,
     *   refratm,sdevd,ratsdevs,refmass,sdevm,refradg,sdevr,
     *   chiavgmin,chiavgmax,chivarmin,chivarmax,
     *   reverse,ipixlow,ipixhigh,nrings
	 do n=1,nrings
	  read(idevparam,*) refavg(n), refvar(n)
	 end do
	 read(idevparam,*) indmaxy, indminy
	 do i=1,indmaxy
	  read(idevparam,*) indexmax(i,1), indexmax(i,2)
	 end do
	 do i=1,indminy
	  read(idevparam,*) indexmin(i,1), indexmin(i,2)
	 end do
	 read(idevparam,*) sectref
         close(idevparam)
	 radius = rad
	 go to 1820
	end if
 9000   write(6,'(''All files processed, normal termination.'')')
	stop
 9500   write(6,'(''!! Error reading file'')')
	stop
	end
C*******************************************************************
C***
	subroutine calcparams(mxy,boxbuf,maskbuf)
C***
C*******************************************************************
C*** subroutine to iterate centre calculation and calculate radius 
C*** of gyration 
C*** final centre calculation used to window input box correctly
C*** note : box centre is taken between pixels i.e. halfbox + 0.5
C***
	include 'sleuth.inc'
	real*4		boxbuf(*)
	real*4		maskbuf(*)
        real*4          scalebuf(maxbox)
C***    
	ierr = 0
	fmy = float(mxy)
C*** save centre
        cenx = xcentre
        ceny = ycentre
	xcen = xcentre
	ycen = ycentre
        iter = 0
C*** copy unscaled box array into new array for scaling
	do ixy=1,mxy * mxy
	 scalebuf(ixy) = boxbuf(ixy)
	end do
C*** calculate box mean and sdev
	call denstats(mxy,mxy,2,scalebuf)
C*** threshold densities between mean +/- 3.0 * sdev
	call denthresh(mxy,mxy,scalebuf)
C*** mask the particle around the centre and taper densities to mask edge
 200	call calctaper(mxy,scalebuf,maskbuf)
C*** threshold the densities and calculate the centre
C*** calculate min, max, mean sdev
	call denstats(mxy,mxy,2,maskbuf)
	call calcentre(mxy,maskbuf)
C*** test centre not too far out
	if(cenx + radius .gt. fmy .or. cenx - radius .lt. 0.0 .or.
     *     ceny + radius .gt. fmy .or. ceny - radius .lt. 0.0) then
	 ierr = 1
	 return
	end if
        iter = iter + 1
        xdiff = abs(xcen - cenx)
        ydiff = abs(ycen - ceny)
        if(iter .ge. maxiters) then
         write(6,'(''Warning - >'',i3,
     *   '' iterations required for centre calculation'')')
     *   maxiters
	 if(gui) write(idevlog,'(''Warning - >'',i3,
     *    '' iterations required for centre calculation'')')
     *    maxiters
         go to 300
        end if
        if(xdiff .gt. threshmin .or. ydiff .gt. threshmin) then
         xcen = cenx
         ycen = ceny
         go to 200
        end if
C***********************************************************
C*** calculate radius of gyration and mass within the mask
C***********************************************************
  300   call calcradg(mxy,maskbuf)
C*** calculate ring mean and variance
C*** add 1.0 to centres to calculate offsets for array elements
	call ringparams(mxy,maskbuf)
	if(ierr .eq. 1) return
C*** calculate sector sums
	call denstats(mxy,mxy,3,maskbuf)
	call denclose(mxy,mxy,maskbuf)
	call sectparams(mxy,maskbuf)
C***********************************************************
C*** rewrite maskbuf from shifted unscaled input array boxbuf
C***********************************************************
        do ixy=1,mxy * mxy
	 maskbuf(ixy) = denmin
	end do
        ixstart1 = max(1, nint(cenx - xcentre))
        ixend1   = min(mxy,nint(cenx + xcentre))
        iystart1 = max(1, nint(ceny - ycentre))
        iyend1   = min(mxy,nint(ceny + ycentre))
        ixstart2 = max(1,nint(xcentre - cenx))
        iystart2 = max(1,nint(ycentre - ceny)) - 1
        do iy=iystart1,iyend1
         nxy = (iy - 1) * mxy
         jxy = (iystart2 + (iy - iystart1)) * mxy
         do ix=ixstart1,ixend1
          ixy = nxy + ix
          den = boxbuf(ixy)
          kxy = jxy + ixstart2 + ix - ixstart1
          maskbuf(kxy) = den
         end do
        end do
	return
	end
C*******************************************************************
C***
        subroutine calcradg(mxy,maskbuf)
C***
C*******************************************************************
C*** subroutine to calculate radius of gyration from a 1D array
C***
	include 'sleuth.inc'
	real*4		maskbuf(*)
C*** calculate radius of gyration about centre if sum of densities
C*** > 0.0
C*** first calculate mass
	densum = 0.
	do ixy=1,mxy * mxy
	 densum = densum + maskbuf(ixy)
	end do
	radgyr = 0.
	if(densum .gt. 0.) then
         do iy=1,mxy
          nxy = mxy * (iy - 1)
          fy = float(iy) - ceny
          fysq = fy * fy
          do ix=1,mxy
           den = maskbuf(nxy + ix)
           if(den .ge. threshold) then
            den = den - threshold
            fx = float(ix) - cenx
            fxsq = fx * fx
            radgyr = radgyr + den * (fxsq + fysq)
           end if
          end do
         end do
         radgyr = sqrt(radgyr / densum)
	end if
        return
        end
C*******************************************************************
C***
	subroutine calctaper(mxy,transbuf,maskbuf)
C***
C********************************************************************
C*** taper densities from centre to edge - weight 1 at centre to 
C*** very small value at edge, using cosine to decrease densities
C********************************************************************
	include 'sleuth.inc'
	real*4		transbuf(*)
	real*4		maskbuf(*)
C*** initialize output array
	do nxy=1,mxy*mxy
	 maskbuf(nxy) = 0.
	end do
	icenx = int(cenx)
	ncenx1 = nint(cenx + 0.5)
	ncenx2 = nint(cenx - 0.5)
	nradmax = nint(radmax)
C*** calculate start and end points in y for circular mask
	iystart = max(1,  nint(ceny + 0.5) - nradmax)
	iyend   = min(mxy,nint(ceny - 0.5) + nradmax)
        do iy=iystart,iyend
         nxy = (iy - 1) * mxy 
         ydist = ceny - float(iy)
	 ydistsq = ydist * ydist
         diffsq = radmaxsq - ydistsq
C*** calculate start and end points in x for circular mask
C*** check for sqrt zero
	 if(diffsq .gt. 0.0) then
	  nxdiff = nint(sqrt(diffsq))
          ixstart = max(1,  ncenx1 - nxdiff)
	  ixend   = min(mxy,ncenx2 + nxdiff)
         else
          ixstart = icenx
          ixend   = icenx + 1
         end if
C*** exponential tapering
         do ix=ixstart,ixend
          xdist = cenx - float(ix)
          xdistsq = xdist * xdist
          rdist = sqrt(xdistsq + ydistsq)
          ixy = nxy + ix
          den = transbuf(ixy)
	  if(rdist .le. radmax) then
           raddist = radmax - rdist
           weight = 1. - taperfac2 *
     *     exp(-taperfac1 * raddist * raddist / radmaxsq)
           maskbuf(ixy) = den * weight
	  else
	   maskbuf(ixy) = denmin
	  end if
         end do
	end do
	return
	end
C*******************************************************************
C***
	subroutine calcentre(mxy,maskbuf)
C***
C*******************************************************************
C*** subroutine to threshold density array at mean+sdev
C*** and calculate density centre
	include 'sleuth.inc'
	real*4		maskbuf(*)
	ixmin = mxy
	ixmax = 0
	iymin = mxy
	iymax = 0
	do iy=1,mxy
	 nxy = mxy * (iy - 1)
	 do ix=1,mxy
	  den = maskbuf(nxy + ix)
	  if(den .ge. threshold) then
	   ixmin = min(ixmin,ix)
	   ixmax = max(ixmax,ix)
	   iymin = min(iymin,iy)
	   iymax = max(iymax,iy)
	  end if
	 end do
	end do
	cenx = float(ixmin) + float(ixmax - ixmin) * 0.5
	ceny = float(iymin) + float(iymax - iymin) * 0.5
	return
	end
C******************************************************************
C***
	subroutine check_integer(ival,return_string)
C***
C******************************************************************
C*** subroutine to check integer in string
	include 'sleuth.inc'
	ierr = 0
	nchars = lnblank(return_string)
C*** test to see if character typed in
	do n=1,nchars
	 if(return_string(n:n).ge.'a' .and.
     *      return_string(n:n).le.'z' .or.
     *	    return_string(n:n).ge.'A' .and.
     *      return_string(n:n).le.'Z') then
	  ierr = 1
	  return
	 end if
	end do
	read(return_string,*,err=100,end=100) ival
	return
  100   ierr = 1
	return
	end
C******************************************************************
C***
	subroutine check_real(val,return_string)
C***
C******************************************************************
C*** subroutine to check integer in string
	include 'sleuth.inc'
	ierr = 0
	nchars = lnblank(return_string)
C*** test to see if character typed in
	do n=1,nchars
	 if(return_string(n:n).ge.'a' .and.
     *      return_string(n:n).le.'z' .or.
     *	    return_string(n:n).ge.'A' .and.
     *      return_string(n:n).le.'Z') then
	  ierr = 1
	  return
	 end if
	end do
	read(return_string,*,err=100,end=100) val
	return
  100   ierr = 1
	return
	end
C******************************************************************
C***
	subroutine colour_contrast
C***
C******************************************************************
C*** subroutine to put up slider bar to adjust contrast for b/w
C***
	include 'sleuth.inc'
	red(min_den) = min_den
	blue(min_den) = min_den
	green(min_den) = min_den
	do i=min_den+1,max_den
	 irgb = min(max_colour,nint(colourscale * float(i)))
	 red(i) = irgb
	 green(i) = irgb
	 blue(i) = irgb
	end do
	call ximagesetcolourtable(min_den,max_den,red,green,blue)
C*** set up slider bar for resetting limits
	write(iolabel(nlabels+1),'(''min, max ='',2f8.1)') 
     *  cmin, cmax
	iolabel(nlabels+2) = 
     *  'Use left and right sliders to set lower and upper thresholds'
	call ximagelabeldisplay(iolabel,nlabels+2)
	arange = cmax - cmin
	min_thresh = min_den + 1
	max_thresh = max_den
	bmin = max(cmin, cmin + arange * float(min_thresh) / grey)
	bmax = min(cmax, cmin + arange * float(max_thresh) / grey)
	percent1 = 0.0
	percent2 = 100.0
	call ximagesliderinit(2,0.0,0.95)
	call ximagecolourbarinit
	iopt = -1
  200   call ximagewait(iopt)
	if(iopt .lt. 0) then
	 go to 200
C*** stop reading slider, return main menu
	else if(iopt .eq. 0) then
	 call ximagelabelhide
         call ximagecolourbarhide
         call ximagesliderhide
	 return
C*** reset lower threshold
        else if(iopt .eq. 101) then
	 call ximagesliderread(percent1)
	 min_thresh = max(min_den+1,
     *   min(nint(percent1 * grey * 0.01),max_thresh - 1))
	 scale = fmaxcolour / float(max_thresh - min_thresh + 1)
C*** set to min_den up to threshold
	 do i=min_den+1,max(min_den+1,min_thresh - 1)
	  red(i) = min_den
	  green(i) = min_den
	  blue(i) = min_den
	 end do
C*** set from min to max threshold
         do i=min_thresh,max_thresh
	  icolour = nint(scale * float(i - min_thresh))
	  red(i) = icolour
	  green(i) = icolour
	  blue(i) = icolour
	 end do
	 bmin = max(cmin, cmin + arange * float(min_thresh) / grey)
	 call ximagesetcolourtable(min_den,max_den,red,green,blue)
C*** reset upper threshold
	else if(iopt .eq. 102) then
	 call ximagesliderread(percent2)
	 max_thresh = max(nint(percent2 * grey * 0.01),min_thresh + 1)
	 scale = fmaxcolour / float(max_thresh - min_thresh + 1)
C*** set to from min_thresh to max_thresh
        do i=min_thresh,max_thresh
	  icolour = nint(scale * float(i - min_thresh))
	  red(i) = icolour
	  green(i) = icolour
	  blue(i) = icolour
	 end do
C*** set above upper threshold to max
	 do i=min(max_den,max_thresh+1),max_den
	  red(i) = max_colour
	  green(i) = max_colour
	  blue(i) = max_colour
	 end do
	 bmax = min(cmax, cmin + arange * float(max_thresh) / grey)
	 call ximagesetcolourtable(min_den,max_den,red,green,blue)
	end if
	go to 200
	return
	end
C*****************************************************************
C***
        subroutine colour_table
C***
C*****************************************************************
C*** subroutine to set up colour table
        include 'sleuth.inc'
C***
C***
C***
        fmaxcolour = float(maxcolour)
        frange = float(max_den - min_den)
        colourscale = (fmaxcolour + 1.0) / frange
        do i=min_den+1,max_den
         irgb = min(maxcolour,nint(colourscale * float(i)))
         red(i) = irgb
         green(i) = irgb
         blue(i) = irgb
        end do
        call ximagesetcolourtable(min_den,max_den,red,green,blue)
        return
        end
C**********************************************************************
C***
	subroutine denclose(mx,my,maskbuf)
C***
C**********************************************************************
C*** subroutine to perform binary close operation on thresholded image
C***
	include 'sleuth.inc'
        real*4          maskbuf(*)
        real*4          storebuf(maxbox)
        real*4          transbuf(maxbox)
C*** threshold the image
	mxy = mx * my
	do ixy=1,mxy
	 if(maskbuf(ixy) .gt. centmean) then
	  transbuf(ixy) = 1.
	 else
	  transbuf(ixy) = 0.
	 end if
	end do
	call denerode(mx,my,1.,transbuf,storebuf)
	call denerode(mx,my,1.,storebuf,transbuf)
	call denerode(mx,my,1.,transbuf,storebuf)
	call denerode(mx,my,0.,storebuf,transbuf)
	call denerode(mx,my,0.,transbuf,storebuf)
	call denerode(mx,my,0.,storebuf,maskbuf)
	return
	end
C*******************************************************************
C***      
        subroutine denclean(mx,my,mapbuf)
C***
C*******************************************************************
C*** subroutine to clean image of noise by pixel averageing :
C*******************************************************************
        include 'sleuth.inc'
C***
	real*4		mapbuf(*)
	real*4		pixels(maxpix)
	real*4		transbuf(maxsize)
C***
C*** store array, initialize variables
        do ixy=1,mx * my
	 den = mapbuf(ixy)
         transbuf(ixy) = den
        end do
C*************************************************************
C*** rectangular low pass spatial filtering by local averaging
C*************************************************************
        do iy = 1, my
         iystart = max(1,iy - ipixlow)
         iyend   = min(my,iy + ipixlow)
         iycent = mx * (iy - 1)
         do 300 ix = 1, mx
          ixstart = max(1,ix - ipixlow)
          ixend   = min(mx,ix + ipixlow)
          ixycent = iycent + ix
          denav = 0.
	  fpix = 0.
          do irow = iystart, iyend
           nrow = (irow - 1) * mx
           do icol = ixstart, ixend
            ncolrow = nrow + icol
            denav = denav + transbuf(ncolrow)
	    fpix = fpix + 1.0
           end do
          end do
C*** calculate average
	  den = denav / fpix
          mapbuf(ixycent) = den
  300    continue
        end do
C***********************************************************
C*** high pass spatial filtering - Gonzalez & Woods p195
C***********************************************************
	tmin = bignum
	tmax = -bignum
        do ixy=1,mx*my
         transbuf(ixy) = mapbuf(ixy)
        end do
        do iy = 1, my
         iystart = max(1,iy - ipixhigh)
         iyend   = min(my,iy + ipixhigh)
         iycent = mx * (iy - 1)
         do 500 ix = 1, mx
          ixstart = max(1,ix - ipixhigh)
          ixend   = min(mx,ix + ipixhigh)
          ixycent = iycent + ix
          denav = 0.
	  fpix = 1.
          do irow = iystart, iyend
           nrow = (irow - 1) * mx
           do icol = ixstart, ixend
            ncolrow = nrow + icol
            den = transbuf(ncolrow)
            if(ncolrow .ne. ixycent) then
             denav = denav + den * -1.
	     fpix = fpix + 1.
C*** save centre density
            else
             density = den
            end if
           end do
          end do
C*** calculate average
          denav = denav + density * (fpix - 1.)
          den = denav / fpix
          mapbuf(ixycent) = den
	  tmax = max(tmax,den)
	  tmin = min(tmin,den)
  500    continue
        end do
C******************************************************
C*** apply square transfer function. Make densities +ve
C******************************************************
	transcale = 1. / (tmax - tmin)
	tempmax = tmax
	tempmin = tmin
	tmax = -bignum
	tmin = bignum
        do ixy=1,mx*my
	 den = mapbuf(ixy)
         den = transcale * (den - tempmin) ** 2.
	 tmin = min(tmin,den)
	 tmax = max(tmax,den)
	 mapbuf(ixy) = den
        end do
        return
        end
C*******************************************************************
C***
	subroutine dencompress(nxin,nyin,mapin,transbuf)
C***
C*******************************************************************
C*** subroutine to compress image
C*******************************************************************
	include 'sleuth.inc'
	real*4		mapin(*)
	real*4		transbuf(*)
C***
	nxout = nxin / icompress
	nyout = nyin / icompress
C*** calculate exact input maxima
	noxin = nxout * icompress
	noyin = nyout * icompress
	ficompsq = float(icompress * icompress)
	do iy=1,noyin,icompress
	 nxy = int((iy - 1) / icompress) * nxout + 1
	 do ix=1,noxin,icompress
	  dens = 0.
	  do ky=1,icompress
	   kxy = (iy + ky - 2) * nxin - 1
	   do kx=1,icompress
	    dens = dens + mapin(kxy + ix + kx)
	   end do
	  end do
	  jx = int((ix - 1) / icompress)
	  transbuf(nxy + jx) = dens / ficompsq
	 end do
	end do
	return
	end
C********************************************************************
C***
	subroutine denenhance(mx,my,transbuf)
C***
C********************************************************************
C*** subroutine to enhance the image by applying histogram cutoffs
C********************************************************************
	include 'sleuth.inc'
	real*4		transbuf(*)
	call denhisto(mx,my,transbuf)
C*** threshold the image, re-calculate tmin,tmax
	tempmin = tmin
	tempmax = tmax
	do ixy=1,mx * my
	 den = max(tempmin,min(tempmax,transbuf(ixy)))
	 transbuf(ixy) = den
	 tmin = min(tmin,den)
	 tmax = max(tmax,den)
	end do
	return
	end
C*******************************************************************
C***
	subroutine denerode(mx,my,fill,transbuf,storebuf)
C***
C*******************************************************************
C*** subroutine to erode (fill=0.) or dilate (fill=1.) image
C*******************************************************************
	include 'sleuth.inc'
        real*4          storebuf(maxbox)
        real*4          transbuf(maxbox)
C*** 
	do ixy=1,mx * my
	 storebuf(ixy) = transbuf(ixy)
	end do
	do iy=1,my
	 iystart = max(1, iy - 1)
	 iyend   = min(my,iy + 1)
	 iycent  = mx * (iy - 1)
	 do 200 ix=1,mx
	  ixstart = max(1,ix - 1)
	  ixend   = min(mx,ix + 1)
	  ixycent = iycent + ix
	  do irow = iystart,iyend
	   nrow = (irow - 1) * mx
	   do icol = ixstart, ixend
	    ncolrow = nrow + icol
	    if(transbuf(ncolrow) .eq. fill) then
	     storebuf(ixycent) = fill
	     go to 200
	    end if
	   end do
	  end do
  200	 continue
	end do
	return
	end
C********************************************************************
C***
	subroutine denfloat(mx,my,transbuf)
C***
C********************************************************************
C*** subroutine to float the image
C********************************************************************
	include 'sleuth.inc'
	real*4		transbuf(*)
	tmax = -bignum
	do ixy=1,mx * my
	 den = max(0.,transbuf(ixy) - threshlow)
	 transbuf(ixy) = den
	 tmax = max(tmax,den)
	end do
        return
        end
C********************************************************************
C***
	subroutine deninvert(mx,my,transbuf)
C***
C********************************************************************
C*** subroutine to invert densities if flag set, make all +ve
C********************************************************************
	include 'sleuth.inc'
	real*4		transbuf(*)
C***
C*** reverse densities
	mxy = mx * my
	if(reverse) then
	 tmin = bignum
	 do ixy=1,mxy
	  den = -transbuf(ixy)
	  transbuf(ixy) = den
	  tmin = min(tmin,den)
	 end do
	else
C*** no reversal, calc min, max
	 tmin = bignum
	 do ixy=1,mxy
	  den = transbuf(ixy)
	  tmin = min(tmin,den)
	 end do
	end if
C*** force all densities +ve
	tmax = -bignum
	do ixy=1,mxy
	 den = (transbuf(ixy) - tmin)
	 transbuf(ixy) = den
	 tmax = max(tmax,den)
	end do
	tmin = 0.
	return
	end
C********************************************************************
C***
	subroutine denhisto(mx,my,transbuf)
C***
C********************************************************************
C*** subroutine to calculate histogram from cleaned image,
C*** return mean and standard deviation from major peak
C********************************************************************
	include 'sleuth.inc'
	INTEGER*4	NAV
	PARAMETER	(NAV = 5)
	INTEGER*4	NAVHALF
	PARAMETER	(NAVHALF = NAV / 2)
C***
	integer*4	ihisto(maxbins)
	INTEGER*4	IHISTAV(MAXBINS)
C***
	real*4		transbuf(*)
	real*4		bins
C***
C******************************************************
C*** calculate histogram
C******************************************************
	nbins = nint((tmax - tmin + 1.) / binsize)
	bins = float(nbins)
	do ibin=1,maxbins
	 ihisto(ibin) = 0
	end do
	do iy=1, my
	 ixy = (iy - 1) * mx
	 do ix=1,mx
	  ibin = int((transbuf(ixy + ix) - tmin) / binsize) + 1
	  ihisto(ibin) = ihisto(ibin) + 1
	 end do
	end do
C*** smooth histogram with averaging
	do n=navhalf+1,nbins-navhalf
	 denav = 0.
	 do i=n-navhalf,n+navhalf
	  denav = denav + ihisto(i)
	 end do
	 ihistav(n) = denav / nav
	end do
	do n=navhalf+1,nbins-navhalf
	 ihisto(n) = ihistav(n)
	end do
C*** find maximum, excluding end 5% which often contain background
	ibinmax = 0
	ibincut = max(1,nint(bins * histedge))
	do nbin=ibincut,nbins-ibincut
	 if(ihisto(nbin) .gt. ibinmax) then
          nbinmax = nbin
	  ibinmax = ihisto(nbin)
	 end if
	end do
	ithresh = nint(float(ibinmax) * histcut)
	tmean = float(nbinmax) * binsize
C**************************************************************
C*** set cutoff to user-specified % of peak - lhs
C**************************************************************
	nbin = nbinmax
  100   nbin = nbin - 1
C*** quit if at end
	if(nbin .le. 1) then
	 nbin = 1
	 go to 200
	end if
	if(ihisto(nbin) .gt. ithresh) go to 100
  200	tmin = float(nbin)
C**************************************************************
C*** rhs of peak if no carbon in image
C**************************************************************
	if(mictype .le. 1) then
	 nbin = nbinmax
  300    nbin = nbin + 1
	 if(nbin .ge. nbins) then
	  nbin = nbins
	  go to 400
	 end if
	 if(ihisto(nbin) .gt. ithresh) go to 300
  400    tmax = float(nbin)
C*** use cutoff from lhs if carbon image
	else
	 tmax = tmean + tmean - tmin
	end if
	end
C********************************************************************
C***      
        subroutine denmajority(mx,my,maskin,maskout,transbuf)
C*** 
C********************************************************************
C*** subroutine to reset spurious pixels using minority black operator
C********************************************************************
        include 'sleuth.inc'
C***
        byte            maskin(*)
        byte	        maskout(*)
        real*4          transbuf(*)
C***
	nmask = 1
C*** remove minor reject flagged areas with majority black operator
        do iy = 1, my
         iystart = max(1,iy - nmask)
         iyend   = min(my,iy + nmask)
         iycent = mx * (iy - 1)
         do ix = 1, mx
          ixstart = max(1,ix - nmask)
          ixend   = min(mx,ix + nmask)
          ixycent = iycent + ix
	  nflags = 0
          do irow = iystart, iyend
           nrow = irow * mx
           do icol = ixstart, ixend
	    if(maskin(nrow + icol) .eq. 1) nflags = nflags + 1
           end do
          end do
C*** set flag to 1 if 4 or more pixels = 1, otherwise to 0
	  if(nflags .ge. 4) then
	   maskout(ixycent) = 1
	   transbuf(ixycent) = tmean
	  else
	   maskout(ixycent) = 0
	  end if
         end do
        end do
	return
	end
C********************************************************************
C***
	subroutine denmask(mx,my,transbuf,maskout)
C***
C********************************************************************
C*** subroutine to calculate output mask
C********************************************************************
	include 'sleuth.inc'
C***
        byte	        maskout(*)
        byte            mask(maxsize)
C***
        real*4          transbuf(*)
	tmin = bignum
	tmax = -bignum
	mxy = mx * my
	do ixy=1,mxy
	  den = transbuf(ixy)
	  tmin = min(tmin,den)
	  tmax = max(tmax,den)
	end do
	call denhisto(mx,my,transbuf)
C*** set up reject mask map
	do ixy=1, mx * my
	 if(transbuf(ixy) .le. tmin) then
	  mask(ixy) = 1
	  transbuf(ixy) = tmean
         else if(transbuf(ixy) .ge. tmax) then
	  mask(ixy) = 1
	  transbuf(ixy) = tmean
	 else
	  mask(ixy) = 0
	 end if
	end do
C*** reset spurious pixels
	call denmajority(mx,my,mask,maskout,transbuf)
C*** iterate
	call denmajority(mx,my,maskout,mask,transbuf)
C*** iterate
	call denmajority(mx,my,mask,maskout,transbuf)
	return
	end
C********************************************************************
C***
	subroutine denscale(mx,my,transbuf)
C***
C********************************************************************
C*** subroutine to scale from 0-255.
C********************************************************************
	include 'sleuth.inc'
	real*4		transbuf(*)
C*** no reversal, calc min, max
	tmax = -bignum
	tmin = bignum
	do ixy=1,mx * my
	 den = transbuf(ixy)
	 tmin = min(tmin,den)
	 tmax = max(tmax,den)
	end do
C*** force all densities +ve and scale from 0 - 255
	scale = denmax / (tmax - tmin)
	do ixy=1,mx * my
	 den = scale * (transbuf(ixy) - tmin)
	 transbuf(ixy) = den
	end do
	tmin = 0.
	tmax = denmax
	return
	end
C*******************************************************************
C***
	subroutine denstats(mx,my,iopt,transbuf)
C***
C*******************************************************************
C*** subroutine to calculate min, max, mean, density sum and sdev 
C*** for whole box, central densities, safety band densities
C*** this subroutine calculates distances fro reference set
C*** opt = 1 mean for whole box
C***     = 2 mean + variance for whole box
C***     = 3 mean for central densities
C***     = 4 mean + variance for central densities
C***     = 5 mean for safety band
C***     = 6 mean + variance for safety band
	include 'sleuth.inc'
	real*4		transbuf(*)
	real*4		storebuf(maxbox)
	ierr = 0
C*******************************************************************
C*** Mean within radmaxsq
C*******************************************************************
	if(iopt .le .2) then
	 boxmin = bignum
	 boxmax = -bignum
	 boxsum = 0.
	 ncent = 0
	 do iy=1,my
	  nxy = mx * (iy - 1)
	  ydist = ceny - float(iy)
	  ydistsq = ydist * ydist
	  do ix=1,mx
	   xdist = cenx - float(ix)
	   xdistsq = xdist * xdist
	   distsq = xdistsq + ydistsq 
C*** pixel density inside particle radius
	   if(distsq .le. radmaxsq) then
	    den = transbuf(nxy + ix)
	    boxsum = boxsum + den
	    ncent = ncent + 1
	    storebuf(ncent) = den
	   end if
	  end do
	 end do
	 if(boxsum .le. 0.0) then
	  ierr = 1
	  return
	 end if
	 fcent = float(ncent)
	 boxmean = boxsum / fcent
C*******************************************************************
C*** calculate whole box standard deviation
C*******************************************************************
	 if(iopt .eq. 2) then
          boxvar = 0.
          do ixy=1,ncent
           den = storebuf(ixy)
           diff = den - boxmean
           boxvar = boxvar + diff * diff
          end do
	  boxvar = boxvar / fcent
          boxsdev = sqrt(boxvar)
	  threshold = boxmean + boxsdev
	 end if
C*******************************************************************
C*** central densities mean
C*******************************************************************
	else if(iopt .le. 4) then
	 centsum = 0.
	 ncent = 0
	 do iy=1,my
	  nxy = mx * (iy - 1)
	  ydist = ceny - float(iy)
	  ydistsq = ydist * ydist
	  do ix=1,mx
	   xdist = cenx - float(ix)
	   xdistsq = xdist * xdist
	   distsq = xdistsq + ydistsq 
C*** pixel density inside particle radius
	   if(distsq .le. radsq) then
	    den = transbuf(nxy + ix)
	    centsum = centsum + den
	    ncent = ncent + 1
	    storebuf(ncent) = den
	   end if
	  end do
	 end do
	 if(centsum .le. 0.0) then
	  ierr = 2
	  return
	 end if
	 fcent = float(ncent)
	 centmean = centsum / fcent
C*******************************************************************
C*** central variance
C*******************************************************************
	 if(iopt .eq. 4) then
	  centvar = 0.
	  do ixy=1,ncent
	   diff = storebuf(ixy) - centmean
	   centvar = centvar + diff * diff
	  end do
	  centvar = centvar / fcent
	 end if
C*******************************************************************
C*** safety band mean
C*******************************************************************
	else if(iopt .le. 6) then
	 bandsum = 0.
	 nband = 0
	 do iy=1,my
	  nxy = mx * (iy - 1)
	  ydist = ceny - float(iy)
	  ydistsq = ydist * ydist
	  do ix=1,mx
	   xdist = cenx - float(ix)
	   xdistsq = xdist * xdist
	   distsq = xdistsq + ydistsq 
C*** pixel density inside particle radius
	   if(distsq .le. radmaxsq .and .distsq .gt. radsq) then
	    den = transbuf(nxy + ix)
	    bandsum = bandsum + den
	    nband = nband + 1
	    storebuf(nband) = den
	   end if
	  end do
	 end do
	 if(bandsum .le. 0.0) then
	  ierr = 3
	  return
	 end if
	 fband = float(nband)
	 bandmean = bandsum / fband
C******************************************************************
C*** band variance
C******************************************************************
	 if(iopt .eq. 6) then
	  bandvar = 0.
	  do ixy=1,nband
	   diff = storebuf(ixy) - bandmean
	   bandvar = bandvar + diff * diff
	  end do
	  bandvar = bandvar / fband
	 end if
	end if
	return
	end
C********************************************************************
C***      
        subroutine denthresh(mx,my,transbuf)
C*** 
C********************************************************************
C*** subroutine to threshold main bulk of densities in range mean +/-
C*** 3.0 * sdev
C********************************************************************
        include 'sleuth.inc'
        real*4          transbuf(*)
C***
        sdevs = boxsdev * defsdevs
        cutmin = boxmean - sdevs
        cutmax = boxmean + sdevs
        dscale = denmax / (cutmax - cutmin)
        do iy=1,my
         nxy = mx * (iy - 1)
         do ix=1,mx
          den = min(denmax,
     *    max(1.,dscale * (transbuf(nxy + ix) - cutmin)))
          transbuf(nxy + ix) = den
         end do
        end do
        return
        end
C*******************************************************************
C***
	subroutine dentouch(mxy,transbuf)
C***
C*******************************************************************
C*** subroutine to calculate ratios for mean, standard deviations
C*** between centred circle of particle radius and band of density
C*** just outside up to radmax in sectors. Inner sector cpmpared
C*** to outer. If they both match, and match particle density, set
C*** ierr=1 for rejection
C*******************************************************************
	include 'sleuth.inc'
        real*4          fedge(maxsectors/2)
        real*4          sectedge(maxsectors/2)
	real*4		transbuf(*)
C***
	ierr = 0
	cutoff = centmean
	do i=1,maxsectors/2
	 sectedge(i) = 0.
	 fedge(i) = 0
	end do
	do iy=1,mxy
	 nxy = mxy * (iy - 1)
	 ydist = ceny - float(iy)
	 ydistsq = ydist * ydist
	 do ix=1,mxy
	  xdist = cenx - float(ix)
	  xdistsq = xdist * xdist
	  distsq = xdistsq + ydistsq 
C*** calculate which sector pixel is in, if inside radmax
	  if(distsq .le. radmaxsq .and. distsq .gt. radsq) then
C*** centre column
	   if(xdist .eq. 0.0) then
	    if(ydist .lt. 0.0) then
	     theta = -pio2
	    else	    
	     theta = pio2
	    end if
C*** centre row
	   else if(ydist .eq. 0.0) then
	    if(xdist .lt. 0.0) then
	     theta = pi
	    else
	     theta = 0.0
	    end if
C*** angle from pixel to centre
	   else
	    theta = atan2(ydist, xdist)
	   end if
	   nsect = int(theta / pio4) + 1
	   if(theta .lt. 0.) nsect = nsect + 7
C*** edge density
	    sectedge(nsect) = sectedge(nsect) + transbuf(nxy + ix)
	    fedge(nsect) = fedge(nsect) + 1.0
	  end if
	 end do
	end do
	do i=1,maxsectors/2
	 ratedge = sectedge(i) / fedge(i)
	 if(ratedge .gt. cutoff) then
	  ierr = 1
	  return
	 end if
	end do 
	return
	end

C*******************************************************************
C***
	  subroutine display_image(numx,numy,mapout)
C***
C*******************************************************************
C*** subroutine to write error messages to text or iobox
C***
	include 'sleuth.inc'
C***
	byte		mapout(maxsize)
C***
	ierr = 0
C*** check image size will fit on screen
	max_display_width = max(max_screen_width, numx)
	max_display_height = max(max_screen_height,numy)
	call ximageclearimage
	call ximagecheckimage
     *  (max_display_width,max_display_height,imap,ierr)
        if(ierr .eq. 1) then
         iolabel(nlabels+1) = 
     *   'Insufficient memory to display this image.'
         iolabel(nlabels+2) = 'Press any key to continue'
	 return_string = ' '
	 call ximageioboxdisplay(iolabel,return_string,nlabels+2)
	 return
	end if
	call ximagedrawimage(numx,numy,imap,mapout,ierr)
	if(ierr .ne. 0) then
	 call ximagemenuhide
	 if(ierr .eq. 1) then
	  iolabel(nlabels+1) = 'Warning - X event error'
	 else if(ierr .eq. 2) then
	  iolabel(nlabels+1) = 'Warning - error reading back pixmap'
	 else if(ierr .eq. 3) then
	  iolabel(nlabels+1) = 'Warning - map all zeroes'
	 end if
	 return_string = ' '
	 call ximageioboxdisplay(iolabel,return_string,nlabels+1)
	 stop
	end if
	return
	end
C*******************************************************************
C***
	  subroutine display_stack(idevdisp,nox,noy,noz,filename)
C***
C*******************************************************************
C*** subroutine to write error messages to text or iobox
C***
	include 'sleuth.inc'
C***
	byte		mapout(maxsize)
	character*512	filename
C****
C*** open reference stack file
	open(unit=idevdisp,file=filename,status='old',form='unformatted')
	do iz=1,noz
	 do iy=1,noy
	  read(idevdisp) (aline(ix),ix=1,nox)
	  do ix=1,nox
	   den = aline(ix)
	   amin = min(amin,den)
	   amax = max(amax,den)
	  end do
	 end do
	end do
	scl = grey / (amax - amin)
	ixcr = nox + iautoinc
	iycr = noy + iautoinc
	numsecx = nint(sqrt(float(noz)))
	numsecy = noz / numsecx + min(1,mod(noz,numsecx))
C*** check image size will fit on screen
	max_display_width = max(max_screen_width, numsecx * nox)
	max_display_height = max(max_screen_height,numsecy * noy)
	call ximageclearimage
	call ximagecheckimage
     *  (max_display_width,max_display_height,imap,ierr)
        if(ierr .eq. 1) then
         iolabel(nlabels+1) = 
     *   'Insufficient memory to display this stack.'
         iolabel(nlabels+2) = 'Press any key to continue'
	 return_string = ' '
	 call ximageioboxdisplay(iolabel,return_string,nlabels+2)
	 write(6,'(''Stack display error'',3i10)')
     *   max_display_width,max_display_height,imap
	 return
	end if
C*** zero map buffer
	do i=1,maxsize
	 mapout(i) = 0
	end do
	icx = nox / 2 + iautoinc
	icy = max_display_height - (noy / 2 + iautoinc)
	icenx = icx
	iceny = icy
C*** read sections from file and load into output buffer
	close(idevdisp)
	open(unit=idevdisp,file=filename,status='old',form='unformatted')
	inum = 0
	do iz=1,noz
	 inum = inum + 1
	 if(inum .gt. numsecx) then
	  inum = 1
	  icenx = icx
	  iceny = iceny - iycr
	 else if(inum .gt. 1) then
	  icenx = icenx + ixcr
	 end if
         ixmin = icenx - nox / 2
         ixmax = ixmin + nox - 1
         iymin = iceny - noy / 2
         iymax = iymin + noy - 1
	 iystart = max_display_height - iymin
	 do iy=1,noy
	  read(idevdisp) (aline(ix),ix=1,nox)
          ny = max_display_width * (iystart - (iy-1))
          do ix = 1,nox
           nx = ixmin + ix - 1
           nxy = ny + nx
           den = (aline(ix) - amin) * scl
           den = min(grey, max(0.,den))
           mapout(nxy) = nint(den)
          end do
	 end do
	end do
	call ximagedrawimage
     *  (max_display_width,max_display_height,imap,mapout,ierr)
	if(ierr .ne. 0) then
	 if(ierr .eq. 1) then
	  iolabel(nlabels+1) = 'Warning - X event error'
	 else if(ierr .eq. 2) then
	  iolabel(nlabels+1) = 'Warning - error reading back pixmap'
	 else if(ierr .eq. 3) then
	  iolabel(nlabels+1) = 'Warning - map all zeroes'
	 end if
	 return_string = ' '
	 call ximageioboxdisplay(iolabel,return_string,nlabels+1)
	 stop
	end if
	close(idevdisp)
	return
	end
C*******************************************************************
C***
	  subroutine prog_error(nlines)
C***
C*******************************************************************
C*** subroutine to write error messages to text or iobox
C***
	include 'sleuth.inc'
	if(gui) then
	 return_string = ' '
	 iolabel(nlines+1) = 'Press <cr> to quit'
	 call ximageioboxdisplay(iolabel,return_string,nlines+1)
	 do i=1,nlines
	  write(idevlog,'(a)') iolabel(i)
	 end do
	else
	 do i=1,nlines
	  write(6,'(a)') iolabel(i)
	 end do
	end if
	return
	end
C*******************************************************************
C***
	  subroutine ringparams(mxy,maskbuf)
C***
C*******************************************************************
C*** subroutine to calculate ring means and variances excluding those
C*** close to the centre
	include 'sleuth.inc'
	real*4		maskbuf(*)
        real*4          ringden(maxdens)
C***
	ierr = 0
C***
	ringmina = bignum
	ringmaxa = -bignum
	ringminv = bignum
	ringmaxv = -bignum
C*** calculate start and end y values
	iceny = int(ceny)
	iystart = iceny - nrad - 1
	iyend   = iceny + nrad + 1
	if(iystart .le. 0 .or. iyend .ge. mxy) then
	 ierr = 1
	 return
	end if
	do n=1,nrings
	 sumavg = 0.
	 navg = 0
C*** first calculate mean for each 'ring'
C*** sample 2 values at dist=radius
	 rad = float(nrad - n + 1)
	 sqrad = rad * rad
	 do iy=iystart+n,iyend-n
	  ydist = ceny - float(iy)
	  diffsq = sqrad - ydist * ydist
	  if(diffsq .gt. 0.0) then
	   xdist = sqrt(diffsq)
	  else
	   xdist = 0.0
	  end if
	  ix1 = nint(cenx - xdist)
	  ix2 = nint(cenx + xdist)
	  ixy = mxy * (iy - 1)
C*** Store densities for normalisation and sdev calculation
	  if(iy .eq. iystart .or. iy .eq. iyend) then
C*** only one density used here
	   density = maskbuf(ixy + ix1)
	   sumavg = sumavg + density
	   navg = navg + 1
	   ringden(navg) =  density
C*** densities at each side of circle
	  else
	   density = maskbuf(ixy + ix1)
	   sumavg = sumavg + density
	   navg = navg + 1
	   ringden(navg) = density
	   density = maskbuf(ixy + ix2)
	   sumavg = sumavg + density
	   navg = navg + 1
	   ringden(navg) =  density
	  end if
	 end do
	 favg = float(navg)
	 ringavg = sumavg / favg
	 ringmina = min(ringmina,ringavg)
	 ringmaxa = max(ringmaxa,ringavg)
	 ringmean(n) = ringavg
C*** calculate variance for each 'ring'
	 sumvar = 0.
	 do iy=1,navg
	  diff = ringden(iy) - ringavg
	  diffsq = diff * diff
	  ringminv = min(ringminv,diffsq)
	  ringmaxv = max(ringmaxv,diffsq)
	  sumvar = sumvar + diffsq
	 end do
	 ringsdev(n) = sqrt(sumvar / favg)
	end do
C*** scale ringavgs between 0 and denmax
	ringscalea = denmax / (ringmaxa - ringmina + 1.0)
	ringscalev = denmax / (ringmaxv - ringminv + 1.0)
	do n=1,nrings
	 ringmean(n) = ringscalea * (ringmean(n) - ringmina + 1.0)
	 ringsdev(n) = ringscalev * (ringsdev(n) - ringminv + 1.0)
	end do
	return
	end
C*******************************************************************
C***
	subroutine sectparams(mxy,transbuf)
C***
C*******************************************************************
C*** subroutine to add pixel densities in each of 16 sectors
C*******************************************************************
	include 'sleuth.inc'
C***
	real*4		transbuf(*)
C***
	ierr = 0
	do i=1,maxsectors
	 sectsum(i) = 0.
	end do
	do iy=1,mxy
	 nxy = mxy * (iy - 1)
	 ydist = ceny - float(iy)
	 ydistsq = ydist * ydist
	 do ix=1,mxy
	  xdist = cenx - float(ix)
	  xdistsq = xdist * xdist
	  distsq = xdistsq + ydistsq 
C*** calculate which sector pixel is in, if inside radmax
	  if(distsq .le. radsq) then
C*** centre column
	   if(xdist .eq. 0.0) then
	    if(ydist .lt. 0.0) then
	     theta = -pio2
	    else	    
	     theta = pio2
	    end if
C*** centre row
	   else if(ydist .eq. 0.0) then
	    if(xdist .lt. 0.0) then
	     theta = pi
	    else
	     theta = 0.0
	    end if
C*** angle from pixel to centre
	   else
	    theta = atan2(ydist, xdist)
	   end if
	   nsect = int(theta / pio8) + 1
	   if(theta .lt. 0.) nsect = nsect + 15
           sectsum(nsect) = sectsum(nsect) + transbuf(nxy + ix)
	  end if
	 end do
	end do
	return
	end
C******************************************************************
C***
	subroutine sectalign(sectref,sectest)
C***
C******************************************************************
C*** subroutine to align sector sums by simple least squares
	include 'sleuth.inc'
	real*4		sectref(*)
	real*4		sectest(*)
C***
	nstart = -1
	sectmin = bignum
	do nstartref=1,maxsectors
	 nsectref = nstartref - 1
	 sum = 0.
	 do i=1,maxsectors
	  nsectref = nsectref + 1
	  if(nsectref .gt. maxsectors) nsectref = 1
	  diff = sectref(nsectref) - sectest(i)
	  sum = sum + diff * diff
	 end do
	 if(sum .lt. sectmin) then
	  sectmin = sum
	  minsect = nstartref
	 end if
	end do
	return
	end
