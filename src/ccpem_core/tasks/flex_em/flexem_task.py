#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
import json
try:
    import modeller
    modeller_available = True
    from ccpem_progs.flex_em import flexem
except ImportError:
    modeller_available = False
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.mrc_map_io_clipper import read_mrc_header
from ccpem_core.tasks.flex_em import flexem_results


class FlexEM(task_utils.CCPEMTask):
    '''
    CCPEM FlexEM task.
    '''

    task_info = task_utils.CCPEMTaskInfo(
        name='Flex-EM',
        author='M. Topf, K. Lasker, B. Webb, H. Wolfson, W. Chiu, A. Sali',
        version='1.0',
        description=(
            'Fitting and refinement of atomic structures guided by cryoEM density'),
        short_description=(
            'Fitting and refinement of atomic structures guided by cryoEM density'),
        documentation_link='http://topf-group.ismb.lon.ac.uk/flex-em/',
        references=None)

    def __init__(self,
                 task_info=task_info,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        command = ['ccpem-python', os.path.realpath(flexem.__file__)]
        super(FlexEM, self).__init__(command=command,
                                     task_info=task_info,
                                     database_path=database_path,
                                     args=args,
                                     args_json=args_json,
                                     pipeline=pipeline,
                                     job_location=job_location,
                                     parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        mode = parser.add_argument_group()
        mode.add_argument(
            '-mode',
            '--mode',
            choices=['MD',
                     'CG'],
            help='Molecular dynamics or conjugate gradient optimisation',
            metavar='Mode',
            type=str,
            default='MD')
        #
        input_pdb = parser.add_argument_group()
        input_pdb.add_argument(
            '-input_pdb',
            '--input_pdb',
            help='Input coordinate file (pdb format)',
            metavar='Input PDB',
            type=str,
            default=None)
        #
        input_map = parser.add_argument_group()
        input_map.add_argument(
            '-input_map',
            '--input_map',
            help='Target input map (mrc format)',
            metavar='Input map',
            type=str,
            default=None)
        #
        map_resolution = parser.add_argument_group()
        map_resolution.add_argument(
            '-reso',
            '--map_resolution',
            help='''Resolution of input map (Angstrom)''',
            metavar='Resolution',
            type=float,
            default=None)
        #
        iterations = parser.add_argument_group()
        iterations.add_argument(
            '-iterations',
            '--iterations',
            help='Number of iterations for MD or number runs for CG',
            metavar='Iterations',
            type=int,
            default=10)
        #
        input_rigid = parser.add_argument_group()
        input_rigid.add_argument(
            '-input_rigid',
            '--input_rigid',
            help='Rigid body file from Ribfind',
            metavar='Rigid body',
            type=str,
            default=None)
        #
        max_atom_disp = parser.add_argument_group()
        max_atom_disp.add_argument(
            '-max_atom_disp',
            '--max_atom_disp',
            help=('Maximum atom displacement cutoff recommended to use 0.39 for '
                  'large rigid bodies, 0.15 for SSE refinement, 0.10 for '
                  'all-atom refinement'),
            metavar='Atom disp',
            type=float,
            default=0.39)
        #
        phi_psi_restraints = parser.add_argument_group()
        phi_psi_restraints.add_argument(
            '-phi_psi_restraints',
            '--phi_psi_restraints',
            help='Use phi psi restraints',
            metavar='Phi-psi restraints',
            type=bool,
            default=True)
        #
        test_mode = parser.add_argument_group()
        test_mode.add_argument(
            '-test_mode',
            '--test_mode',
            help='Runs short protocol.  For unit testing only.',
            metavar='Test mode',
            type=bool,
            default=False)


        ####### Refmac Refine Bfactor 
        parser.add_argument(
            '-refine_b',
            '--refine_b',
            help='''Refine Bfactors with Refmac''',
            metavar='Refine Bfactor',
            type=bool,
            default=False)
        # 
        parser.add_argument(
            '-fsc_calc',
            '--fsc_calc',
            help='''Calculate model vs map FSC''',
            metavar='FSC Calc',
            type=bool,
            default=False)

        ####### Half map validation
        parser.add_argument(
            '-validate',
            '--validate',
            help='''Perform validation''',
            metavar='Validate',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-half1',
            '--half_map_1',
            help=('Half map 1 of input map, required for cross validation '
                  'post refinement (mrc format)'),
            metavar='Half map 1',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-half2',
            '--half_map_2',
            help=('Half map 2 of input map, required for cross validation '
                  'post refinement (mrc format)'),
            type=str,
            metavar='Half map 1',
            default=None)
        
        return parser

    def validate_args(self):
        '''
        Check arguments before running job.
        '''
        args_correct = True
        warnings = ''
        # Check input PDB
        warnings += self.set_arg_absolute_path(
            arg=self.args.input_pdb)
        warnings += self.set_arg_absolute_path(
            arg=self.args.input_map)
        warnings += self.set_arg_absolute_path(
            arg=self.args.input_rigid)
        # Get map data, check is cubic
        if self.args.input_map.value is not None:
            if os.path.exists(self.args.input_map.value):
                self.map_data = read_mrc_header.MRCMapHeaderData(
                    self.args.input_map.value)
            if self.map_data.is_map_file:
                if not self.validate_cubic_map():
                    # Raise error if map not cubic
                    xyz = self.map_data.mx_my_mz
                    text = ('FlexEM requires cubic maps (i.e. mx = my = mz).\n'
                            'Input map grid dimensions\n'
                            '    x = {0}\n'
                            '    y = {1}\n'
                            '    z = {2}').format(xyz[0], xyz[1], xyz[2])
                    warnings += text
            else:
                warnings += '\nNot valid input map'
        if self.args.map_resolution.value is None:
            warnings += '\nMap resolution not set'
        # Display warnings in parent GUI
        if warnings != '':
            args_correct = False
            print warnings
        return args_correct

    def validate_cubic_map(self):
        xyz = self.map_data.mx_my_mz
        if xyz[0] == xyz[1] == xyz[2]:
            return True
        else:
            return False

    def job_flexem(self):
        #
        flex_em_json_args = {
            'optimization': self.args.mode.value,
            'input_pdb_file': self.args.input_pdb.value,
            'code': 'xxxx',
            'em_map_file': self.args.input_map.value,
            'map_format': 'mrc',
            'apix': self.map_data.voxx_voxy_voxz[2],  # Voxel size
            'box_size': self.map_data.mx_my_mz[2],
            'resolution': 10,  # Self.args.map_resolution.value,
            'x': self.map_data.origin[0],
            'y': self.map_data.origin[1],
            'z': self.map_data.origin[2],
            'path': self.args.job_location.value,
            'init_dir': 1,
            'num_of_iter': self.args.iterations.value,
            'rigid_filename': self.args.input_rigid.value,
            'job_location': self.args.job_location.value,
            'max_atom_disp': self.args.max_atom_disp.value,
            'phi_psi_restraints': self.args.phi_psi_restraints.value,
            'test_mode': self.args.test_mode.value}
        jsonpath = os.path.join(self.job_data, 'flex_em_args.json')
        json.dump(flex_em_json_args,
                  open(jsonpath, 'w'),
                  indent=4,
                  separators=(',', ': '))
        self.flexem_process = process_manager.CCPEMProcess(
            name='Flex-EM',
            command=self.command,
            args=[jsonpath],
            location=self.job_location,
            stdin=None)

    def run_pipeline(self, job_id=None, db_inject=None):
        # Directory to store intermediate data
        self.job_data = os.path.join(self.job_location, 'job_data')
        ccpem_utils.check_directory_and_make(self.job_data)
        # Generate processes
        # FlexEM
        self.job_flexem()
        pl = [[self.flexem_process]]
        
        # Custom finish
        if self.args.mode.value == 'CG':
            metadata_file = os.path.join(self.args.job_location.value,
                                         '1_CG/metadata.json')
        else:
            metadata_file = os.path.join(self.args.job_location.value,
                                         '1_MD/metadata.json')
        custom_finish = FlexEMResultsOnFinish(
            pipeline_path=self.job_location+'/task.ccpem',
            metadata_file=metadata_file)
        
        if [self.args.refine_b(),
            self.args.validate(),
            self.args.fsc_calc()].count(True) > 0:

            from ccpem_core.tasks.refmac import refmac_task
            # Refmac refine b / fsc
            self.process_maptomtz = refmac_task.RefmacMapToMtz(
                command=self.command,
                resolution=self.args.resolution.value,
                mode='Global',
                name='Map to MTZ',
                job_location=self.job_location,
                map_path=self.args.input_map.value,
                blur_array=None,
                sharp_array=None,
                atomsf_path=refmac_task.Refmac.atomsf)

        # Pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            on_finish_custom=custom_finish,
            title=self.args.job_title.value)
        self.pipeline.start()


class FlexEMResultsOnFinish(process_manager.CCPEMPipelineCustomFinish):
    '''
    Generate RVAPI results on finish.
    '''
    def __init__(self,
                 pipeline_path,
                 metadata_file):
        super(FlexEMResultsOnFinish, self).__init__()
        self.pipeline_path = pipeline_path
        self.metadata_file = metadata_file

    def on_finish(self, parent_pipeline=None):
        flexem_results.FlexEMResultsViewer(
            metadata_file=self.metadata_file,
            pipeline_path=self.pipeline_path)

def main():
    '''
    Run task
    '''
    if modeller_available:
        task_utils.command_line_task_launch(
            task=FlexEM)
    else:
        print ('Modeller required and not available'
               '\nFor installation details please see: '
               '\n    https://salilab.org/modeller/')

if __name__ == '__main__':
    main()
