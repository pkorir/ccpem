#!/usr/bin/env python
# coding: utf-8

import unittest
import numpy as np
from helper_functions.nptomo import MRCfile
import npy2mrc
import os


class TestAnalysisMethods(unittest.TestCase):

    def setUp(self):
        self.test_npy = "unittests/testdata/autocorr_in.npy"
        self.test_npy_array = np.load("unittests/testdata/autocorr_in.npy")

    def tearDown(self):
        os.system("rm unittests/testdata/npy2mrc_test.mrc")

    def test_npy2mrc(self):
        npy2mrc.npy2mrc(self.test_npy, "unittests/testdata/npy2mrc_test.mrc")

        with MRCfile("unittests/testdata/npy2mrc_test.mrc") as test_mrc:
            test_mrc_data = test_mrc.get_all_tom_density_values()

        np.testing.assert_array_equal(self.test_npy_array, test_mrc_data, "Saved mrc file did not match npy array.")

    def test_save_npy_as_mrc(self):
        npy2mrc.save_npy_as_mrc_file("unittests/testdata/npy2mrc_test.mrc", self.test_npy_array)

        with MRCfile("unittests/testdata/npy2mrc_test.mrc") as test_mrc:
            test_mrc_data = test_mrc.get_all_tom_density_values()

        np.testing.assert_array_equal(self.test_npy_array, test_mrc_data, "Saved mrc file did not match npy array.")

if __name__ == "__main__":
        unittest.main()
