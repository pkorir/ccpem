C
C
C                        **  HLXFIT  **
C
C     Program finds the common origin for a "test" and "reference"
C     helical object, and calculates the relative amplitude and
C     radial scales.
C
C     The common origin is determined by minimizing the phase
C     residuals between reference datasets, according to:
C            phiF = sqrt(sum(|Fref|*dphi**2)/sum|Fref|)
C     Program also gives R-factors:
C              Rf = sum(||Fref| - |Ftest||)/sum|Fref|
C
C     Two alternative output files may be generated:
C     (a) new test dataset, oriented and radially scaled to fit
C         the reference set
C     (b) "phase error" dataset, like (a) except that the phases
C         output are the difference phases: phi(r) - phi(t)
C
C     equatorial data are omitted.
C
C
C     CNTFILE        - control data file
C     RDLFILE        - output file listing:
C                      Rf x 1000, phimin, zmin, rscalmin, phiF x 1000
C     NEWFILE        - new test dataset
C
C     Data cards in CNTFILE:
C     HLXTITLE       - image title
C     REFILE         - reference filename
C     NLL            - number of repeats (equivalent to llfact)
C     rep_dis        - repeat distance*NLL (A)
C     DELR1, DELR2   - layer-line sampling intervals (ref., test)
C     ACUT           - amplitude cutoff (% of max. amp. in ref.)
C     LLSPEC(I), WTSPEC(I) 6(I5.F5.1)
C		     - up to 6 special layer-lines and weights
C     TFILE          - test filename
C     ISIDE          - 0 for near, 1 for far side
C     IPOLE          - 1 to rotate test helix by 180 degrees
C     PHIMID         - estimated phi value to search about (degrees)
C     DPHI           - search increment in phi
C     IPL            - no. of steps in phi (max. 8)
C     ZMID           - estimated z value to search about (A)
C     DELZ           - search increment in z
C     IZL            - no. of steps in z
C     RSCMID         - estimated rscal to search about
C     DRSC           - search increment in rscal
C     IRL            - no. of steps in rscal (max. 8)
C     IOUT           - 1 to write out NEWFILE
C
C     Version 1.0 18-Jan-95 NU:Fortran version of hlxfit by CT
C     Version 1.1 17-Nov-95 RB:Automated to find minimum to within
C                 0.01 in phi and z if initial search missed it;
C                 writes out RDLFILE
C     Version 1.12 23-Feb-96 NU:more extended documentation
C     Version 1.15 12-Jul-99 Special weights in array form
C     Version 1.16 05-Dec-00 Bug fixed to declare 'OUT'
C
      CHARACTER*19 TEXT
      CHARACTER*19 TEXT1(1000)
      CHARACTER*50 REFILE,TFILE
      CHARACTER*40 CNTFILE,NEWFILE,ERRFILE,RDLFILE
      CHARACTER*40 HLXTITLE,LLTITLE,LLTITLE1
      CHARACTER*2 NOT
      CHARACTER LINES(73)
      INTEGER*4 OUT
      PARAMETER (IN1=5,OUT=6)
C      DIMENSION WT(1000),NN(1000),NL(1000),WT1(1000),NN1(1000),NL1(1000)
      DIMENSION WT(1000),NN(1000),NL(1000),NN1(1000),NL1(1000)
      DIMENSION A(1000,1000),P(1000,1000),A1(1000,1000),P1(1000,1000)
      DIMENSION A2(1000,1000),P2(1000,1000),RM(10),PM(10),ZM(10),RF(10)
      DIMENSION RES(10,25),PRES(10),ZRES(25),RSC(10)
      DIMENSION WTSPEC(6),LLSPEC(6)
      REAL*8 head(10)
C
      IPOLE=0
      scale=1.0
C
      WRITE(OUT,101)
      READ(IN1,102)CNTFILE
      OPEN(UNIT=7,FILE=CNTFILE,STATUS='OLD')
      READ(IN1,102)RDLFILE
      OPEN(UNIT=8,FILE=RDLFILE,STATUS='UNKNOWN')
      READ(7,103)HLXTITLE
      READ(7,113)REFILE
      READ(7,*)NLL,rep_dis,DELR1,DELR2
      READ(7,*)ACUT
      READ(7,'(6(I5,F5.1))')(LLSPEC(I),WTSPEC(I),I=1,6)
      NLSPEC = 0
      DO I=1,6
       IF(LLSPEC(I) .NE. 0) NLSPEC = I
      END DO
      READ(7,113)TFILE
      READ(7,*)ISIDE,IPOLE
      READ(7,*)PHIMID,DPHI,IPL
         PHIMIN = PHIMID - DPHI*INT(IPL/2)
         PHIMAX = PHIMID + DPHI*INT((IPL-1)/2)
      READ(7,*)ZMID,DELZ,IZL
         ZMIN = ZMID - DELZ*INT(IZL)/2
         ZMAX = ZMID + DELZ*INT((IZL-1)/2)
      READ(7,*)RSCMID,DRSC,IRL
         RSCMIN = RSCMID - DRSC*INT(IRL)/2
         RSCMAX = RSCMID + DRSC*INT((IRL-1)/2)
      READ(7,*)IOUT
c      CALL DOPEN(1,REFILE,'RO','F')
      CALL ccpdpn(1,REFILE,'READONLY','F',0,0)
c      CALL DOPEN(2,TFILE,'RO','F')
      CALL ccpdpn(2,TFILE,'READONLY','F',0,0)
      IF(IOUT.EQ.1) THEN
      WRITE(OUT,105)
      READ(IN1,102)NEWFILE
c      CALL DOPEN(3,NEWFILE,'NEW','F')
      CALL ccpdpn(3,NEWFILE,'NEW','F',0,0)
      ENDIF
      IF(IOUT.EQ.2) THEN
      WRITE(OUT,106)

      READ(IN1,102)ERRFILE
c      CALL DOPEN(4,ERRFILE,'NEW','F')
      CALL ccpdpn(4,ERRFILE,'NEW','F',0,0)
      ENDIF
C
      NOT='--'
      DEG=57.2957795
      DELF=DELR1/DELR2
      CONVD=360./rep_dis
      NRMAX=500                                     !!!!!!!!!!!
      IRMAX=0
C      LLSPEC=0
C      WTSPEC=0.0
      ASUM=0.0
      A1SUM=0.0
      ILAST=0
      AMAX=0.0
      LLMAX=1000
      PN=PHIMIN
      PX=PHIMAX
      IF(PHIMIN.GT.PHIMAX) THEN
      PHIMAX=PN
      PHIMIN=PX
      ENDIF
      ZN=ZMIN
      ZX=ZMAX
      IF(ZMIN.GT.ZMAX) THEN
      ZMAX=ZN
      ZMIN=ZX
      ENDIF
      RN=RSCMIN
      RX=RSCMAX
      IF(RSCMIN.GT.RSCMAX) THEN
      RSCMAX=RN
      RSCMIN=RX
      ENDIF
      IPL=(PHIMAX-PHIMIN)/DPHI+1.5
      IZL=(ZMAX-ZMIN)/DELZ+1.5
      IF(IPL.GT.8) THEN
      IPL=8
      PHIMAX=PHIMIN+5.0*DPHI
      WRITE(OUT,104)
      ENDIF
      IRL=(RSCMAX-RSCMIN)/DRSC+1.5
      IF(IRL.GT.8) THEN
      IRL=8
      RSCMAX=RSCMIN+5.0*DRSC
      WRITE(OUT,108)
      ENDIF
C
C     **  input reference layer-lines  **
C
      DO 8 L=1,1000
      DO 1 IR=1,NRMAX
      A(L,IR)=0.0
      A1(L,IR)=0.0
      P(L,IR)=0.0
      P1(L,IR)=0.0
1     CONTINUE
      WT(L) = 1.0
8     CONTINUE
      WRITE(OUT,109)REFILE
      NCOUNT=0
      L=0
4     L=L+1
      READ(1,110,END=2)head,scale,NN(L),NL(L)
3     READ(1,111)R,AMP,PHI
      IF(R.EQ.0.0.AND.AMP.EQ.0.0) GO TO 4
C      IF(NL(L).EQ.LLSPEC)WT(L)=WTSPEC
      IF(NLSPEC .GT. 0) THEN
       DO N=1,NLSPEC
        IF(NL(L).EQ.LLSPEC(N))WT(L)=WTSPEC(N)
       END DO
      END IF
      IR=R/DELR1+1.1
      A(L,IR)=AMP * WT(L)
      P(L,IR)=PHI
      IF(NL(L).EQ.0) GO TO 3
      ASUM=A(L,IR)+ASUM
      NCOUNT=NCOUNT+1
      IF(A(L,IR).GT.AMAX)AMAX=A(L,IR)
      GO TO 3
2     LLMAX=L-1
      AMIN=(ACUT*AMAX)/100.0
C
C     **  input test data; check for no match  **
C
      WRITE(OUT,112)TFILE
      NCOUNT1=0
      L=0
6     L=L+1
      READ(2,110,END=5)head,scale,NN1(L),NL1(L)
      DO 22 I=1,LLMAX
22    IF(NL1(L).EQ.NL(I).AND.NN1(L).EQ.NN(I)) GO TO 7
      WRITE(OUT,107)NL1(L),NN1(L)
      NN1(L)=-999
7     READ(2,111)R,AMP,PHI
      IF(R.EQ.0.0.AND.AMP.EQ.0.0) GO TO 6
      IF(IPOLE.EQ.1)PHI=-PHI
      IF(ISIDE.EQ.1)PHI=PHI+NN1(L)*180.
      IR=R/DELR2+1.1
      IF(IR.GT.IRMAX)IRMAX=IR
      A1(L,IR)=AMP * WT(L)
      P1(L,IR)=PHI
      IF(NL1(L).EQ.0) GO TO 7
      A1SUM=A1(L,IR)+A1SUM
      NCOUNT1=NCOUNT1+1
      GO TO 7
5     LL1MAX=L-1
      WRITE(OUT,200)LLTITLE
      WRITE(OUT,206)LLTITLE1
      WRITE(OUT,201)HLXTITLE
      WRITE(OUT,203)REFILE,TFILE
      WRITE(OUT,204)rep_dis
      WRITE(OUT,205)DELR1,DELR2
      WRITE(OUT,202)ASUM,NCOUNT
      WRITE(OUT,214)A1SUM,NCOUNT1
      WRITE(OUT,207)AMIN,ACUT
C
C     **  rescale test image by interpolating transform  **
C
      IRMAX=IRMAX+20
25    DO 30 NR=1,IRL
      DO 17 L=1,LL1MAX
      DO 18 IR=1,NRMAX
      A2(L,IR)=0.0
      P2(L,IR)=0.0
18    CONTINUE
17    CONTINUE
      IRSMAX=0
      RSC(NR)=RSCMIN+(NR-1)*DRSC
      DS=DELR1/RSC(NR)
      DO 11 L=1,LL1MAX
      IF (NN1(L).EQ.-999) GOTO 11
      RS=0.0
10    RRS=RS/DELR2+1.0
      IR=RRS+0.1
      IF(IR.GT.IRMAX)GO TO 11
      IF(A1(L,IR).EQ.0.0) GO TO 12
      RBIT=RRS-IR
      RBAR=1.0-RBIT
      AP=A1(L,IR)*COS(P1(L,IR)/DEG)*RBAR
     1  +A1(L,IR+1)*COS(P1(L,IR+1)/DEG)*RBIT
      BP=A1(L,IR)*SIN(P1(L,IR)/DEG)*RBAR
     1  +A1(L,IR+1)*SIN(P1(L,IR+1)/DEG)*RBIT
      R2=RS*RSC(NR)
      IRS=R2/DELR1+1.1
      IF(IRS.GT.IRSMAX)IRSMAX=IRS
      A2(L,IRS)=SQRT(AP*AP+BP*BP)
      P2(L,IRS)=ATAN2(BP,AP)*DEG
12    RS=RS+DS
      GO TO 10
11    CONTINUE
      IF(ILAST.EQ.1) GO TO 26
C
C     **  compare test with reference image  **
C
      RM(NR)=150.
      ZSHIFT=ZMIN*CONVD
      DZ=DELZ*CONVD
      DO 21 NZ=1,IZL
      ZRES(NZ)=ZSHIFT/CONVD
      PHISHF=PHIMIN
      DO 20 NP=1,IPL
      PRES(NP)=PHISHF
      NPTS=0
      FDIFF=0.0
      FDSUM=0.0
      F2SUM=0.0
      FSUM=0.0
      DO 15 L=1,LL1MAX
      PHASH=NN1(L)*PHISHF-NL1(L)*ZSHIFT
      DO 14 I=1,LLMAX
14    IF(NL1(L).EQ.NL(I).AND.NN1(L).EQ.NN(I)) GO TO 13
      GO TO 15
13    DO 16 IR=1,IRSMAX
      IF(A2(L,IR).EQ.0.0) GO TO 16
      IF(A(I,IR).LT.AMIN) GO TO 16
      DP=AMOD(P(I,IR)-P2(L,IR)+PHASH,360.)
      IF(ABS(DP).GT.180.)DP=360.-ABS(DP)
      FDSUM=A(I,IR)*DP*DP+FDSUM
      FSUM=A(I,IR)+FSUM
      F2SUM=A2(L,IR)+F2SUM
      FDIFF=ABS(A(I,IR)-SF*A2(L,IR))+FDIFF
      NPTS=NPTS+1
16    CONTINUE
15    CONTINUE
      SF=FSUM/F2SUM
      RF(NR)=FDIFF/FSUM
C
C     **  find minimum residuals  **
C
      RES(NP,NZ)=SQRT(FDSUM/FSUM)
      IF(RES(NP,NZ).LT.RM(NR)) THEN
      RM(NR)=RES(NP,NZ)
      PM(NR)=PRES(NP)
      ZM(NR)=ZRES(NZ)
      ENDIF
      PHISHF=PHISHF+DPHI
20    CONTINUE
      ZSHIFT=ZSHIFT+DZ
21    CONTINUE

      IEND=10+7*IPL
      DO 31 N=1,IEND
31    LINES(N)='-'
      WRITE(OUT,117) RSC(NR)
      WRITE(OUT,114) (PRES(N),N=1,IPL)
      WRITE(OUT,116) (LINES(N),N=1,IEND)
      DO 32 J=1,IZL
32    WRITE(OUT,115)ZRES(J),(RES(N,J),N=1,IPL)
      WRITE(OUT,116) (LINES(N),N=1,IEND)
      WRITE(OUT,118) RM(NR),PM(NR),ZM(NR)
      WRITE(OUT,119)NPTS
      WRITE(OUT,120)FSUM
      WRITE(OUT,121)SF
      WRITE(OUT,122)RF(NR)
30    CONTINUE

C
C     **  find global minimum  **
C
      WRITE(OUT,124)
      WRITE(OUT,125)
      WRITE(OUT,126)
      WRITE(OUT,123) (RSC(N),PM(N),ZM(N),RF(N),RM(N),N=1,IRL)
      WRITE(OUT,126)
      GMIN=150.
      DO 33 NR=1,IRL
      IF(RM(NR).LT.GMIN) THEN
      RFMIN=RF(NR)
      GMIN=RM(NR)
      PHIMIN=PM(NR)
      ZMIN=ZM(NR)
      RSCMIN=RSC(NR)
      ENDIF
33    CONTINUE
      WRITE(OUT,127)GMIN,PHIMIN,ZMIN,RSCMIN
C
C     **  if missed minimum, redo  **
C
      IF(RSCMIN.EQ.RSC(1).OR.RSCMIN.EQ.RSC(IRL)
     &.OR.PHIMIN.EQ.PRES(1).OR.PHIMIN.EQ.PRES(IPL)
     &.OR.ZMIN.EQ.ZRES(1).OR.ZMIN.EQ.ZRES(IZL))THEN
         DELZ=DELZ*10.0
         DPHI=DPHI*10.0
         IF (DELZ.GT.10.0) DELZ=10.0
         IF (DPHI.GT.1.0) DPHI=1.0
         RSCMIN=RSCMIN-INT(IRL/2)*DRSC
         PHIMIN=PHIMIN-INT(IPL/2)*DPHI
         ZMIN=ZMIN-INT(IZL/2)*DELZ
         GOTO 25
      ELSE IF (DELZ.GT.0.01.OR.DPHI.GT.0.01) THEN
         DELZ=DELZ/5.0
         DPHI=DPHI/5.0
         IF (DELZ.LT.0.01) DELZ=0.01
         IF (DPHI.LT.0.01) DPHI=0.01
         RSCMIN=RSCMIN-INT(IRL/2)*DRSC
         PHIMIN=PHIMIN-INT(IPL/2)*DPHI
         ZMIN=ZMIN-INT(IZL/2)*DELZ
         GOTO 25
      ENDIF
C
C     **  calculate phase residual for each layer-line  **
C
      ILAST=1
      IRL=1
      GO TO 25
26    WRITE(OUT,208)
      DO 28 L=1,LL1MAX
      NPTS=0
      IPT0=-1
      IPM0=0
      RA=0.0
      TA=0.0
      ZR=0.0
      BSUM=0.00001
      BDSUM=0.0
      B2SUM=0.0
      RESL=0.0
      PHASH=NN1(L)*PHIMIN-NL1(L)*CONVD*ZMIN
      DO 42 I=1,LLMAX
42    IF(NL1(L).EQ.NL(I).AND.NN1(L).EQ.NN(I)) GO TO 41
      GO TO 28
41    DO 27 IR=1,IRSMAX
      IF(A2(L,IR).EQ.0.0) GO TO 27
      IF(IPT0.GT.-1) GO TO 29
      IPT0=IR-1
29    IPTM=IR-1
      IF(A(I,IR).LT.AMIN) GO TO 27
      DP=AMOD(P(I,IR)-P2(L,IR)+PHASH,360.)
      IF(ABS(DP).GT.180.)DP=360.-ABS(DP)
      BDSUM=A(I,IR)*DP*DP+BDSUM
      BSUM=A(I,IR)+BSUM
      B2SUM=A2(L,IR)+B2SUM
      NPTS=NPTS+1
27    CONTINUE
      IF(NL1(L).EQ.0) GO TO 43
      ZR=rep_dis/NL1(L)
      RA=(BSUM/FSUM)*100.
      TA=(B2SUM/F2SUM)*100.
      RESL=SQRT(BDSUM/BSUM)
43    IF(RESL.EQ.0.0) THEN
C      WRITE(OUT,215)L,NL1(L),NN1(L),ZR,WT1(L),NPTS,IPT0,IPTM,RA,TA,NOT
      WRITE(OUT,215)L,NL1(L),NN1(L),ZR,WT(L),NPTS,IPT0,IPTM,RA,TA,NOT
      GO TO 28
      ENDIF
C23    WRITE(OUT,213)L,NL1(L),NN1(L),ZR,WT1(L),NPTS,IPT0,IPTM,RA,TA,RESL
23    WRITE(OUT,213)L,NL1(L),NN1(L),ZR,WT(L),NPTS,IPT0,IPTM,RA,TA,RESL
28    CONTINUE
      WRITE(OUT,131)GMIN,PHIMIN,ZMIN,RSCMIN
      IGMIN=GMIN*1000
      IRFMIN=(RFMIN-1)*100000
      WRITE(8,132)IRFMIN,PHIMIN,ZMIN,RSCMIN,IGMIN
C
C     **  write fitted output file  **
C
      IF(IOUT.EQ.0) GO TO 35
      IF(IOUT.EQ.2) GO TO 36
      ZERO=0.0
      PHASH=0.0
      WRITE(OUT,128)NEWFILE
      L=0
40    IR=1
      L=L+1
      IF(L.GT.LL1MAX) GO TO 37
C      WRITE(3,110)TEXT1(L),LLTITLE1,WT1(L),NN1(L),NL1(L)
      WRITE(3,110)TEXT1(L),LLTITLE1,WT(L),NN1(L),NL1(L)
      PHASH=NN1(L)*PHIMIN-NL1(L)*CONVD*ZMIN
39    IF(A2(L,IR).EQ.0.0.AND.A2(L,IR+1).GE.0.0) GO TO 38
      AMP=A2(L,IR)
      PHI=AMOD(P2(L,IR)-PHASH,360.)
      IF(PHI.LT.0.0)PHI=PHI+360.
      R=DELR1*(IR-1)
      WRITE(3,130)R,AMP,PHI
      IF(A2(L,IR).GT.0.0.AND.A2(L,IR+1).EQ.0.0) THEN
      WRITE(3,130)ZERO,ZERO,ZERO
      GO TO 40
      ENDIF
38    IR=IR+1
      GO TO 39
37    CLOSE(3)
      GO TO 35
36    CONTINUE
C
35    CLOSE(2)
      CLOSE(1)
C
100   FORMAT(2X,'HLX2FD : ',12A4,2X,A9,2X,A8)
101   FORMAT(2X,'control file : ')
102   FORMAT(A40)
103   FORMAT(A)
104   FORMAT(/2X,'** too many steps in phi! **')
105   FORMAT(2X,'new file : ')
106   FORMAT(//2X,'error file :')
107   FORMAT(2X,'!! no ref. layer-line corresponding to l=
     2',I3,', n=',I4)
108   FORMAT(/2X,'** too many steps in rscal! **')
109   FORMAT(2X,'--- reading in reference data : ',A50)
110   FORMAT(10A5,f10.3,2i5)
111   FORMAT(3E10.3)
112   FORMAT(2X,'--- reading in test data : ',A50//)
113   FORMAT(A50)
114   FORMAT(2X,' z phi | ',8F7.2)
115   FORMAT(2X,F7.2,': ',8F7.2)
116   FORMAT(2X,73A)
117   FORMAT(//2X,'radial scaling factor :  ',F10.4,/)
118   FORMAT(2X,'   minimum residual : ',F10.3,'    at phi = ',
     2F7.2,', z = ',F7.2)
119   FORMAT(2X,'no. points compared : ',I10)
120   FORMAT(2X,'amp (ref.) compared : ',F11.0)
121   FORMAT(2X,'amp sc. fact. (r/t) : ',F10.4)
122   FORMAT(2X,'           R factor : ',F10.4)
123   FORMAT(2X,F7.4,2F11.2,4X,F10.4,3X,F10.3)
124   FORMAT(//2X,'**** minimum locations ****')
125   FORMAT(2X,' rscale       phi         z         R factor
     2    residual')
126   FORMAT(2X,'------------------------------------------
     2---------------')
127   FORMAT(//2X,'**** global minimum ****',/2X,'residual =',
     2F8.3,'   at phi = ',F7.2,', z = ',F7.2,', rscale = ',F8.4,/)
128   FORMAT(2X,'--- now writing fitted test file: ',A)
130   FORMAT(1p,3E12.5)
131   FORMAT(2X,'-------------------------------------------------
     2------------------',/2X,'residual =',
     3F8.3,'   at phi = ',F7.2,', z = ',F7.2,', rscale = ',F8.4,/)
132   FORMAT(I10,F7.2,F8.2,F8.4,I10)
200   FORMAT(/2X,'     ref. layer-line title : ',A)
206   FORMAT(2X,'     test layer-line title : ',A)
201   FORMAT(2X,'                 new title : ',A)
202   FORMAT(2X,'   sum of amplitude (ref.) : ',F8.0,' (',I6,'
     2 points)')
203   FORMAT(2X,'              reference in : ',A50,/2X,
     2'                   test in : ',A50)
204   FORMAT(2X,'           repeat distance : ',F9.2,' Angstroms')
205   FORMAT(2X,'layer-line sampling (ref.) : ',F13.9,
     2' recip. Angstroms',/2X,'layer-line sampling (test) : ',
     3F13.9,' recip. Angstroms')
207   FORMAT(2X,'         cut-off amplitude : ',F7.2,'  ( ',F4.2,
     2'% of max ref. amp)')
208   FORMAT(/2X,'phase residual for each test layer-line at g
     2lobal minimum ',/2X,'----------------------------------
     3---------------------------------',
     4/2X,'        L    N   1/Z   weight    points      ref-amp-te
     5st  residual',
     6/2X,'                 (A)             (g.u.)           %
     7    (degree)',
     8/2X,'------------------------------------------------------
     9-------------')
213   FORMAT(2X,I3,':',2I5,F7.1,F7.1,I5,' (',I3,'-',I3,')',2F7.2,F9.2)
215   FORMAT(2X,I3,':',2I5,F7.1,F7.1,I5,' (',I3,'-',I3,')',2F7.2,6X,A)
214   FORMAT(2X,'   sum of amplitude (test) : ',F8.0,' (',I6,'
     2 points)')
      STOP
      END
