#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
'''
Visualise the input maps and PDB for TEMPy in Chimera
'''

from sys import argv
from chimera import runCommand as rc
import VolumeViewer as VV

#Sets the transparency and level for input map1
rc("open %s"%(argv[1]))
v = VV.active_volume()
stdev = v.data.full_matrix().std()
rc("volume #0 level %f transparency 0.5"%(stdev))

#Sets the transparency and level for input map 2 or opens pdb file
if argv[2].split('.')[1] == 'pdb':
    rc("open %s"%(argv[2]))
else:
    rc("open %s"%(argv[2]))
    v = VV.active_volume()
    stdev = v.data.full_matrix().std()
    rc("volume #1 level %f transparency 0.5"%(stdev))