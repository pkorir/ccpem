#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
from PyQt4 import QtGui, QtCore
import sys
import os
from ccpem_core import process_manager
from ccpem_gui.utils import ccpem_widgets


class CCPEMProcessViewer(QtGui.QMainWindow):
    def __init__(self, pipeline=None, pipeline_path=None, parent=None):
        super(CCPEMProcessViewer, self).__init__(parent)
        self.pipeline = pipeline
        self.pipeline_path = pipeline_path
        self.pipeline_widget = CCPEMPipelineWidget(
            pipeline=self.pipeline,
            pipeline_path=self.pipeline_path,
            parent=self)
        self.job_widget = CCPEMProcessDetailsWidget(parent=self)
        # Set first job to be displayed
        if self.pipeline is not None:
            self.job_widget.set_job(self.pipeline.pipeline[0][0])
        self.splitter = QtGui.QSplitter(QtCore.Qt.Horizontal)
        self.splitter.addWidget(self.pipeline_widget)
        self.splitter.addWidget(self.job_widget)
        self.splitter.setStretchFactor(1, 0)
        self.splitter.setStretchFactor(1, 1)
        self.setCentralWidget(self.splitter)


class CCPEMProcessDetailsWidget(QtGui.QGroupBox):
    def __init__(self, job=None, parent=None):
        super(CCPEMProcessDetailsWidget, self).__init__(parent)
        self.setTitle('Process information')
        layout = QtGui.QVBoxLayout()
        self.setLayout(layout)
        self.job = job
        # Job information
        self.name_display = ccpem_widgets.CCPEMTextDisplay(
            label_text='Name',
            line_text='')
        layout.addWidget(self.name_display)
        self.status_display = ccpem_widgets.CCPEMTextDisplay(
            label_text='Status',
            line_text='')
        layout.addWidget(self.status_display)
        self.location_display = ccpem_widgets.CCPEMTextDisplay(
            label_text='Location',
            line_text='')
        self.location_display.setToolTip('Double click to view')
        layout.addWidget(self.location_display)
        self.log_display = ccpem_widgets.CCPEMTextDisplay(
            label_text='Log',
            line_text='')
        self.log_display.setToolTip('Double click to view')
        layout.addWidget(self.log_display)
        self.error_display = ccpem_widgets.CCPEMTextDisplay(
            label_text='Error',
            line_text='')
        self.error_display.setToolTip('Double click to view')
        layout.addWidget(self.error_display)
        self.log_select_options = {'Log':0, 'Error':1}
        self.log_select = QtGui.QComboBox()
        self.log_select.setFixedWidth(100)
        self.log_select.addItems(self.log_select_options.keys())
        self.log_select.setToolTip('Select standard log or error log to view')
        self.log_select.activated.connect(self.user_set_text_browser)
        layout.addWidget(self.log_select)
        # Hide by error widgets by default
        self.error_display.hide()
        self.log_select.hide()
        self.user_selected_output = False

        # Log browser
        self.browser = ccpem_widgets.CCPEMTextBrowser(
            filepath=None,
            parent=self)
        layout.addWidget(self.browser)

        # Auto update
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.set_job_status)
        self.timer.start(1000)

    def set_job(self, job):
        self.job = job
        self.name_display.line.setText(job.name)
        self.status_display.line.setText(job.get_status().title())
        self.location_display.line.setText(job.location)
        self.log_display.line.setText(job.stdout)
        self.error_display.line.setText(job.stderr)
        self.error_display.hide()
        self.log_select.hide()
        self.log_select.setCurrentIndex(self.log_select_options['Log'])
        self.set_text_browser(display=0)

    def show_error(self):
        self.error_display.show()
        self.log_select.show()
        if not self.user_selected_output:
            self.log_select.setCurrentIndex(
                self.log_select_options['Error'])
            self.set_text_browser(display=1)

    def set_job_status(self):
        if self.job is not None:
            if hasattr(self.job, 'json'):
                self.status_display.line.setText(self.job.get_status().title())
                # Show error log if file size > 0
                if os.path.exists(self.job.stderr):
                    statinfo = os.stat(self.job.stderr)
                    if statinfo.st_size > 0:
                        self.show_error()

    @QtCore.pyqtSlot(int)
    def user_set_text_browser(self, display=0):
        self.user_selected_output = True
        self.set_text_browser(display=display)

    def set_text_browser(self, display=0):
        if self.job is not None:
            if display == 1:
                if self.browser.filepath is not self.job.stderr:
                    self.browser.set_filepath(filepath=self.job.stderr,
                                              clear_text=True)
            else:
                if self.browser.filepath is not self.job.stdout:
                    self.browser.set_filepath(filepath=self.job.stdout,
                                              clear_text=True)

class CCPEMPipelineWidget(QtGui.QGroupBox):
    def __init__(self,
                 pipeline=None,
                 pipeline_path=None,
                 parent=None):
        super(CCPEMPipelineWidget, self).__init__(parent)
        self.parent = parent
        self.setTitle('Job pipeline')
        self.process_viewer = parent
        self.setWindowTitle('CCP-EM Pipeline Viewer')
        self.tree = QtGui.QTreeView(self)
        self.tree.setHeaderHidden(True)
        layout = QtGui.QHBoxLayout(self)
        layout.addWidget(self.tree)
        self.root_model = QtGui.QStandardItemModel(self)
        self.root_item = self.root_model.invisibleRootItem()
        self.tree.setModel(self.root_model)
        if pipeline_path is not None:
            self.set_pipeline_from_path(pipeline_path=pipeline_path)
        elif pipeline is not None:
            self.set_pipeline(pipeline=pipeline)
        self.user_acted = False
        self.set_json = ''

    def set_pipeline_from_path(self, pipeline_path):
        pipeline = process_manager.CCPEMPipeline(
            import_json=pipeline_path)
        self.set_pipeline(pipeline=pipeline)

    def set_pipeline(self, pipeline):
        self.pipeline = pipeline
        self.populate_tree()
        self.set_update()
        # Expand all at first
        self.tree.expandAll()
        self.tree.clicked.connect(self.get_selected_job)
        self.tree.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.setToolTip('Click job to view details')
        # Set job
        if hasattr(self.process_viewer, 'job_widget'):
            if self.process_viewer.job_widget.job is None:
                self.set_job(job=self.pipeline.pipeline[0][0])

    def set_update(self):
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.populate_tree)
        self.timer.start(1000)

    def populate_tree(self):
        # Set colours
        colour_status = {
            'ready': QtGui.QColor(127, 127, 127),
            'running': QtGui.QColor(0, 0, 127),
            'finished': QtGui.QColor(0, 127, 0),
            'failed': QtGui.QColor(255, 0, 0),
            }
        auto_set_job = self.pipeline.pipeline[0][0]
        for n, jobs in enumerate(self.pipeline.pipeline):
            child = 'Stage {0}'.format(n+1)
            jobs_item = QtGui.QStandardItem(child)
            # Check if item already present
            find_stage = self.root_model.findItems(
                child,
                flags=QtCore.Qt.MatchFixedString)
            if len(find_stage) == 0:
                self.root_item.appendRow(jobs_item)
            else:
                jobs_item = find_stage[0]
            for job in jobs:
                # Check if item present
                found_child = -1
                for n in xrange(jobs_item.rowCount()):
                    if jobs_item.child(n).name == job.name:
                        found_child = n
                if found_child == -1:
                    if hasattr(job, 'name'):
                        if job.name is not None:
                            job_item = QtGui.QStandardItem(job.name)
                            job_item.name = job.name
                            jobs_item.appendRow(job_item)
                else:
                    job_item = jobs_item.child(found_child)
                job_item.job = job
                
                if hasattr(job, 'json'):
                    status = job.get_status()
                    if status in ['running', 'finished']:
                        auto_set_job = job
                #
                job_item.setForeground(colour_status[status])
                tooltip = 'Status: ' + status.title()
                job_item.setToolTip(tooltip)
 
        #Autoset job to most recent running or finished job
        if not self.user_acted and self.parent.job_widget.browser.scroll_at_max:
            self.set_job(job=auto_set_job)

    def get_selected_job(self):
        self.user_acted = True
        index = self.tree.selectedIndexes()[-1]
        item = index.model().itemFromIndex(index)
        if hasattr(item, 'job'):
            self.set_job(job=item.job)

    def set_job(self, job):
        if self.set_json != job.json:
            self.set_json = job.json
            self.process_viewer.job_widget.set_job(job=job)

    def closeEvent(self, event):
        '''
        Stop timer on close.
        '''
        self.timer.stop()

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    pipeline_path = os.path.join('/home/tom/test_project/Refmac5_28/task.ccpem')
    assert os.path.exists(path=pipeline_path)
    main = CCPEMProcessViewer(pipeline_path=pipeline_path)
    main.show()
    sys.exit(app.exec_())
