C*FFTPRINT.FOR************************************************************
C     Program developed from trnout to shift quadrant of FFT to produce
C     suitable amplitude and phase output for helical particles.
C      i.e. layer lines are continuous across meridian.
C     Output to LP  log base 2 (AMPLITUDE) on scale 0 to 9
C                   PHASE in 10 deg intervals coded A-Z 0-9
C
C       FMAX  sets level above which all amplitudes set to 9
C	IXMAX sets halfwidth in transform steps
C       IYMAX sets no of Y-lines output
C       XSHFT X-shift for phase origin in sample steps
C       YSHFT Y-shift for phase origin in sample steps
C	IOUT controls output medium
C		0 output written to 'trnout.log'
C		1    "      "     " line printer
C		2    "      "     " postscript file trnout.ps
C		3 amps only "     "      "
C		4 phases "  "     "      "
C
C     Input transform on stream 1 (IN)
C     Output amplitudes for TONE on stream 2 (OUT)  NL: if not reqd
C      Dimensioned for transforms of 2048x2048 images (maximum) and
C      for 256 lines maximum of line printer output.
C
C       VERSION 1.01     29OCT82   RAC  FOR VAX
C       VERSION 1.1      18FEB98   jms
C	VERSION 1.15     20JUL98   jms
C	VERSION 1.18     11DEC98   jms
C	VERSION 1.19     29MAR00   jms
C	VERSION 1.20	 09OCT08   jms
C
      COMMON//NX,NY,NZ
      PARAMETER (MAXARR = 2049)
      PARAMETER (MAXSIZ = 1024)
      PARAMETER (MAXEXT = MAXSIZ + 1)
      PARAMETER (NCOLS = 128)
      PARAMETER (MAXROWS = 512)

C***
      BYTE   FAMP(MAXEXT,MAXSIZ)
C*** jms 17.06.2010
	character phi(maxext,maxsiz)
	character pcode(36)
	byte		ff
c      BYTE		PHI(MAXEXT,MAXSIZ)
c      BYTE		PCODE(36),FF
C***
      CHARACTER  DAT*24
      CHARACTER  FILIN*80
C***
      INTEGER*4  LABELS(20,10)
      INTEGER*4  MXYZ(3)
      INTEGER*4  NXYZ2(3)
      INTEGER*4  NXYZ1(3)
C***
      REAL*4     ARRAY(MAXARR*MAXARR)
      REAL*4     ARRF(MAXARR)
      REAL*4     ARRP(MAXARR)
      REAL*4     TITLE(20)
CTSH++
	CHARACTER*80 TMPTITLE
	EQUIVALENCE (TMPTITLE,TITLE)
CTSH--
C***
      EQUIVALENCE (NX,NXYZ1)
C***
      DATA PCODE/'A','B','C','D','E','F','G','H','I','J','K','L','M',
     1 'N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1',
     2 '2','3','4','5','6','7','8','9'/
      DATA TWOPI/6.283185/,DEG/57.2958/
      FF='14'O
      CALL GETENV('IN',FILIN)
C***
      WRITE(6,1000)
1000  FORMAT(' ',//'   TRNOUT V 1.15 : Program to output FFT'//)
      CALL IMOPEN(1,'IN','RO')
      CALL IMOPEN(2,'OUT','NEW')
C***
      CALL FDATE(DAT)
CTSH      ENCODE(80,1100,TITLE) DAT(5:24)
CTSH++
      WRITE(TMPTITLE,1100) DAT(5:24)
CTSH++
1100  FORMAT(' TRNOUT : Move quadrant of FFT',20X,A20)
      CALL IRDHDR(1,NXYZ1,MXYZ,MODE,DMIN,DMAX,DMEAN)
      CALL IRTLAB(1,LABELS,NLAB)
      CALL IRTORG(1,XOR,YOR,ZOR)
      WRITE(6,'(//''   Phase origin stored with transform'',2F10.2)')
     * XOR,YOR
      READ(5,*) FMAX,IXMAX,IYMAX,XSHFT,YSHFT,IOUT
      WRITE(6,'(///
     *          ''  OUTPUT PARAMETERS''
     *         /''     FMAX for amplitude scale    '',F10.1,
     *         /''     No of X-lines halfwidth     '',I10,
     *         /''     No of Y-lines output        '',I10,
     *         /''     X-shift of phase origin     '',F10.2,''  steps'',
     *         /''     Y-shift of phase origin     '',F10.2,''  steps'',
     *         /''     Output flag                 '',I10)')
     * FMAX,IXMAX,IYMAX,XSHFT,YSHFT,IOUT
C***
      NBLOCKS = IYMAX / NCOLS
      IF(NBLOCKS * NCOLS .LT. IYMAX) NBLOCKS = NBLOCKS + 1
      IF(IYMAX .GT. MAXROWS) THEN
         IYMAX = MAXROWS
         WRITE(6,'(///''  Number of lines output changed to '',I4)')
     *   MAXROWS
      ENDIF
C***
      IF(IOUT .EQ. 2 .AND. IYMAX .GT. MAXROWS) THEN
	IYMAX = MAXROWS
        WRITE(6,'(///
     *  '' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!''/
     *  ''  ERROR - Number of lines output changed to '',I4/
     *  ''  Set output flag to 0 (log file only) or 1 (lineprinter)''/
     *  ''  for number of lines >'',I4/
     *  '' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!''/
     *  )') MAXROWS, MAXROWS
      END IF
C***
      NXP2=2*NX
      NXM1=NX-1
C*** Number of pages of output
      NPAGES = MIN(IXMAX,NXM1) / 64
      NX2=2*NXM1
C*** Line length in transposed transform in reals
      KX=2*NXM1+1
      IF(KX .GT. MAXEXT) THEN
	WRITE(6,'(''Error - map too large for program dimensions'')')
	STOP
      END IF
C*** No of lines in transposed transform
      KY=NY/2
C***
      NXYZ2(1)=KX
      NXYZ2(2)=KY
      NXYZ2(3)=1
      CALL ICRHDR(2,NXYZ2,MXYZ,2,TITLE,0)
      CALL ITRLAB(2,1)
      CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
      CALL IRDSEC(1,ARRAY,*99)
      NPRINT = KX
C*** set output device
      IF(IOUT .EQ. 0) THEN
	IDEVOUT = 6
      ELSE
	IDEVOUT = 3
	OPEN(UNIT=3,FILE='trnout.out',STATUS='UNKNOWN')
      END IF
C***
      SCALE=9./ALOG10(FMAX)
      DELPX=-TWOPI*(XOR+XSHFT)/NX2
      DELPY=-TWOPI*(YOR+YSHFT)/NY
      DO 150 LY=1,KY
C*** Line no in original transform counting from 0
       IY=LY+KY-1
       DO 110 LX=1,KX
C*** Column no in original transform counting from -NX+1
        IX=LX-NX
        IF(IX.LT.0) THEN
C*** First half line -  use Friedel mate
         INDEX=(NY-IY)*NXP2-2*IX+1
        ELSE
C*** Second half line
         INDEX=IY*NXP2+2*IX+1
        END IF
        PSHFT=IX*DELPX+(LY-1)*DELPY
        IF(IX.LT.0) PSHFT=-PSHFT
        CALL SHIFT(A,B,PSHFT,INDEX,ARRAY)
        IF(IX.LT.0) B=-B
         ARRF(LX)=SQRT(A*A+B*B)
         IF(ARRF(LX).EQ.0.) THEN
         ARRP(LX)=0.
        ELSE
         ARRP(LX)=ATAN2(B,A)*DEG
        ENDIF
        IF(ARRP(LX).LT.0.) ARRP(LX)=ARRP(LX)+360.
110    CONTINUE
       CALL IWRLIN(2,ARRF)
C***
       IF(LY.GT.IYMAX) GO TO 150
       DO 120 LX=1,NPRINT
        LLX=LX+(KX-NPRINT)/2
        IF(ARRF(LLX).EQ.0.) THEN
         FMOD=0
        ELSE
         FMOD=SCALE*ALOG10(ARRF(LLX))
        ENDIF
        IF(FMOD.GT.9) FMOD=9
        IF(FMOD.LT.0) FMOD=0
        FAMP(LX,LY)=FMOD
        I=ARRP(LLX)/10.+1
        IF(I.LT.0.OR.I.GT.36) I=1
        PHI(LX,LY)=PCODE(I)
120    CONTINUE
150   CONTINUE
C***
C*** Output amplitude map
      LSTART = NX - NCOLS * NPAGES / 2
      IF(IOUT .EQ. 4) GO TO 200
      IF(IOUT .EQ. 0)
     * WRITE(6,'('' '',1A/
     * ''  Amplitudes (log base 2)'',10X,20A4/35X,20A4/)')
     * FF, (LABELS(M,1),M=1,20),(LABELS(M,NLAB),M=1,20)
      DO N=1,NPAGES
       ICOUNT = 0
       IBLOCK = 1
       L1 = LSTART + NCOLS * (N - 1)
       L2 = MIN(L1 + NCOLS,NX2)
       IF(IOUT .EQ. 0) THEN
	WRITE(IDEVOUT,'(''PAGE '',I1,'' OF '',I1)') N, NPAGES
       ELSE
        WRITE(IDEVOUT,'(
     *  ''AMPS(log base 2):BLOCK'',I2,'' OF'',I2,'',P'',I2,
     *  '' OF'',I2,'' XSHIFT '',F5.1,1X,A)')
     *  IBLOCK,NBLOCKS,N,NPAGES,XSHFT,FILIN
       END IF
       DO LY=1,IYMAX
        LLY=IYMAX-LY+1
	ICOUNT = ICOUNT + 1
        WRITE(IDEVOUT,'(1X,129I1)') (FAMP(LLX,LLY),LLX=L1,L2)
	IF((IOUT .EQ. 2 .OR. IOUT .EQ. 3) .AND. ICOUNT .EQ. NCOLS
     *                 .AND. LY .LT. IYMAX) THEN
         WRITE(IDEVOUT,'('' '',1A)') FF
	 ICOUNT = 0
	 IBLOCK = IBLOCK + 1
	 WRITE(IDEVOUT,'(''BLOCK '',I1,'' PAGE '',I1)') IBLOCK,N
	END IF
       END DO
       IF(N .LT. NPAGES) WRITE(IDEVOUT,'('' '',1A)') FF
      END DO
C*** Mark F(0,0)
      IF(NPAGES .EQ. 1) WRITE(IDEVOUT,'(65X,''^'')')
      WRITE(IDEVOUT,'('' '',1A)') FF
C*** Output phase map
  200 IF(IOUT .EQ. 3) GO TO 500
      IF(IOUT .EQ. 0)
     * WRITE(6,'(
     * ''  Phases (Coded A-Z 0-9)'',10X,20A4/34X,20A4/)')
     * (LABELS(M,1),M=1,20),(LABELS(M,NLAB),M=1,20)
      DO N=1,NPAGES
       ICOUNT = 0
       IBLOCK = 1
       L1 = LSTART + NCOLS * (N - 1)
       L2 = MIN(L1 + NCOLS,NX2)
       IF (N .EQ. NPAGES) L2 = L2 - 1
       IF(IOUT .EQ. 0) THEN
	WRITE(IDEVOUT,'(''PAGE '',I1,'' OF '',I1)') N, NPAGES
       ELSE
        WRITE(IDEVOUT,'(
     *  ''PHASES (coded A-Z 0-9):BLOCK'',I2,'' OF'',I2,'',PAGE'',I2,
     *  '' OF'',I2,1X,A)')
     *  IBLOCK,NBLOCKS,N,NPAGES,FILIN
       END IF
       DO LY=1,IYMAX
	ICOUNT = ICOUNT + 1
        LLY=IYMAX-LY+1
        WRITE(IDEVOUT,'(1X,129A1)') (PHI(LLX,LLY),LLX=L1,L2)
	IF((IOUT .EQ. 2 .OR. IOUT .EQ. 4) .AND. ICOUNT .EQ. NCOLS
     *                 .AND. LY .LT. IYMAX) THEN
         WRITE(IDEVOUT,'('' '',1A)') FF
	 ICOUNT = 0
	 IBLOCK = IBLOCK + 1
	 WRITE(IDEVOUT,'(''BLOCK '',I1,'' PAGE '',I1)') IBLOCK,N
	END IF
       END DO
       IF(N .LT. NPAGES) WRITE(IDEVOUT,'('' '',1A)') FF
      END DO
      IF(NPAGES .EQ. 1) WRITE(IDEVOUT,'(65X,''^'')')
C*** jms 17.06.2010
  500 IF(IOUT .EQ. 1) THEN
	close(idevout)
        WRITE(6,'(//''Printing on line printer...'')')
	CALL SYSTEM('lpr -Plprint trnout.out')
      ELSE IF(IOUT .GE. 2) THEN
	close(idevout)
        WRITE(6,'(//''Sending to trnout.ps ...'')')
	CALL SYSTEM
     * ('lasertext -pagelines=134 -pointsize=5.5 trnout.out'//
     *  ' > trnout.ps')
      END IF
      CALL IMCLOSE(1)
      CALL IMCLOSE(2)
      STOP
99    WRITE(6,1400)
1400  FORMAT(////'  End of data on stream 1')
      STOP
      END
C*************************************************************
C***
C*** Routine to return phase origin corrected transform value
C***
C*************************************************************
      SUBROUTINE SHIFT(A,B,PSHFT,IND,ARRAY)
      DIMENSION ARRAY(1)
      C=COS(PSHFT)
      S=SIN(PSHFT)
      A=ARRAY(IND)*C-ARRAY(IND+1)*S
      B=ARRAY(IND)*S+ARRAY(IND+1)*C
      RETURN
      END
