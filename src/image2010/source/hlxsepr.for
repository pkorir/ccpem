C*HLXSEPR.FOR****************************************************************
C     Program to separate overlapping Bessels , using views of helix
C     in different orientations. Uses LINFIL least squares package.
C       Input layer line data on direct access file 2 , created by
C        HLXSEPDAT program.
C       Output separated Bessel functions on stream 3 , in HLXFOUR
C        format. PLot graphs of separated Bessels on printer.
C
C        TITLE  Title for separated data.
C
C        NVIEW  No. of views (ie. sides)
C        LLMAX  No. of layer lines in each view
C        RADIUS Radius of particle in angstroms - used for determining
C               where Bessel contribution starts in transform.
C        IGRAPH Plot graphs of sep. Bessels if IGRAPH.NE.0
C
C        RSCAL(NV),NV=1,NVIEW  Radial scale factors from HLXFIT
C
C       Then for each layer line:
C        NL     Layer line no.
C        NORD   No. of different Bessel orders on layer line
C        IORD(NO),NO=1,NORD  Bessel orders on layer line
C
C        FITWT(NV),NV=1,NVIEW   Amplitude weight factors for each view
C               for this layer line.
C
C       Dimensions set for maximum no. of Bessels per layer line= 10,
C       max no. of views = 32, max no of layer lines 20.
C
C       Compile  HLXSEPR+LINFIL+LLGRAPH, then   PIMLINK
C
C       VERSION 1.01      RAC       FOR VAX      13DEC82
C       VERSION 1.02      Including radial scale factors  30MAR83
C       VERSION 1.03      Corrections for meridionals     16SEP83
C       VERSION 1.04      Dump of least squares residuals 13NOV83
C       VERSION 1.05      Tidying up to check Bessel orders 15NOV85
C       VERSION 2.0       Convert to imsubs2000           13NOV00
C
      DIMENSION TITLE(10),IORD(10),NREC(33),PHI(32),MINR(32),MAXR(32),
     1 F(200,32),PSI(200,32),CSMAT(64,20),RHS(64,150),SOLN(64,150),
     2 ATA(20,20),ATB(20,150),VEC(20,20),VAL(20),IPOINT(20),W(100)
      DIMENSION FITWT(32),RSCAL(32),NOV(200),NBES(200),NBSTRT(50)
      DIMENSION FOUT(200,10),PHIOUT(200,10),ROUT(200),FLLG(200),
     1 PHILLG(200),RLLG(200),JSTART(10)
      DIMENSION RESID(32,20),RESID0(32,20),RESIDR(32,20),
     1 RESIDR0(32,20),RES(32),RESR(32),TOTF(32),TOTF0(32),
     2 TOTFR(32),TOTFR0(32),TOTLL(20),TOTLLR(20),LLNO(20),NPOINT(20)
      LOGICAL LAST,FIRST
      character filin*80
      character filout*80
C
      ICSDIM=64
      FILT=1.E-2
      PI=3.141593
      CRAD=0.0174532
C
      WRITE(6,1000)
1000  FORMAT('1',///' HLXSEPR 1.05 : Bessel function separation'//)
C
c      DEFINE FILE 2(320,608,U,N2)
      read(5,'(a)') filin
CTSH      open(unit=2,file=filin,status='unknown',
CTSH     * form='unformatted',recl=1216,access='direct',
CTSH     * associatevariable=n2)
CTSH++
      open(unit=2,file=filin,status='unknown',
     * form='unformatted',recl=1216,access='direct')
C See comment in hlxsepdat.for
CTSH--
      read(5,'(a)') filout
      OPEN(UNIT=3,file=filout,STATUS='unknown')
C
      READ(5,1001) TITLE
1001  FORMAT(10A4)
      WRITE(6,1002) TITLE
1002  FORMAT(///'   Title for plots and output file : ',10A4/)
      READ(5,*) NVIEW,LLMAX,RADIUS,IGRAPH
      WRITE(6,1003) NVIEW,LLMAX,RADIUS,IGRAPH
1003  FORMAT(///'   No of views         ',I10,
     1         /'   No of layer lines   ',I10,
     2         /'   Radius of particle  ',F10.1,' Angstroms',
     3         /'   Graphs output (N/Y 0/1)',I10/)
      READ(5,*) (RSCAL(NV),NV=1,NVIEW)
      WRITE(6,1004) (RSCAL(NV),NV=1,NVIEW)
1004  FORMAT(///'  Radial scale factors for different views'//
     1  4(5X,10F10.3/))
C!!!!      READ(2'1) NREC,PHI,DELBR,CANG
      read(2,rec=1) nrec,phi,delbr,cang
      WRITE(6,1005) NREC
1005  FORMAT(//'  Address block on stream 2'/2(/2X,30I4))
      WRITE(6,1006) (PHI(NV),NV=1,NVIEW)
1006  FORMAT(//'  Angles of view'/4(5X,10F10.1/))
      WRITE(6,1007) DELBR,CANG
1007  FORMAT(//'  Layer line sampling ',F10.6,'  rec Angstroms',
     1        /'  Helical repeat      ',F10.1,'  Angstroms'/)
C
      DR=2.*PI*DELBR*RADIUS
      DO I=1,32
        TOTF(I)=0.
        TOTF0(I)=0.
        TOTFR(I)=0.
        TOTFR0(I)=0.
        DO J=1,20
          RESID(I,J)=0.
          RESID0(I,J)=0.
          RESIDR(I,J)=0.
          RESIDR0(I,J)=0.
        ENDDO
      ENDDO
      DO J=1,20
        TOTLL(J)=0.
        TOTLLR(J)=0.
      ENDDO
C
C
C     Loop over all layer lines
C
      FIRST=.TRUE.
C     Logical FIRST is used to distinguish between two halves a of
C     layer line with meridional J0.   Note that HLXDUMP should have
C     been run with MERID=0, so that each "side" of helical data
C     contains both halves of any meridional layer line.
      DO 100 LL=1,LLMAX
      READ(5,*) NL,NORD,(IORD(NO),NO=1,NORD)
C     Check Bessel orders specified in order of increasing modulus
      IF(NORD.GE.2) THEN
        DO NO=2,NORD
          IF(IABS(IORD(NO)).LT.IABS(IORD(NO-1))) THEN
            WRITE(6,1008)
1008        FORMAT(////'   ERROR  Bessel functions on a given layer
     1 line must be specified in order of increasing modulus'////)
            STOP
          ENDIF
        ENDDO
      ENDIF
      LLNO(LL)=NL
      NPOINT(LL)=0
      WRITE(6,1009) NL,NORD,(IORD(NO),NO=1,NORD)
1009  FORMAT(//////'**********************************************'
     1        //'  LAYER LINE NO. ',I10,
     2         /'  No. of Bessels ',I10,
     3         /'  Bessel orders  ',10I10/)
      READ(5,*) (FITWT(NV),NV=1,NVIEW)
      WRITE(6,1010) (FITWT(NV),NV=1,NVIEW)
1010  FORMAT(//'  Weights for different views from HLXFIT'/
     1 5X,2(/16F7.2))
C
C     Read in given layer line data for each view
      WRITE(6,1011)
1011  FORMAT(//'  Data from disc :'/)
      IR1=200
      IR2=0
      DO IR=1,200
        NOV(IR)=0
      ENDDO
C     Clear arrays
      CALL ZERO(F,25600)
      CALL ZERO(PSI,25600)
      DO 200 NV=1,NVIEW
      N2=NREC(NV)
C!!!!      READ(2'N2) NLL,JBES,IRMIN,IRMAX,(F(IR,NV),PSI(IR,NV),
C!!!!     1 IR=IRMIN,IRMAX)
      read(2,rec=n2) nll,jbes,irmin,irmax,(f(ir,nv),psi(ir,nv),
     1 ir=irmin,irmax)
      WRITE(6,1020) NV,NLL,JBES,IRMIN,IRMAX,N2
1020  FORMAT('  View no',I5,' Layer line',I5,' Bessel order',I5,
     1' IRMIN',I5,' IRMAX',I5,'  N2',I5)
C     Check layer line and Bessel order agree
      IF((NLL.NE.LLNO(LL)).OR.(JBES.NE.IORD(1))) THEN
        WRITE(6,1025)
1025    FORMAT(////'  ERROR  Layer line no or Bessel order in control
     1 data does not agree with that in direct access area'////)
        STOP
      ENDIF
C
      NREC(NV)=N2
      MINR(NV)=(IRMIN-1)*RSCAL(NV)+1.
      IF(RSCAL(NV).GT.1.) MINR(NV)=MINR(NV)+1
      MAXR(NV)=(IRMAX-1)*RSCAL(NV)
      IF(MINR(NV).LT.IR1) IR1=MINR(NV)
      IF(MAXR(NV).GT.IR2) IR2=MAXR(NV)
      NPOINT(LL)=NPOINT(LL)+MAXR(NV)-MINR(NV)+1
C     Count no. of views contributing at each radius
      DO IR=MINR(NV),MAXR(NV)
       NOV(IR)=NOV(IR)+1
      ENDDO
200   CONTINUE
C
C     Solve Bessels in bands of increasing overlap
C     Increase no of orders by 1 unless J0 layer line, then 2
      NSTEP=1
      IF(IORD(1).EQ.0) NSTEP=2
      DO 210 NO=1,NORD,NSTEP
C     Set radial range for next overlap band
      IF(NO.EQ.1) THEN
        JR1=IR1
          ELSE
        JR1=JR2+1
      ENDIF
      IF(NO.EQ.NORD) THEN
        JR2=IR2
          ELSE
        JR2=(IABS(IORD(NO+1))-2)/DR
      ENDIF
C     Record radial start of this Bessel order
      JSTART(NO)=JR1
      IF(NSTEP.EQ.2.AND.NO.NE.1) JSTART(NO-1)=JR1
      WRITE(6,1030) JR1,JR2,(IORD(N),N=1,NO)
1030  FORMAT(/'  Radial range',I8,'  to',I8,'    Bessel orders',10I7)
      DO JR=JR1,JR2
       NBES(JR)=NO
      ENDDO
210   CONTINUE
C     Loop over all radii doing separation in bands as far as possible.
C     Particular radial band has same no. of contributing views and
C     same no. of overlapping Bessels.
      NBANDS=1
      IR11=IR1+1
      NBSTRT(1)=IR1
      DO 220 IR=IR11,IR2
      IF(NBES(IR).EQ.NBES(IR-1).AND.NOV(IR).EQ.NOV(IR-1)) GO TO 220
      NBANDS=NBANDS+1
      NBSTRT(NBANDS)=IR
220   CONTINUE
C
      WRITE(6,1040) IR1,IR2,(NOV(IR),IR=IR1,IR2)
1040  FORMAT(//'  No. of views at each radius from ',I5,'  to',I5,
     1 /10(/5X,20I5))
      WRITE(6,1041) (NBES(IR),IR=IR1,IR2)
1041  FORMAT(//'  No. of bessels at each radius'/10(/5X,20I5))
      WRITE(6,1042) NBANDS,(NBSTRT(NB),NB=1,NBANDS)
1042  FORMAT(//'  No of bands',I10/'  Radial starts of bands'/
     1  5(/5X,10I5))
C
      DO 250 NB=1,NBANDS
      JR1=NBSTRT(NB)
      IF(NB.EQ.NBANDS) THEN
          JR2=IR2
            ELSE
          JR2=NBSTRT(NB+1)-1
      ENDIF
C
C     Set observational matrix
C     Convention is that for positive Bessel orders NEAR side
C     has PHI=270 , FAR side has PHI=90, so that for negative
C     Bessel orders must add 180 to PHI before using.
      NV0=0
      DO 260 NV=1,NVIEW
      IF(JR1.LT.MINR(NV).OR.JR2.GT.MAXR(NV)) GO TO 260
      NV0=NV0+1
      ANG=PHI(NV)*CRAD
      DO 261 NO=1,NBES(JR1)
      IF(IORD(1).GE.0.AND.FIRST) THEN
          ANGN=IORD(NO)*ANG
             ELSE
          ANGN=IORD(NO)*(ANG+PI)
      ENDIF
      C=COS(ANGN)
      S=SIN(ANGN)
      CSMAT(NV0,NO)=C
      CSMAT(NOV(JR1)+NV0,NO)=S
      CSMAT(NV0,NBES(JR1)+NO)=-S
261   CSMAT(NOV(JR1)+NV0,NBES(JR1)+NO)=C
260   CONTINUE
C
      NRHS=JR2-JR1+1
      NVIEW2=2*NOV(JR1)
      NO2=2*NBES(JR1)
C
C     Set RHS vectors for this band of radii, interpolating l.l. data
      DO 270 JR=JR1,JR2
      KR=JR-JR1+1
C     Count no. of views at this radius in NV0
      NV0=0
      DO 280 NV=1,NVIEW
      IF(JR.LT.MINR(NV).OR.JR.GT.MAXR(NV)) GO TO 280
      NV0=NV0+1
      R=(JR-1)/RSCAL(NV)+1.
      IR=R
      RFR=R-IR
      RBAR=1.-RFR
      A1=F(IR,NV)*COS(PSI(IR,NV)*CRAD)
      B1=F(IR,NV)*SIN(PSI(IR,NV)*CRAD)
      A2=F(IR+1,NV)*COS(PSI(IR+1,NV)*CRAD)
      B2=F(IR+1,NV)*SIN(PSI(IR+1,NV)*CRAD)
      RHS(NV0,KR)=FITWT(NV)*(A1*RBAR+A2*RFR)
      RHS(NOV(JR)+NV0,KR)=FITWT(NV)*(B1*RBAR+B2*RFR)
C     Accumulate total intensity and power by view and layer line
      FSQ=RHS(NV0,KR)**2+RHS(NOV(JR)+NV0,KR)**2
      FSQR=(JR-1)*FSQ
      TOTF(NV)=TOTF(NV)+FSQ
      TOTFR(NV)=TOTFR(NV)+FSQR
      TOTLL(LL)=TOTLL(LL)+FSQ
      TOTLLR(LL)=TOTLLR(LL)+FSQR
C     Totals without equator
      IF(NL.EQ.0) GO TO 280
      TOTF0(NV)=TOTF0(NV)+FSQ
      TOTFR0(NV)=TOTFR0(NV)+FSQR
C
280   CONTINUE
270   CONTINUE
C
C     Solve normal equations
      CALL LINFIL(CSMAT,RHS,SOLN,ICSDIM,NVIEW2,NO2,NRHS,FILT,NFILT,
     1   ATA,ATB,VEC,VAL,IPOINT,W)
      WRITE(6,1050) NB,NFILT,NO2,(VAL(IPOINT(J)),J=1,NO2)
1050  FORMAT(/'  Eigenvalues of normal matrix for band',I5,
     1       /I5,' out of',I5,' are above filter level'/
     2       /2(5X,10E12.4/))
C
C     Convert G' to G then to "F" and then amp and phase
      DO 300 JR=JR1,JR2
      KR=JR-JR1+1
      DO 305 N=1,NBES(JR1)
      NSIGN=IORD(N)/2
      IF(MOD(IORD(N),2).NE.0)  NSIGN=(IORD(N)-1)/2
      SIGN=1.
      IF(MOD(NSIGN,2).NE.0) SIGN=-1.
      IF(MOD(IORD(N),2).EQ.0) THEN
          A=SIGN*SOLN(N,KR)
          B=SIGN*SOLN(NBES(JR1)+N,KR)
             ELSE
          A=SIGN*SOLN(NBES(JR1)+N,KR)
          B=-SIGN*SOLN(N,KR)
      ENDIF
      IF(IORD(N).LT.0.AND.MOD(IORD(N),2).NE.0) THEN
         A=-A
         B=-B
      ENDIF
      FOUT(JR,N)=SQRT(A*A+B*B)
      PHIOUT(JR,N)=0.
      IF(FOUT(JR,N).NE.0.) PHIOUT(JR,N)=ATAN2(B,A)/CRAD
305   CONTINUE
C
C     Calculate residuals
      R=JR-1
      NV0=0
      DO 310 NV=1,NVIEW
      IF(JR1.LT.MINR(NV).OR.JR2.GT.MAXR(NV)) GO TO 310
      NV0=NV0+1
      DELSQ=RHS(NV0,KR)**2+RHS(NV0+NOV(JR1),KR)**2
      RESID(NV,LL)=RESID(NV,LL)+DELSQ
      RESIDR(NV,LL)=RESIDR(NV,LL)+R*DELSQ
C     Residuals without equator
      IF(NL.EQ.0) GO TO 310
      RESID0(NV,LL)=RESID0(NV,LL)+DELSQ
      RESIDR0(NV,LL)=RESIDR0(NV,LL)+R*DELSQ
310   CONTINUE
C
300   CONTINUE
C
250   CONTINUE
C
C     Output separated Bessels in HLXFOUR format
      DO 340 IR=IR1,IR2
340   ROUT(IR)=(IR-1)*DELBR
      SCALE=1.
      DO 350 NO=1,NORD
C     Dont dump negative Bessels on equator
      IF(NL.EQ.0.AND.IORD(NO).LT.0) GO TO 350
      WRITE(3,1060) TITLE,NL,SCALE,IORD(NO),NL
1060  FORMAT(10A4,'LL',I3,5X,F10.3,2I5)
      FMAX=0.
      DO 360 IR=JSTART(NO),IR2
      WRITE(3,1061) ROUT(IR),FOUT(IR,NO),PHIOUT(IR,NO)
1061  FORMAT(3E10.3)
      INDR=IR-JSTART(NO)+1
      FLLG(INDR)=FOUT(IR,NO)
      PHILLG(INDR)=PHIOUT(IR,NO)
      RLLG(INDR)=ROUT(IR)
      IF(FLLG(INDR).GT.FMAX) FMAX=FLLG(INDR)
360   CONTINUE
      RR=0.
      WRITE(3,1061) RR,RR,RR
C
      IF(IGRAPH.EQ.0) GO TO 350
C     Graphs of separated Bessels
      LAST=.FALSE.
      IF(LL.EQ.LLMAX.AND.NO.EQ.NORD) LAST=.TRUE.
      SPLL=NL/CANG
      NRAD=IR2-JSTART(NO)+1
      CALL LLGRAPH(FLLG,PHILLG,RLLG,0.,0.1,FMAX,NL,SPLL,IORD(NO),
     1  NRAD,TITLE,LAST)
C
350   CONTINUE
C
      IF(NL.NE.0.AND.IORD(1).EQ.0.AND.FIRST) THEN
            FIRST=.FALSE.
              ELSE
            FIRST=.TRUE.
      ENDIF
100   CONTINUE
      CLOSE(3)
C
C     Output residuals
      WRITE(6,1070)
1070  FORMAT('1'///' Residuals for different views with equator'//
     1'  View  Intensity     Total       Fract.      Power       Total
     2       Fract.'/
     3'   no.    resid.    intensity     resid.      resid.      power
     4       power'//)
      DO 400 NV=1,NVIEW
      RES(NV)=0.
      RESR(NV)=0.
      DO 410 LL=1,LLMAX
      RES(NV)=RES(NV)+RESID(NV,LL)
410   RESR(NV)=RESR(NV)+RESIDR(NV,LL)
      FRAC=RES(NV)/TOTF(NV)
      FRACR=RESR(NV)/TOTFR(NV)
      WRITE(6,1071) NV,RES(NV),TOTF(NV),FRAC,RESR(NV),TOTFR(NV),FRACR
1071  FORMAT(I5,6E12.3)
400   CONTINUE
C
      WRITE(6,1072)
1072  FORMAT(/////' Residuals for different views without equator'//
     1'  View  Intensity     Total       Fract.      Power       Total
     2       Fract.'/
     3'   no.    resid.    intensity     resid.      resid.      power
     4       power'//)
      DO 420 NV=1,NVIEW
      RES(NV)=0.
      RESR(NV)=0.
      DO 430 LL=1,LLMAX
      RES(NV)=RES(NV)+RESID0(NV,LL)
430   RESR(NV)=RESR(NV)+RESIDR0(NV,LL)
      FRAC=RES(NV)/TOTF0(NV)
      FRACR=RESR(NV)/TOTFR0(NV)
      WRITE(6,1073) NV,RES(NV),TOTF0(NV),FRAC,RESR(NV),TOTFR0(NV),
     1 FRACR
1073  FORMAT(I5,6E12.3)
420   CONTINUE
C
      WRITE(6,1074)
1074  FORMAT(/////' Residuals for different layer lines'//
     1'  l.l.  Intensity     Total       Fract.      Power       Total
     2       Fract.        RMS'/
     3'   no.    resid.    intensity     resid.      resid.      power
     4       power       delta F'//)
      DO 440 LL=1,LLMAX
      RES(LL)=0.
      RESR(LL)=0.
      DO 450 NV=1,NVIEW
      RES(LL)=RES(LL)+RESID(NV,LL)
450   RESR(LL)=RESR(LL)+RESIDR(NV,LL)
      FRAC=RES(LL)/TOTLL(LL)
      FRACR=RESR(LL)/TOTLLR(LL)
      RMSF=SQRT(RES(LL)/NPOINT(LL))
      WRITE(6,1075) LLNO(LL),RES(LL),TOTLL(LL),FRAC,RESR(LL),
     1 TOTLLR(LL),FRACR,RMSF
1075  FORMAT(I5,7E12.3)
440   CONTINUE
      WRITE(6,1076)
1076  FORMAT(/////)
      STOP
      END
C*LINFIL.FOR*****************************************************************
C     LINEAR LEAST SQUARES FILTERING PACKAGE
C     Solves rectangular set of linear equations by least squares, using
C     filter level set by user to avoid noise amplification.
C     Given observational equations   Ax(j)=b(j)    j=1,NRHS
C     computes solutions     x(j)=VZ(1/lam)V'A'b(j)    for arbitrary
C     number NRHS of righthandside vectors b(j).
C     Where ' denotes transposed matrix, (1/lam) contains inverses of
C     eigenvalues of normal matrix A'A, Z=diag(1,1,1,...0,0,0,) is
C     filter matrix which cuts out inverses of eigenvalues below filter
C     level.
C
C*****CALL LINFIL(A,B,C,IABDIM,NOBS,NPARM,NRHS,FILT,NFILT,ATA,ATB,VEC,VAL,
C      IPOINT,W)
C        A(I,J) is observational matrix, I=1,NOBS, J=1,NPARM
C        NOBS is number of observations
C        NPARM is number of parameters to be fitted
C        B(I,K) is matrix of RHS vectors  I=1,NOBS  K=1,NRHS containing
C            NRHS data vectors all to be fitted by single inversion of
C            normal matrix  Contains residuals on return.
C        C(I,K) returns solutions
C        IABDIM is first dimension of arrays A B and C in calling program,
C            which may be greater than NOBS
C        FILT is filter level.  Eigenvectors with eigenvalue
C            LAMBDA<FILT*LABDAMAX are not used in fitting.  Appropriate
C            setting of FILT depends on noise level in data but 1.E-2
C            may often be OK.
C        NFILT is returned as number of eigenvalues above filter level.
C        ATA(I,J) ATB(I,K) VEC(I,J) VAL(I) IPOINT(I)  I,J=1,NPARM K=1,NRHS
C            are arrays used for storing intermediate results and must be
C            dimensioned sufficiently large in calling program.
C        W(I)  is work area which must be dimensioned greater than
C            5*NPARM in calling program.
C
C        Uses subroutines EA06C for computing eigensystem and SHLSRT
C            for sorting eigenvalues into descending order.
C
C                 Version 1.01      22-Mar-82      RAC    FOR VAX
C
C************************************************************************
C
      SUBROUTINE LINFIL(A,B,C,IABDIM,NOBS,NPARM,NRHS,FILT,NFILT,ATA,
     1ATB,VEC,VAL,IPOINT,W)
      DIMENSION A(IABDIM,NPARM),B(IABDIM,NRHS),C(IABDIM,NRHS),
     1ATA(NPARM,NPARM),ATB(NPARM,NRHS),VEC(NPARM,NPARM),
     2VAL(NPARM),IPOINT(1),W(1)
C
C     Construct lower triangle of normal matrix A'A used by EA06C
      DO 100 J=1,NPARM
      DO 100 I=J,NPARM
      ATA(I,J)=0.
      DO 100 K=1,NOBS
100   ATA(I,J)=ATA(I,J)+A(K,I)*A(K,J)
C
C     Construct modified RHS  A'b(j)
      DO 110 J=1,NRHS
      DO 110 I=1,NPARM
      ATB(I,J)=0.
      DO 110 K=1,NOBS
110   ATB(I,J)=ATB(I,J)+A(K,I)*B(K,J)
C
C     Compute eigenvalues and eigenvectors of normal matrix
      CALL EA06C(ATA,VAL,VEC,NPARM,NPARM,NPARM,W)
C
C     Write out eigenvectors
C      WRITE(6,1000)
C1000  FORMAT(//'  Eigenvectors across page')
C      DO 900 I=1,NPARM
C900   WRITE(6,1010) (VEC(J,I),J=1,NPARM)
C1010  FORMAT(5X,10E12.4)
C
C     Sort eigenvalues into descending order using pointers
      CALL SHLSRT(VAL,NPARM,IPOINT,-1)
C
C     Count how many eigenvalues above filter level
      NFILT=1
      DO 120 N=2,NPARM
      IF(VAL(IPOINT(N)).LT.FILT*VAL(IPOINT(1))) GO TO 130
120   NFILT=NFILT+1
C
C     Solve using only NFILT eigenvecs corresponding to largest evals
C     First multiply by VEC', storing result in C
130   DO 140 NR=1,NRHS
      DO 140 N=1,NFILT
      I=IPOINT(N)
      C(N,NR)=0.
      DO 140 J=1,NPARM
140   C(N,NR)=C(N,NR)+VEC(J,I)*ATB(J,NR)
C
C     Now weight with NFILT inverse eigenvalues, storing result in ATB
      DO 150 N=1,NFILT
      WT=1./VAL(IPOINT(N))
      DO 150 NR=1,NRHS
150   ATB(N,NR)=C(N,NR)*WT
C
C     Dump eigensolutions
C      WRITE(6,1020)
C1020  FORMAT(//' Eigensolutions across page')
C      DO 910 NR=1,NRHS
C910   WRITE(6,1030) (ATB(N,NR),N=1,NFILT)
C1030  FORMAT(5X,10E12.4)
C
C     Now mult by VEC, returning final parameters in C
      DO 160 NR=1,NRHS
      DO 160 J=1,NPARM
      C(J,NR)=0.
      DO 160 N=1,NFILT
      I=IPOINT(N)
160   C(J,NR)=C(J,NR)+VEC(J,I)*ATB(N,NR)
C
C     Now calculate residuals in B
      DO 170 NR=1,NRHS
      DO 170 K=1,NOBS
      DO 170 J=1,NPARM
170   B(K,NR)=B(K,NR)-A(K,J)*C(J,NR)
C
      RETURN
      END
      SUBROUTINE SHLSRT(KEY,N,IPOINT,INCDEC)
C
C     SHELL SORT
C     REFERENCES:  D.L. SHELL, CACM 2, 32 (JULY 1959)
C                  D.E. KNUTH, TAOCP III, SECT. 5.2.1
C
C     CALLING SEQUENCE:
C
C     KEY    IS AN ARRAY OF KEYS ON WHICH TO SORT
C     N      IS THE NUMBER OF ITEMS
C     IPOINT IS THE ARRAY OF POINTERS
C            (ONLY THE POINTERS WILL MOVE)
C     INCDEC .GE. 0 FOR SORTING INTO INCREASING ORDER;
C            .LT. 0 FOR SORTING INTO DECREASING ORDER
C
C
      REAL*4 KEY,K
      INTEGER H,S,T
      DIMENSION KEY(1),IPOINT(1)
C
C     NEXT STATEMENT JUST IN CASE N .EQ. 1
      IPOINT(1) = 1
C
C     CHECK N
C
      IF (N - 1) 11,11,1
C
C
C     INITIALIZE POINTER ARRAY
C
C     (NOTE THAT N MUST BE .GT. 1 IF WE GOT HERE)
    1 DO 2 I = 2,N
    2 IPOINT(I) = I
C
C     CHOICE OF SEQUENCE OF INCREMENTS SUGGESTED
C     BY KNUTH III, EQ. 8, P. 95.   HIS FORMULA
C     IS EQUIVALENT TO:
C
C            H(S) = (3**S - 1)/2
C            INITIAL VALUE OF S IS MINIMAL INTEGER
C              SUCH THAT H(S+2) .GE. N
C
C
C     SMAX = (ALOG(2N + 1)/ALOG(3)) - 2 + 1
      S = INT( (ALOG(FLOAT(2*N+1))/1.09861229) - 0.95 )
      S = MAX0(S,1)
C
      H = (3**S - 1)/2
C
C
      DO 7 T = 1,S
C
      JMIN = H + 1
      DO 6 J = JMIN,N
C
      I = J - H
      JJ = IPOINT(J)
      K = KEY(JJ)
      IPT = IPOINT(J)
C
    3 II = IPOINT(I)
      IF (K - KEY(II)) 4,4,5
C
    4 IPLUSH = I + H
      IPOINT(IPLUSH) = IPOINT(I)
      I = I - H
      IF (I) 5,5,3
C
    5 IPLUSH = I + H
      IPOINT(IPLUSH) = IPT
C
    6 CONTINUE
C
C     CHANGE INCREMENT
C
      IF (H - 1) 8,8,7
    7 H = (H-1)/3
C
C
C      CHECK INCDEC: IF NEGATIVE, SWITCH POINTER ARRAY
C
    8 IF (INCDEC) 9,11,11
C
    9 M = N/2
C     NP1MI = N + 1 - I
      NP1MI = N
      DO 10 I = 1,M
      NTEMP = IPOINT(I)
      IPOINT(I) = IPOINT(NP1MI)
      IPOINT(NP1MI) = NTEMP
   10 NP1MI = NP1MI - 1
C
   11 RETURN
      END
