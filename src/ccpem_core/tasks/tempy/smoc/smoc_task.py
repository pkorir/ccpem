#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.tempy.smoc import smoc_process

class SMOC(task_utils.CCPEMTask):
    '''
    CCPEM / TEMPy difference map wrapper.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='TEMPySMOC',
        author='M. Topf, A. Joseph',
        version='1.1',
        description=(
            'SMOC local scoring using TEMPy library'),
        short_description=(
            '[BETA TEST] Local fragment Score based on Manders\' Overlap '
            'Coefficient'),
        documentation_link='http://topf-group.ismb.lon.ac.uk/TEMPY.html',
        references=None)

    def __init__(self,
                 task_info=task_info,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        command = ['ccpem-python', os.path.realpath(smoc_process.__file__)]
        #
        super(SMOC, self).__init__(command=command,
                                   task_info=task_info,
                                   database_path=database_path,
                                   args=args,
                                   args_json=args_json,
                                   pipeline=pipeline,
                                   job_location=job_location,
                                   parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        map_path = parser.add_argument_group()
        map_path.add_argument(
            '-map_path',
            '--map_path',
            help='Input map (mrc format)',
            metavar='Input map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-input_pdbs',
            '--input_pdbs',
            help='Input pdb(s)',
            metavar='Input PDB',
            type=str,
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-input_pdb_chains',
            '--input_pdb_chains',
            help='Input PDB chain(s)',
            metavar='Input PDB chain selections',
            type=str,
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-map_resolution',
            '--map_resolution',
            help=('Resolution of map (Angstrom). Minimum recommended '
                  'resolution 7.5 Angstrom'),
            metavar='Resolution map',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-auto_fragment_length',
            '--auto_fragment_length',
            help='''Automatically set number of residues in averaging window''',
            metavar='Auto window',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-fragment_length',
            '--fragment_length',
            help='''Number of resiudes in averaging window''',
            metavar='Fragment length',
            type=int,
            default=9)
        #
        return parser

    def run_pipeline(self, job_id=None, db_inject=None):
        
        # Generate process
        self.smoc_wrapper = SMOCWrapper(
            command=self.command,
            job_location=self.job_location,
            name='SMOC score')

        pl = [[self.smoc_wrapper.process]]
        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

class SMOCWrapper(object):
    '''
    Wrapper for TEMPy SMOC process.
    '''
    def __init__(self,
                 command,
                 job_location,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__

        # Set args
        self.args = os.path.join(self.job_location,
                                 'args.json')

        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)

def main():
    '''
    Run task
    '''
    task_utils.command_line_task_launch(
        task=SMOC)

if __name__ == '__main__':
    main()
