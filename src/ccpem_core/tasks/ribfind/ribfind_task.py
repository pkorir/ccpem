#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_progs.ribfind import run_ribfind
from ccpem_core import settings
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils


class OnFinish(process_manager.CCPEMProcessCustomFinish):
    def __init__(self,
                 ribfind_xml_path,
                 job_location):
        super(OnFinish, self).__init__()
        self.ribfind_xml_path = ribfind_xml_path
        self.job_location = job_location

    def on_finish(self, parent_process=None):
        import xml.etree.ElementTree
        import shutil
        import glob
        doc = xml.etree.ElementTree.parse(self.ribfind_xml_path).getroot()
        value = doc.find('preferredSolution')
        pref_sol = value.text
        job_data_path = os.path.dirname(self.ribfind_xml_path)
        pref_file = glob.glob(job_data_path + '/*denclust_' + pref_sol)[0]
        if os.path.exists(pref_file):
            shutil.copy(pref_file,
                        self.job_location + '/rigid.txt')
        if parent_process is not None:
            parent_process.set_metadata(key='selected_cutoff',
                                        value=int(pref_sol))


class Ribfind(task_utils.CCPEMTask):
    '''
    CCPEM Ribfind task.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='Ribfind',
        author='Pandurangan, AP, Topf, M',
        version='1.0',
        description=(
            'Finds rigid bodies in protein structures'),
        short_description=(
            'Finds rigid bodies in protein structures'),
        documentation_link='http://ribfind.ismb.lon.ac.uk/documentation.html',
        references=None)

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 task_info=task_info,
                 job_location=None,
                 parent=None):
        command = ['ccpem-python', os.path.realpath(run_ribfind.__file__)]
        super(Ribfind, self).__init__(command=command,
                                      task_info=task_info,
                                      database_path=database_path,
                                      args=args,
                                      args_json=args_json,
                                      pipeline=pipeline,
                                      job_location=job_location,
                                      parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        input_pdb = parser.add_argument_group()
        input_pdb.add_argument(
            '-input_pdb',
            '--input_pdb',
            help='Input coordinate file (pdb format)',
            metavar='Input PDB',
            type=str,
            default=None)
        #
        output_dssp = parser.add_argument_group()
        output_dssp.add_argument(
            '-output_dssp',
            '--output_dssp',
            help='File name for dssp output',
            metavar='Output dssp',
            type=str,
            default=None)
        #
        contact_distance = parser.add_argument_group()
        contact_distance.add_argument(
            '-contact_distance',
            '--contact_distance',
            help='Distance cut-off for contacts (Angstrom)',
            metavar='Contact dist.',
            type=float,
            default=6.5)
        return parser

    def job_dssp(self):
        dssp_command = settings.which('mkdssp')
        args = [self.args.input_pdb.value, self.args.output_dssp.value]
        self.dssp_process = process_manager.CCPEMProcess(
            name='DSSP',
            command=dssp_command,
            args=args,
            location=self.job_location,
            stdin=None,
            on_finish_custom=None)

    def job_ribfind(self):
        assert self.args.jsonfile is not None
        args = [self.args.jsonfile]
        ribfind_xml_path = os.path.join(self.args.job_location.value,
                                        'job_data/ribfind.xml')
        on_finish = OnFinish(ribfind_xml_path=ribfind_xml_path,
                             job_location=self.job_location)
        self.ribfind_process = process_manager.CCPEMProcess(
            name='Ribfind',
            command=self.command,
            args=args,
            location=self.job_location,
            stdin=None,
            on_finish_custom=on_finish)

    def run_pipeline(self, job_id=None, db_inject=None):
        # Directory to store intermediate data
        self.job_data = os.path.join(self.job_location, 'job_data')
        # Generate processes
        # dssp
        self.job_dssp()
        pl = [[self.dssp_process]]
        # ribfind
        self.job_ribfind()
        pl.append([self.ribfind_process])
        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

    def validate_args(self):
        '''
        Check arguments before running job.
        '''
#         return True
        args_correct = True
        warnings = ''
        # Check input PDB
        if self.args.output_dssp.value is None:
            self.args.output_dssp.value = os.path.basename(
                self.args.input_pdb.value) + '.dssp'
        self.args.output_dssp.value = os.path.join(
            self.job_location,
            os.path.basename(self.args.output_dssp.value))
        if self.args.contact_distance.value <= 0:
            warnings += \
                '\nContact distance not greater than zero : {0}'.format(
                    self.args.contact_distance.value)
            args_correct = False
        # Display warnings in parent GUI
        if warnings != '':
            args_correct = False
            print warnings
        return args_correct


def main():
    '''
    Run task
    '''
    task_utils.command_line_task_launch(
        task=Ribfind)

if __name__ == '__main__':
    main()
