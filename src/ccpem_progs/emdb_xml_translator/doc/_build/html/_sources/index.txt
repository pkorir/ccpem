.. emdbXMLTranslator documentation master file, created by
   sphinx-quickstart on Wed Jan  6 20:48:25 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README

Contents:
---------

.. toctree::
   :maxdepth: 4

   emdbXMLTranslator

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

