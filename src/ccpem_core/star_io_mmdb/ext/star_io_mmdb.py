# This file was automatically generated by SWIG (http://www.swig.org).
# Version 2.0.11
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.





from sys import version_info
if version_info >= (2,6,0):
    def swig_import_helper():
        from os.path import dirname
        import imp
        fp = None
        try:
            fp, pathname, description = imp.find_module('_star_io_mmdb', [dirname(__file__)])
        except ImportError:
            import _star_io_mmdb
            return _star_io_mmdb
        if fp is not None:
            try:
                _mod = imp.load_module('_star_io_mmdb', fp, pathname, description)
            finally:
                fp.close()
            return _mod
    _star_io_mmdb = swig_import_helper()
    del swig_import_helper
else:
    import _star_io_mmdb
del version_info
try:
    _swig_property = property
except NameError:
    pass # Python < 2.2 doesn't have 'property'.
def _swig_setattr_nondynamic(self,class_type,name,value,static=1):
    if (name == "thisown"): return self.this.own(value)
    if (name == "this"):
        if type(value).__name__ == 'SwigPyObject':
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name,None)
    if method: return method(self,value)
    if (not static):
        self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)

def _swig_setattr(self,class_type,name,value):
    return _swig_setattr_nondynamic(self,class_type,name,value,0)

def _swig_getattr(self,class_type,name):
    if (name == "thisown"): return self.this.own()
    method = class_type.__swig_getmethods__.get(name,None)
    if method: return method(self)
    raise AttributeError(name)

def _swig_repr(self):
    try: strthis = "proxy of " + self.this.__repr__()
    except: strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)

try:
    _object = object
    _newclass = 1
except AttributeError:
    class _object : pass
    _newclass = 0


class SwigPyIterator(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, SwigPyIterator, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, SwigPyIterator, name)
    def __init__(self, *args, **kwargs): raise AttributeError("No constructor defined - class is abstract")
    __repr__ = _swig_repr
    __swig_destroy__ = _star_io_mmdb.delete_SwigPyIterator
    __del__ = lambda self : None;
    def value(self): return _star_io_mmdb.SwigPyIterator_value(self)
    def incr(self, n=1): return _star_io_mmdb.SwigPyIterator_incr(self, n)
    def decr(self, n=1): return _star_io_mmdb.SwigPyIterator_decr(self, n)
    def distance(self, *args): return _star_io_mmdb.SwigPyIterator_distance(self, *args)
    def equal(self, *args): return _star_io_mmdb.SwigPyIterator_equal(self, *args)
    def copy(self): return _star_io_mmdb.SwigPyIterator_copy(self)
    def next(self): return _star_io_mmdb.SwigPyIterator_next(self)
    def __next__(self): return _star_io_mmdb.SwigPyIterator___next__(self)
    def previous(self): return _star_io_mmdb.SwigPyIterator_previous(self)
    def advance(self, *args): return _star_io_mmdb.SwigPyIterator_advance(self, *args)
    def __eq__(self, *args): return _star_io_mmdb.SwigPyIterator___eq__(self, *args)
    def __ne__(self, *args): return _star_io_mmdb.SwigPyIterator___ne__(self, *args)
    def __iadd__(self, *args): return _star_io_mmdb.SwigPyIterator___iadd__(self, *args)
    def __isub__(self, *args): return _star_io_mmdb.SwigPyIterator___isub__(self, *args)
    def __add__(self, *args): return _star_io_mmdb.SwigPyIterator___add__(self, *args)
    def __sub__(self, *args): return _star_io_mmdb.SwigPyIterator___sub__(self, *args)
    def __iter__(self): return self
SwigPyIterator_swigregister = _star_io_mmdb.SwigPyIterator_swigregister
SwigPyIterator_swigregister(SwigPyIterator)

class DoubleVector(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, DoubleVector, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, DoubleVector, name)
    __repr__ = _swig_repr
    def iterator(self): return _star_io_mmdb.DoubleVector_iterator(self)
    def __iter__(self): return self.iterator()
    def __nonzero__(self): return _star_io_mmdb.DoubleVector___nonzero__(self)
    def __bool__(self): return _star_io_mmdb.DoubleVector___bool__(self)
    def __len__(self): return _star_io_mmdb.DoubleVector___len__(self)
    def pop(self): return _star_io_mmdb.DoubleVector_pop(self)
    def __getslice__(self, *args): return _star_io_mmdb.DoubleVector___getslice__(self, *args)
    def __setslice__(self, *args): return _star_io_mmdb.DoubleVector___setslice__(self, *args)
    def __delslice__(self, *args): return _star_io_mmdb.DoubleVector___delslice__(self, *args)
    def __delitem__(self, *args): return _star_io_mmdb.DoubleVector___delitem__(self, *args)
    def __getitem__(self, *args): return _star_io_mmdb.DoubleVector___getitem__(self, *args)
    def __setitem__(self, *args): return _star_io_mmdb.DoubleVector___setitem__(self, *args)
    def append(self, *args): return _star_io_mmdb.DoubleVector_append(self, *args)
    def empty(self): return _star_io_mmdb.DoubleVector_empty(self)
    def size(self): return _star_io_mmdb.DoubleVector_size(self)
    def clear(self): return _star_io_mmdb.DoubleVector_clear(self)
    def swap(self, *args): return _star_io_mmdb.DoubleVector_swap(self, *args)
    def get_allocator(self): return _star_io_mmdb.DoubleVector_get_allocator(self)
    def begin(self): return _star_io_mmdb.DoubleVector_begin(self)
    def end(self): return _star_io_mmdb.DoubleVector_end(self)
    def rbegin(self): return _star_io_mmdb.DoubleVector_rbegin(self)
    def rend(self): return _star_io_mmdb.DoubleVector_rend(self)
    def pop_back(self): return _star_io_mmdb.DoubleVector_pop_back(self)
    def erase(self, *args): return _star_io_mmdb.DoubleVector_erase(self, *args)
    def __init__(self, *args): 
        this = _star_io_mmdb.new_DoubleVector(*args)
        try: self.this.append(this)
        except: self.this = this
    def push_back(self, *args): return _star_io_mmdb.DoubleVector_push_back(self, *args)
    def front(self): return _star_io_mmdb.DoubleVector_front(self)
    def back(self): return _star_io_mmdb.DoubleVector_back(self)
    def assign(self, *args): return _star_io_mmdb.DoubleVector_assign(self, *args)
    def resize(self, *args): return _star_io_mmdb.DoubleVector_resize(self, *args)
    def insert(self, *args): return _star_io_mmdb.DoubleVector_insert(self, *args)
    def reserve(self, *args): return _star_io_mmdb.DoubleVector_reserve(self, *args)
    def capacity(self): return _star_io_mmdb.DoubleVector_capacity(self)
    __swig_destroy__ = _star_io_mmdb.delete_DoubleVector
    __del__ = lambda self : None;
DoubleVector_swigregister = _star_io_mmdb.DoubleVector_swigregister
DoubleVector_swigregister(DoubleVector)

class IntVector(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, IntVector, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, IntVector, name)
    __repr__ = _swig_repr
    def iterator(self): return _star_io_mmdb.IntVector_iterator(self)
    def __iter__(self): return self.iterator()
    def __nonzero__(self): return _star_io_mmdb.IntVector___nonzero__(self)
    def __bool__(self): return _star_io_mmdb.IntVector___bool__(self)
    def __len__(self): return _star_io_mmdb.IntVector___len__(self)
    def pop(self): return _star_io_mmdb.IntVector_pop(self)
    def __getslice__(self, *args): return _star_io_mmdb.IntVector___getslice__(self, *args)
    def __setslice__(self, *args): return _star_io_mmdb.IntVector___setslice__(self, *args)
    def __delslice__(self, *args): return _star_io_mmdb.IntVector___delslice__(self, *args)
    def __delitem__(self, *args): return _star_io_mmdb.IntVector___delitem__(self, *args)
    def __getitem__(self, *args): return _star_io_mmdb.IntVector___getitem__(self, *args)
    def __setitem__(self, *args): return _star_io_mmdb.IntVector___setitem__(self, *args)
    def append(self, *args): return _star_io_mmdb.IntVector_append(self, *args)
    def empty(self): return _star_io_mmdb.IntVector_empty(self)
    def size(self): return _star_io_mmdb.IntVector_size(self)
    def clear(self): return _star_io_mmdb.IntVector_clear(self)
    def swap(self, *args): return _star_io_mmdb.IntVector_swap(self, *args)
    def get_allocator(self): return _star_io_mmdb.IntVector_get_allocator(self)
    def begin(self): return _star_io_mmdb.IntVector_begin(self)
    def end(self): return _star_io_mmdb.IntVector_end(self)
    def rbegin(self): return _star_io_mmdb.IntVector_rbegin(self)
    def rend(self): return _star_io_mmdb.IntVector_rend(self)
    def pop_back(self): return _star_io_mmdb.IntVector_pop_back(self)
    def erase(self, *args): return _star_io_mmdb.IntVector_erase(self, *args)
    def __init__(self, *args): 
        this = _star_io_mmdb.new_IntVector(*args)
        try: self.this.append(this)
        except: self.this = this
    def push_back(self, *args): return _star_io_mmdb.IntVector_push_back(self, *args)
    def front(self): return _star_io_mmdb.IntVector_front(self)
    def back(self): return _star_io_mmdb.IntVector_back(self)
    def assign(self, *args): return _star_io_mmdb.IntVector_assign(self, *args)
    def resize(self, *args): return _star_io_mmdb.IntVector_resize(self, *args)
    def insert(self, *args): return _star_io_mmdb.IntVector_insert(self, *args)
    def reserve(self, *args): return _star_io_mmdb.IntVector_reserve(self, *args)
    def capacity(self): return _star_io_mmdb.IntVector_capacity(self)
    __swig_destroy__ = _star_io_mmdb.delete_IntVector
    __del__ = lambda self : None;
IntVector_swigregister = _star_io_mmdb.IntVector_swigregister
IntVector_swigregister(IntVector)

class StringVector(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, StringVector, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, StringVector, name)
    __repr__ = _swig_repr
    def iterator(self): return _star_io_mmdb.StringVector_iterator(self)
    def __iter__(self): return self.iterator()
    def __nonzero__(self): return _star_io_mmdb.StringVector___nonzero__(self)
    def __bool__(self): return _star_io_mmdb.StringVector___bool__(self)
    def __len__(self): return _star_io_mmdb.StringVector___len__(self)
    def pop(self): return _star_io_mmdb.StringVector_pop(self)
    def __getslice__(self, *args): return _star_io_mmdb.StringVector___getslice__(self, *args)
    def __setslice__(self, *args): return _star_io_mmdb.StringVector___setslice__(self, *args)
    def __delslice__(self, *args): return _star_io_mmdb.StringVector___delslice__(self, *args)
    def __delitem__(self, *args): return _star_io_mmdb.StringVector___delitem__(self, *args)
    def __getitem__(self, *args): return _star_io_mmdb.StringVector___getitem__(self, *args)
    def __setitem__(self, *args): return _star_io_mmdb.StringVector___setitem__(self, *args)
    def append(self, *args): return _star_io_mmdb.StringVector_append(self, *args)
    def empty(self): return _star_io_mmdb.StringVector_empty(self)
    def size(self): return _star_io_mmdb.StringVector_size(self)
    def clear(self): return _star_io_mmdb.StringVector_clear(self)
    def swap(self, *args): return _star_io_mmdb.StringVector_swap(self, *args)
    def get_allocator(self): return _star_io_mmdb.StringVector_get_allocator(self)
    def begin(self): return _star_io_mmdb.StringVector_begin(self)
    def end(self): return _star_io_mmdb.StringVector_end(self)
    def rbegin(self): return _star_io_mmdb.StringVector_rbegin(self)
    def rend(self): return _star_io_mmdb.StringVector_rend(self)
    def pop_back(self): return _star_io_mmdb.StringVector_pop_back(self)
    def erase(self, *args): return _star_io_mmdb.StringVector_erase(self, *args)
    def __init__(self, *args): 
        this = _star_io_mmdb.new_StringVector(*args)
        try: self.this.append(this)
        except: self.this = this
    def push_back(self, *args): return _star_io_mmdb.StringVector_push_back(self, *args)
    def front(self): return _star_io_mmdb.StringVector_front(self)
    def back(self): return _star_io_mmdb.StringVector_back(self)
    def assign(self, *args): return _star_io_mmdb.StringVector_assign(self, *args)
    def resize(self, *args): return _star_io_mmdb.StringVector_resize(self, *args)
    def insert(self, *args): return _star_io_mmdb.StringVector_insert(self, *args)
    def reserve(self, *args): return _star_io_mmdb.StringVector_reserve(self, *args)
    def capacity(self): return _star_io_mmdb.StringVector_capacity(self)
    __swig_destroy__ = _star_io_mmdb.delete_StringVector
    __del__ = lambda self : None;
StringVector_swigregister = _star_io_mmdb.StringVector_swigregister
StringVector_swigregister(StringVector)

class MMDBStarObj(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, MMDBStarObj, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, MMDBStarObj, name)
    __repr__ = _swig_repr
    __swig_setmethods__["file"] = _star_io_mmdb.MMDBStarObj_file_set
    __swig_getmethods__["file"] = _star_io_mmdb.MMDBStarObj_file_get
    if _newclass:file = _swig_property(_star_io_mmdb.MMDBStarObj_file_get, _star_io_mmdb.MMDBStarObj_file_set)
    __swig_setmethods__["file_name"] = _star_io_mmdb.MMDBStarObj_file_name_set
    __swig_getmethods__["file_name"] = _star_io_mmdb.MMDBStarObj_file_name_get
    if _newclass:file_name = _swig_property(_star_io_mmdb.MMDBStarObj_file_name_get, _star_io_mmdb.MMDBStarObj_file_name_set)
    __swig_setmethods__["data_name"] = _star_io_mmdb.MMDBStarObj_data_name_set
    __swig_getmethods__["data_name"] = _star_io_mmdb.MMDBStarObj_data_name_get
    if _newclass:data_name = _swig_property(_star_io_mmdb.MMDBStarObj_data_name_get, _star_io_mmdb.MMDBStarObj_data_name_set)
    __swig_setmethods__["mode"] = _star_io_mmdb.MMDBStarObj_mode_set
    __swig_getmethods__["mode"] = _star_io_mmdb.MMDBStarObj_mode_get
    if _newclass:mode = _swig_property(_star_io_mmdb.MMDBStarObj_mode_get, _star_io_mmdb.MMDBStarObj_mode_set)
    __swig_setmethods__["rc"] = _star_io_mmdb.MMDBStarObj_rc_set
    __swig_getmethods__["rc"] = _star_io_mmdb.MMDBStarObj_rc_get
    if _newclass:rc = _swig_property(_star_io_mmdb.MMDBStarObj_rc_get, _star_io_mmdb.MMDBStarObj_rc_set)
    __swig_setmethods__["verbose"] = _star_io_mmdb.MMDBStarObj_verbose_set
    __swig_getmethods__["verbose"] = _star_io_mmdb.MMDBStarObj_verbose_get
    if _newclass:verbose = _swig_property(_star_io_mmdb.MMDBStarObj_verbose_get, _star_io_mmdb.MMDBStarObj_verbose_set)
    def __init__(self, *args): 
        this = _star_io_mmdb.new_MMDBStarObj(*args)
        try: self.this.append(this)
        except: self.this = this
    def ReadMMCIFFile(self): return _star_io_mmdb.MMDBStarObj_ReadMMCIFFile(self)
    def WriteMMCIFFile(self, *args): return _star_io_mmdb.MMDBStarObj_WriteMMCIFFile(self, *args)
    def DeleteMMCIFData(self, *args): return _star_io_mmdb.MMDBStarObj_DeleteMMCIFData(self, *args)
    def DeleteCategory(self, *args): return _star_io_mmdb.MMDBStarObj_DeleteCategory(self, *args)
    def DeleteStructField(self, *args): return _star_io_mmdb.MMDBStarObj_DeleteStructField(self, *args)
    def DeleteLoopField(self, *args): return _star_io_mmdb.MMDBStarObj_DeleteLoopField(self, *args)
    def DeleteLoopRows(self, *args): return _star_io_mmdb.MMDBStarObj_DeleteLoopRows(self, *args)
    def GetBlockNameList(self): return _star_io_mmdb.MMDBStarObj_GetBlockNameList(self)
    def IsBlockLoop(self, *args): return _star_io_mmdb.MMDBStarObj_IsBlockLoop(self, *args)
    def GetCatNameList(self, *args): return _star_io_mmdb.MMDBStarObj_GetCatNameList(self, *args)
    def GetStructFieldNameList(self, *args): return _star_io_mmdb.MMDBStarObj_GetStructFieldNameList(self, *args)
    def GetStructFieldDataList(self, *args): return _star_io_mmdb.MMDBStarObj_GetStructFieldDataList(self, *args)
    def GetLoopNameList(self, *args): return _star_io_mmdb.MMDBStarObj_GetLoopNameList(self, *args)
    def GetRealStructValue(self, *args): return _star_io_mmdb.MMDBStarObj_GetRealStructValue(self, *args)
    def GetIntegerStructValue(self, *args): return _star_io_mmdb.MMDBStarObj_GetIntegerStructValue(self, *args)
    def GetStringStructValue(self, *args): return _star_io_mmdb.MMDBStarObj_GetStringStructValue(self, *args)
    def GetLoopRealValue(self, *args): return _star_io_mmdb.MMDBStarObj_GetLoopRealValue(self, *args)
    def GetLoopIntegerValue(self, *args): return _star_io_mmdb.MMDBStarObj_GetLoopIntegerValue(self, *args)
    def GetLoopStringValue(self, *args): return _star_io_mmdb.MMDBStarObj_GetLoopStringValue(self, *args)
    def IsLoopVectorReal(self, *args): return _star_io_mmdb.MMDBStarObj_IsLoopVectorReal(self, *args)
    def IsLoopVectorInt(self, *args): return _star_io_mmdb.MMDBStarObj_IsLoopVectorInt(self, *args)
    def IsLoopVectorStr(self, *args): return _star_io_mmdb.MMDBStarObj_IsLoopVectorStr(self, *args)
    def GetLoopRealVector(self, *args): return _star_io_mmdb.MMDBStarObj_GetLoopRealVector(self, *args)
    def GetLoopIntVector(self, *args): return _star_io_mmdb.MMDBStarObj_GetLoopIntVector(self, *args)
    def GetLoopStringVector(self, *args): return _star_io_mmdb.MMDBStarObj_GetLoopStringVector(self, *args)
    def AddCIFData(self, *args): return _star_io_mmdb.MMDBStarObj_AddCIFData(self, *args)
    def PutDataField(self, *args): return _star_io_mmdb.MMDBStarObj_PutDataField(self, *args)
    def PutLoopData(self, *args): return _star_io_mmdb.MMDBStarObj_PutLoopData(self, *args)
    __swig_destroy__ = _star_io_mmdb.delete_MMDBStarObj
    __del__ = lambda self : None;
MMDBStarObj_swigregister = _star_io_mmdb.MMDBStarObj_swigregister
MMDBStarObj_swigregister(MMDBStarObj)

# This file is compatible with both classic and new-style classes.


