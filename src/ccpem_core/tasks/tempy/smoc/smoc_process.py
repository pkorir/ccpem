#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Run TEMPy:SMOC scoring process.
    Take input list of structures and score against single map.
    Output csv table of results
'''

import sys
import os
import json
import pandas as pd
from ccpem_core.TEMPy.MapParser import MapParser
from ccpem_core.TEMPy.StructureParser import PDBParser
from ccpem_core.TEMPy.ScoringFunctions import ScoringFunctions
from ccpem_core import ccpem_utils
from ccpem_core.tasks.tempy.smoc import smoc_results

def main(json_path, verbose=True):
    # Process arguments from json parameter file
    args = json.load(open(json_path, 'r'))
    map_path = args['map_path']
    map_resolution =  args['map_resolution']
    pdb_path_list = args['input_pdbs']
    job_location = args['job_location']
    if args['auto_fragment_length']:
        if map_resolution < 3.0:
            fragment_length = 5
        elif map_resolution < 5.0:
            fragment_length = 7
        else: fragment_length = 9
        print '\nAuto set fragment length : {0}'.format(fragment_length)
    else:
        fragment_length = args['fragment_length']
        print '\nFragment length : {0}'.format(fragment_length)

    if job_location is None:
        job_location = os.getcwd() 

    if not isinstance(pdb_path_list, list):
        pdb_path_list = [pdb_path_list]

    if verbose:
        ccpem_utils.print_sub_header('Process SMOC scores')
    # Run smoc score
    smoc_df = process_smoc_scores(
        map_path=map_path,
        map_resolution=map_resolution,
        pdb_path_list=pdb_path_list,
        fragment_length=fragment_length)

    # Save smoc scores as csv
    csv_path = os.path.join(job_location,
                            'smoc_score.txt')
    if verbose:
        ccpem_utils.print_sub_header('Save raw scores')
        ccpem_utils.print_sub_sub_header(csv_path)
    with open(csv_path, 'w') as path:
        smoc_df.to_csv(path)

    # Set jsrview
    smoc_results.SMOCResultsViewer(
        smoc_dataframe=smoc_df,
        directory=job_location)


def process_smoc_scores(map_path,
                        map_resolution,
                        pdb_path_list,
                        fragment_length):
    em_map = MapParser.readMRC(map_path)

    # Get score for each structure in list
    smoc_frames = []
    residue_label = 'residue'
    score_label = 'smoc'

    for pdb_path in pdb_path_list:
        pdb_id = os.path.basename(pdb_path)
        # Read structure
        structure = PDBParser.read_PDB_file(
            structure_id=pdb_id,
            filename=pdb_path,
            hetatm=False,
            water=False)

        # Get sccores
        smoc_chain_scores = get_smoc_score(
            em_map=em_map,
            map_resolution=map_resolution,
            structure=structure,
            fragment_length=fragment_length)

        # Put residue scores into dataframe
        for chain, scores in smoc_chain_scores.iteritems():
            if chain.isspace():
                chain = ''
            else:
                chain = '_' + chain
            columns = pd.MultiIndex.from_tuples([(pdb_id+chain, residue_label),
                                                 (pdb_id+chain, score_label)])
            df = pd.DataFrame({'residue':scores.keys(), 'smoc':scores.values()})
            df.columns = columns
            smoc_frames.append(df)

    return pd.concat(smoc_frames, axis=1)

def get_smoc_score(em_map,
                   map_resolution,
                   structure,
                   rigid_body_path=None,
                   fragment_length=9,
                   sigma_coeff=0.187):
    '''
    Score window (number residues)
    Sigma coeff
    Returns chain scores dictionary
    '''
    # Calculate SMOC scores
    score_class = ScoringFunctions()
    chain_scores, chain_residues = score_class.SMOC(
        map_target=em_map,
        resolution_densMap=map_resolution,
        structure_instance=structure,
        win=fragment_length,
        rigid_body_file=rigid_body_path,
        sigma_map=sigma_coeff)
    return chain_scores


def import_smoc_scores_from_csv(path):
    return pd.read_csv(path, index_col=0)


if __name__ == '__main__':
    if len(sys.argv) <= 1:
        main(json_path='./test_data/unittest_args.json')
#         print 'Please supply json parameter file'
    else:
        main(json_path=sys.argv[1])
