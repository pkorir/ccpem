#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import numpy as np
from PyQt4 import QtGui
from PIL import Image, ImageOps, ImageEnhance, ImageStat
from ccpem_core.map_io import numpy_mrc_io as mrc_io

# Allowed format extensions
# MRC format
mrc_ext = ('.mrc', '.mrcs', '.map')
# Pil supported image formats
# Other formats are supported as read in / read out only see:
# http://pillow.readthedocs.org/en/3.0.x/handbook/image-file-formats.html
pil_ext = ('.bmp', '.eps', '.gif', '.im', '.jpg', '.jpeg', '.j2p', '.jpx',
           '.j2k', '.pcx', '.png', '.ppm', '.spi', '.tif', '.tiff')
# Numpy file
npy_ext = ('.npy')


class CCPEMImage():
    def __init__(self, pil_image, metadata):
        self.pil_image = pil_image
        self.metadata = metadata


class ImportStackDialog(QtGui.QDialog):
    '''
    Dialog to request range of images.
    '''
    def __init__(self, z_dim, parent=None):
        QtGui.QLabel.__init__(self, parent)
        self.z_dim = z_dim
        self.setWindowTitle('Import from image stack')
        layout = QtGui.QGridLayout()
        self.setLayout(layout)
        #
        label = QtGui.QLabel()
        text = ('Image stack contains {0} image(s).  Please set range to '
                'import.').format(z_dim)
        label.setText(text)
        layout.addWidget(label, 0, 0, 1, 3)
        #
        label = QtGui.QLabel('Image range')
        self.z_min = 1
        self.z_max = self.z_dim
        self.z_min_text = QtGui.QLineEdit()
        self.z_min_text.setText(str(self.z_min))
        self.z_max_text = QtGui.QLineEdit()
        self.z_max_text.setText(str(self.z_max))
        layout.addWidget(label, 1, 0)
        layout.addWidget(self.z_min_text, 1, 1)
        layout.addWidget(self.z_max_text, 1, 2)
        self.z_min_text.editingFinished.connect(self.set_z_min)
        self.z_max_text.editingFinished.connect(self.set_z_max)
        #
        button_box = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok)
        button_box.accepted.connect(self.accept)
        layout.addWidget(button_box, 2, 0, 1, 3)

    def set_z_min(self):
        self.z_min = max(int(self.z_min_text.text()), 1)
        self.z_min = min(int(self.z_min), self.z_dim)
        self.z_min_text.setText(str(self.z_min))
        if self.z_min > self.z_max:
            self.z_max_text.setText(str(self.z_min))

    def set_z_max(self):
        self.z_max = max(int(self.z_max_text.text()), 1)
        self.z_max = min(int(self.z_max), self.z_dim)
        self.z_max_text.setText(str(self.z_max))
        if self.z_max <= self.z_min:
            self.z_min_text.setText(str(self.z_max))


def autoscale_pil_image(pil_image, h_size=300):
    '''
    Scales pictures by given horizontal pixel size maintaining aspect ratio.
    '''
    v_size = int(
        float(pil_image.size[1]) * float(h_size/float(pil_image.size[0])))
    pil_image = pil_image.resize((h_size, v_size), Image.ANTIALIAS)
    return pil_image


def set_brightness_pil_image(pil_image, factor):
    '''
    An enhancement factor of 0.0 gives a black image. A factor of 1.0 gives the
    original image.
    '''
    return ImageEnhance.Brightness(pil_image).enhance(factor)


def set_constrast_pil_image(pil_image, factor):
    '''
    An enhancement factor of 0.0 gives a solid grey image. A factor of 1.0
    gives the original image.
    '''
    return ImageEnhance.Contrast(pil_image).enhance(factor)


def set_auto_contrast_pil_image(pil_image):
    return ImageOps.autocontrast(pil_image)


def set_equalize_pil_image(pil_image):
    return ImageOps.equalize(pil_image)


def import_images(filenames):
    '''
    Import image from single filepath or list of filepaths.  Returns list of
    ccpem image objects.
    '''
    ccpem_images = []
    if filenames is None:
        return None
    if not isinstance(filenames, list):
        filenames = [filenames]
    for filename in filenames:
        images = None
        if os.path.exists(filename):
            if filename.endswith(pil_ext):
                image = import_image_std_format(filename)
                if image is not None:
                    images = [image]
            elif filename.endswith(npy_ext):
                image = import_image_numpy(filename)
                if image is not None:
                    images = [image]
            elif filename.endswith(mrc_ext):
                images = import_images_mrc(filename)
        if images is not None:
            ccpem_images += images
    if len(ccpem_images) > 0:
        return ccpem_images
    else:
        return None


def import_image_numpy(filename, clip_min=1, clip_max=10):
    image = np.load(filename)
    # Offset to remove -ve values
    image -= image.min()
    assert image.min() >= 0
    # XXX Debug:
    #   Clip to test displaying diffraction images
    image = np.clip(image, clip_min, clip_max)
    # Normalise
    image *= (255.0/image.max())
    pil_image = Image.fromarray(image).convert('L')
    size = pil_image.size
    metadata = {'filename': filename,
                'mode': pil_image.mode,
                'width': size[0],
                'height': size[1]}
    ccpem_image = CCPEMImage(pil_image=pil_image,
                             metadata=metadata)
    return ccpem_image


def import_image_std_format(filename):
    '''
    Import images with standard image format.
    '''
    if filename.endswith(pil_ext):
        pil_image = Image.open(filename)
        # Convert from RBGA to RGB, set alpha channel to white
        if pil_image.mode == 'RGBA':
            pil_image.load()
            background = Image.new("RGB", pil_image.size, (255, 255, 255))
            background.paste(pil_image, mask=pil_image.split()[3])
            pil_image = background
        size = pil_image.size
        metadata = {'filename': filename,
                    'mode': pil_image.mode,
                    'width': size[0],
                    'height': size[1]}
        ccpem_image = CCPEMImage(pil_image=pil_image,
                                 metadata=metadata)
        return ccpem_image
    else:
        print 'Extension not supported : {0}'.format(filename)
        return None


def get_image_range(z_dim):
    stack_dialog = ImportStackDialog(z_dim=z_dim)
    if stack_dialog.exec_():
        # Adjust for python counting at zero
        z_min = stack_dialog.z_min-1
        z_max = stack_dialog.z_max
    return z_min, z_max


def import_images_mrc(filename, from_z=None, to_z=None, range_dialog=True):
    '''
    Import images from mrc (single image) or mrcs (image stack).
    '''
    if filename.endswith(mrc_ext):
        ccpem_images = []
        try:
            header, data = mrc_io.read_mrc(filename=filename)
            z_dim = data.shape[0]
            if z_dim > 1 and range_dialog:
                from_z, to_z = get_image_range(z_dim=z_dim)
            if from_z is None:
                from_z = 0
            if to_z is None:
                to_z = z_dim
            #
            for z in range(from_z, to_z):
                # N.B. y axis swapped here.
                # PIL uses Cartesian pixel coordinate system: 0,0 = upper left
                image = data[z, ::-1, :]
                image.flags.writeable = True
                # Offset to remove -ve values
                image -= image.min()
                if image.dtype in ['uint8', 'uint16']:
                    # Assume image already on 0-255 scale
                    pass
                else:
                    # Normalise for greyscale display
                    image *= (255.0/image.max())
                # N.B. RGB is used as L or F modes cause bugs with QImage
                # / QPixmap
                pil_image = Image.fromarray(image).convert('RGB')
                # Convert metadata to python dictionary
                metadata = {name: header[name] for name in header.dtype.names}
                # Tag filename with image numbmer for stack
                if z_dim > 1:
                    d_name = os.path.dirname(filename)
                    f_name = str(z+1) + '_' + os.path.basename(filename)
                    metadata['filename'] = os.path.join(d_name, f_name)
                else:
                    metadata['filename'] = filename
                metadata['format'] = 'MRC'
                ccpem_image = CCPEMImage(pil_image=pil_image,
                                         metadata=metadata)
                ccpem_images.append(ccpem_image)
            return ccpem_images
        except:
            print 'Error loading : {0}'.format(filename)
            return None
    else:
        print 'Extension not supported : {0}'.format(filename)
        return None


def show_pil_stats(pil_image):
    print 'Pil stats'
    image_stats = ImageStat.Stat(pil_image)
    print 'Size (w,h)  : ', pil_image.size
    print 'Extrema     : ', image_stats.extrema
    print 'Rms         : ', image_stats.rms


def convert_mrc_to_spider(pil_image, filename):
    pil_image.save(fp=filename, format='SPIDER')
