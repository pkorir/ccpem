#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Return a JSmol widget
'''

import os
from PyQt4 import QtCore
from PyQt4.QtWebKit import QWebView, QWebPage, QWebSettings
from PyQt4.QtNetwork import QNetworkProxy
from ccpem_core.gui_ext import jsmol


class MolecularBrowserNoServer(QWebView):
    '''
    Class for molecular browser, does not require server.  Can not display 
    binary files (i.e. .mrc).
    '''
    def __init__(self, parent=None, debug=False):
        super(MolecularBrowserNoServer, self).__init__(parent)
        self.debug = debug

    def load_file(self,
                  model1,
                  model2='',
                  ribfind_xml='null',
                  result='false',
                  program='preview'):
        jsmol_path = os.path.dirname(jsmol.__file__)
        local_url = os.path.join(jsmol_path, 'molecule_preview_no_server.htm')
        assert program in ['preview', 'flexem', 'ribfind']
        assert os.path.isfile(local_url)
        assert os.path.isfile(model1)
        if model2 is not '':
            assert os.path.isfile(model2)
        # N.B. if ribfind_xml is not a path to a valid xml file it must be set
        # to 'null' string.
        if ribfind_xml is not 'null':
            assert os.path.isfile(ribfind_xml)
        if self.debug:
            self.pageOut = WebPageNoServer()
            self.setPage(self.pageOut)
        self.molview = QtCore.QUrl.fromLocalFile(
            QtCore.QFileInfo(local_url).absoluteFilePath())
        self.molview.setQueryItems([('program', program),
                                    ('model1', model1),
                                    ('model2', model2),
                                    ('ribfind_xml', ribfind_xml),
                                    ('result', result)])
        self.loadFinished.connect(self._result_available)
        self.load(self.molview)

    def _result_available(self, connected):
        '''
        Callback function for loadFinished.connect
        '''
        pass
        # XXX below appears unnecessary.
#         loaded_page = self.page()
#         frame = loaded_page.mainFrame()
        # If we don't put this here, then scrollbars randomly appear even though
        # the sizes match up!
#         frame.setScrollBarPolicy(QtCore.Qt.Vertical,
#                                  QtCore.Qt.ScrollBarAlwaysOff)
#         frame.setScrollBarPolicy(QtCore.Qt.Horizontal,
#                                  QtCore.Qt.ScrollBarAlwaysOff)


# class WebPageNoServer(QWebPage):
#     '''
#     Debug assistant for Molecular browser.
#     '''
#     def __init__(self):
#         super(WebPageNoServer, self).__init__()
# 
#     def javaScriptConsoleMessage(self, msg, line, source):
#         print '%s line %d: %s' % (source, line, msg)
# 
# 
# class WebPage(QWebPage):
#     '''
#     Inherited class of QWebPage to capture JS console log text
#     '''
#     ##
#     # Inherited class of QWebPage is so we can capture all JS console log text
#     ##
# 
#     def __init__(self):
#         QWebPage.__init__(self)
# 
#         self.dict = QWebSettings.globalSettings()
#         self.dict.setAttribute(QWebSettings.LocalContentCanAccessRemoteUrls,
#                                True)
#         self.dict.setAttribute(QWebSettings.LocalContentCanAccessFileUrls,
#                                True)
#         proxy = QNetworkProxy(QNetworkProxy.HttpProxy,
#                               'wwwcache.rl.ac.uk',
#                               8080,
#                               '',
#                               '') #NOT http://wwwcache.rl.ac.uk !!!
#         QNetworkProxy.setApplicationProxy(proxy)
# 
#     @classmethod
#     def javaScriptConsoleMessage(cls, msg, line, source): #useful for js errors
#         '''
#         Display javascript console error messages to stderr
#         '''
#         print '%s line %d: %s' % (source, line, msg)


# class MolecularBrowser(QWebView):
# 
#     '''
#     Custom QWebView to view a JSmol object
#     '''
#     def __init__(self, parent):
#         QWebView.__init__(self, parent)
#         self.molview = None
#         self._options = {'preview' : self.preview,
#                          'ribfind' : self.ribfind,
#                          'flexem' : self.flexem
#                         }
# 
#     def preview(self, **kwargs):
#         '''
#         Set query items for the preview view
#         '''
#         self.molview.setQueryItems([('model1', kwargs['model1']),
#                                    ('programme', 'preview')])
# 
#     def flexem(self, **kwargs):
#         '''
#         Set query items for the flexem view
#         '''
#         self.molview.setQueryItems([('model1', kwargs['model1']),
#                                ('model2', kwargs['model2']),
#                                ('map', kwargs['map']),
#                                ('programme', 'flexem'),
#                                ('platformSpeed', '1'),
#                                ('script', 'python')])
#         
#     def ribfind(self, **kwargs):
#         '''
#         Set query items for the ribfind view
#         '''
#         self.molview.setQueryItems([('model1', kwargs['model1']),
#                                     ('map', kwargs['map']),
#                                     ('programme', 'ribfind'),
#                                     ('platformSpeed', '1'),
#                                     ('script', 'python')])
# 
#     def load_file(self, programme, **kwargs):
#         '''
#         Load the webpage object and pass all data to it
#         '''
#         print '\n Webpage'
#         print 'http://localhost:' + str(kwargs['port']) + '/molecule_preview.htm'
#         #
#         self.molview = QtCore.QUrl('http://localhost:' + str(kwargs['port']) +
#                                    '/molecule_preview.htm')
#         self.setPage(WebPage())
#         self._options[programme](**kwargs)
#         print 'QUERY ITEMS:'
#         print self.molview.queryItems()
#         self.loadFinished.connect(self._result_available)
#         self.load(self.molview)
# 
#     def _result_available(self, connected):
#         '''
#         Callback function for loadFinished.connect
#         '''
#         if not connected:
#             print 'Loading failed'
# 
#         loaded_page = self.page()
#         frame = loaded_page.mainFrame()
#         # If we don't put this here, then scrollbars randomly appear even though
#         # the sizes match up!
#         frame.setScrollBarPolicy(QtCore.Qt.Vertical,
#                                  QtCore.Qt.ScrollBarAlwaysOff)
#         frame.setScrollBarPolicy(QtCore.Qt.Horizontal,
#                                  QtCore.Qt.ScrollBarAlwaysOff)
