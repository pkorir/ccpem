C     HLX2FD.FOR
C
C     Program finds 2-fold origin for a helical object and optionally
C     makes a 2-fold forced dataset; origin search is based on
C     minimizing amplitude-weighted phase deviations from nearest
C     centrosymmetric values.
C
C     equatorial data are omitted.
C
C     CNTFILE        - control data file
C     FD2FILE        - two-fold constrained dataset
C
C     Data cards in CNTFILE:    
C     HLXTITLE       - image title
C     INFILE         - input filename
C     NLL            - dummy card
C     rep_dis        - repeat distance (A)
C     ACUT           - amplitude cutoff (% of max. amp. in ref.)
C     LLSPEC, WTSPEC - special layer-lines and weights
C     PHIMIN, PHIMAX - min. and max. phi values in search  (degrees)
C     DPHI           - search increment in phi
C     ZMIN, ZMAX     - min. and max. z value in search (A)
C     DELZ           - search increment in z
C     I2FD           - 1 to write out FD2FILE
C
C     Version 1.0 20-Jul-95 NU (Equivalent to CT's HLX2FLD)
C   
C
      CHARACTER*19 TEXT(2000)
      CHARACTER*20 HLXTITLE
      CHARACTER*40 LLTITLE
      CHARACTER*50 CNTFILE,INFILE,FD2FILE
      CHARACTER LINES(73)
      CHARACTER*2 NOT
      PARAMETER (IN1=5,OUT=6)
      DIMENSION WT(2000),NN(2000),NL(2000),RES(80,200),PRES(80)
      DIMENSION R(2000,800),A(2000,800),P(2000,800),ZRES(200)
C
      WRITE(OUT,101)
      READ(IN1,102)CNTFILE
      OPEN(UNIT=7,FILE=CNTFILE,STATUS='OLD')
      READ(7,103)HLXTITLE
      READ(7,102)INFILE
      READ(7,*)NLL,rep_dis
      READ(7,*)ACUT
      READ(7,*)LLSPEC,WTSPEC
      READ(7,*)PHIMIN,PHIMAX,DPHI
      READ(7,*)ZMIN,ZMAX,DELZ
      READ(7,*)I2FD
c      CALL DOPEN(1,INFILE,'RO','F')
      CALL ccpdpn(1,INFILE,'READONLY','F',0,0)
      IF(I2FD.EQ.1) THEN
      WRITE(OUT,105)
      READ(IN1,102)FD2FILE
c      CALL DOPEN(2,FD2FILE,'NEW','F')
      CALL ccpdpn(2,FD2FILE,'NEW','F',0,0)
      ENDIF
C
      NOT='--'
      DEG=57.2957795
      LLSPEC=0
      WTSPEC=0.0
      ATSUM=0.0
      IPT0=0
      IPTM=0
      AMAX=0.0
      PN=PHIMIN
      PX=PHIMAX
      IF(PHIMIN.GT.PHIMAX) THEN
      PHIMAX=PN
      PHIMIN=PX
      ENDIF
      ZN=ZMIN
      ZX=ZMAX
      IF(ZMIN.GT.ZMAX) THEN
      ZMAX=ZN
      ZMIN=ZX
      ENDIF
      IPL=(PHIMAX-PHIMIN)/DPHI+1.5
      IZL=(ZMAX-ZMIN)/DELZ+1.5
      IF(IPL.GT.8) THEN
      IPL=8
      PHIMAX=PHIMIN+5.0*DPHI
      WRITE(OUT,104)
      ENDIF
C
C     **  input layer-lines  **
C
      WRITE(OUT,109)INFILE
      NCOUNT=0
      L=0
4     I=1
      L=L+1
      READ(1,110,END=2)TEXT(L),LLTITLE,WT(L),NN(L),NL(L)
      write(6,110)TEXT(L),LLTITLE,WT(L),NN(L),NL(L)
3     READ(1,111)R(L,I),A(L,I),P(L,I)
      IF(R(L,I).EQ.0.0.AND.A(L,I).EQ.0.0) GO TO 4
      IF(NL(L).EQ.LLSPEC)WT(L)=WTSPEC
      IF(NL(L).EQ.0) GO TO 1 
      A(L,I)=A(L,I)*WT(L)
      NCOUNT=NCOUNT+1
      ATSUM=A(L,I)+ATSUM
      IF(A(L,I).GT.AMAX)AMAX=A(L,I)
1     I=I+1
      GO TO 3
2     LLMAX=L-1
      AMIN=(ACUT*AMAX)/100.0
      DELR=R(1,2)-R(1,1)
C
C     **  calculate phase residuals  **
C
      I=1
      CONVD=360./rep_dis
      ZSHIFT=ZMIN*CONVD
      DZ=DELZ*CONVD
      DO 13 NZ=1,IZL
      PHISHF=PHIMIN
      DO 12,NP=1,IPL
      NPTS=0
      ASUM=0.0
      APSUM=0.0
      DO 10 L=1,LLMAX
      PHASH=NN(L)*PHISHF-NL(L)*ZSHIFT
c	write(6,*) l,i,r(l,i),a(l,i),nl(l),amin
14    IF(R(L,I).EQ.0.0.AND.A(L,I).EQ.0.0) GO TO 11
      IF(NL(L).EQ.0) GO TO 15
      IF(A(L,I).LT.AMIN) GO TO 15
      PHI=AMOD(P(L,I)-PHASH,360.)
      IF(PHI.LT.0.)PHI=PHI+360.
      IF(PHI.GT.180.)PHI=PHI-180.
      IF(PHI.GT.90.) THEN
      DP=180.-PHI
      ELSE
      DP=PHI
      ENDIF
      APSUM=A(L,I)*DP+APSUM
      ASUM=A(L,I)+ASUM
      NPTS=NPTS+1
15    I=I+1
      GO TO 14      
11    I=1
10    CONTINUE
      if(asum .ne. 0.0) then
      RES(NP,NZ)=APSUM/ASUM
      PHISHF=PHISHF+DPHI
      end if
12    CONTINUE
      ZSHIFT=ZSHIFT+DZ
13    CONTINUE
C
      NGCOUNT=NPTS
      AGSUM=ASUM
      WRITE(OUT,200)LLTITLE
      WRITE(OUT,201)HLXTITLE
      WRITE(OUT,203)INFILE
      WRITE(OUT,204)rep_dis
      WRITE(OUT,205)DELR
      WRITE(OUT,206)AMAX
      WRITE(OUT,207)AMIN,ACUT
      WRITE(OUT,208)AGSUM,ATSUM
      WRITE(OUT,209)NGCOUNT,NCOUNT
      WRITE(OUT,210)CNTFILE
C
C     **  find minimum residual  **
C
      RM=90.
      DO 17 NZ=1,IZL
      ZRES(NZ)=ZMIN+(NZ-1)*DELZ
      DO 16 NP=1,IPL
      PRES(NP)=PHIMIN+(NP-1)*DPHI
      IF(RES(NP,NZ).LT.RM) THEN
      RM=RES(NP,NZ)
      PM=PRES(NP)
      ZM=ZRES(NZ)
      ENDIF
16    CONTINUE
17    CONTINUE
      IEND=10+7*IPL
      DO 27 N=1,IEND
27    LINES(N)='-'
      WRITE(OUT,214) (PRES(N),N=1,IPL)
      WRITE(OUT,216) (LINES(N),N=1,IEND)
      DO 26 J=1,IZL
26    WRITE(OUT,215)ZRES(J),(RES(N,J),N=1,IPL)
      WRITE(OUT,216) (LINES(N),N=1,IEND)
      WRITE(OUT,211)RM,PM,ZM
C
C     **  calculate phase residual for each layer-line  **
C
      I=1
      ZR=0.0
      AMP=0.0
      RESL=0.0
      WRITE(OUT,212)
      DO 18 L=1,LLMAX
      NPTS=0
      ASUM=0.00001
      APSUM=0.0
      PHASH=NN(L)*PM-NL(L)*CONVD*ZM
20    IF(R(L,I).EQ.0.0.AND.A(L,I).EQ.0.0) GO TO 22
      IF(NL(L).EQ.0) GO TO 21
      IF(NL(L).EQ.LLSPEC)A(L,I)=A(L,I)*WTSPEC
      IF(A(L,I).LT.AMIN) GO TO 21
      PHI=AMOD(P(L,I)-PHASH,360.)
      IF(PHI.LT.0.)PHI=PHI+360.
      IF(PHI.GT.180.)PHI=PHI-180.
      IF(PHI.GT.90.) THEN
      DP=180.-PHI
      ELSE
      DP=PHI
      ENDIF
      APSUM=A(L,I)*DP+APSUM
      ASUM=A(L,I)+ASUM
      NPTS=NPTS+1
21    I=I+1
      GO TO 20      
22    IPT0=(R(L,1)/DELR)+0.5
      IPTM=(R(L,I-1)/DELR)+0.5
      I=1
      IF(NL(L).EQ.0) GO TO 24
      ZR=rep_dis/NL(L)
      AMP=(ASUM/AGSUM)*100.
      RESL=APSUM/ASUM
24    IF(RESL.EQ.0.0) THEN
      WRITE(OUT,217)L,NL(L),NN(L),ZR,WT(L),NPTS,IPT0,IPTM,AMP,NOT
      GO TO 18
      ENDIF
23    WRITE(OUT,213)L,NL(L),NN(L),ZR,WT(L),NPTS,IPT0,IPTM,AMP,RESL
18    CONTINUE
c      WRITE(OUT,107)PM,ZM
      write(OUT,112)
      write(OUT,211)RM,PM,ZM
C
C     **  write 2fd-forced output  **
C
      IF(I2FD.NE.1) GO TO 29
      WRITE(OUT,106)FD2FILE
      L=0
30    I=1
      L=L+1
      IF(L.GT.LLMAX) GO TO 31
      WRITE(2,110)TEXT(L),LLTITLE,WT(L),NN(L),NL(L)
      PHASH=NN(L)*PM-NL(L)*CONVD*ZM
32    PHI=AMOD(P(L,I)-PHASH,360.)
      IF(PHI.LT.0.)PHI=PHI+360.
      AMP=ABS(A(L,I)*COS(PHI/DEG))
      IF(PHI.LT.90.0.OR.PHI.GT.270.0) THEN
      PHI=0.0
      GO TO 33
      ENDIF
      PHI=180.0
      IF(R(L,I).EQ.0.0.AND.A(L,I).EQ.0.0) PHI=0.0      
33    WRITE(2,111)R(L,I),AMP,PHI
      IF(R(L,I).EQ.0.0.AND.A(L,I).EQ.0.0) GO TO 30
      I=I+1
      GO TO 32
31    CLOSE(2)
29    CLOSE(1)
C
100   FORMAT(2X,'HLX2FD : ',12A4,2X,A9,2X,A8)      
101   FORMAT(2X,'control file : ')   
102   FORMAT(A50)
103   FORMAT(A)
104   FORMAT(/2X,'** too many steps in phi! **')
105   FORMAT(2X,'2fd-forced file : ')   
106   FORMAT(2X,'--- now writing 2fd-forced file : ',A,/)
107   FORMAT(2X,'---------------------------------------------------
     2----------',/2X,' at phi = ', F7.3,', z =',F7.3/)
109   FORMAT(2X,'--- reading in layer-line data : ',A50//)
110   FORMAT(A,A,40X,1p,E12.5,2I5)
111   FORMAT(1p,3E12.5)
112   format(2X,'---------------------------------------------------
     2----------')
200   FORMAT(2X,'     title of layer-line : ',A)
201   FORMAT(2X,'               new title : ',A)
203   FORMAT(2X,'                      in : ',A50)
204   FORMAT(2X,'         repeat distance : ',F10.2,' Angstroms')
205   FORMAT(2X,'     layer-line sampling : ',F14.9,
     2' recip. Angstroms')
206   FORMAT(2X,'       maximum amplitude : ',F10.2)
207   FORMAT(2X,'        cutoff amplitude : ',F10.2,' ( ',F4.2,'%)')
208   FORMAT(2X,'   sum of amplitude used : ',F10.1, ' out of ',F10.1)
209   FORMAT(2X,'   number of points used : ',I10,' out of ',I10)
210   FORMAT(2X,'         control data in : ',A50//)
211   FORMAT(2X,'  minimum residual : ',F10.3,' at phi = ',F8.3,
     2', z = ',F8.3,/)
212   FORMAT(2X,'phase residual for each layer-line at global minimum',
     2/2X,'----------------------------------------------------------
     3---',
     4/2X,'        L    N   1/Z   weight     points      amp    residu
     5al',
     6/2X,'                 (A)              (g.u.)      (%)    (degre
     7e)',
     8/2X,'----------------------------------------------------------
     9---')
213   FORMAT(1X,I4,':',2I5,F7.1,F7.1,I5,' (',I3,'-',I3,')',F7.2,F9.1)
217   FORMAT(1X,I4,':',2I5,F7.1,F7.1,I5,' (',I3,'-',I3,')',F7.2,6X,A)
214   FORMAT(2X,' z  phi | ',8F7.3)
215   FORMAT(2X,F8.3,': ',8F7.3)
216   FORMAT(2X,73A)
      STOP
      END
