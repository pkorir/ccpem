#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import shutil
import os
import time
import sys
import tempfile
from ccpem_core import ccpem_utils
from PyQt4 import QtGui, QtCore
from PyQt4.QtTest import QTest
from ccpem_core.tasks.mrc_allspacea import  mrc_allspacea_task
from ccpem_gui.tasks.mrc_allspacea import  mrc_allspacea_window
from ccpem_core import process_manager
from ccpem_core.test_data import get_test_data_path

app = QtGui.QApplication(sys.argv)


class Test(unittest.TestCase):
    '''
    Test mrc allspace task
    '''
    def setUp(self):
        self.test_data = get_test_data_path()
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.test_output)

    def test_run_mrc_allspacea_integration(self):
        '''
        Run mrc allspacea and check for expected output.
        '''
        ccpem_utils.print_header(message='Unit test - MRCAllspaca')
        run_task = mrc_allspacea_task.MrcAllspacea(
            job_location=self.test_output)
        # Set args
        datafile = os.path.join(self.test_data, 'allspace/allspace.data')
        assert os.path.exists(datafile)
        run_task.args.set_values(
            datafile=datafile,
            symmetry='ALL',
            ispg='None',
            search=True,
            refine=True,
            tilt=True,
            ncycle=2,
            origh=0.0,
            origk=0.0,
            tilth=0.0,
            tiltk=0.0,
            stepsize=6.0,
            phasesize=61,
            alpha=62.45,
            beta=62.45,
            gamma=120.0,
            rin=150.0,
            rout=3.5,
            cs=2.0,
            kv=120.0,
            ilist=False,
            rot180=0,
            iqmax=7)
        # Run via gui
        self.window = mrc_allspacea_window.MrcAllspaceaWindow(
            task=run_task)
        QTest.mouseClick(
            self.window.tool_bar.widgetForAction(
                self.window.tb_run_button),
            QtCore.Qt.LeftButton)
        # Run w/out gui
#         run_task.run_task()
        stdout = run_task.pipeline.pipeline[0][0].stdout
        # Wait for run to complete
        self.job_completed = False
        timeout = 0
        t_inc = 1
        while not self.job_completed and timeout < 500:
            print 'Mrcallspacea running for {0} secs (timeout = 500)'.format(
                timeout)
            time.sleep(t_inc)
            timeout += t_inc
            status =\
                process_manager.get_process_status(run_task.pipeline.json)
            if status == 'finished':
                if os.path.isfile(stdout):
                    tail = ccpem_utils.tail(stdout, maxlen=10)
                    if tail.find('CCP-EM process finished') != -1:
                        self.job_completed = True
        # Check timeout
        assert timeout < 500
        # Check job completed
        assert self.job_completed
        # Check standard file
        assert os.path.isfile(stdout)
        tail = ccpem_utils.tail(filename=stdout, maxlen=10)
        assert tail.find('CCP-EM process finished') != -1
        assert tail.find('REVHK option in ORIGTILT.') != -1

if __name__ == '__main__':
    unittest.main()
