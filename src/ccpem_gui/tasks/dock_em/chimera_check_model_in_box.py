#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Checks for the position of the pdb (search object) and positions it in the 
center of the box if it is outside the grid of the target map.

'''

from sys import argv
from chimera import runCommand as rc
from chimera import openModels, Point

# open map
map_vol = openModels.open(argv[1])[0]
sigma = map_vol.data.full_matrix().std()

#voxel size
istep, jstep, kstep = map_vol.data.step

#box dimensions
Zdim = map_vol.data.full_matrix().shape[0]
Ydim = map_vol.data.full_matrix().shape[1]
Xdim = map_vol.data.full_matrix().shape[2]

#map origin
origin = map_vol.data.origin
cenX = origin[0]+(Xdim*float(istep))/2.0
cenY = origin[1]+(Ydim*float(istep))/2.0
cenZ = origin[2]+(Zdim*float(istep))/2.0

# display level at 1 sigma
rc("volume #0 level %f transparency 0.5"%(sigma))

# calculate bounds
x0,x1 = origin[0],origin[0]+Xdim*float(istep)
y0,y1 = origin[1],origin[1]+Ydim*float(istep)
z0,z1 = origin[2],origin[2]+Zdim*float(istep)

# open pdb
search_pdb = openModels.open(argv[2])[0]
atoms  =  search_pdb.atoms
com = Point([a.coord() for a in atoms],[a.element.mass for a in atoms])
comX,comY,comZ = com.x,com.y,com.z

# if com inside the map box
if (x0<comX<x1) and (y0<comY<y1) and (z0<comZ<z1):
    print 'Model inside the map grid'
else:
    rc("move x %f models #1"%(cenX-comX))
    rc("move y %f models #1"%(cenY-comY))
    rc("move z %f models #1"%(cenZ-comZ))
    rc("write #1 %s"%(argv[3])) 
