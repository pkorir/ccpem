#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import sys
from pandas import DataFrame
from PyQt4 import QtGui
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt4agg import (
    FigureCanvasQTAgg as FigureCanvas)
from matplotlib.backends.backend_qt4agg import (
    NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure
from ccpem_gui.icons import icon_utils
from ccpem_core import settings
from ccpem_gui.utils import window_utils
from ccpem_core.map_tools.mean_amplitudes import get_mean_amplitudes

# To do -> extend to use non-refmac column labels

# Make class for setting data... then launch in separate thread
# -> callback = when canvas is ready (i.e. once data has been parsed etc.).


class RefmacMapSharpPlotWidget(QtGui.QWidget):
    plt.style.use('ggplot')
    # Set legend font size; standard too large
    params = {'legend.fontsize': 10}
    plt.rcParams.update(params)

    def __init__(self,
                 mtz_in_path,
                 blur_array,
                 sharp_array,
                 results_path,
                 pdb_in_path=None,
                 set_plot=True,
                 parent=None):
        super(RefmacMapSharpPlotWidget, self).__init__(parent)
        self.pdb_in_path = pdb_in_path
        self.mtz_in_path = mtz_in_path
        self.blur_array = blur_array
        self.blur_array.sort(reverse=True)
        self.sharp_array = sharp_array
        self.sharp_array.sort()
        self.results_path = results_path
        self.mean_amps = None
        self.mean_amps_log = None
        # Get amplitude data from mtz
        if os.path.exists(self.results_path):
            self.import_mean_amplitudes()
        else:
            self.mean_amps, self.mean_amps_log = get_mean_amplitudes(
                blur_array=self.blur_array,
                sharp_array=self.sharp_array,
                mtz_in_path=self.mtz_in_path,
                results_path=self.results_path)
        # Set layout
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)
        # Set plot
        if self.mean_amps is not None and set_plot:
            self.set_plot()

    def set_plot(self, dpi=100):
        self.fig = Figure(dpi=dpi)
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self)
        self.canvas.updateGeometry()

        self.axes = self.fig.add_subplot(211) # 111
        self.mean_amps.plot(ax=self.axes)
        # Use LaTex to set Angstrom symbol
        xlabel = (r'Resolution ($\AA$)')
        self.axes.set_xlabel(xlabel)
        self.axes.set_ylabel('<|F|>')
        y_max = 2.0 * self.mean_amps[r'In 0 $\AA^2$'].max()
        self.axes.set_ylim(0, y_max)

        if self.mean_amps_log is not None:
            self.axes_log = self.fig.add_subplot(212) # 111
            self.mean_amps_log.plot(ax=self.axes_log)
            # Use LaTex to set Angstrom symbol
            xlabel = (r'Resolution ($\AA$)')
            self.axes_log.set_xlabel(xlabel)
            self.axes_log.set_ylabel('log <|F|>')
            y_max_log = 2.0 * self.mean_amps_log[r'In 0 $\AA^2$'].max()
            self.axes_log.set_ylim(0, y_max_log)

        self.layout.addWidget(self.canvas)
        # Create the navigation toolbar, tied to the canvas
        self.mpl_toolbar = NavigationToolbar(self.canvas, self)
        # Add coot icon
        if settings.which(program='coot') is not None:
            coot_icon = os.path.join(icon_utils.get_other_icons_path(),
                                     'coot_icon.png')
            self.coot_button = self.mpl_toolbar.addAction(
                QtGui.QIcon(coot_icon),
                'Launch coot to view maps',
                self.launch_coot)
        self.layout.addWidget(self.mpl_toolbar)

    def import_mean_amplitudes(self):
        self.mean_amps = DataFrame.from_csv(self.results_path)
        try:
            self.mean_amps_log = DataFrame.from_csv(
                self.results_path+'log')
        except:
            pass

    def launch_coot(self):
        '''
        Launch coot with blurred / sharpened maps.
        '''
        # Convert blur / sharp arrays to coot column label names
        column_labels = []
        if self.blur_array is not None:
            for b in self.blur_array:
                column_labels.append('FoutBlur_{0:.2f}'.format(b))
        column_labels.append('Fout0')
        if self.sharp_array is not None:
            for b in self.sharp_array:
                column_labels.append('FoutSharp_{0:.2f}'.format(b))
        make_and_draw_map = ''
        # Make python script string to pass to coot
        for label in column_labels:
            make_and_draw_map += \
                'make_and_draw_map("{0}", "{1}", "Pout0", "", 0, 0);'.format(
                    os.path.abspath(self.mtz_in_path),
                    label)
        # Get args
        args = []
        if self.pdb_in_path is not None:
            args += ['--pdb', os.path.abspath(self.pdb_in_path)]
        args += ['--no-state-script',
                 '--python', '-c',
                 make_and_draw_map]
        window_utils.run_coot(args=args)


def main():
    '''
    For testing.
    '''
    app = QtGui.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon(icon_utils.get_ccpem_icon()))
    #
    blur_array = [50, 100, 150, 200]
    sharp_array = [50, 100, 150, 200]
    mtz_in_path = os.path.join('./test_data/'
                               'starting_map.mtz')
    pdb_in_path = os.path.join('./test_data/'
                               '1ake_start.pdb')
    #
    mw = RefmacMapSharpPlotWidget(mtz_in_path=mtz_in_path,
                                  blur_array=blur_array,
                                  sharp_array=sharp_array,
                                  pdb_in_path=pdb_in_path)
    mw.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
