<?xml version="1.0" encoding="UTF-8"?>
<emd emdb_id="EMD-2024" version="2.0">
    <admin>
        <current_status>
            <code>REL</code>
            <processing_site>PDBe</processing_site>
        </current_status>
        <sites>
            <deposition>PDBe</deposition>
            <last_processing>PDBe</last_processing>
        </sites>
        <key_dates>
            <deposition>2012-01-04</deposition>
            <header_release>2012-01-20</header_release>
            <map_release>2012-01-27</map_release>
            <update>2012-09-05</update>
        </key_dates>
        <title>Three-dimensional reconstruction of another targeted individual 17nm nascent high-density lipoprotein (HDL) particle by individual-particle electron tomography (IPET). The reconstruction displayed a ring-shaped structure of containing three protein - apoipoprotein A-Is (28 kDa each).</title>
        <authors_list>
            <author>Zhang L</author>
            <author>Ren G</author>
        </authors_list>
        <keywords>reconstituted 17nm nascent high-density lipoprotein (HDL)</keywords>
    </admin>
    <crossreferences>
        <citation_list>
            <primary_citation>
                <journal_citation published="true">
                    <author order="1">Zhang L</author>
                    <author order="2">Ren G</author>
                    <title>IPET and FETR: experimental approach for studying molecular structure dynamics by cryo-electron tomography of a single-molecule structure.</title>
                    <journal>PLOS ONE</journal>
                    <volume>7</volume>
                    <first_page>e30249</first_page>
                    <last_page>e30249</last_page>
                    <year>2012</year>
                    <external_references type="pubmed">22291925</external_references>
                    <external_references type="doi">doi:10.1371/journal.pone.0030249</external_references>
                </journal_citation>
            </primary_citation>
        </citation_list>
    </crossreferences>
    <sample>
        <name>second 17nm nascent HDL particle</name>
        <supramolecule_list>
            <supramolecule id="1000" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="sample">
                <details>Fresh sample was prepared with 2 days before using.</details>
                <oligomeric_state>monomer</oligomeric_state>
                <number_unique_components>1</number_unique_components>
                <molecular_weight>
                    <experimental units="MDa">0.2</experimental>
                    <theoretical units="MDa">0.2</theoretical>
                </molecular_weight>
            </supramolecule>
        </supramolecule_list>
        <macromolecule_list>
            <macromolecule id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="protein_or_peptide">
                <name synonym="17 nm nascent HDL">17 nm nascent HDL</name>
                <natural_source>
                    <organism ncbi="9606">Homo sapiens</organism>
                    <strain>nascent HDL</strain>
                    <synonym_organism>Human</synonym_organism>
                    <cellular_location>Plasma</cellular_location>
                </natural_source>
                <molecular_weight>
                    <experimental units="MDa">0.2</experimental>
                    <theoretical units="MDa">0.2</theoretical>
                </molecular_weight>
                <details>HDL contains lipids and three apoA-I molecules. Each A-I molecular weight is about 28kDa.</details>
                <number_of_copies>1</number_of_copies>
                <oligomeric_state>monomer</oligomeric_state>
                <recombinant_exp_flag>false</recombinant_exp_flag>
                <recombinant_expression/>
                <sequence/>
            </macromolecule>
        </macromolecule_list>
    </sample>
    <structure_determination_list>
        <structure_determination>
            <method>subtomogramAveraging</method>
            <specimen_preparation_list>
                <specimen_preparation id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="tomography_preparation_type">
                    <concentration units="mg/mL">0.01</concentration>
                    <buffer>
                        <ph>7.4</ph>
                        <details>1X Dulbeccos phosphate-buffered saline (Invitrogen, La Jolla, CA), 2.7 mM KCl, 1.46 mM KH2PO4, 136.9 mM NaCl, and 8.1 mM Na2HPO4</details>
                    </buffer>
                    <staining>
                        <type>negative</type>
                        <details>EM Specimens were prepared by standard cryo-EM protocol.</details>
                    </staining>
                    <grid>
                        <details>200 mesh glow-discharged holey thin carbon-coated EM grids (Cu-200HN, Pacific Grid-Tech, USA)</details>
                    </grid>
                    <vitrification>
                        <cryogen_name>NITROGEN</cryogen_name>
                        <chamber_humidity>90</chamber_humidity>
                        <chamber_temperature>103</chamber_temperature>
                        <instrument>HOMEMADE PLUNGER</instrument>
                        <details>Vitrification instrument: Manual</details>
                        <method>Blot for 2 seconds before plunging</method>
                    </vitrification>
                </specimen_preparation>
            </specimen_preparation_list>
            <microscopy_list>
                <microscopy id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="tomography_microscopy_type">
                    <microscope>FEI TECNAI 12</microscope>
                    <illumination_mode>FLOOD BEAM</illumination_mode>
                    <imaging_mode>BRIGHT FIELD</imaging_mode>
                    <electron_source>LAB6</electron_source>
                    <acceleration_voltage>120</acceleration_voltage>
                    <nominal_cs>2.2</nominal_cs>
                    <nominal_defocus_min>1000</nominal_defocus_min>
                    <nominal_defocus_max>2000</nominal_defocus_max>
                    <nominal_magnification>67000.</nominal_magnification>
                    <specimen_holder_model>GATAN LIQUID NITROGEN</specimen_holder_model>
                    <temperature>
                        <temperature_average units="Kelvin">100</temperature_average>
                    </temperature>
                    <details>Tilt step is 1.5 degree</details>
                    <image_recording_list>
                        <image_recording>
                            <film_or_detector_model category="CCD">GATAN ULTRASCAN 4000 (4k x 4k)</film_or_detector_model>
                            <digitization_details>
                                <sampling_interval units="um">1.73</sampling_interval>
                            </digitization_details>
                            <number_real_images>93</number_real_images>
                            <average_electron_dose_per_image units="e/A**2">140</average_electron_dose_per_image>
                            <bits_per_pixel>16.</bits_per_pixel>
                        </image_recording>
                    </image_recording_list>
                    <specimen_holder>Gatan</specimen_holder>
                    <tilt_series>
                        <axis1>
                            <min_angle units="degrees">-69</min_angle>
                            <max_angle units="degrees">69</max_angle>
                        </axis1>
                    </tilt_series>
                </microscopy>
            </microscopy_list>
            <image_processing id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="subtomogram_averaging_processing_type">
                <details>Single targeted particle"s images were reconstructed by focus ET reconstruction (FETR) algorithm of individual particle electron tomography (IPET) method. Average number of tilts used in the 3D reconstructions: 93. Average tomographic tilt angle increment: 1.5.</details>
                <final_reconstruction>
                    <algorithm>Focused ET reconstruction algorithms</algorithm>
                    <resolution>36</resolution>
                    <resolution_method>36</resolution_method>
                    <software_list>
                        <software>
                            <name>IPET and FETR</name>
                        </software>
                    </software_list>
                    <details>Map was reconstructed by individual-particle electron tomography (IPET)and Focus ET Reconstruction Algorithm.</details>
                </final_reconstruction>
                <ctf_correction>
                    <details>TOMOCTF</details>
                </ctf_correction>
                <final_angle_assignment>
                    <details>Tomography tilt angle from -60 to 60 in step of 1.5</details>
                </final_angle_assignment>
            </image_processing>
        </structure_determination>
    </structure_determination_list>
    <map size_kbytes="31251" format="CCP4">
        <file>emd_2024.map.gz</file>
        <symmetry>
            <space_group>1</space_group>
        </symmetry>
        <data_type>Image stored as Reals</data_type>
        <dimensions>
            <col>200</col>
            <row>200</row>
            <sec>200</sec>
        </dimensions>
        <origin>
            <col>0</col>
            <row>0</row>
            <sec>0</sec>
        </origin>
        <spacing>
            <x>200</x>
            <y>200</y>
            <z>200</z>
        </spacing>
        <cell>
            <a>346.0</a>
            <b>346.0</b>
            <c>346.0</c>
            <alpha>90.0</alpha>
            <beta>90.0</beta>
            <gamma>90.0</gamma>
        </cell>
        <axis_order>
            <fast>X</fast>
            <medium>Y</medium>
            <slow>Z</slow>
        </axis_order>
        <statistics>
            <minimum>-0.05142312</minimum>
            <maximum>0.15066135</maximum>
            <average>0.00679005</average>
            <std>0.02501582</std>
        </statistics>
        <pixel_spacing>
            <x>1.73</x>
            <y>1.73</y>
            <z>1.73</z>
        </pixel_spacing>
        <contour_list>
            <contour primary="true">
                <level>0.1</level>
                <source>AUTHOR</source>
            </contour>
        </contour_list>
        <annotation_details>17nm high density lipoprotein (HDL) particle</annotation_details>
        <details>::::EMDATABANK.org::::EMD-2024::::</details>
    </map>
    <interpretation>
        <modelling_list>
            <modelling/>
        </modelling_list>
    </interpretation>
</emd>
