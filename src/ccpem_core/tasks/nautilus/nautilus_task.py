#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core.settings import which
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.refmac import refmac_task
from ccpem_core.tasks.buccaneer import buccaneer_task

class Nautilus(task_utils.CCPEMTask):
    '''
    CCPEM Nautilus task.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='Nautilus',
        author='Cowtan K',
        version='0.5',
        description=(
            'Nautilus performs automated building of RNA/DNA from electron '
            'density.\n'
            'Nautilus does not currently perform refinement - you will need '
            'to refine and recycle for further model building yourself. '
            'N.B. requires CCP4.'),
        short_description=(
            'Automated model building.  Requires CCP4'),
        documentation_link='http://www.ccp4.ac.uk/html/cnautilus.html',
        references=None)

    def __init__(self,
                 task_info=task_info,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 verbose=False,
                 parent=None):
        self.atomsf = os.path.join(
            os.environ['CLIBD'],
            'atomsf_electron.lib')
        command = which(program='refmac5')
        super(Nautilus, self).__init__(
            command=command,
            task_info=task_info,
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            verbose=verbose,
            parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-mapin',
            '--input_map',
            help='''Target input map (mrc format)''',
            type=str,
            metavar='Input map',
            default=None)
        #
        parser.add_argument(
            '-resolution',
            '--resolution',
            help='''Resolution of input map (Angstrom)''',
            metavar='Resolution',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-input_seq',
            '--input_seq',
            help='Input sequence file in any common format (e.g. pir, fasta)',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-extend_pdb',
            '--extend_pdb',
            help='Initial PDB model to extend (pdb format)',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-ncycle',
            '--ncycle',
            help='Number of Nautilus pipeline cycles',
            metavar='Build cycles',
            type=int,
            default=5)
        #
        parser.add_argument(
            '-ncycle_refmac',
            '--ncycle_refmac',
            help='Number of refmac cycles',
            metavar='Build cycles',
            type=int,
            default=10)
        
        #
        parser.add_argument(
            '-map_sharpen',
            '--map_sharpen',
            metavar='Map sharpen',
            help=('B-factor to apply to map. Negative B-factor to sharpen, '
                  'positive to blur, zero to leave map as input.'),
            type=float,
            default=0)
        #
        parser.add_argument(
            '-keywords',
            '--keywords',
            help='Keywords for advanced options.  Select file or define text',
            type=str,
            metavar='Keywords',
            default='')
        #
        return parser

    def run_pipeline(self, job_id=None, run=True, db_inject=None):
        '''
        Generate job classes and process.  Run=false for reloading.
        '''
        # Convert map to mtz (refmac)
        bfactor = self.args.map_sharpen()
        if bfactor >= 0:
            sharp_array = None
            blur_array = [bfactor]
        else:
            sharp_array = [-1 * bfactor]
            blur_array = None
        self.process_maptomtz = refmac_task.RefmacMapToMtz(
            command=self.command,
            resolution=self.args.resolution.value,
            mode='Global',
            name='Map to MTZ',
            job_location=self.job_location,
            map_path=self.args.input_map.value,
            blur_array=blur_array,
            sharp_array=sharp_array,
            atomsf_path=self.atomsf)
        pl = [[self.process_maptomtz.process]]

        # Set MTZ SigF and FOM with SFTools
        self.process_sftools_buccanneer = buccaneer_task.SFToolsBuccaneer(
            job_location=self.job_location,
            hklin=self.process_maptomtz.hklout_path,
            name='Set MTZ SigF and SG')
        pl.append([self.process_sftools_buccanneer.process])

        # Set column labels with Cad
        self.process_cad_labels = buccaneer_task.CADBuccaneerLabels(
            job_location=self.job_location,
            resolution=self.args.resolution.value,
            hklin=self.process_sftools_buccanneer.hklout,
            sharpen_bfactor=self.args.map_sharpen.value,
            name='Set MTZ labels')
        pl.append([self.process_cad_labels.process])

        # Create R free flags
        hklout = os.path.join(self.job_location,
                              'nautilus.mtz')
        self.process_free_r_flags = buccaneer_task.FreeRFlags(
            job_location=self.job_location,
            hklin=self.process_cad_labels.hklout,
            hklout=hklout,
            name='Set Rfree')
        pl.append([self.process_free_r_flags.process])

        # Save seq if sequence string is provided rather than file path
        if (not os.path.exists(self.args.input_seq.value) and
                isinstance(self.args.input_seq.value, str)):
            path = os.path.join(self.job_location,
                                'input.seq')
            f = open(path, 'w')
            f.write(self.args.input_seq.value)
            f.close()
            self.args.input_seq.value = f.name

        # Run Nautilus - new pipeline = Nautilus->Refmac refine pipeline->repeat...
        refine_keywords='''labin PHIB=PHIem FOM=FOMem FREE=FreeR_flag FP=Fem{0} SIGFP=SIGFem{0}
PHOUT
PNAME nautilus
DNAME nautilus
'''.format(self.process_cad_labels.label_out_suffix)

        for i in range(1, (self.args.ncycle.value + 1)) :
            # 1st cycle Nautilus
            if i == 1 :
                self.process_nautilus_pipeline = NautilusPipeline(
                            job_location=self.job_location,
                            hklin=self.process_free_r_flags.hklout,
                            seqin=self.args.input_seq.value,
                            label_out_suffix=self.process_cad_labels.label_out_suffix,
                            ncycle=i,
                            resolution=self.args.resolution.value,
                            pdbin=self.args.extend_pdb.value,
                            pdbout=None,
                            name='Nautilus build',
                            job_title=self.args.job_title.value,
                            keywords=self.args.keywords.value)
                pl.append([self.process_nautilus_pipeline.process])

                # Run RefmacRefine (global)
                self.process_refine = refmac_task.RefmacRefine(
                    command=self.command,
                    job_location=self.job_location,
                    pdb_path=self.process_nautilus_pipeline.pdbout,
                    mtz_path=self.process_free_r_flags.hklout,
                    resolution=self.args.resolution.value,
                    mode='Global',
                    name='Refmac refine (global)',
                    sharp=self.args.map_sharpen.value,
                    ncycle=self.args.ncycle_refmac(),
                    output_hkl=True,
                    keywords=refine_keywords)
                pl.append([self.process_refine.process])
            else :
                #nth cycle Nautilus
                self.process_nautilus_pipeline = NautilusPipeline(
                            job_location=self.job_location,
                            hklin=self.process_refine.hklout_path,
                            seqin=self.args.input_seq.value,
                            label_out_suffix=self.process_cad_labels.label_out_suffix,
                            ncycle=i,
                            resolution=self.args.resolution.value,
                            pdbin=self.process_refine.pdbout_path,
                            pdbout=None,
                            name='Nautilus build',
                            job_title=self.args.job_title.value,
                            keywords=self.args.keywords.value)
                pl.append([self.process_nautilus_pipeline.process])

                # Run RefmacRefine (global)
                self.process_refine = refmac_task.RefmacRefine(
                    command=self.command,
                    job_location=self.job_location,
                    pdb_path=self.process_nautilus_pipeline.pdbout,
                    mtz_path=self.process_free_r_flags.hklout,
                    resolution=self.args.resolution.value,
                    mode='Global',
                    name='Refmac refine (global)',
                    sharp=self.args.map_sharpen.value,
                    ncycle=10,
                    output_hkl=True,
                    keywords=refine_keywords)
                pl.append([self.process_refine.process])

        custom_finish = NautilusResultsOnFinish(
            stdout=self.process_nautilus_pipeline.process.stdout,
            refine_process=self.process_refine)

        if run:
            os.chdir(self.job_location)
            self.pipeline = process_manager.CCPEMPipeline(
                pipeline=pl,
                job_id=job_id,
                args_path=self.args.jsonfile,
                location=self.job_location,
                db_inject=db_inject,
                database_path=self.database_path,
                taskname=self.task_info.name,
                title=self.args.job_title.value,
                verbose=self.verbose,
                on_finish_custom=custom_finish)
            self.pipeline.start()


    def validate_args(self):
        # Now do at gui level
        return True

class NautilusPipeline(object):
    '''
    Real time nucleic acid chain tracing (N.B. runs cnautilus)
    '''
    def __init__(self,
            job_location,
            hklin,
            seqin,
            label_out_suffix,
            ncycle=1,
            resolution=2.0,
            pdbin=None,
            pdbout=None,
            name=None,
            job_title=None,
            keywords=None):
        command = which('cnautilus')
        # counter for overall NautilusPipeline cycles, start with 1
        assert command is not None
        self.job_location = job_location
        self.hklin = hklin
        self.seqin = seqin
        assert os.path.exists(path=self.seqin)
        self.label_out_suffix = label_out_suffix
        self.pdbout = pdbout
        self.ncycle = ncycle
        self.resolution = resolution
        self.pdbin = pdbin
        if self.pdbout is None:
            self.pdbout = os.path.join(job_location, 'build.pdb')
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.job_title = job_title
        self.stdin = None
        self.stdin_extra = None
        self.keywords = keywords
        #
        self.set_args()
        self.set_stdin()
        #
        self.process = process_manager.CCPEMProcess(
                name=self.name,
                command=command,
                args=self.args,
                location=self.job_location,
                stdin=self.stdin)

    def set_args(self):
        self.args = ['-stdin']

    def set_stdin(self):
        pdbin_ref = os.path.join(os.environ['CLIBD'],
            'nautilus_lib.pdb')
        if self.ncycle == 1:
            self.buc_cycle = 5
            self.stdin_extra = '''colin-phifom PHIem, FOMem
            '''
        else:
            self.buc_cycle = 3
            self.stdin_extra = '''colin-hl HLACOMB, HLBCOMB, HLCCOMB, HLDCOMB
colin-fc FWT, PHWT
'''

        self.stdin = '''
title {0}
pdbin-ref {1}
seqin {2}
mtzin {3}
colin-fo Fem{4},SIGFem{4}
colin-free FreeR_flag
pdbout {5}
cycles {6}
resolution {7}
anisotropy-correction
xmlout program.xml
'''.format(
    self.job_title,
    pdbin_ref,
    self.seqin,
    self.hklin,
    self.label_out_suffix,
    self.pdbout,
    self.buc_cycle,
    self.resolution)

        self.stdin = self.stdin + self.stdin_extra
        # Add optional nautilus keywords
        if isinstance(self.keywords, str):
            if self.keywords != '':
                # Remove trailing white space and new lines
                keywords = self.keywords.strip()
                for line in keywords.split('\n'):
                    self.stdin += line + '\n'
        if self.pdbin is not None:
            self.stdin += 'pdbin {0}\n'.format(self.pdbin)

class NautilusResultsOnFinish(process_manager.CCPEMPipelineCustomFinish):
    '''
    Generate RVAPI results on finish.
    '''
    def __init__(self,
                 stdout, refine_process):
        super(NautilusResultsOnFinish, self).__init__()
        #self.pipeline_path = pipeline_path
        self.nautilus_stdout = stdout
        self.refine_process = refine_process

    def on_finish(self, parent_pipeline=None):
        #copy refine.pdb to nautilus.pdb just as the old pipeline
        target_path = os.path.dirname(self.refine_process.pdbout_path)
        targetpdb_path = os.path.join(target_path, 'nautilus.pdb')
        os.system('cp {0} {1}' .format(self.refine_process.pdbout_path, targetpdb_path))

        #concatenate nautilus stdout and refinement stdout
        #old pipeline has these in one stdout file, new pipeline produces separate stdout
        targetout_path = os.path.join(target_path, 'NautilusPipeline_joined.txt')
        os.system('cat {0} {1} > {2}' .format(self.nautilus_stdout,
                                              self.refine_process.process.stdout,
                                              targetout_path))
        # Start rvapi process
        import subprocess
        args = ['ccpem-python', '-m', 'ccpem_core.gui_ext.tasks.nautilus.nautilus_results',
                targetout_path]
        directory = os.path.join(os.path.dirname(self.nautilus_stdout),
                                 'report')

        try:
            os.stat(directory)
        except OSError:
            os.mkdir(directory)
        subprocess.Popen(args=args,
                         stdout=open(os.path.join(directory, 'rv.out'), 'w'),
                         stderr=open(os.path.join(directory, 'rv.err'), 'w'))


def main():
    '''
    Run task
    '''
    task_utils.command_line_task_launch(
        task=Nautilus)

if __name__ == '__main__':
    main()
