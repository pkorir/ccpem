#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import shutil
import unittest
import numpy as np
try:
    from ccpem_core.swig_eg.array_eg.ext import swig_eg_array as array_eg_ext
    swig_available = True
except ImportError:
    swig_available = False

class Test(unittest.TestCase):
    '''
    Unit test for mmdb_coord_tools.
    '''
    def setUp(self):
        self.test_data = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'test_data')
        self.test_output = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'test_output')
        os.mkdir(self.test_output)

    def tearDown(self):
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)

    @unittest.skipIf(not swig_available,
                     'Skipping swig examples')
    def test_swig_arrays(self):
        '''
        Testing various c++/swig bindings for use with arrays
        '''
        # Compute factorial of n
        n = int(2.1)
        print '\n\n*** Passing arguments to c function'
        print 'Pass int and return its factorial'
        print '    n = ', n, '    |  Factorial = ', array_eg_ext.Fact(n)
        if True:
            # Test vector functions
            print '\n\n*** Test vector functions'
            print 'std_vector support from swig'
            tst = array_eg_ext.VectorInitFunc(3)
            print 'Print vector from python: ', tst
            print 'Print element 0 of vector from python: ', tst[0]
            print 'Initiate vector of size n from python'
            array_eg_ext.VectorTest(4)

            # Vector class functions
            print '\n\nTesting vector class'
            vector_class = array_eg_ext.VectorClass(1)
            print vector_class.test_int
            print vector_class.other_int
            # Multi dimension std vector
            print '\n\nTest multi dimension std vector support'
            # 2d
            vec_2d = array_eg_ext.ReturnVector2D()
            print 'Returned 2d tuple        : ', vec_2d
            # Convert returned tuple to list for assignment
            list_2d = map(list, vec_2d)
            list_2d[0][1] = 99.9
            print 'List with modified value : ', list_2d
            # 3d
            vec_3d = array_eg_ext.ReturnVector3D()
        # Testing inplace numpy arrays
        if True:
            print '\n\n*** Test swig//numpy input array functions'
            # http://docs.scipy.org/doc/numpy/reference/swig.interface-file.html
            a = np.array([1, 2, 3], 'd')
            print a
            array_eg_ext.ArrayInPlaceMutliply(a)
            b = a
            array_eg_ext.ArrayInPlaceMutliply(b)
            print a
            print b
            b = a.copy()
            array_eg_ext.ArrayInPlaceMutliply(b)
            print a
            print b
            print type(b)
        if True:
            print '\n\nSimple class constructors test'
            rectangle_a = array_eg_ext.DummyClassConstructor('test', 2 , 3)
            rectangle_b = array_eg_ext.DummyClassConstructor('test', 20, 30)
            print rectangle_a.area()
            print rectangle_b.area()
            rectangle_default = array_eg_ext.DummyClassConstructor(
                'test', 2, 3)
            rectangle_default.PrintVariables()
            rectangle_default.PrintTest()
            print '\n\nArray function test'
            a = np.array([5,6,7], 'd')
            b = np.array([4,2,3], 'd')
            array_eg_ext.ModArrays(a, b)
            print a
            print b
            print '\n\nArray class test'
            print '\nSTART: Numpy python arrays'
            print a
            print b
            test = array_eg_ext.ArrayClassConstructor('tst_file_name', a, b)
            test.ArrayTest()
            test.MultipyArray1()
            print '\nEnd: Numpy python arrays'
            print a
            print b
            test.ArrayTest()
            c = np.append(b, [1, 3])
            print b
            print c
            test.ArrayTest()
        if True:
            print '\n\n Numpy/swig type maps'
            #  swig/numpy does not provide support for output arrays
            #  see: http://docs.scipy.org/doc/numpy/reference/swig.interface-file.html
            #     ---- trying with typemaps instead
            # Class with numpy array member
            print '\nTest c class with swig/numpy '
            np_array = np.array([1,2,3], 'd')
            numpy_array_class = array_eg_ext.NumpyArrayClass()
        # Testing carrays
        if True:
            print '\n\n*** Creating an array with carrays.i'
            a = array_eg_ext.new_array_double(10)
            for i in xrange(10):
                array_eg_ext.array_double_setitem(a, i, float(1.9+i))
            print 'Pass array and print from C function'
            array_eg_ext.print_array(a)
            print 'Get item from array and print from python'
            for x in xrange(10):
                print array_eg_ext.array_double_getitem(a, x)
            int_array = array_eg_ext.IntArrayClass(10)
            for i in range(0, 10):
                int_array[i] = i+10
            print int_array
            array_eg_ext.print_int_array(int_array)
            print a
        # Testing returning an array of given size from c
        if True:
            # Float array of size 3
            array_float_3 = array_eg_ext.ReturnArrayFloat3()
            print array_float_3
            # Int array of size 3
            array_int_3 = array_eg_ext.ReturnArrayInt3()
            print array_int_3
            # Int array of size 3; return tuple instead of list
            tuple_int_3 = array_eg_ext.ReturnArrayInt3Tuple()
            print tuple_int_3
            # N.B. these functions only work for given size as specified in the
            # .i file for potential method of determing the size on-the-fly
            # see this:
            # http://stackoverflow.com/questions/11679023/python-swig-output-an-array/11680521
        if True:
            # C++ standard vector array input from python numpy array
            # 1D
            array_1d = np.array([1, 2, 10, 2], dtype=float)
            array_eg_ext.OneDArrayTest(array_1d)
            print '\nNumpy shape : ', array_1d.shape
            # 2D
            array_2d = np.array([[1, 2, 3], [11, 12, 13]], dtype=float)
            array_eg_ext.TwoDArrayTest(array_2d)
            print '\nNumpy shape : ', array_2d.shape
            # 3D
            array_3d = np.array([[[1, 1], [2, 2]],
                                 [[3, 3], [4, 4]]], dtype=float)
            array_eg_ext.ThreeDArrayTest(array_3d)
            print '\nNumpy shape : ', array_3d.shape

if __name__ == '__main__':
    unittest.main()
