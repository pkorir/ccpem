#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import sys
import os
import json
from collections import OrderedDict
from ccpem_core import ccpem_utils
from ccpem_core.ccpem_utils import ccpem_argparser


class CCPEMBibtexRef(object):
    '''
    Store bibtex formated reference.
    N.B. Citeulike.org is a good source of Bibtex formated refs.
    '''
    def __init__(self,
                 author,
                 journal,
                 year,
                 title,
                 volume,
                 number,
                 pages):
        self.author = ' '.join(author.split()).replace(' and', ',')
        self.journal = journal
        self.year = int(year)
        self.title = title
        self.volume = volume
        self.number = number
        self.pages = pages


class CCPEMTaskInfo(object):
    '''
    Store program information.
    '''
    def __init__(self,
                 name,
                 author,
                 version,
                 description,
                 short_description,
                 documentation_link=None,
                 references=None):
        self.name = name
        self.author = author
        self.version = version
        self.id = self.name + '_' + str(version)
        self.description = description
        self.documentation_link = documentation_link
        assert len(short_description) < 200
        self.short_description = short_description
        self.references = references

    def add_reference(self, bibtex_ref):
        '''
        Add bibtex reference to task
        '''
        self.references.append(bibtex_ref)

class CCPEMTask(object):
    '''
    Base class for CCPEM tasks
    '''

    task_info = CCPEMTaskInfo(
        name='example_prog',
        author='Doe J',
        version='1.0',
        description=(
            'Complete description'),
        short_description=(
            'Short for tooltips (less 100 characters)'),
        documentation_link=None,
        references=None)

    def __init__(self,
                 command,
                 task_info=task_info,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 verbose=False,
                 parent=None):
        self.parent = parent
        assert command is not None
        self.command = command
        self.database_path = database_path
        self.task_info = task_info
        self.verbose = verbose
        # Set args
        if args_json is not None or args is None:
            self.set_args(jsonfile=args_json)
        else:
            self.args = args
        self.pipeline = pipeline
        if self.pipeline is not None:
            self.set_args(jsonfile=self.pipeline.args_path)
            self.job_location = self.pipeline.location
        else:
            self.job_location = job_location

    def set_args(self, jsonfile=None):
        if jsonfile is not None:
            if not os.path.exists(jsonfile):
                ccpem_utils.print_warning(
                    message='Json file not found : ' + jsonfile)
        self.args = self.parser().generate_arguments(
            jsonfile=jsonfile)

    def parser(self):
        '''
        Set args from CCPEM args parser.
        '''
        parser = ccpem_argparser.ccpemArgParser()
        return parser

    def run_task(self,
                 run=True,
                 job_id=None,
                 db_inject=None,
                 set_job_location=True):
        '''
        Validate args, set job location, save args and launch pipeline.
        '''
        # Set job location (allows job location to be used in path generation 
        # in validate args
        if set_job_location:
            self.set_job_location(job_id=job_id)
        if self.validate_args():
            # Make job directory - only make directory when needed
            self.create_job_directory()
            # Save arguments
            self.args.output_args_as_json(os.path.join(self.job_location,
                                                       'args.json'))
            # Run job
            if run:
                self.run_pipeline(job_id=job_id,
                                  db_inject=db_inject)

    def validate_args(self):
        '''
        Check arguments before running pipeline.  Return true if ok, return 
        false and give warning if not.  Unique to each task.
        '''
        return True

    def set_job_location(self, job_id=None):
        '''
        Set job location but do not create directory
        '''
        if self.job_location is not None:
            path = self.job_location
        elif self.database_path is not None:
            if job_id is not None:
                path = os.path.join(
                    os.path.dirname(self.database_path),
                    self.task_info.name + '_' + str(job_id))
            else:
                path = os.path.join(
                    os.path.dirname(self.database_path),
                    self.task_info.name + '_1')
        else:
            exec_path = os.path.dirname(os.path.realpath(sys.argv[0]))
            path = os.path.join(exec_path,
                                self.task_info.name + '_1')
        self.job_location = ccpem_utils.check_directory_and_make(
            path,
            verbose=True,
            auto_suffix=True,
            make=False)
        if hasattr(self.args, 'job_location'):
            self.args.job_location.value = self.job_location

    def create_job_directory(self):
        '''
        Create job directory
        '''
        try:
            os.mkdir(self.job_location)
        except OSError:
            pass
        assert os.path.exists(self.job_location)

    def run_pipeline(self, job_id=None):
        '''
        Setup and run pipeline.  Unique to each task.
        '''
        pass

    def set_arg_absolute_path(self, arg):
        '''
        Check file input is given and can be found.  Converts to abs path.
        '''
        warnings = ''
        if arg.value is None:
            warnings += '\n{0} not defined'.format(arg.metavar)
        else:
            arg.value = os.path.abspath(arg.value)
            if not os.path.exists(path=arg.value):
                warnings += '\n{0} not found : {1}'.format(
                    arg.metavar,
                    arg.value)
        return warnings

    def print_command_line_warnings(self, warnings):
        print '\n\nError: {0} failed'.format(self.task_info.name)
        print warnings

def command_line_task_launch(task,
                             args_json=None):
    '''
    Launch task from command line.
    '''
    eg_args = False

    # Print header
    message = 'CCP-EM | ' + task.task_info.name
    ccpem_utils.print_header(message=message)
    message = 'CCP-EM command line task'
    ccpem_utils.print_sub_header(message=message)
    print '  Usage:'
    info = OrderedDict([
        ('ccpem-<name>-task --eg_args', 'Generate example input arguments (.json)'),
        ('ccpem-<name>-task <args.json>', 'Set inputs from JSON file')])
    for key in info.keys():
        print '    {0: <34} {1}'.format(key, info[key])

    for arg in sys.argv:
        if args_json is None:
            if arg[-5:] == '.json':
                try:
                    json.load(open(arg, 'r'))
                    args_json = arg
                except ValueError:
                    args_json = None
                    message = 'Can not read {0}'.format(arg)
                    ccpem_utils.print_warning(message=message)

        if arg in ['--eg_args', '--egargs', '--eg-args']:
            eg_args = True

    # Set job location
    job_location = os.path.join(os.getcwd(),
                                task.task_info.name + '_1')
    # Run task
    run_task = task(args_json=args_json,
                    job_location=job_location)

    # Check args have been given.
    if args_json is None and not eg_args:
        eg_args = True
        ccpem_utils.print_sub_header(
            message='Warning: no JSON args supplied')

    # Output example args json file
    if eg_args:
        ccpem_utils.print_sub_header(
            message='Generate example argument JSON input')
        job_args = run_task.parser().generate_arguments()
        task_name = run_task.task_info.name
        filename = task_name.lower() + '_eg.json'
        job_args.output_args_as_json(filename=filename)
        print '  Example argument file : ', filename
        ccpem_utils.print_footer()

    # Run command line if no GUI requested and args file is given
    if args_json is not None:
        ccpem_utils.print_sub_header(message='Arguments')
        print run_task.args.output_args_as_text()
        run_task.run_task()
        print '  \nTask running in background, use ctrl-z to regain terminal'
        ccpem_utils.print_footer()
        sys.exit()
