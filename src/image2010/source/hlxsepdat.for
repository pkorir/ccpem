C*HLXSEPDAT.FOR***********************************************************
C     FEEDER FOR HLXSEPR
C     COPY DATA FROM FILES CREATED BY HLXDUMP FOR DIFFERENT VIEWS
C     IN KNOWN ORIENTATIONS TO A DIRECT ACCESS AREA
C      Input on stream 2 with filenames specified in data
C      Output to direct access area on stream 3
C      N.B.  PROGRAM ASSUMES THAT EACH SIDE CONTAINS THE SAME SET
C            OF LAYER LINES.
C      	Control data:
C        NVIEW,LLMAX,DELBR,C,IREST (*)
C       Then for each side:
C        FILIN
C        PHI,XSHIFT,ZSHIFT,IPOLE
C
C        NVIEW  No. of "sides" to be input for this run
C        LLMAX  No. of layer lines for each side
C        DELBR  Layer line sampling in rec. Ang.
C        C      Helical repeat in Angstroms
C        IREST  0  Not restart ie Starting new direct access area
C               1  Restart     ie Adding data to existing area
C
C        FILIN  File name for this side of data
C        PHI    Angle of view in degrees  For reference particle near
C               side has PHI=270, far side has PHI=90. Relative values
C               of PHI found in HLXFIT then add to these. e.g. If
C               HLXFIT gives a value -10 degrees for a second particle
C               corresponding PHI values for near and far sides will
C               be 260 and 80 respectively.
C        XSHIFT Extra x-shift of origin in angstroms. Generally zero.
C        ZSHIFT y-shift in angstroms. Used to refer all particles to
C               common origin.  Use the value from HLXFIT.
C        IPOLE  Used to invert particle in z-direction if HLXFIT
C               indicates this is necessary.
C        XSHIFT,ZSHIFT,IPOLE act on layer line data before it is
C               written onto direct access data set
C
C     VERSION 1.0 FOR VAX     RAC     29NOV82
C     Transferred and tested on Alpha (Remove define file)  RAC  8dec97
C     VERSION 2.0  with imsubs2000    RAC 13NOV00
C
C     Set max no of views and points on layer line
      PARAMETER (NVIEWMAX=32,NPTMAX=150)
c      DEFINE FILE 3(320,608,U,N2)
      REAL*8 HEAD(10)
      DIMENSION NREC(NVIEWMAX+1),PHI(NVIEWMAX),F(NPTMAX),PSI(NPTMAX)
      DIMENSION IPOLE(NVIEWMAX),ZSHIFT(NVIEWMAX),XSHIFT(NVIEWMAX)
      character filin*80
      character filout*80
C
      WRITE(6,1030)
      read(5,1005) filout
      write(6,1006) filout
CTSH      open(unit=3,name=filout,status='unknown',
CTSH     * form='unformatted',recl=1216,access='direct',
CTSH     * associatevariable=n2)
CTSH++
c      open(unit=3,name=filout,status='unknown',
c     * form='unformatted',recl=1216,access='direct')
C*** jms 16.06.2010
      open(unit=3,file=filout,status='unknown',
     * form='unformatted',recl=1216,access='direct')
C the way N2 is used, I dont think it needs to be declared as the
C 'associate variable' (Linux objects to 'associate variable' and other
C OPEN parameters)
CTSH--
      TWOPI=360.0
C
      READ(5,*) NVIEW,LLMAX,DELBR,C,IREST
      WRITE(6,1000) NVIEW,LLMAX,DELBR,C,IREST
C     Set input stream and direct access record no
      NN=2
      N2=2
      NVIEWMAX1=NVIEWMAX+1
      DO 20 IV=1,NVIEWMAX1
   20 NREC(IV)=0
      IV1=0
      IF(IREST.EQ.0) GO TO 10
C
C     RESTART FACILITY - ADD MORE VIEWS TO EXISTING DISC AREA
C      READ(3'1) NREC,PHI,DELBR,C
      read(3,rec=1) nrec,phi,delbr,c
      DO 15 I=1,NVIEWMAX
      II=I
      IF(NREC(I).EQ.0) GO TO 16
   15 CONTINUE
C
      WRITE(6,1040)
      STOP
C
   16 N2=NREC(II-1)
      IV1=II-2
C
   10 DO 500 IV=1,NVIEW
C     Read filename and open input stream
      READ(5,1020) FILIN
      WRITE(6,1021) FILIN
C      OPEN(UNIT=NN,NAME=FILIN,STATUS='OLD')
C*** jms 16.06.2010
      open(unit=nn,file=filin,status='old')
      READ(5,*) PHI(IV+IV1),XSHIFT(IV),ZSHIFT(IV),IPOLE(IV)
      WRITE(6,1001) PHI(IV+IV1),XSHIFT(IV),ZSHIFT(IV),IPOLE(IV)
C
      WRITE(6,1003) IV,N2
      IF(IV+IV1.GT.NVIEWMAX) THEN
         WRITE(6,1040)
         STOP
      ENDIF
      NREC(IV+IV1)=N2
      XSHIFT(IV)=XSHIFT(IV)*TWOPI
      ZSHIFT(IV)=ZSHIFT(IV)*TWOPI/C
C
      DO 100 L=1,LLMAX
      IP=0
      READ(NN,1010,END=490) HEAD,SCALE,N,NL
      WRITE(6,1002) HEAD,SCALE,N,NL
   50 IF(IP+1.GT.NPTMAX) THEN
         WRITE(6,1041) NPTMAX
         STOP
      ENDIF
C
      READ(NN,1011,END=100) R,F(IP+1),PSI(IP+1)
      IF(R.EQ.0.0.AND.IP.GT.0) GO TO 100
      IP=IP+1
      IF(IPOLE(IV).EQ.1) PSI(IP)=360.0-PSI(IP)
      PSI(IP)=PSI(IP)+NL*ZSHIFT(IV)+R*XSHIFT(IV)
      IR=R/DELBR+1.5
      IF(IP.EQ.1) IRMIN=IR
      IRMAX=IR
      GO TO 50
C     IRMIN, IRMAX index arrays starting from 1 at R=0
C      100 WRITE(3'N2) NL,N,IRMIN,IRMAX,(F(I),PSI(I),I=1,IP)
  100 write(3,rec=n2) nl,n,irmin,irmax,(f(i),psi(i),i=1,ip)
C     Skip any layer lines not wanted
  101 READ(NN,1010,END=490) HEAD(1)
      GO TO 101
C     Close this input file so stream can be used for next
  490 CLOSE(NN)
  500 CONTINUE
C
      NREC(NVIEW+IV1+1)=N2
C      WRITE(3'1) NREC,PHI,DELBR,C
      write(3,rec=1) nrec,phi,delbr,c
      STOP
C
 1000 FORMAT(///'  Number of views       ',I10,
     1         /'  Number of layer lines ',I10,
     2         /'  Layer line sampling   ',F10.6,' rec ang',
     3         /'  Helical repeat        ',F10.1,' angstroms',
     4         /'  Restart?  No/Yes 0/1  ',I10/)
 1001 FORMAT(///'  View angle             ',F10.2,' degrees'
     1         /'  X shift                ',F10.2,' angstroms'
     2         /'  Z shift                ',F10.2,' angstroms'
     3         /'  IPOLE invert No/yes 0/1',I10/)
 1002 FORMAT(1X,10A5,F10.0,2I5)
 1003 FORMAT('0VIEW',I4,'  STORED IN DIRECT ACCESS AREA,STARTING AT
     1 RECORD NO.',I4)
 1005 format(a)
 1006 format('  Name of output direct access data storage file :
     1  '//3x,a)
 1010 FORMAT(10A5,F10.0,2I5)
 1011 FORMAT(3E10.3)
 1020 FORMAT(A)
 1021 FORMAT(//'  Layer line input file : ',A/)
 1030 FORMAT('1'///'  HLXSEPDAT : Set up data for HLXSEPR'//)
 1040 FORMAT(///'  Direct access area full - too many views')
 1041 FORMAT(///'  Too many points on layer line - max allowed',I5)
      END
