!=******************************************************************************
!=* WeightedFLCF_lib.f90            		  AUTHORS: S.DOLGOBRODOV       *
!=*                                    		  AUTHORS: A.ROSEMAN           *
!=*									       *
!=* Subroutines and functions to support implementation of the WeightedFLCF,   *
!=* for particle finding in electron micrographs.			       *
!*            								       *
!=* Copyright (C) 2012 The University of Manchester 	                       *
!=*                                                                            *
!=*                                                                            *
!=*    This program is free software: you can redistribute it and/or modify    *
!=*    it under the terms of the GNU General Public License as published by    *
!=*    the Free Software Foundation, either version 3 of the License, or       *
!=*    (at your option) any later version.                                     *  
!=*        								       *
!=*    This program is distributed in the hope that it will be useful,         *
!=*    but WITHOUT ANY WARRANTY; without even the implied warranty of          *
!=*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
!=*    GNU General Public License for more details.                            *
!=* 									       *
!=*    You should have received a copy of the GNU General Public License       *
!=*    along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
!=* 									       *
!=*    For enquiries contact:						       *
!=*    									       *
!=*    Alan Roseman  							       *
!=*    Faculty of Life Sciences						       *
!=*    University of Manchester						       *
!=*    The Michael Smith Building					       *
!=*    Oxford Road							       *
!=*    Manchester. M13 9PT						       *
!=*    Email:Alan.Roseman@manchester.ac.uk    				       *
!=*									       *
!=*   									       *
!=******************************************************************************
!=*   									       *
!=*      Publication to cite: DOLGOBRODOV and ROSEMAN (2013)		       *
!=*									       *
!*******************************************************************************


include 'mrc_image.f90'

MODULE Weighted_vs
  use mrc_image
  implicit none

  INTERFACE dprod
     MODULE procedure dprodWv, dprodWm, dprodv, dprodm
  END INTERFACE
  INTERFACE mean
     MODULE procedure meanV, meanM
  end  INTERFACE
  INTERFACE norm
     MODULE procedure  normv, normm, normvc, normmc, normWv, normWm
  END INTERFACE
  interface normalize
     MODULE procedure  normalizeV, normalizeM, normalizeWV, normalizeWM
  end interface
  interface printM
     MODULE procedure  printrmr, printrmc, printmrfile
  end interface

  INTERFACE rotate_pad
     MODULE procedure rotate_pad, rotate_padt
  END INTERFACE

  CHARACTER(LEN=80)  :: imagefilename, templfilename, outfilename='junk.mrc', & 
       NEWimagefilename, maskfilename, Wfilename
  LOGICAL :: weighting
  real ( kind = 8 ), parameter :: pi=3.1415927535896
  character :: tab=achar(9)

CONTAINS

subroutine selectSize(M_in,N_in,M,N)
integer (kind = 8), intent(out) :: M,N
integer (kind = 8), intent(in) :: M_in,N_in
  select case(max(M_in,N_in))
  case (:128)
     M = 128; N = 128;
  case (129:512)
     M = 512; N = 512;
  case (513:1024)
     M = 1024; N = 1024;
  case (2049:)
     stop "Image too big (size > 2048), can't proceed!"
  CASE DEFAULT
     M = 2048; N = 2048
  end select
end subroutine  selectSize

subroutine rotate_padt(img_out, img_in, mask, angle)
  real (kind=8), intent(out) :: img_out(:,:)
  real (kind=8), target, intent(in) ::  img_in(:,:), mask(:,:), angle
  real (kind=8) :: si, co, ir, jr, t, u,cent
  INTEGER ( kind = 8 ) i,j,sz(2), szm(2), Mt,Nt, mm, mn

  sz = SHAPE(img_in); Mt=sz(1); Nt=sz(2);
  szm = shape(mask); mm = szm(1); mn = szm(2)
  si = sin(-angle); co = cos(angle)

  cent=int(mt/2.)+1. !amr, assumes square template

  img_out = 0.
  do i=1,Mt
     do j=1,Nt
!       ir = co*(i-mt/2.) + si*(j-mt/2.) + mt/2.
!        jr = -si*(i-mt/2.) + co*(j-mt/2.) + mt/2.

        ir = co*(i-cent) + si*(j-cent) + cent  !amr
        jr = -si*(i-cent) + co*(j-cent) + cent !amr
        if(ir+1<=mm .and. ir>=1 .and. jr+1<=mn .and. jr>=1) then
           if ( mask(ir,jr) > 0. ) then
              !  Do bilinear interpolation    
              t = ir - int(ir); u = jr - int(jr)
              img_out(i, j) = &
                   (1-t)*(1-u)*img_in(ir,jr) + t*(1-u)*img_in(int(ir)+1,jr) + &
                   t*u*img_in(int(ir)+1,int(jr)+1) + (1-t)*u*img_in(int(ir),int(jr)+1)
           end if
        end if

!        if ( mask(ir,jr) > 0 ) img_out(i, j) = img_in(ir, jr)
     end do
  end do
end subroutine rotate_padt

subroutine rotate_pad(img_out, img_in, mw, angle)
  real (kind=8), intent(out) :: img_out(:,:)
  real (kind=8), target, intent(in) ::  img_in(:,:), angle
  real (kind=8) :: si, co, ir, jr, t, u ,cent
  INTEGER ( kind = 8 ) i,j,m,n,mw, sz(2),ms,mb,ns,nb
  real (kind=8), pointer ::  tmp(:,:)

  sz = SHAPE(img_in); Ms=sz(1); Ns=sz(2); 
  if ((Ms.lt.mw).OR.(Ns.lt.mw))  stop "ERROR in the array sizes in >rotate_pad."

  sz = SHAPE(img_out); Mb=sz(1); Nb=sz(2); 
  si = sin(-angle); co = cos(angle)

	
!   print*,mb,nb,ms,ns,mw

!  if ((sqrt(2.)*mw > m) .OR. (sz(1) /= M)) then
!     stop "ERROR in the array sizes in 'rotate_pad"; 
!  end if

  tmp => img_in((Ms-mw)/2.+1:(Ms+mw)/2.,(Ns-mw)/2.+1:(Ns+mw)/2.)
 ! tmp => img_in((Ms)/2.+1:(Ms)/2.,(Ns)/2.+1:(Ns)/2.)
  
  cent=int(Mw/2.)+1. !amr, assumes square template  

!  img_out = 0.
  do i=1,mw ! Now rotate and pad the smaller object into the "M x N"
     do j=1,mw
!       ir = co*(i-mw/2.) + si*(j-mw/2.)+mw/2.
!       jr = -si*(i-mw/2.) + co*(j-mw/2.)+mw/2.
        ir = co*(i-cent) + si*(j-cent)+cent !amr
        jr = -si*(i-cent) + co*(j-cent)+cent !amr

        if(( ir < mw .and. ir >= 1) .and. (jr < mw .and. jr >= 1)) then
           !  Do bilinear interpolation
           t = ir - int(ir); u = jr - int(jr)
           img_out(i + (Mb-mw)/2. , j + (Nb-mw)/2. ) = &  
                (1-t)*(1-u)*tmp(int(ir),int(jr)) + t*(1-u)*tmp(int(ir)+1,int(jr)) + &
                t*u*tmp(int(ir)+1,int(jr)+1) + (1-t)*u*tmp(int(ir),int(jr)+1) 
        end if

     end do
  end do
end subroutine rotate_pad

subroutine imgcut(imgsml, imgbig)
  real (kind=8), intent(inout) :: imgsml(:,:)
  real (kind=8), intent(in) :: imgbig(:,:)
  integer i,j,ms,ns,mb,nb,sz(2)
  sz = SHAPE(imgsml); ms=sz(1); ns=sz(2)
  sz = SHAPE(imgbig); mb=sz(1); nb=sz(2)
    if (ms.gt.mb .OR. ns.gt.nb) stop "Size too big in imgcut"
  imgsml = 0.
  forall (i=1:ms, j=1:ns)  imgsml(i,j) = imgbig(i+(mb-ms)/2.,j+(nb-ns)/2.)
!amr
    if ((ms.eq.mb) .AND. (ns.eq.nb)) imgsml=imgbig

end subroutine imgcut

subroutine imgpad(imgsml, imgbig)
  real (kind=8), intent(in) :: imgsml(:,:)
  real (kind=8), intent(inout) :: imgbig(:,:)
  integer i,j,ms,ns,mb,nb,sz(2)
  sz = SHAPE(imgsml); ms=sz(1); ns=sz(2)
  sz = SHAPE(imgbig); mb=sz(1); nb=sz(2)
  if ((ms.gt.mb) .OR. (ns.gt.nb)) stop "Size too big in imgpad"

!  imgbig=sum(imgsml)/(ns*ms) !amr, pad with mean val
!amr
    if ((ms.eq.mb) .AND. (ns.eq.nb)) imgbig=imgsml
    if ((ms.eq.mb) .AND. (ns.eq.nb)) print*,"imgpad:sml=big"
!  imgbig = meanM(imgsml)
! if ((ms<mb).OR. ns<nb) forall (i=1:ms, j=1:ns) imgbig(i+(mb-ms)/2.,j+(nb-ns)/2.) = imgsml(i,j);
!amr

  if ((ms.lt.mb).AND. (ns.lt.nb)) forall (i=1:ms, j=1:ns) imgbig(i+(mb-ms)/2.,j+(nb-ns)/2.) = imgsml(i,j)
end subroutine imgpad

function inimg(filename)
  REAL  ( kind = 8 ), POINTER :: inimg(:,:)
  CHARACTER(LEN=80)  :: filename
  INTEGER j, MODE 

  call IMOPEN(1,filename,'old')    ! READ INITIAL IMAGE FROM THE STREAM 1
  CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN); ! print *," inimg: NX,NY = ", NX,NY

  allocate (inimg(1:NX,1:NY))

  do j=1,NY
     call IRDLIN(1,ALINE,*98);   inimg(1:NX,j)=ALINE(1:NX)
  enddo;   call IMCLOSE(1)
  return
98 print *,j,NX,NY; stop 'Error in image input'; 
end function inimg


function inimgST(filename)
  REAL  ( kind = 8 ), POINTER :: inimgST(:,:,:)
  CHARACTER(LEN=80), intent(in)  :: filename
  INTEGER :: i,j, MODE 

  call IMOPEN(1,filename,'old')    ! READ INITIAL IMAGE FROM THE STREAM 1
  CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN); ! print *," inimg: NX,NY = ", NX,NY

  allocate (inimgST(1:NX,1:NY,1:NZ))

  do i=1,NZ
     do j=1,NY
        call IRDLIN(1,ALINE,*98);   inimgST(1:NX,j,i)=ALINE(1:NX)
     enddo
  enddo;   call IMCLOSE(1)
  return
98 print *,j,i,NX,NY,NZ; stop 'Error in image input'; 
end function inimgST


subroutine outimg(img,outfilename)
  REAL  ( kind = 8 ) :: img(:,:)
  CHARACTER* (*)  outfilename
  INTEGER j, sz(2),l
  sz = SHAPE(img); NX = sz(1); NY = sz(2); MXYZ =  NXYZ
  DMIN=MINVAL(img); DMAX=MAXVAL(img);   DMEAN=sum(img)/(NX*NY)
  print *,"OUTPUT IMAGE: ",TRIM(outfilename),"  min/max,mean:  ",DMIN,DMAX,DMEAN,NX,NY

  call IMOPEN(2,outfilename,'UNKNOWN')
  call ITRHDR(2,1);
  CALL IALSIZ(2, [sz,1],  nxyzst)
  call IALMOD(2,2)
  call IWRHDR(2,TITLE,0,DMIN,DMAX,DMEAN)
  do j=1,NY
     ALINE(1:NX) = img(1:NX,j); call IWRLIN(2,ALINE)
  enddo;  call IMCLOSE(2)
end subroutine outimg


subroutine outimgST(img,outfilename)
  REAL  ( kind = 8 ) :: img(:,:,:)
  CHARACTER* (*)  outfilename
  INTEGER i,j, sz(3)
  sz = SHAPE(img); NX = sz(1); NY = sz(2); NZ = sz(3); MXYZ =  NXYZ
  DMIN=MINVAL(img); DMAX=MAXVAL(img);   DMEAN=sum(img)/(NX*NY*real(NZ))
  print *,"OUTPUT IMAGE: ",TRIM(outfilename),"  min/max,mean:  ",DMIN,DMAX,DMEAN,NX,NY,NZ
  
  call IMOPEN(2,trim(outfilename),'UNKNOWN')
  call ITRHDR(2,1);
  CALL IALSIZ(2, [sz,1],  nxyzst)
  call IALMOD(2,2)
  call IWRHDR(2,TITLE,0,DMIN,DMAX,DMEAN)
  do i=1,NZ
     do j=1,NY
        ALINE(1:NX) = img(1:NX,j,i); call IWRLIN(2,ALINE)
     enddo;  
  enddo
  call IMCLOSE(2)
end subroutine outimgST

function dprodv(x,y)
  real (kind=8), intent(in) :: x(:), y(:)
  real (kind=8) :: dprodv
  dprodv = dot_product(x,y)
end function dprodv

function dprodm(x,y)
  real (kind=8), intent(in) :: x(:,:), y(:,:)
  real (kind=8) :: dprodm
  dprodm = sum(x*y)
end function dprodm

  function dprodWv(x,y,W)
    real (kind=8), intent(in) :: x(:), y(:), W(:)
    real (kind=8) :: dprodWv
    dprodWv = dot_product(W*x,y)
  end function dprodWv

  function dprodWm(x,y,W)
    real (kind=8), intent(in) :: x(:,:), y(:,:), W(:,:)
    real (kind=8) :: dprodWm
    dprodWm = sum(W*x*y)
  end function dprodWm

  function normWv(a,W)
    real (kind=8), intent(in) :: a(:), W(:)
    real (kind=8) :: normWv
    normWv = sqrt(dprod(a,a,W))
  end function normWv

  function normWm(a,W)
    real (kind=8), intent(in) :: a(:,:), W(:,:)
    real (kind=8) :: normWm
    normWm = sqrt(dprod(a,a,W))
  end function normWm

  function normV(a)
    real (kind=8), intent(in) :: a(:)
    real (kind=8) :: normV
    normv = sqrt(sum(a*a))
  end function normV

  function normM(a)
    real (kind=8), intent(in) :: a(:,:)
    real (kind=8) :: normM
    normM = sqrt(sum(a*a))
  end function normM

  function normVc(a)
    complex (kind=8), intent(in) :: a(:)
    real (kind=8) :: normVc
    normvc = sqrt(sum(a*CONJG(a)))
  end function normVc

  function normMc(a)
    complex (kind=8), intent(in) :: a(:,:)
    real (kind=8) :: normMc
    normMc = sqrt(sum(a*CONJG(a)))
  end function normMc

  function meanV(z)
    real ( kind = 8 ), intent(in) :: z(:)
    real ( kind = 8 ) :: meanV
    meanV = sum(z)/size(z)
  end function meanV

  function meanM(z)
    real ( kind = 8 ), intent(in) :: z(:,:)
    real ( kind = 8 ) :: meanM
    meanM = sum(z)/size(z)
  end function meanM

  function rmax(x,y)
    real ( kind = 8 ), intent(in) ::  x,y
    real ( kind = 8 ) :: rmax
    if (x > y) then 
       rmax = x
    else
       rmax = y
    endif
  end function rmax

  subroutine normalizeWv(in,W)
    real ( kind = 8 ), intent(inout) :: in(:)
    real (kind=8), intent(in) ::  W(:)
    real  (kind=8) r
    in=in-mean(in)  ! substract mean
    r = norm(in,W)
    if (r>0) then  
       in=in/r  ! normalise 
    end if
  end subroutine normalizeWv

  subroutine normalizeWm(in,W)
    real ( kind = 8 ), intent(inout) :: in(:,:)
    real (kind=8), intent(in) :: W(:,:)
    real  (kind=8) r
    in=in-mean(in)  ! substract mean
    r = norm(in,W)
    if (r>0) then  
       in=in/r  ! normalise 
    end if
  end subroutine normalizeWm

  subroutine normalizeV(in)
    real ( kind = 8 ), intent(inout) :: in(:)
    real  (kind=8) r
    in=in-mean(in)  ! substract mean
    r = norm(in)
    if (r>0) then  
       in=in/r  ! normalise 
    end if
  end subroutine normalizeV

  subroutine normalizeM(in)
    real ( kind = 8 ), intent(inout) :: in(:,:)
    real  (kind=8) r
    in=in-mean(in)  ! substract mean
    r = norm(in)
    if (r>0) then  
       in=in/r  ! normalise 
    end if
  end subroutine normalizeM

  subroutine printrmr(MATr)
    implicit none
    real  ( kind = 8 ), intent(in), dimension ( :,: ) :: MATr; ! (M,N);
    integer i, j, sz(2)
    character*11 String;  

    sz = SHAPE(MATr)
    String='(    F6.2)';  write(String(2:4),'(I3)')sz(2)
    do i=1,sz(1)
       write(*,String),( MATr(i,j), j=1,sz(2) )
    enddo
    return
  end subroutine printrmr

  subroutine printmrfile(MATr,outfilename)
    implicit none
    real  ( kind = 8 ), intent(in), dimension ( :,: ) :: MATr; ! (M,N);
    CHARACTER(LEN=80), intent(in)  :: outfilename
    integer i, j, sz(2)
    character*11 String;  

    sz = SHAPE(MATr)
    String='(    F8.2)';  write(String(2:4),'(I3)')sz(2)
    open (11,file=TRIM(outfilename))
    do i=1,sz(1)
       write(11,String),( MATr(i,j), j=1,sz(2) )
    enddo
    close (11)
    return
  end subroutine printmrfile

  subroutine printrmc(MATc)
    implicit none
    complex  ( kind = 8 ), intent(in), dimension ( :,: ) :: MATc
    integer i, j, sz(2)
    character*11 String;  

    sz = SHAPE(MATc)
    String='(    F6.1)';  write(String(2:4),'(I3)')sz(2)
    do i=1,sz(1)/2+1
       write(*,String),( MATc(i,j), j=1,sz(2) )
    enddo
    return
  end subroutine printrmc


  subroutine smoothM(IMG, s)
! Smoothing matrix with linear interpolation over s X s window
    real ( kind = 8 ), intent(inout), target, dimension ( :,: ) :: IMG
    real ( kind = 8 ), allocatable, dimension ( :,: ) :: tmp
    integer s, m, n, sz(2), i, j 

    sz = shape(IMG); m = sz(1); n = sz(2)
    allocate (tmp(1:m,1:n));    tmp = 0

    do i=1,m
       do j=1,n
          tmp(i,j) = meanM(IMG(max(1,i-s):min(m,i+s),max(1,j-s):min(n,j+s)))
       enddo
    enddo

    IMG = tmp
    deallocate (tmp)
  end subroutine smoothM

  function varmap(S, R)   ! Variance map of the S within window R
    real ( kind = 8 ), intent(in), target ::  S(:,:)
    REAL  ( kind = 8 ), POINTER :: varmap(:,:), mask (:,:)
    INTEGER i, j, z(2), M, N
    REAL  ( kind = 8 ) :: mean, R

    z = shape(S); M = z(1); N = z(2)
    allocate (varmap(1:M,1:N))
    do i=1,M
       do j=1,N
          mask => S(aint(max(i-R,1.)):aint(min(i+R,real(M))),  &
               aint(max(j-R,1.)):aint(min(j+R,real(N))))
          mean = meanM(mask)
          varmap(i,j) = sum((mask-mean)**2)/size(mask)
       enddo
    enddo
  end function varmap

  function varianceM(S)
    real ( kind = 8 ) :: varianceM, mean
    real ( kind = 8 ), intent(in) :: S(:,:)

    mean = meanM(S)
    varianceM = sum((S-mean)**2)/size(S)
  end function varianceM


  subroutine CM(img_in,templ_in,binmask_in,W_in,Om_out,angle_step, max_angle,eps,iThrdsin,vmap)
    include 'fftw3.f'

    integer ( kind = 8 ), intent(in), optional ::  iThrdsin
    real  ( kind = 8 ), dimension (:,:), intent(inout)  :: img_in
    real  ( kind = 8 ), dimension (:,:), intent(in)  :: templ_in, binmask_in, W_in
    real  ( kind = 8 ), dimension (:,:), intent(out)  :: Om_out ,vmap
    real  ( kind = 8 ), intent(in)  ::  eps, angle_step, max_angle
    integer ( kind = 8 ) ::  M=2048, N=2048, MN,Mt,Nt, forward, backward, i,j, z(2),iThrds
    real  ( kind = 8 ), target, allocatable, dimension ( :,: ) :: img, img2ftprint,&
         imgftprint, mask, rtmp, cw, cwc, templ, Om, imgStdDev, Templ0, Wmask, Wmask0, W,WmaskSQ,MaskRT,templ1
!   real  ( kind = 8 ), target, allocatable, dimension ( :,: ) :: vmap

    complex  ( kind = 8 ), allocatable, dimension ( :,: ) :: imgF, templF, prdktF, img2F,&
         Wmaskf ,WmaskF2,maskF !(M/2+1,N)
    real  ( kind = 8 ) ::  masksize, angle, oldmasksize,avg

    if(present(iThrdsin))then
       iThrds=iThrdsin
    else
       iThrds = 1
    endif

    z = SHAPE(img_in)
    print*,"Img_in:",Z(1),z(2)
    call selectSize(z(1),z(2),M,N)
    MN = M*N
    z = SHAPE(templ_in);
    Mt = z(1);   Nt = Z(2)
    print*,"Templ_in:",mt,nt

    z = SHAPE(binmask_in)
    print*,"Binmask_in:",Z(1),z(2)

    allocate ( img(1:M,1:N), imgF(1:(M/2+1),1:N), img2ftprint(1:M,1:N), imgftprint(1:M,1:N) )
    allocate ( templF(1:(M/2+1),1:N), rtmp(1:M,1:N), templ(1:M,1:N), Wmask(1:M,1:N), &
         Wmask0(1:M,1:N) , WmaskSQ(1:M,1:N),maskRT(1:M,1:N))
    allocate ( mask(1:M,1:N), img2F(1:(M/2+1),1:N), WmaskF(1:(M/2+1),1:N), &
         prdktF(1:(M/2+1),1:N) , WmaskF2(1:(M/2+1),1:N),maskF(1:(M/2+1),1:N)) 
    allocate ( cw(1:M,1:N), cwc(1:M,1:N), Om(1:M,1:N), imgStdDev(1:M,1:N), &
         Templ0(1:M,1:N), W(1:Mt,1:Nt) ,Templ1(1:M,1:N))

    img=mean(img_in)
    call imgpad(img_in,img)
    img=img+minval(img)
!    img=img/varianceM(img)


    mask = 0.;  !  MxN
    call imgpad(binmask_in, mask)

!amr, binarise mask
    where (mask.ge.0.5) 
		mask=1.
	elsewhere 
		mask = 0.	
    end where

    call imgcut(W,W_in);

    if (((maxval(W) - minval(W)) .lt. 0.0001)) then
      W = 1.
      print*,"Warning, weighting mask trivial."
    else
       W = W-minval(W) 
       W = (eps*((W/maxval(W)))-1.)+1.
    end if

!amr 
    if (eps.le.0.0) W=1.
    Wmask0=0.

    call imgpad(W,Wmask0)




!   masksize = SUM(binmask_in); 

    masksize = SUM(mask)
    print*,"Masksize=",masksize
    oldmasksize=masksize


    call dfftw_init_threads(i)
    if (i == 0)  stop "CM:  Error in multi-threads"
    call dfftw_plan_with_nthreads(iThrds);  !  with 'iThreads' threads
    call dfftw_plan_dft_r2c_2d(forward,M,N,img,imgF,FFTW_ESTIMATE)  ! Initiate the FFT plans
    call dfftw_plan_dft_c2r_2d(backward,M,N,imgF,rtmp,FFTW_BACKWARD,FFTW_ESTIMATE)

    call dfftw_execute_dft_r2c(forward, img, imgF)       !  Fourier of image
    call dfftw_execute_dft_r2c(forward, img*img, img2F)  !  Fourier of image^2



    Wmask0 = mask*Wmask0
   

    call imgpad(templ_in,templ)


!    templ = Wmask0*templ
     templ = mask*templ

    Templ0=0.
    avg=SUM(templ)/masksize
    where ( mask > 0 )  Templ0 = templ - avg;! mean masked points=0

!    where ( mask > 0 )  Templ0 = templ - SUM(templ)/masksize;! mean masked points=0
    ! make var=1
    Templ0=templ0/(sum(Templ*Templ)/masksize)
    

    cwc = -1.3                     ! "Correlation map with the template"
    Om = 0.
    do angle = 0.00,max_angle,angle_step
       Wmask=0.
       call rotate_pad(Wmask, Wmask0, Mt, pi/180.*angle)
!amr, binarise mask --only good if W=1 allover
!      where (Wmask.ge.0.5) 
!		Wmask=1.
!	elsewhere 
!		Wmask = 0.	
!
!     end where

!       print*,"sum=",sum(mask)
       maskRT=0.
       call rotate_pad(maskRT, mask, Mt, pi/180.*angle)
       where (maskRT.ge.0.5) 
		maskRT=1.
	elsewhere 
		maskRT = 0.	
       end where

      masksize = SUM(maskRT)

      Wmask = Wmask*maskRT
      WmaskSQ=Wmask*Wmask

!    if (oldmasksize.ne.masksize)  print*,angle,"  rtMasksize=",masksize
     print*,angle,"  rtMasksize=",masksize
       templ=0.
       call rotate_pad(templ, Templ0, Mt, pi/180.*angle); 

       templ=templ*Wmask
!renorm rotated template
      avg=SUM(templ)/masksize
      where ( maskRT > 0 )  Templ = templ - avg;! mean masked points=0
    ! make var=1
     Templ=templ/(sum(Templ*Templ)/masksize)
   


       templ1=templ
       templ=templ*Wmask

! amr REV conj needed for dot products
       call dfftw_execute_dft_r2c(forward, maskRT, maskF)   ! Fourier image of the weighted mask
       call dfftw_execute_dft_r2c(forward, Wmask, WmaskF)   ! Fourier image of the weighted mask
       call dfftw_execute_dft_r2c(forward, WmaskSQ, WmaskF2)   ! Fourier image of the weighted mask
       call dfftw_execute_dft_c2r(backward, (imgF*CONJG(WmaskF)), imgftprint)  ! mask footprint on image
       imgftprint = imgftprint/MN; 
       call dfftw_execute_dft_c2r(backward, img2F*CONJG(WmaskF2), img2ftprint)  ! mask footprint on image^2
       img2ftprint = img2ftprint/MN

       imgStdDev = sqrt((img2ftprint-(imgftprint*imgftprint)/masksize)/masksize)  ! Scale factor
     
       call dfftw_execute_dft_r2c(forward, templ, templF)    ! Fourier image of the template

       prdktF = (imgF)*CONJG(templF)   ! Fourier image of the scalar product
       call dfftw_execute_dft_c2r(backward, prdktF, rtmp)
       
       where (imgStdDev.gt.0.000001)
			rtmp = rtmp/MN/imgStdDev/sqrt(masksize)/normM(templ1) ! restore  scaling
       elsewhere
			rtmp= -1.2
       end where

       cw(1:M/2,1:N/2) = rtmp(M/2+1:M,N/2+1:N)      !  Convert image frame of origin to the centre
       cw(M/2+1:M,N/2+1:N) = rtmp(1:M/2,1:N/2)
       cw(1:M/2,N/2+1:N) = rtmp(M/2+1:M,1:N/2)
       cw(M/2+1:M,1:N/2) = rtmp(1:M/2,N/2+1:N)

       forall ( i=1:M, j=1:N, cw(i,j) .ge. cwc(i,j) )
          Om(i,j) = angle
          cwc(i,j) = cw(i,j)
       end forall
    end do  ! rotation cycle

    call imgcut(img_in,cwc);   
    call imgcut(Om_out,Om)
    call dfftw_destroy_plan(backward)
    call dfftw_destroy_plan(forward)
    call dfftw_cleanup_threads()

    !    deallocate ( templF, rtmp, templ, Wmask, Wmask0, img, imgF, img2ftprint, &
    !         imgftprint, mask, img2F, WmaskF, prdktF, cw, cwc, Om, imgStdDev, &
    !         Templ0, W)
    vmap=imgStdDev
  end subroutine CM

END MODULE  Weighted_vs
