#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import sys
import os
import subprocess
import platform
import argparse
import json
import re
import ccpem_core.gui_ext

class CCPEMVersion(object):
    def __init__(self):
        # Import version tag
        self.build_version = False
        try:
            from ccpem_core import version
            self.build_version = True
        except ImportError:
            pass
        if self.build_version:
            self.version = version.version
            self.git_revision = version.git_revision
            self.build_time = version.build_time
        else:
            self.version = 'dev'
            self.git_revision = get_git_revision()
            self.build_time = 'N/A'
        self.name = self.version + '_' + self.git_revision

class DictToClass(object):
    def __init__(self, dct):
        self.__dict__.update(dct)

def get_home_directory():
    '''
    Return home directory dependent on platform
    '''
    user_platform = platform.system()
    path = os.getcwd()
    # Linux
    if user_platform == 'Linux':
        path = os.getenv('XDG_DATA_HOME',
                         os.path.expanduser('~'))
    # Mac
    elif user_platform == 'Darwin':
        path = os.path.expanduser('~')

    # Windows
    # XXX needs testing
#     else:
#         try:
#             from win32com.shell import shell, shellcon
#             path = shell.SHGetFolderPath(0, shellcon.CSIDL_DESKTOP, None, 0)
#             if path is None:
#                 path = shell.SHGetFolderPath(0, shellcon.CSIDL_LOCAL_APPDATA, None, 0)
#             path = os.path.normpath(_get_win_folder(const))
#         except:
#             ImportError
    return path

def check_directory_and_make(directory,
                             verbose=False,
                             auto_suffix=False,
                             make=True):
    '''
    Makes directory if not already present. Auto suffix appends "_n" suffix.
    '''
    make_dir = False
    # Check directory already present or present and empty
    if not os.path.exists(directory):
        make_dir = True
    elif os.listdir(directory) == []:
        make_dir = True
    #
    if make_dir:
        if make:
            try:
                os.makedirs(directory)
                if verbose:
                    print '  Create directory : ', directory
                return directory
            except OSError:
                return None
        else:
            return directory
    elif auto_suffix:
        suffixSearch = re.compile(r'(_\d+)$').search(directory)
        if suffixSearch is None:
            new_directory = directory + '_1'
        else:
            suffix = suffixSearch.group(1)
            i = int(suffix.replace('_', '')) + 1
            new_directory = directory.replace(suffix, '_'+str(i))
        new_directory = check_directory_and_make(directory=new_directory,
                                                 verbose=verbose,
                                                 auto_suffix=auto_suffix,
                                                 make=make)
        return new_directory
    else:
        return None


def check_file_exists(filename):
    '''
    Returns bool value for presence of specified file
    '''
    return os.path.isfile(filename)


def save_json_object(object, filename):
    '''
    Convenience function to save json objects.
    '''
    json_file = open(filename, 'w')
    json.dump(object, json_file)


def tail(filename, maxlen=100):
    from collections import deque
    tail_str = ''
    with open(filename) as openfile:
        for line in deque(openfile, maxlen=maxlen):
            tail_str += line
    return tail_str


def print_warning(message):
    '''
    Generic ccpem warning message.
    '''
    print '\nCCP-EM Warning:'
    print '  {0}'.format(message)


def print_error(message):
    '''
    Generic ccpem error message.
    '''
    print '\nCCP-EM Error:'
    print '  {0}'.format(message)


def print_header(message=None, return_str=False, total_len=80):
    mess_len = len(message)
    fill_len = total_len - mess_len + 1
    fill_rl = fill_len // 2
    fill_l = fill_rl
    fill_r = fill_rl
    if (fill_rl * 2 != fill_len):
        fill_r +=1
    #
    header = '\n' + '_' * total_len + '\n'
    if len(header) > 80:
        header += '\n' + '_' * (fill_l-1) + ' ' + message + ' ' + '_' * (fill_r-2) + '\n'
    else:
        header += '\n' + '_' * (fill_l-1) + ' ' + message + ' ' + '_' * (fill_r-1) + '\n'
    #
    if return_str:
        return header
    else:
        print header


def print_sub_header(message=None, return_str=False, total_len=80):
    mess_len = len(message)
    fill_len = total_len - mess_len +1 
    sub_header = '\n' + message + ' ' + '_' * (fill_len-2) + '\n'
    if return_str:
        return sub_header
    else:
        print sub_header


def print_sub_sub_header(message=None, return_str=False, total_len=80):
    message = '__ ' + message
    mess_len = len(message)
    fill_len = total_len - mess_len + 1
    sub_header = '\n' + message + ' ' + '_' * (fill_len-2) + '\n'
    if return_str:
        return sub_header
    else:
        print sub_header


def print_footer():
    print '_' * 80 + '\n'


def return_footer():
    return '_' * 80 + '\n'


def print_pass_regression_test_footer(message=None, total_len=80):
    mess_len = len(message)
    fill_len = total_len - mess_len
    sub_sub_header = '\n' + message + ' ' + '_' * (fill_len-2) + '\n'
    print sub_sub_header
    print '_' * total_len


def get_image_list_from_directory(image_file_directory=None):
    print_sub_header(message='Get images from directory')
    images = []
    print "  From directory :\n", image_file_directory
    extension = [".png", ".jpg", ".tif", ".bmp"]
    try:
        for filename in sorted(os.listdir(image_file_directory)):
            for ext in extension:
                if filename.lower().endswith(ext):
                    print "    Get image : ", filename
                    images.append(image_file_directory + filename)
    except OSError:
        msg = "\n\nError. Directory not found : \n" + image_file_directory
        raise Exception(msg)
    print "\n  Total number of images : ", len(images)
    return images


def convert_unicode_to_string(input):
    if isinstance(input, dict):
        return {
            convert_unicode_to_string(key):
                convert_unicode_to_string(
                    value) for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [convert_unicode_to_string(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input



def get_git_revision():
    path = os.path.dirname(__file__)
    try:
        revision = subprocess.check_output(
            "git describe --always HEAD",
            shell=True,
            cwd=path).strip()
        if revision == '':
            revision = None
    except subprocess.CalledProcessError as e:
        print e
    return revision


def get_path_abs(path):
    '''
    Assert path exists and return absolute path.
    '''
    path = os.path.abspath(path)
    assert os.path.exists(path)
    return path


def main():
    '''
    For testing
    '''
    version = CCPEMVersion()
    print version.version
    print version.build_time
    print version.name

if __name__ == '__main__':
    main()
