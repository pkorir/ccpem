#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
from ccpem_core.mmdb_coord_tools.coord_tools import manager
from ccpem_core.mmdb_coord_tools.coord_tools import mmdb_copy_and_paste
from ccpem_core.mmdb_coord_tools.coord_tools import mmdb_selection_type
from ccpem_core.mmdb_coord_tools.coord_tools import mmdb_selection_key


class Test(unittest.TestCase):
    '''
    Unit test for mmdb_coord_tools.
    '''
    def setUp(self):
        self.test_data = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'test_data')
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)

    def test_delete_residues(self):
        model = manager(self.test_data + '/1DOS.pdb')
        sel = model.make_selection(res_min=10, res_max=50)
        model.delete_selected(sel)
        model.write_pdb_file(self.test_output + '/1DOS_del_res.pdb')

    def test_delete_chain(self):
        model = manager(self.test_data + '/1DOS.pdb')
        sel = model.make_selection(sel_type=mmdb_selection_type().chain(),
                                   chain='B')
        model.delete_selected(sel)
        model.write_pdb_file(self.test_output + '/1DOS_del_chain_b.pdb')

    def test_delete_solvent(self):
        model = manager(self.test_data + '/1DOS.pdb')
        model.delete_solvent()
        model.write_pdb_file(self.test_output + '/1DOS_del_sol.pdb')

    def test_delete_alt_locs(self):
        model = manager(self.test_data + '/3K0N.pdb')
        model.delete_alternative_locations()
        model.write_pdb_file(self.test_output + '/3K0M_no_alt.pdb')

    def test_generate_symmetry_mates(self):
        model = manager(self.test_data + '/1DOS.pdb')
        model.generate_symmetry_mates()
        model.write_pdb_file(self.test_output + '/1DOS_sym_mates.pdb')

    def test_delete_nmr_models(self):
        model = manager(self.test_data + '/2MOG_nmr.pdb')
        sel = model.make_empty_selection()
        for n_model in range(10,20):
            print n_model
            sel = model.make_selection(sel_num=sel,
                                       sel_type=mmdb_selection_type().model(),
                                       model=n_model,
                                       sel_key=mmdb_selection_key().or_key()
                                       )
        model.delete_selected(sel)
        model.write_pdb_file(self.test_output + '/2MOG_nmr_del_models.pdb')

    def test_delete_high_bfactor_atoms(self):
        model = manager(self.test_data + '/1DOS.pdb')
        model.delete_high_bfactor_atoms(bmax=100)
        model.write_pdb_file(self.test_output + '/1DOS_del_atom_bmax.pdb')

    def test_delete_high_bfactor_residues(self):
        model = manager(self.test_data + '/1DOS.pdb')
        model.delete_high_bfactor_residues(bmax=100)
        model.write_pdb_file(self.test_output + '/1DOS_del_res_bmax.pdb')

    def test_copy_paste(self):
        model_copy = manager(self.test_data + '/1DOS.pdb')
        model_copy.mol.PrintEntryId()
        copy_sel = model_copy.make_selection(
            sel_type=mmdb_selection_type().atom(),
            chain='A',
            res_min=10,
            res_max=15)
        model_paste = manager()
        model_paste.mol.PrintEntryId()
        #
        mmdb_copy_and_paste(copy_sel, model_copy.mol, model_paste.mol)
        model_paste.write_pdb_file(self.test_output + '/1DOS_copy_pasted.pdb')

    def test_rename_chain(self):
        model = manager(self.test_data + '/1DOS.pdb')
        model.rename_chain('A', 'Y')
        model.write_pdb_file(self.test_output + '/1DOS_rename_chn.pdb')

    def test_apply_rot_trans(self):
        model = manager(self.test_data + '/1DOS.pdb')
        translatation_matrix = (1, 1, 1)
        rotation_matrix = ((1, 0, 0),
                           (0, 1, 0),
                           (0, 0, 1))
        model.apply_rot_trans(rot_mat=rotation_matrix,
                              trans_mat=translatation_matrix)
        model.write_pdb_file(self.test_output + '/1DOS_rt.pdb')

    def test_get_and_set_atom_coords(self):
        model = manager(self.test_data + '/1DOS.pdb')
        sel = model.make_selection(sel_type=mmdb_selection_type().atom(),
                                   chain='A',
                                   res_min=1,
                                   res_max=5)
        # N.B. must convert to list to reassign values
        coords = list(model.get_atom_coords(sel_num=sel))
        # Set coords
        for n, xyz in enumerate(coords):
            coords[n] = tuple(2 * n for n in xyz)
        print coords[-1]
        model.set_atom_coords(coords, sel)
        # Check coords
        new_coords = list(model.get_atom_coords(sel_num=sel))
        assert coords[-1] == new_coords[-1]
        model.write_pdb_file(self.test_output + '/1DOS_ed_xyz.pdb')

if __name__ == '__main__':
    unittest.main()
