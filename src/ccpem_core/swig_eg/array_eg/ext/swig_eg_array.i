/* File : swig_eg_array.i */
%module swig_eg_array

%{
  /* the resulting C file should be built as a python extension */
  #define SWIG_FILE_WITH_INIT
  /*  Includes the header in the wrapper code */
  #include "swig_eg_array.h"
  extern float * ReturnArrayFloat3();
  extern int * ReturnArrayInt3();
%}

// Numpy support
/*  include the numpy typemaps */
%include "numpy.i"
/*  need this for correct module initialization */
%init %{
  import_array();
%}
// Inplace arrays
%apply (double* INPLACE_ARRAY1, int DIM1) {(double* array_dbl, int n)}
%apply (int DIM1, double* INPLACE_ARRAY1) {(int len1, double* vec1), (int len2, double* vec2)}
// For ArrayClassConstructor
%apply (double* INPLACE_ARRAY1, int DIM1) {(double* array1_dbl, int array1_len)}
%apply (double* INPLACE_ARRAY1, int DIM1) {(double* array2_dbl, int array2_len)}
// End for ArrayClassConstructor

%rename (ModArrays) py_ModArrays;
%inline %{
void py_ModArrays(int len1, double* vec1, int len2, double* vec2) {
    ModArrays(len1, len2, vec1, vec2);
}
%}
// End numpy support

// c_array support
%include "carrays.i"
%array_functions(double, array_double);
%array_class(int, IntArrayClass);
%array_class(double, DoubleArrayClass);
// End c_array support

// Std::vector support
%include "std_vector.i"

namespace std {
    //
    %template(IntVector) vector<int>;
    //
    %template(DoubleVector) vector<double>;
    %template(DoubleDoubleVector) std::vector<std::vector<double> >;
    %template(DoubleDoubleDoubleVector) std::vector<std::vector<std::vector<double> > >;
    //
    %template(FloatVector) vector<float>;
    %template(FloatFloatVector) std::vector<std::vector<float> >;
    %template(FloatFloatFloatVector) std::vector<std::vector<std::vector<float> > >;
}
// End std::vector support

// Typemap for c array return
%typemap(out) float* ReturnArrayFloat3() {
  int size = 3;
  $result = PyList_New(size);
  for (int i = 0; i < size; i++) {
    PyObject *o = PyFloat_FromDouble((double) $1[i]);
    PyList_SetItem($result,i,o);
  }
}

%typemap(out) int* ReturnArrayInt3() {
  int size = 3;
  $result = PyList_New(size);
  for (int i = 0; i < size; i++) {
    PyObject *o = PyInt_FromLong((int) $1[i]);
    PyList_SetItem($result,i,o);
  }
}

%typemap(out) int* ReturnArrayInt3Tuple() {
  int size = 3;
  $result = PyTuple_New(size);
  for (int i = 0; i < size; i++) {
    PyObject *o = PyInt_FromLong((int) $1[i]);
    PyTuple_SetItem($result,i,o);
  }
}
// End typemap for c array return

// N.B. should be last (after vector support)
%include swig_eg_array.h
