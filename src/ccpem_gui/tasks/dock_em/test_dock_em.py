#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import sys
import shutil
import time
import tempfile
from PyQt4 import QtGui, QtCore
from PyQt4.QtTest import QTest
from ccpem_core.tasks.dock_em import dock_em_task
from ccpem_gui.tasks.dock_em import dock_em_window
from ccpem_core.test_data.tasks import dock_em as dock_em_test
from ccpem_core import ccpem_utils
from ccpem_core import process_manager

app = QtGui.QApplication(sys.argv)

class Test(unittest.TestCase):
    '''
    Unit test for DockEM (invokes GUI).
    '''
    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_data = os.path.dirname(dock_em_test.__file__)
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        '''
        Remove temporary data.
        '''
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)

    def test_dock_em_window_integration(self):
        '''
        Test DockEM via GUI.
        '''
        ccpem_utils.print_header(message='Unit test - DockEM')
        # Load args and set task
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        args_path = os.path.join(self.test_data, 'unittest_args.json')
        run_task = dock_em_task.DockEM(args_json=args_path,
                                       job_location=self.test_output)

        # Setup GUI
        self.window = dock_em_window.DockEMMainWindow(task=run_task)
        # Mouse click run
        QTest.mouseClick(
            self.window.tool_bar.widgetForAction(self.window.tb_run_button),
            QtCore.Qt.LeftButton)

        # Wait for run to complete
        self.job_completed = False
        run_time = 0
        timeout = 500

        while not self.job_completed and run_time < timeout:
            print 'DockEM running for {0} secs (timeout = {1})'.format(
                run_time,
                timeout)
            time.sleep(5.0)
            run_time += 5
            status =\
                process_manager.get_process_status(run_task.pipeline.json)
            if status == 'finished':
                soln = '10'
                pdb_sol = ('tophit' + run_task.args.job_prefix.value +
                           '.0' + soln + '.pdb')
                assert pdb_sol in os.listdir(self.test_output)
                self.job_completed = True
        assert status == 'finished'
        # Check timeout
        assert run_time < timeout
        # Check job completed
        assert self.job_completed

if __name__ == '__main__':
    unittest.main()
