<?xml version="1.0" encoding="UTF-8"?>
<emd emdb_id="EMD-5796" version="2.0">
    <admin>
        <current_status>
            <code>REL</code>
            <processing_site>RCSB</processing_site>
        </current_status>
        <sites>
            <deposition>RCSB</deposition>
            <last_processing>RCSB</last_processing>
        </sites>
        <key_dates>
            <deposition>2013-11-19</deposition>
            <header_release>2013-12-18</header_release>
            <map_release>2013-12-18</map_release>
            <update>2014-01-15</update>
        </key_dates>
        <title>Structure of the Ribosome with Elongation Factor G Trapped in the Pre-Translocation State</title>
        <authors_list>
            <author>Brilot AF</author>
            <author>Korostelev AA</author>
            <author>Ermolenko DN</author>
            <author>Grigorieff N</author>
        </authors_list>
        <keywords>protein structure, translation, EF-G, electron cryo-microscopy, single particle analysis</keywords>
    </admin>
    <crossreferences>
        <citation_list>
            <primary_citation>
                <journal_citation published="true">
                    <author order="1">Brilot AF</author>
                    <author order="2">Korostelev AA</author>
                    <author order="3">Ermolenko DN</author>
                    <author order="4">Grigorieff N</author>
                    <title>Structure of the Ribosome with Elongation Factor G Trapped in the Pre-Translocation State</title>
                    <journal>PROC.NAT.ACAD.SCI.USA</journal>
                    <volume>110</volume>
                    <first_page>20994</first_page>
                    <last_page>20999</last_page>
                    <year>2013</year>
                    <external_references type="pubmed">24324137</external_references>
                    <external_references type="doi">doi:10.1073/pnas.1311423110</external_references>
                </journal_citation>
            </primary_citation>
        </citation_list>
        <emdb_list>
            <emdb_reference id="1">
                <emdb_id>EMD-5797</emdb_id>
                <relationship>
                    <in_frame>FULLOVERLAP</in_frame>
                </relationship>
            </emdb_reference>
            <emdb_reference id="2">
                <emdb_id> EMD-5798</emdb_id>
                <relationship>
                    <in_frame>FULLOVERLAP</in_frame>
                </relationship>
            </emdb_reference>
            <emdb_reference id="3">
                <emdb_id> EMD-5799</emdb_id>
                <relationship>
                    <in_frame>FULLOVERLAP</in_frame>
                </relationship>
            </emdb_reference>
            <emdb_reference id="4">
                <emdb_id> EMD-5800</emdb_id>
                <relationship>
                    <in_frame>FULLOVERLAP</in_frame>
                </relationship>
            </emdb_reference>
        </emdb_list>
    </crossreferences>
    <sample>
        <name>Ribosome with A, P, E sites occupied by tRNA</name>
        <supramolecule_list>
            <supramolecule id="1000" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="sample">
                <details>Sample was monodisperse</details>
                <number_unique_components>4</number_unique_components>
                <molecular_weight>
                    <theoretical units="MDa">3</theoretical>
                </molecular_weight>
            </supramolecule>
            <supramolecule id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="complex">
                <name>70S ribosome</name>
                <category>ribosome-prokaryote</category>
                <recombinant_exp_flag>false</recombinant_exp_flag>
                <natural_source>
                    <organism ncbi="562">Escherichia coli</organism>
                    <strain>MRE600</strain>
                </natural_source>
                <recombinant_expression/>
                <molecular_weight>
                    <theoretical units="MDa">3</theoretical>
                </molecular_weight>
                <ribosome-details>ALL</ribosome-details>
            </supramolecule>
        </supramolecule_list>
        <macromolecule_list>
            <macromolecule id="2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rna">
                <name synonym="tRNA">Transfer RNA</name>
                <natural_source>
                    <organism ncbi="562">Escherichia coli</organism>
                    <strain>K-12</strain>
                </natural_source>
                <molecular_weight>
                    <experimental units="MDa">0.025</experimental>
                </molecular_weight>
                <details>Three tRNA present (A, P and E sites).</details>
                <classification>TRANSFER</classification>
                <structure>DOUBLE HELIX</structure>
                <synthetic_flag>false</synthetic_flag>
            </macromolecule>
            <macromolecule id="3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rna">
                <name synonym="mRNA">Messenger RNA</name>
                <natural_source>
                    <organism ncbi="32630">synthetic construct</organism>
                </natural_source>
                <molecular_weight>
                    <theoretical units="MDa">0.012</theoretical>
                </molecular_weight>
                <sequence>
                    <string>GGCAAGGAGGUAAAAAUGUUUAAACGUAAAUCUACU</string>
                </sequence>
                <classification>OTHER</classification>
                <structure>SINGLE STRANDED</structure>
                <synthetic_flag>true</synthetic_flag>
            </macromolecule>
            <macromolecule id="4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ligand">
                <name synonym="Vio">Viomycin</name>
                <natural_source>
                    <organism ncbi="32644">unidentified</organism>
                </natural_source>
                <molecular_weight>
                    <theoretical units="MDa">0.001</theoretical>
                </molecular_weight>
                <number_of_copies>1</number_of_copies>
                <recombinant_exp_flag>false</recombinant_exp_flag>
                <natural_source>
                    <organism ncbi="32644">unidentified</organism>
                </natural_source>
                <recombinant_expression/>
            </macromolecule>
        </macromolecule_list>
    </sample>
    <structure_determination_list>
        <structure_determination>
            <method>singleParticle</method>
            <aggregation_state>particle</aggregation_state>
            <specimen_preparation_list>
                <specimen_preparation id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="base_preparation_type">
                    <concentration units="mg/mL">0.4</concentration>
                    <buffer>
                        <ph>7.6</ph>
                        <details>10 mM HEPES-KOH, 5 mM MgCl2, 90 mM NH4Cl, 2 mM spermidine, 0.1 mM spermine, 6 mM BME, 0.5 mM viomycin, 0.5 mM GTP, 0.5 mM fusidic acid</details>
                    </buffer>
                    <grid>
                        <details>C-flat 1.2/1.3 holey carbon 400 mesh copper grid, glow discharged with a current of -20 mA for 45 seconds in an EMITECH K100X glow discharge unit</details>
                    </grid>
                    <vitrification>
                        <cryogen_name>ETHANE</cryogen_name>
                        <chamber_humidity>95</chamber_humidity>
                        <instrument>FEI VITROBOT MARK II</instrument>
                        <method>Freshly glow-disharged grids were loaded into an FEI Mark II Vitrobot and equilibrated to 95% relative humidity at 22 degrees Celsius. 2 microliters of sample was applied through the side port, blotted for 7 seconds with a positional offset of 2, and plunged into liquid ethane.</method>
                    </vitrification>
                </specimen_preparation>
            </specimen_preparation_list>
            <microscopy_list>
                <microscopy id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="base_microscopy_type">
                    <microscope>FEI TITAN KRIOS</microscope>
                    <illumination_mode>FLOOD BEAM</illumination_mode>
                    <imaging_mode>BRIGHT FIELD</imaging_mode>
                    <electron_source>FIELD EMISSION GUN</electron_source>
                    <acceleration_voltage>300</acceleration_voltage>
                    <nominal_cs>0.01</nominal_cs>
                    <nominal_defocus_min>1150</nominal_defocus_min>
                    <nominal_defocus_max>6950</nominal_defocus_max>
                    <nominal_magnification>133333.</nominal_magnification>
                    <calibrated_magnification>134615.</calibrated_magnification>
                    <specimen_holder_model>FEI TITAN KRIOS AUTOGRID HOLDER</specimen_holder_model>
                    <alignment_procedure>
                        <legacy>
                            <astigmatism>Automatically corrected using FEI software</astigmatism>
                        </legacy>
                    </alignment_procedure>
                    <date>2012-11-02</date>
                    <image_recording_list>
                        <image_recording>
                            <film_or_detector_model category="CCD">FEI FALCON I (4k x 4k)</film_or_detector_model>
                            <digitization_details>
                                <sampling_interval units="um">14.0</sampling_interval>
                            </digitization_details>
                            <number_real_images>13341</number_real_images>
                            <average_electron_dose_per_image units="e/A**2">30</average_electron_dose_per_image>
                            <bits_per_pixel>16.</bits_per_pixel>
                        </image_recording>
                    </image_recording_list>
                </microscopy>
            </microscopy_list>
            <image_processing id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="singleparticle_processing_type">
                <details>Refinement and 3D classification performed by Frealign. See primary citation Supplementary Information for details.</details>
                <ctf_correction>
                    <details>CTFFIND3, FREALIGN per micrograph</details>
                </ctf_correction>
                <final_reconstruction>
                    <number_images_used>356854</number_images_used>
                    <applied_symmetry>
                        <point_group>C1</point_group>
                    </applied_symmetry>
                    <algorithm>Projection Matching</algorithm>
                    <resolution>5.9</resolution>
                    <resolution_method>FSC 0.143, semi-independent</resolution_method>
                    <software_list>
                        <software>
                            <name>EMAN2, IMAGIC, FREALIGN, RSAMPLE, CTFFIND3</name>
                        </software>
                    </software_list>
                    <details>Refinement included data to 12 Angstrom resolution to limit FSC bias. See primary citation Supplementary Information for details.</details>
                </final_reconstruction>
            </image_processing>
        </structure_determination>
    </structure_determination_list>
    <map size_kbytes="128001" format="CCP4">
        <file>emd_5796.map.gz</file>
        <symmetry>
            <space_group>1</space_group>
        </symmetry>
        <data_type>Image stored as Reals</data_type>
        <dimensions>
            <col>320</col>
            <row>320</row>
            <sec>320</sec>
        </dimensions>
        <origin>
            <col>0</col>
            <row>0</row>
            <sec>0</sec>
        </origin>
        <spacing>
            <x>320</x>
            <y>320</y>
            <z>320</z>
        </spacing>
        <cell>
            <a>332.8</a>
            <b>332.8</b>
            <c>332.8</c>
            <alpha>90.0</alpha>
            <beta>90.0</beta>
            <gamma>90.0</gamma>
        </cell>
        <axis_order>
            <fast>X</fast>
            <medium>Y</medium>
            <slow>Z</slow>
        </axis_order>
        <statistics>
            <minimum>-0.63754106</minimum>
            <maximum>2.03202462</maximum>
            <average>-0.02734382</average>
            <std>0.17733996</std>
        </statistics>
        <pixel_spacing>
            <x>1.04</x>
            <y>1.04</y>
            <z>1.04</z>
        </pixel_spacing>
        <contour_list>
            <contour primary="true">
                <level>0.4</level>
                <source>AUTHOR</source>
            </contour>
        </contour_list>
        <annotation_details>70S ribosome with A, P and E tRNA (Class I)</annotation_details>
        <details>::::EMDATABANK.org::::EMD-5796::::</details>
    </map>
    <interpretation>
        <modelling_list>
            <modelling/>
        </modelling_list>
    </interpretation>
</emd>
