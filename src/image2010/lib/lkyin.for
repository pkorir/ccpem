C
C     =========================================================
      SUBROUTINE LKYIN(MINDX,LSPRGI,NLPRGI,NTOK,LINE,IBEG,IEND)
C     ==========================================================
C
C-----Subroutine to read standard input lines of the form
C              LABIN  item1=name1 item2=name2 ...
C     Use this to read the inputs, and then LRASSN to setup the
C     program label to file label assignments.
C
C---- Arguments:
C
C     LSPRGI    (I)	CHARACTER*30    program label strings (array)
C                               	L(abel) S(tring) PRG(rammme) I(nput)
C
C     NLPRGI    (I)	INTEGER         number of program input labels
C                               	N(umber of) L(abels) PRG(ramme) I(nput)
C
C     NTOK      (I)	INTEGER         from Parser, number of tokens on line
C
C     LINE      (I)	CHARACTER*(*)   the input line
C
C     IBEG,IEND (I)	INTEGER         arrays from the parser, delimiters
C                               	for each token
C
C
C
C     .. Parameters ..
      INTEGER MCOLS,MFILEX
      PARAMETER (MCOLS=200,MFILEX=9)
C     ..
C     .. Scalar Arguments ..
      INTEGER NLPRGI,NTOK,MINDX
      CHARACTER LINE* (*)
C     ..
C     .. Array Arguments ..
      INTEGER IBEG(*),IEND(*)
      CHARACTER*30 LSPRGI(*)
C     ..
C     .. Arrays in Common ..
      CHARACTER LSUSRI*30,LSUSRO*30
      INTEGER NLUSRI,NLUSRO
C     ..
C     .. Local Scalars ..
      INTEGER JDO,JLOOP,JSTART,JTOK
      CHARACTER CWORK*30,CWORK2*30,LC1*30,LC2*30,STROUT*400
C     ..
C     .. Local Arrays ..
      LOGICAL SetPrgLab(MCOLS)
C     ..
C     .. External Functions ..
      INTEGER LENSTR
      EXTERNAL LENSTR
C     ..
C     .. External Subroutines ..
      EXTERNAL PUTLIN,CCPERR
C     ..
C     .. Common blocks ..
      COMMON /MTZLAB/NLUSRI(MFILEX),NLUSRO(MFILEX)
      COMMON /MTZLBC/LSUSRI(MFILEX,MCOLS),LSUSRO(MFILEX,MCOLS)
C     ..
C     .. Save statement ..
      SAVE /MTZLAB/, /MTZLBC/
C     ..
C
      JTOK = NTOK
      JSTART = 2
      DO 10 JDO = 1,NLPRGI
        SetPrgLab(JDO) = .FALSE.
   10 CONTINUE
C
C---- Keyword  LABIN  item1=name1 item2=name2 ...
C
C      item == program label
C      name == user label
C
      IF (NLPRGI.LE.0) THEN
C
C            ***********************
        CALL PUTLIN(' Error this program has no PROGRAM LABELS',
     +       'ERRWIN')
C            ***********************
C
      ELSE IF (JTOK.LE.1) THEN
C
C            ***********************
        CALL PUTLIN(' **** Warning no argument given for LABIN ',
     +       'ERRWIN')
C            ***********************
C
      ELSE
C
C---- Find input label assignments
C
        DO 70 JLOOP = JSTART,NTOK,2
C
          IF ((JLOOP+1).GT.JTOK) THEN
            GO TO 80
          ELSE
            CWORK = LINE(IBEG(JLOOP) :IEND(JLOOP))
            LC1 = CWORK
C
C                *************
C-- file labels are case sensitive!!            CALL CCPUPC(CWORK)
C                *************
C
            CWORK2 = LINE(IBEG(JLOOP+1) :IEND(JLOOP+1))
            LC2 = CWORK2
C
C                **************
C-- file labels are case sinsitive!!            CALL CCPUPC(CWORK2)
C                **************
C
C           first try to match file label to left of assignment
C           (canonical order) after capitalisation
            DO 20 JDO = 1,NLPRGI
              IF (CWORK.EQ.LSPRGI(JDO)) THEN
                IF (SetPrgLab(JDO)) GOTO 30
                GO TO 60
              ENDIF
   20       CONTINUE
C           else, try to match file label on rhs as an option
   30       DO 40 JDO = 1,NLPRGI
              IF (CWORK2.EQ.LSPRGI(JDO)) THEN
                IF (SetPrgLab(JDO)) THEN
                 WRITE (STROUT,FMT=
     +            '('' ERROR with label assignments '',A,'' and '',A)') 
     +            CWORK(1:LENSTR(CWORK)),CWORK2(1:LENSTR(CWORK2))
C
C                      ***********************************************
                  CALL PUTLIN(STROUT,'ERRWIN')
                  CALL CCPERR(1,' ERROR Program label assigned twice')
C                      ***********************************************
C
                ENDIF
                GO TO 50
              ENDIF
   40       CONTINUE
C
            WRITE (STROUT,FMT=
     +        '(''  Neither '',A,'' nor '',A,'' recognised'')')
     +         CWORK(1:LENSTR(CWORK)),CWORK2(1:LENSTR(CWORK2))
C
C                ***********************
            CALL PUTLIN(STROUT,'ERRWIN')
C                ***********************
C
            WRITE (STROUT,FMT=
     +        '(''  OR maybe '',A,'' has been assigned twice '')')
     +        CWORK(1:LENSTR(CWORK))
C
C                ***********************
            CALL PUTLIN(STROUT,'ERRWIN')
            CALL CCPERR(1, 'Input column assignment does not match'//
     +           ' program labels')
C                ***********************
C
   50       NLUSRI(MINDX) = NLUSRI(MINDX) + 1
            LSUSRI(MINDX,JDO) = LC1
            SetPrgLab(JDO) = .TRUE.
            GO TO 70
   60       NLUSRI(MINDX) = NLUSRI(MINDX) + 1
            LSUSRI(MINDX,JDO) = LC2
            SetPrgLab(JDO) = .TRUE.
          END IF
   70   CONTINUE
        RETURN
   80   CONTINUE
C
C            ***********************
        CALL PUTLIN(' **** Error !!!! for LABIN ****','ERRWIN')
        CALL PUTLIN(' **** NOT ENOUGH ARGUMENT PAIRS of type ','ERRWIN')
        CALL PUTLIN('Prog_label = User_label','ERRWIN')
C            ***********************
C
      END IF
C
      END
