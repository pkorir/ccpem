#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
surfreader.py
========================

Read Amira HxSurface model (.surf) files.

This script employs a two-pass strategy in unravelling .surf files. First of all,
all the data is read into memory.

In the first pass, the location of the binary streams are determined. 

The second pass consists of parsing - both grammatical (ASCII) and structural (binary)
in order to populate the various field in the model.

About the model
===============
- The model consists of three classes of fields: Parameters, Generic fields, and Patches.

- Parameters hold descriptions of Materials, BoundaryIds, GridBox, GridSize and the Filename

- There are five types of Generic fields: Vertices, NBranchingPoints, NVerticesOnCurves,
  BoundaryCurves, and Patches

- Patches define segments and each consists of five fields: InnerRegion, OuterRegion (both of 
  which are described using materials defined within Parameters), BoundaryID, BranchingPoints,
  and Triangles (which describe the surface of the region)
  
- Optional fields: Surfaces

TODO:
- find generic way to extract 'Materials' field of header
- handle HyperSurface ASCII files (current version only handles BINARY files)


Version history:
0.0.1, 2015-10-30, First working version for ad hoc .surf binary filess
0.0.2, 2015-11-05, Incorporate colors from materials

"""
from __future__ import division

__author__ = 'Paul K. Korir, PhD'
__email__ = 'pkorir@ebi.ac.uk'
__date__ = '2015-10-30'


import sys
import re
import struct

# simpleparse
from simpleparse.parser import Parser
from simpleparse.common import numbers, strings
from simpleparse.dispatchprocessor import DispatchProcessor, getString, dispatchList, dispatch

# .surf files are specified by a VRML-like syntax
# the declaration below defines an EBNF grammar for this syntax
# in brief, the file consists of a header composed of blocks
# the blocks may have innerblocks
# innerblocks contain useful parameters such as materials specifications
declaration = r'''
surface        :=    tsn, header, tsn, block*, tsn
header         :=    "#", ts, propname, ts, number*, ts, propname
block          :=    propname*, ts, "{", tsn, innerblock1*, tsn, vallines*, tsn, "}", tsn
innerblock1    :=    propname*, ts, "{", tsn, innerblock2*, tsn, vallines*, tsn, "}", tsn
innerblock2    :=    propname*, ts, "{", tsn, innerblock3*, tsn, vallines*, tsn, "}", tsn
innerblock3    :=    propname*, ts, "{", tsn, vallines*, tsn, "}", tsn
vallines       :=    valline+
valline        :=    ts, propname, ts, values, ts, c*, tsn
values         :=    number_seq / string
number_seq     :=    number, (ts, number)*
propname       :=    [A-Za-z], [A-Za-z0-9_]*
<tsn>          :=    [ \t\n]*            # this production rule will not be reported!
<ts>           :=    [ \t]*
<c>            :=    ","
<eq>           :=    [=]
'''

class SurfProcessor(DispatchProcessor):
    """
    Class that captures the results of parsing the grammar

    Once the data is parsed using the parser above it needs to be accessed.
    This is done using a processor. Processors must inherit from DispatchProcessor (DP).
    The DP contains methods (termed handlers) to the various production rules e.g. 'header'
    with the exception of the root production rule (in the example above 'surface' is the root
    production rule). 
    
    If a production rule is a terminal rule containing data then a call to getString returns its content.
    If it is a container then the call depends on how it is structured:
    - if it is a unitary container then a call to dispatch returns child production rule results
    - if it is a list-like container then a call to dispatchList returns a list-like dataset of child production results
    
    Unfortunately, the implementation of EBNF grammar does not allow recursion.
    """
    # primitives
    def number(self, (tag, left, right, taglist), buffer):
        return getString((tag, left, right, taglist), buffer)

    def propname(self, (tag, left, right, taglist), buffer):
        return getString((tag, left, right, taglist), buffer)
    
    def anglename(self, (tag, left, right, taglist), buffer):
        return getString((tag, left, right, taglist), buffer)
    
    # hyphname (an extension of propname by '-')
    def hyphname(self, (tag, left, right, taglist), buffer):
        return getString((tag, left, right, taglist), buffer)

    def string(self, (tag, left, right, taglist), buffer):
        return getString((tag, left, right, taglist), buffer)

    def superblock(self, (tag, left, right, taglist), buffer):
        return dispatchList(self, taglist, buffer)

    # header
    def header(self, (tag, left, right, taglist), buffer):
        return dispatchList(self, taglist, buffer)

    # block
    def block(self, (tag, left, right, taglist), buffer):
        return dispatchList(self, taglist, buffer)

    # innerblock1
    def innerblock1(self, (tag, left, right, taglist), buffer):
        return dispatchList(self, taglist, buffer)

    # innerblock2
    def innerblock2(self, (tag, left, right, taglist), buffer):
        return dispatchList(self, taglist, buffer)

    # innerblock3
    def innerblock3(self, (tag, left, right, taglist), buffer):
        return dispatchList(self, taglist, buffer)

    # vallines
    def vallines(self, (tag, left, right, taglist), buffer):
        return dispatchList(self, taglist, buffer)

    def valline(self, (tag, left, right, taglist), buffer):
        return dispatchList(self, taglist, buffer)

    def values(self, (tag, left, right, taglist), buffer):
        return dispatch(self, taglist[0], buffer)

    def number_seq(self, (tag, left, right, taglist), buffer):
        return dispatchList(self, taglist, buffer)

    # vertices
    def vertices(self, (tag, left, right, taglist), buffer):
        return dispatchList(self, taglist, buffer)       


class Model(object):
    """
    Class that encapsulates data from a .surf file
    """    
    def __init__(self, version, format):
        self.version = version
#         print format
        assert format == 'BINARY' or format == 'BINARY-LITTLE-ENDIAN'
        self.format = format
        
        # flags
        self.is_populated = False
        
        # materials
        self.Materials = None
        self.id_to_metadata = dict()
        
        # Vertices
        self.Vertices_count = 0
        self.NBranchingPoints_count = 0
        self.NVerticesOnCurves_count = 0
        self.BoundaryCurves_count = 0
        self.Patches_count = 0
        self.Regions_count = 0
        
        # Offsets
        self.Vertices_start_offset = -1
        self.Vertices_end_offset = -1
        self.NBranchingPoints_start_offset = -1
        self.NBranchingPoints_end_offset = -1
        self.VerticesOnCurves_start_offset = -1
        self.VerticesOnCurves_end_offset = -1
        self.BoundaryCurves_start_offset = -1
        self.BoundaryCurves_end_offset = -1
        self.Patches_start_offset = -1
        self.Patches_end_offset = -1
        
        # data
        self.Vertices           = list()
        self.NBranchingPoints   = list()
        self.NVerticesOnCurves  = list()
        self.BoundaryCurves     = list()
        self.Patches            = list()
        
        # Regions
        self.Patches = dict()
    
    def __repr__(self):
        patches = ""
        for p,P in self.Patches.iteritems():
            triangles1 = P.Triangles[:10]
            triangles2 = P.Triangles[-10:]
            patches += """      
Patch%s
%s        %s,...,%s
""" % (p+1, P, triangles1, triangles2)
        vertices1 = self.Vertices[:10]
        vertices2 = self.Vertices[-10:]
        materials = ""
        for m,M in self.Materials.iteritems():
            materials += "%s\n" % M
        string = """
            \r*******************************************************************************
            \rAmira HyperSurface file
            \rVersion:              %s
            \rFormat:               %s
            \r*******************************************************************************
            \rMaterials:
            \r%s
            \r
            \rVertices              %s
            \r        %s,...,%s
            \r
            \rNBranchingPoints      %s
            \rNVerticesOnCurves     %s
            \rBoundaryCurves        %s
            \rPatches               %s
            \r
            \r%s
            \r*******************************************************************************
            \r""" % (
               self.version,
               self.format,
               materials,
               self.Vertices_count,
               vertices1, vertices2,
#                self.Vertices_start_offset, self.Vertices_end_offset,
               self.NBranchingPoints_count,
               self.NVerticesOnCurves_count,
               self.BoundaryCurves_count,
               self.Patches_count,
#                self.Vertices,
#                self.BranchingPoints,
#                self.VerticesOnCurves,
#                self.BoundaryCurves,
                patches,
            )
        return string

    def set_vertices(self, data):
        points = struct.unpack('>' + 'fff'*self.Vertices_count, data[self.Vertices_start_offset:self.Vertices_end_offset])
        x_points = points[::3]
        y_points = points[1::3]
        z_points = points[2::3]
        self.Vertices = zip(x_points, y_points, z_points)
    
    def set_patches(self, data):
        for p,P in self.Patches.iteritems():
            P.set_triangles(data)            


class Patch(object):
    """
    Class that encapsulates a patch (segment)
    """
    def __init__(self, inner_region, outer_region, boundary_id, branching_points, triangle_count):
        self.inner_region = inner_region
        self.outer_region = outer_region
        self.boundary_id = boundary_id
        self.branching_points = int(branching_points)
        self.triangle_count = int(triangle_count)
        
        # offsets
        self.triangles_start_offset = -1
        self.triangles_end_offset = -1
        
        # data
        self.Triangles = list()
    
    def __repr__(self):
        string = """
            \rInnerRegion           %s
            \rOuterRegion           %s
            \rBoundaryID            %s
            \rBranchingPoints       %s
            \rTriangles             %s
            \r""" % (
               self.inner_region,
               self.outer_region,
               self.boundary_id,
               self.branching_points,
               self.triangle_count,
        #        self.Triangles,
            )
        return string

    def set_triangles(self, data):
        vert_indices = struct.unpack('>' + 'iii'*self.triangle_count, data[self.triangles_start_offset:self.triangles_end_offset])
        vert1 = vert_indices[::3]
        vert2 = vert_indices[1::3]
        vert3 = vert_indices[2::3]
        self.Triangles = zip(vert1, vert2, vert3)


class Material(object):
    """
    Class that encapsulates a material
    """
    def __init__(self, name):
        self.name = name
        self.Id = None
        self.Color = None
        self.Name = None
        self.Group = None
    
    def set_param(self, param_name, param_value):
        if param_name == 'Id':
            self.Id = param_value[0]
        elif param_name == 'Color':
            self.Color = map(float, param_value)
        elif param_name == 'Name':
            self.Name = param_value.strip('"')
        elif param_name == 'Group':
            self.Group = param_value
        # add other params here
    
    def __repr__(self):
        string = """
            \r%s
            \rId                    %s""" % (self.name, self.Id)
        if self.Name is not None:
            string += """
            \rName                  %s""" % (self.Name)
        if self.Color is not None:
            string += """
            \rColor                 %s""" % (self.Color)
        if self.Group is not None:
            string += """
            \rGroup                 %s""" % (self.Group)
        return string

def first_pass(file_object, model_object):
    """
    Determine the offsets for binary data in stream.
    
    Return a model_object with offsets populated.
    
    Arguments:
    :param `file` file_object:        an open file object to a `.surf` file
    :param `SurfModel` model_object:        an empty object of class `SurfModel`
    """
    import pprint

    pp = pprint.PrettyPrinter()
    data = file_object.read()
    
    # parse the header for Materials
    parser = Parser(declaration)
    # the processor to handle the data from the parser
    surf_processor = SurfProcessor()
    # find all the ASCII before the Vertices
    # set the re.S flag to allow . to include \n in the search
    header_match = re.search(r'(?P<data>.*)\nVertices', data, flags=re.S)
    # the header is the 'data' group
    header = header_match.group('data')
    
#     print header
    
    # parse the header with our parser and our processor
    output = parser.parse(header, production='surface', processor=surf_processor)
    
    # get materials
#     pp.pprint(output[1])
#     pp.pprint(output[1][1][1])
    materials = output[1][1][1][1:] # likely to break if 'Materials' is restructured
    
    materials_dict = dict()
    i = 0
    for mat in materials:
        name = mat[0]
        M = Material(name)
        try:
            params = mat[1]
        except IndexError: # no params
            materials_dict[name] = M
            i += 1
            continue
        for param in params:
            param_name = param[0]
            param_value = param[1]
            M.set_param(param_name, param_value)
        materials_dict[name] = M
        i += 1
     
    model_object.Materials = materials_dict
    
    id_to_metadata = dict()
    for m in model_object.Materials.itervalues():
        if m.Id:
            id_to_metadata[int(m.Id)] = (m.name, tuple(m.Color) if m.Color else m.Color)
        else:
            print >> sys.stderr, "Material without Id: {}".format(m)
            print >> sys.stderr
    
#     from pprint import pprint
#     pprint(id_to_metadata)
    
    # Vertices
    vert_pattern = re.compile(r'Vertices (?P<vertices>\d+)\n')
    vert_match = vert_pattern.search(data)
    
    # NBranchingPoints
    bp_pattern = re.compile(r'NBranchingPoints (?P<bp>\d+)\n')
    bp_match = bp_pattern.search(data)
    
    # NVerticesOnCurves
    vc_pattern = re.compile(r'NVerticesOnCurves (?P<vc>\d+)\n')
    vc_match = vc_pattern.search(data)
    
    # BoundaryCurves
    bc_pattern = re.compile(r'BoundaryCurves (?P<bc>\d+)\n')
    bc_match = bc_pattern.search(data)
    
    # Patches
    p_pattern = re.compile(r'Patches (?P<patches>\d+)\n')
    p_match = p_pattern.search(data)
    
    # set count fields in model
    model_object.Vertices_count                 = int(vert_match.group('vertices'))
    model_object.Vertices_start_offset          = vert_match.end()
    model_object.Vertices_end_offset            = vert_match.end() + model_object.Vertices_count*12
    
    model_object.NBranchingPoints_count         = int(bp_match.group('bp'))
    model_object.NBranchingPoints_start_offset  = bp_match.end()
    model_object.NBranchingPoints_end_offset    = bp_match.end() + model_object.NBranchingPoints_count*12
    
    model_object.NVerticesOnCurves_count        = int(vc_match.group('vc'))
    model_object.NVerticesOnCurves_start_offset = vc_match.end()
    model_object.NVerticesOnCurves_end_offset   = vc_match.end() + model_object.NVerticesOnCurves_count*12
    
    model_object.BoundaryCurves_count           = int(bc_match.group('bc'))
    model_object.BoundaryCurves_start_offset    = bc_match.end()
    model_object.BoundaryCurves_end_offset      = bc_match.end() + model_object.BoundaryCurves_count*12 
    
    model_object.Patches_count                  = int(p_match.group('patches'))
    
    # set patches and their offsets
    patches = dict()
    
    pos = p_match.end()
    
    for patch_index in xrange(1, model_object.Patches_count + 1):
        ir_pattern = re.compile(r'InnerRegion (?P<ir_name>\w+)\n')
        ir_match = ir_pattern.search(data, pos)
        ir_name = ir_match.group('ir_name')
        
        or_pattern = re.compile(r'OuterRegion (?P<or_name>\w+)\n')
        or_match = or_pattern.search(data, pos)
        or_name = or_match.group('or_name')
        
        bi_pattern = re.compile(r'BoundaryID (?P<bi>\d+)\n')
        bi_match = bi_pattern.search(data, pos)
        bi = int(bi_match.group('bi'))
        
        bp_pattern = re.compile(r'BranchingPoints (?P<bp>\d+)\n')
        bi_match = bp_pattern.search(data, pos)
        bp = int(bp_match.group('bp'))
        
        tri_pattern = re.compile(r'Triangles (?P<tri_count>\d+)\n')
        tri_match = tri_pattern.search(data, pos)
        triangle_count = int(tri_match.group('tri_count'))
        
        patch = Patch(ir_name, or_name, bi, bp, triangle_count)
        
        # offsets
        patch.triangles_start_offset = tri_match.end()
        patch.triangles_end_offset   = tri_match.end() + triangle_count*12
        
        patches[patch_index] = patch
        
        # set the beginning of the next search as the end of the last
        pos = tri_match.end()
    
    model_object.Patches = patches
    
    # set the is_populated flag
    model_object.is_populated           = True
    
    return model_object


def second_pass(file_object, model_object):
    """
    Parse the stream in file_object with offsets defined in model_object.
    
    Return a model_object with all available data present.
    
    Arguments:
    @param file_object:        an open file object to a .surf file
    @param model_object:        an empty object of class Model
    """
    # check to ensure that the model_object is appropriately populated
    if not model_object.is_populated:
        raise ValueError("Unpopulated model object. Run first_pass() first.")
    
    data = file_object.read()
    
    model_object.set_vertices(data)
    # add other 'set_X()' methods here e.g. set_nbranchingpoints()
    model_object.set_patches(data)
    
    # else carry on
    return model_object


def get_data(f):
    """
    Main function that returns a SurfModel object given the file handle
    
    Arguments:
    @param f:            open file object to .surf file
    """
    # get the file designation from the first line
    design = f.readline().strip().split(' ')
    version = design[2]
    format = design[-1]
    
    # rewind the file handle
    f.seek(0)
    
    SurfModel = Model(version, format)
        
    # set offsets
    SurfModel = first_pass(f, SurfModel)
        
    # you must rewind the file object
    f.seek(0)

    # obtain data
    SurfModel = second_pass(f, SurfModel)

    f.close()
    
    return SurfModel


def parse_args(args):
    """
    Parse commandline arguments.
    """
    import optparse

    usage = "usage: %prog [options] file.surf"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option(
                      '-o', '--output',
                      help='file to write output to'
                      )
    
    if len(args) == 0:
        if __name__ == '__main__':
            parser.print_help()
        raise TypeError('Missing arguments.')
    else:
        options, pos_args = parser.parse_args(args)
    
    if len(pos_args) == 0:
        raise TypeError('Missing .surf file.')
    
    # the .surf file has to be the first positional argument
    if not re.search(r'.surf$', pos_args[0]):
        raise TypeError('Non-surf file. Retry with .surf file.')
    
    return options, pos_args


def main():
    """
    Parse a .surf file.
    """
    try:
        options, pos_args = parse_args(sys.argv[1:])
    except Exception as e:
        print >> sys.stderr, str(e)
        return 1
    
    surf_file = pos_args[0]
    f = open(surf_file, 'rb')    
    SurfModel = get_data(f)
    print >> sys.stderr, SurfModel
    
    return 0 # everything went well


if __name__ == "__main__":
    sys.exit(main())