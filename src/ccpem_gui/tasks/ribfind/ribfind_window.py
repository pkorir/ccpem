#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import glob
import shutil
from PyQt4 import QtGui, QtCore
from ccpem_gui.utils import window_utils
from ccpem_core.tasks.ribfind import ribfind_task
from ccpem_gui.jsmol_viewer.molecular_viewer \
    import MolecularBrowserNoServer


class RibfindWindow(window_utils.CCPEMTaskWindow):
    '''
    Ribfind window.
    '''
    def __init__(self,
                 task,
                 parent=None):
        super(RibfindWindow, self).__init__(task=task,
                                            parent=parent)

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)
        # Contact distance
        contact_distance_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='contact_distance',
            args=self.args)
        self.args_widget.args_layout.addWidget(contact_distance_input)
        # Input pdb
        input_pdb_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_pdb',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(input_pdb_input)
        input_pdb_input.value_line.textChanged.connect(
            self.set_pdb_preview)
        # Display pdb
        self.pdb_preview = MolecularBrowserNoServer()
        self.pdb_preview.setMinimumSize(QtCore.QSize(404, 404))
        self.pdb_preview.setMaximumSize(QtCore.QSize(404, 404))
        self.pdb_preview.setUrl(
            QtCore.QUrl('about:blank'))
        self.args_widget.args_layout.addWidget(self.pdb_preview)
        self.args_widget.args_layout.setAlignment(self.pdb_preview,
                                                  QtCore.Qt.AlignCenter)
        if self.args.input_pdb.value is not None:
            self.set_pdb_preview(filename=self.args.input_pdb.value)
        # Set launcher file
        self.launcher.add_file(
            arg_name='input_pdb',
            file_type='pdb',
            description=self.args.input_pdb.help,
            selected=True)

    @QtCore.pyqtSlot(QtCore.QString)
    def set_pdb_preview(self, filename):
        filename = str(filename)
        if os.path.exists(filename): 
            filename = os.path.abspath(filename)
            if '.pdb' in filename:
                filename = os.path.abspath(filename)
                self.pdb_preview.load_file(program='preview',
                                           model1=filename)

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.  For now show starting, refined
        pdb and experimental map.
        '''
        self.set_results_jsmol()

    def set_rigid_body_cutoff(self, cutoff=None, show_message=False):
        if self.pipeline is not None:
            process = self.pipeline.get_process('Ribfind')
            if process is not None:
                if cutoff is None:
                    cutoff = process.get_metadata('selected_cutoff')
                else:
                    process.set_metadata(key='selected_cutoff',
                                         value=int(cutoff))
                    process.export_json()
                self.cutoff_box.setValue(int(cutoff))

        job_data_path = os.path.join(self.args.job_location.value,
                                     'job_data')
        pref_file = glob.glob(job_data_path + '/*denclust_' + str(cutoff))[0]
        if os.path.exists(pref_file):
            shutil.copy(pref_file,
                        self.rigid_body_file)
        if show_message:
            # Show message that rigid file has been selected from GUI
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Information)
            text = 'Cutoff {0} selected'.format(cutoff)
            msg.setText(text)
            info_text = 'Rigid body file updated : {0}'.format(
                self.rigid_body_file)
            msg.setInformativeText(info_text)
            msg.setWindowTitle('Ribfind')
            msg.setStandardButtons(QtGui.QMessageBox.Ok)
            msg.exec_()

    def handle_select(self):
        self.set_rigid_body_cutoff(cutoff=self.cutoff_box.value(),
                                   show_message=True)

    def set_results_jsmol(self):
        '''
        Show results with jsmol viewer
        '''
        # Set dock and layout
        pdb_dock_layout = QtGui.QVBoxLayout()
        pdb_dock_widget = QtGui.QWidget()
        pdb_dock_widget.setLayout(pdb_dock_layout)
        self.pdb_dock = QtGui.QDockWidget('Results',
                                          self,
                                          QtCore.Qt.Widget)
        self.pdb_dock.setWidget(pdb_dock_widget)
        self.tabifyDockWidget(self.pipeline_dock, self.pdb_dock)

        # Add rigid body file
        self.rigid_body_file = os.path.join(self.args.job_location.value,
                                            'rigid.txt')
        self.launcher.add_file(
            arg_name=None,
            file_type='standard',
            path=self.rigid_body_file,
            description='Rigid body cluster',
            display_from='finished',
            selected=True)

        # Set select rigid body cutoff
        self.cutoff_group = QtGui.QGroupBox()
        self.cutoff_group_layout = QtGui.QHBoxLayout()
        self.cutoff_group.setLayout(self.cutoff_group_layout)
        cutoff_label = QtGui.QLabel('Set rigid body cutoff')
        self.cutoff_box = QtGui.QSpinBox()
        self.cutoff_box.setMinimum(1)
        self.cutoff_box.setMaximum(100)
        self.set_rigid_body_cutoff()
        cutoff_button = QtGui.QPushButton('Select')
        cutoff_button.clicked.connect(self.handle_select)
        self.cutoff_group_layout.addWidget(cutoff_label)
        self.cutoff_group_layout.addWidget(cutoff_button)
        self.cutoff_group_layout.addWidget(self.cutoff_box)
        pdb_dock_layout.addWidget(self.cutoff_group)

        # Set pdb results widget
        self.pdb_results = MolecularBrowserNoServer()
        self.pdb_results.setUrl(QtCore.QUrl('about:blank'))
        ribfind_xml_path = os.path.join(self.args.job_location.value,
                                        'job_data/ribfind.xml')
        assert os.path.isfile(self.args.input_pdb.value)
        assert os.path.isfile(ribfind_xml_path)
        self.pdb_results.load_file(program='ribfind',
                                   ribfind_xml=ribfind_xml_path,
                                   model1=self.args.input_pdb.value)
        pdb_dock_layout.addWidget(self.pdb_results)


def main():
    '''
    Launch stand alone window.
    '''
    window_utils.standalone_window_launch(
        task=ribfind_task.Ribfind,
        window=RibfindWindow)

if __name__ == '__main__':
    main()
