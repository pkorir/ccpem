#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from PyQt4 import QtCore
from ccpem_gui.utils import window_utils
from ccpem_core.tasks.tempy.difference_map import difference_map_task,\
    difference_map_chimera
from ccpem_core.settings import which

# To Do
# Add spectra plot
# Options to add - to be uncomment below when Agnel adds command line arg:
#    -> Model vs map
#    -> Map alignment option (slow)

class DifferenceMapWindow(window_utils.CCPEMTaskWindow):
    '''
    TEMPy Difference map window.
    '''
    def __init__(self,
                 task,
                 parent=None):
        super(DifferenceMapWindow, self).__init__(task=task,
                                                  parent=parent)
        self.output_pdb = None

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input map 1
        self.map_input_1 = window_utils.FileArgInput(
            parent=self,
            arg_name='map_path_1',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_input_1)
        # Map 1 resolution
        self.map_resolution_1 = window_utils.NumberArgInput(
            parent=self,
            arg_name='map_resolution_1',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_resolution_1)

        # Input map or PDB
        self.map_or_pdb = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='map_or_pdb_selection',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_or_pdb)
        self.map_or_pdb.value_line.currentIndexChanged.connect(
            self.set_pdb_or_map_input)

        # Input map 2
        self.map_input_2 = window_utils.FileArgInput(
            parent=self,
            arg_name='map_path_2',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_input_2)
        # Map 2 resolution
        self.map_resolution_2 = window_utils.NumberArgInput(
            parent=self,
            arg_name='map_resolution_2',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_resolution_2)
        # Input pdb
        self.pdb_input = window_utils.FileArgInput(
            parent=self,
            arg_name='pdb_path',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.pdb_input)

        # Extended options
        options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Extended options',
            button_tooltip='Extended difference map options')
        self.args_widget.args_layout.addLayout(options_frame)

#         # XXX TODO: placement holder until Agnel finishes map alignment code
#         # -> Map alignement
#         map_alignment = window_utils.ChoiceArgInput(
#             parent=self,
#             arg_name='map_alignment',
#             args=self.args)
#         options_frame.add_extension_widget(map_alignment)

        # -> Contour map 1
        map_contour_1 = window_utils.NumberArgInput(
            parent=self,
            arg_name='map_contour_1',
            args=self.args)
        options_frame.add_extension_widget(map_contour_1)
        # -> Contour map 2
        map_contour_2 = window_utils.NumberArgInput(
            parent=self,
            arg_name='map_contour_2',
            args=self.args)
        options_frame.add_extension_widget(map_contour_2)
        # -> Shell width
        shell_width = window_utils.NumberArgInput(
            parent=self,
            arg_name='shell_width',
            args=self.args)
        options_frame.add_extension_widget(shell_width)
        # -> Dust filter
        dust_filter = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='dust_filter',
            args=self.args)
        options_frame.add_extension_widget(dust_filter)
        # -> Dust filter
        soft_mask = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='soft_mask',
            args=self.args)
        options_frame.add_extension_widget(soft_mask)

        # Set pdb or map input
        sel = self.args.map_or_pdb_selection.choices.index(
            self.args.map_or_pdb_selection.value)
        self.set_pdb_or_map_input(sel)

        # Add files to launcher
        self.launcher.add_file(
            arg_name='map_path_1',
            file_type='map',
            description=self.args.map_path_1.help,
            selected=True)
        self.launcher.add_file(
            arg_name='map_path_2',
            file_type='map',
            description=self.args.map_path_2.help,
            selected=True)

    def set_pdb_or_map_input(self, sel):
        sel = self.args.map_or_pdb_selection.choices[sel]
        if sel == 'PDB':
            self.map_input_2.hide()
            self.map_resolution_2.hide()
            self.pdb_input.show()
        elif sel == 'Map':
            self.map_input_2.show()
            self.map_resolution_2.show()
            self.pdb_input.hide()

    def set_on_job_running_custom(self):
        if self.args.map_or_pdb_selection.value == 'PDB':
            self.launcher.add_file(
                arg_name='pdb_path',
                file_type='pdb',
                description=self.args.pdb_path.help,
                selected=True)

    def set_on_job_finish_custom(self):
        # Set expected output files as implemented in TEMPy/difference_map
        diff_map1, diff_map2 = self.get_difference_map_names()
        # Add output files to launcher
        if self.args.map_or_pdb_selection.value == 'PDB':
            self.launcher.add_file(
                arg_name=None,
                path=self.get_synthetic_map_name(),
                file_type='map',
                description='Synthetic map from PDB',
                selected=True)
        self.launcher.add_file(
            arg_name=None,
            path=diff_map1,
            file_type='map',
            description='Difference map 1 (map1 - map2)',
            selected=True)
        self.launcher.add_file(
            arg_name=None,
            path=diff_map2,
            file_type='map',
            description='Difference map 2 (map2 - map1)',
            selected=True)
        # Add power spectra plot
        power_spectra_plot = os.path.join(
            self.task.job_location,
            'spectra.png')
        if os.path.exists(power_spectra_plot):
            self.launcher.add_file(
                arg_name=None,
                path=power_spectra_plot,
                description='Matched power spectra of input maps',
                selected=False)

        message = ('Chimera will launch maps and automatically set input maps '
                   'to 1.5 sigma and difference maps to 2.5 sigma')
        self.launcher.set_message_label(message=message)

    def get_difference_map_names(self):
        m1_name =  os.path.basename(self.args.map_path_1.value).split('.')[0]
        if self.args.map_or_pdb_selection.value == 'PDB':
            m2_name = self.args.pdb_path.value
        else:
            m2_name = self.args.map_path_2.value
        m2_name =  os.path.basename(m2_name).split('.')[0]
        diff_map1 = os.path.join(self.task.job_location,
                                 (m1_name + '-' + m2_name+'_diff.mrc'))
        diff_map2 = os.path.join(self.task.job_location,
                                 (m2_name + '-' + m1_name+'_diff.mrc'))
        return diff_map1, diff_map2

    def get_synthetic_map_name(self):
        syn_map_path = os.path.basename(self.args.pdb_path.value).split('.')[0]
        syn_map_path = os.path.join(self.task.job_location,
                                    syn_map_path + '_syn.mrc')
        return syn_map_path

    def run_chimera_custom(self):
        chimera_bin = which('chimera')
        chimera_script = difference_map_chimera.__file__
        chimera_script = chimera_script.replace('.pyc', '.py')
        # Chimera script expects 2 input maps, followed by 2 difference maps
        if self.args.map_or_pdb_selection.value == 'PDB':
            map2 = self.get_synthetic_map_name()
        else:
            map2 = self.args.map_path_2.value
        diff_map1, diff_map2 = self.get_difference_map_names()
        maps = [self.args.map_path_1.value,
                map2,
                diff_map1,
                diff_map2]
        map_args = chimera_script
        # Check map exists else pass 'None'
        for map_ in maps:
            map_path = 'None'
            if map_ is not None:
                if os.path.exists(map_):
                    map_path = map_
            map_args += ' ' + map_path
        if self.args.map_or_pdb_selection.value == 'PDB':
            map_args += (' ' + self.args.pdb_path.value)
        else:
            map_args += (' no_pdb')
        run_args = ['--script']
        run_args.append(map_args)
        process = QtCore.QProcess()
        process.startDetached(chimera_bin, run_args)


def main():
    '''
    Launch stand alone window.
    '''
    window_utils.standalone_window_launch(
        task=difference_map_task.DifferenceMap,
        window=DifferenceMapWindow)

if __name__ == '__main__':
    main()
