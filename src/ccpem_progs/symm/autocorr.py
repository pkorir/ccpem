#!/usr/bin/env python
# coding: utf-8

import numpy as np
from scipy import stats
import itertools


# fast implementation of correlation between rows of the arrays x and y
# adapted from https://gist.github.com/dengemann/65b5f67ec1ae51b1d9d4P:
# Author: denis.engemann@gmail.com
# License: simplified BSD (3 clause)
def vectorised_correlation(x, y):

    # np.seterr(all='raise')  # debugging, raises all warnings as exceptions and halts automatically
    np.seterr(all='call')

    # asarray is a limited version of the np.array object
    x = np.asarray(x)
    y = np.asarray(y)

    # compensate nan values by setting both original and roll "nan" if either of them is nan
    # --> ensures that only value pairs with both entries not nan are correlated
    x = np.where(np.isnan(y), np.nan, x)
    y = np.where(np.isnan(x), np.nan, y)

    # taking the mean of each row
    mean_x = np.nanmean(x, axis=-1)
    mean_y = np.nanmean(y, axis=-1)

    # reshaping the means so that each mean is the row it represents, ie the means for a 3x4 matrix are in a
    # 1x3 matrix/vector, then subtracting the mean from each entry in that row. values previously masked as
    # np.nan stay masked
    x_minus_m, y_minus_m = x - mean_x[..., None], y - mean_y[..., None]

    # numerator: takes the sum of each row multiplied by the row in the 2nd array
    # nansum ignores value pairs where one of the two is nan
    corr_nums = np.nansum(x_minus_m * y_minus_m, axis=-1)

    # denominator: stats.ss is a bit faster than add.reduce(square(...)), but can't deal with nans
    corr_dens = np.sqrt(np.nansum(np.square(x_minus_m), axis=-1) * np.nansum(np.square(y_minus_m), axis=-1))
    corr_vector = corr_nums / corr_dens

    # two bugfixes:
    # - data with too many masked values would produce empty correlation vectors.
    # -- These are now entered as 0 (rather than nan)
    # - y slices in the image file containing only 0s would cause nan/inf for all r in these slices
    # -- the mask updating would set all (y,r) in the mask to 0, instead of just relevant slices
    # -- mask updating now element- instead of row-wise
    warning = 0  # keeping track of whether the warning occurred
    new_mask_row = np.ones(corr_vector.shape)

    if True in np.isnan(corr_vector) or True in np.isinf(corr_vector):
        new_mask_row = np.where(np.logical_or(np.isnan(corr_vector), np.isinf(corr_vector)),
                                np.zeros(corr_vector.shape), np.ones(corr_vector.shape))
        corr_vector = np.where(np.logical_or(np.isnan(corr_vector), np.isinf(corr_vector)),
                               np.zeros(corr_vector.shape), corr_vector)
        warning = 1

    return corr_vector, warning, new_mask_row


# same as above, but without the code necessary to handle nan --> faster
def vectorised_correlation_no_mask(x, y):

    # np.seterr(all='raise')  # debugging, raises all warnings as exceptions and halts automatically
    np.seterr(all='call')

    x = np.asarray(x)
    y = np.asarray(y)

    mean_x = np.mean(x, axis=-1)
    mean_y = np.mean(y, axis=-1)

    x_minus_m, y_minus_m = x - mean_x[..., None], y - mean_y[..., None]

    corr_nums = np.add.reduce(x_minus_m * y_minus_m, axis=-1)

    corr_dens = np.sqrt(stats.ss(x_minus_m, axis=-1) * stats.ss(y_minus_m, axis=-1))
    corr_vector = corr_nums / corr_dens

    return corr_vector


# bringing the data in the right format and iterating over all possible row rolls
def autocorrelation(in_data, old_mask, verbose=False):

    tupel_list = list(itertools.product(range(in_data.shape[1]), range(in_data.shape[2])))

    # this generates a 2D array, each row containing the values in one circle
    flattened_data = np.array([in_data[:, a, b] for (a, b) in tupel_list])
    flattened_mask = np.array([old_mask[:, a, b] for (a, b) in tupel_list])

    # pdb.set_trace()

    # sanity check, array needs to be floats
    if flattened_data.dtype != "float64":
        flattened_data = np.array(flattened_data, dtype=np.float64)

    # setting masked values to nan
    flattened_data[flattened_mask == 0] = np.nan

    # initialises the output
    correlated_data = np.zeros(flattened_data.shape)

    warning = 0  # passing the information about the warning up the nestings
    flattened_mask = np.ones(flattened_mask.shape)  # initialise the new mask

    # correlation between the whole matrix and the matrix rolled by i, yielding a vector of correlation coefficients
    if 0 in old_mask:
        if verbose:
            print '\nMasking...'
        for i in range(flattened_data.shape[1]):
            roll_correlated, warning_new, new_mask_row = vectorised_correlation(flattened_data,
                                                                                np.roll(flattened_data, i, axis=1))
            correlated_data[:, i] = roll_correlated

            # updating the new mask
            flattened_mask[:, i] = new_mask_row

            # relaying the warning up into the main function
            if warning == 0 and warning_new == 1:
                warning = 1

    else:
        if verbose:
            print '\nNo masking...'

        for i in range(flattened_data.shape[1]):
            roll_correlated = vectorised_correlation_no_mask(flattened_data, np.roll(flattened_data, i, axis=1))
            correlated_data[:, i] = roll_correlated

    # reshape the outout array to have the same shape as the input data
    output_data = np.zeros(in_data.shape)
    new_mask = np.ones(old_mask.shape)
    count = 0
    for (a, b) in tupel_list:
        output_data[:, a, b] = correlated_data[count, :]
        new_mask[:, a, b] = flattened_mask[count, :]
        count += 1

    return output_data, new_mask, warning
