#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import sys
import os
import types
import textwrap
import json
from collections import OrderedDict
from PyQt4 import QtGui, QtCore
from ccpem_gui.icons import icon_utils
from ccpem_gui.utils import ccpem_widgets
from ccpem_core.mrc_map_io_clipper import read_mrc_header
from ccpem_core import ccpem_utils
from ccpem_core import settings
from ccpem_core import process_manager
from ccpem_gui import icons
import ccpem_launcher_widget
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_gui.utils import ccpem_pipeline_viewer
from ccpem_gui.project_database import sqlite_project_database

class CCPEMArgsWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(CCPEMArgsWidget, self).__init__(parent)
        self.args_layout = QtGui.QVBoxLayout()
        # Set scroll
        scrollwidget = QtGui.QWidget()
        scrollwidget.setLayout(self.args_layout)
        self.scroll = QtGui.QScrollArea()
        self.scroll.setWidgetResizable(True)  # Set to make the inner widget resize with scroll area
        self.scroll.setWidget(scrollwidget)
        layout = QtGui.QHBoxLayout()
        layout.addWidget(self.scroll)
        self.setLayout(layout)


def standalone_window_launch(task,
                             window,
                             args_json=None,
                             pipeline = None):
    '''
    Launch task standalone task GUI.
    '''
    # Print header
    message = 'CCP-EM | ' + task.task_info.name
    ccpem_utils.print_header(message=message)
    message = 'CCP-EM standalone window task'
    ccpem_utils.print_sub_header(message=message)
    print '  Usage:'
    info = OrderedDict([
        ('ccpem-<name> <args.json>', 'Set inputs from JSON file'),
        ('ccpem-<name> <job directory>', 'Load previous job')])
    for key in info.keys():
        print '    {0: <34} {1}'.format(key, info[key])

    # Read args or pipeline from previous job
    for arg in sys.argv:
        if args_json is None:
            if arg[-5:] == '.json':
                try:
                    json.load(open(arg, 'r'))
                    args_json = arg
                except ValueError:
                    args_json = None
                    message = 'Can not read {0}'.format(arg)
                    ccpem_utils.print_warning(message=message)

        find_pipeline = os.path.join(arg, 'task.ccpem')
        find_pipeline = os.path.abspath(find_pipeline)

        if os.path.exists(find_pipeline):
            pipeline = process_manager.CCPEMPipeline(pipeline=None)
            pipeline.import_json(jsonfile=find_pipeline)

    # Set job location
    job_location = os.path.join(os.getcwd(),
                                task.task_info.name + '_1')
    # Run task
    run_task = task(args_json=args_json,
                    job_location=job_location,
                    pipeline=pipeline)

    # Launch GUI
    app = QtGui.QApplication(sys.argv)
    if sys.platform == 'linux' or sys.platform == 'linux2':
        style = 'plastique'
        app.setStyle(style)
        app.setStyleSheet('''QToolTip {background-color: black;
                                       color: white; 
                                       border: black solid 1px
                                       }''')
    app.setWindowIcon(QtGui.QIcon(icons.icon_utils.get_ccpem_icon()))
    CCPEMStandAloneWindow(
        task=run_task,
        window=window)
    sys.exit(app.exec_())


class CCPEMStandAloneWindow(QtGui.QWidget):
    '''
    Dummy window for launching thread safe stand alone windows.
    '''
    def __init__(self,
                 task,
                 window,
                 parent=None,
                 database_path=None,
                 pipeline=None):
        super(CCPEMStandAloneWindow, self).__init__(parent)
        self.task = task
        self.window = window(
            parent=self,
            task=self.task)
        # Below required to display warning dialogue
        self.task.parent = self
        self.setWindowTitle('CCPEM | App helper')
        self.set_logo_and_view_directory()
        self.check_children = QtCore.QTimer()
        self.check_children.timeout.connect(self.check_children_visible)
        self.check_children.start(200)
        self.show()
        self.launch_task_window()
        self.lower()

    def launch_task_window(self):
        self.window.show()
        self.window.activateWindow()

    def new_task_window(self):
        self.window.create_child()

    def check_children_visible(self):
        '''
        Check children visible, if not close window.
        '''
        children = self.findChildren(QtGui.QMainWindow)
        close_window = True
        for child in children:
            if child.isVisible():
                close_window = False
        if close_window:
            self.close()

    def set_logo_and_view_directory(self):
        layout = QtGui.QHBoxLayout()
        # Logo
        ccpem_label = QtGui.QLabel()
        ccpem_logo = QtGui.QPixmap(icons.icon_utils.get_ccpem_icon())
        ccpem_label.setPixmap(ccpem_logo.scaled(
            50,
            50,
            QtCore.Qt.KeepAspectRatio,
            QtCore.Qt.SmoothTransformation))
        ccpem_label.setToolTip(
            'CCP-EM\n'
            'Collaborative Computational Project for Electron cryo-Microscopy')
        layout.addWidget(ccpem_label)
        toolbar = QtGui.QToolBar()
        toolbar.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        #
        new_icon = os.path.join(icon_utils.get_theme_icons_path(),
                                'actions/window-new.png')
        toolbar.addAction(
            QtGui.QIcon(new_icon),
            'New',
            self.new_task_window)
        #
        folder_icon = os.path.join(icon_utils.get_theme_icons_path(),
                                   'status/folder-open.png')
        toolbar.addAction(
            QtGui.QIcon(folder_icon),
            'Folder',
            self.run_file_browser)
        layout.addWidget(toolbar)
        self.setLayout(layout)

    def run_file_browser(self):
        path = QtCore.QUrl.fromLocalFile(QtCore.QString(os.getcwd()))
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(path))


class CCPEMMainWindow(QtGui.QMainWindow):
    '''
    Simple main window.  Sets expected size and policy.
    '''
    def __init__(self,
                 parent=None):
        super(CCPEMMainWindow, self).__init__(parent)
        size_policy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred,
                                        QtGui.QSizePolicy.Preferred)
        self.setSizePolicy(size_policy)
        avail_geo = QtGui.QDesktopWidget().availableGeometry(self).size()
        self.resize(QtCore.QSize(avail_geo.width() * 0.45,
                                 avail_geo.height() * 0.45 * 1.618))


class CCPEMTaskWindow(QtGui.QMainWindow):
    '''
    Base Class for Task Windows
    '''
    def __init__(self,
                 parent=None,
                 task=None):
        super(CCPEMTaskWindow, self).__init__(parent)
        self.task = task
        self.args = self.task.args
        self.database_path = self.task.database_path
        self.pipeline = self.task.pipeline
        self.status = 'ready'
        self.job_status_timer = QtCore.QTimer()
        self.job_status_timer.timeout.connect(self.check_job_status)
        self.rv_view = None
        # Set toolbar
        self.set_toolbar()
        # Set dock
        self.set_dock()
        size_policy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred,
                                        QtGui.QSizePolicy.Preferred)
        self.setSizePolicy(size_policy)
        avail_geo = QtGui.QDesktopWidget().availableGeometry(self).size()
        self.resize(QtCore.QSize(avail_geo.width() * 0.45,
                                 avail_geo.height() * 0.45 * 1.618))
        # Set program name
        if self.task is not None:
            self.set_window_title()
        # Set file launcher
        self.launcher = ccpem_launcher_widget.CCPEMLauncher(
            parent=self)
        self.set_args()
        # Add stretch to end of args widget
        self.args_widget.args_layout.addStretch(2)

        self.set_pipeline_ui()
        self.set_launcher_ui()
        if self.pipeline is not None:
            self.recreate_process()
        # Show import error if present
        if self.args.import_error is not None:
            self.show_args_import_error()

    def show_args_import_error(self):
        text = self.args.import_error_message
        QtGui.QMessageBox.warning(self,
                                  'Error',
                                  text)

    def set_args(self):
        self.args_widget
        raise NotImplementedError('Subclasses should implement this')

    def create_child(self):
        child_args = self.args.deep_copy()
        child_task = self.task.__class__(args=child_args,
                                         database_path=self.database_path)
        child = self.__class__(parent=self.parent(),
                               task=child_task)
        child.setGeometry(self.geometry().x()+20,
                          self.geometry().y()+20,
                          self.geometry().width(),
                          self.geometry().height())
        child.show()

    def set_dock(self):
        self.set_setup_ui()
        self.setup_dock = QtGui.QDockWidget('Setup', self, QtCore.Qt.Widget)
        self.setup_dock.setWidget(self.setup_ui)
        self.setup_dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea)
        self.setup_dock.setFeatures(
            QtGui.QDockWidget.DockWidgetMovable |
            QtGui.QDockWidget.DockWidgetFloatable)
        self.setup_dock.setToolTip('Set job parameters')
        #
        size_policy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum,
                                        QtGui.QSizePolicy.Maximum)
        self.setup_dock.setSizePolicy(size_policy)
        #
        self.addDockWidget(QtCore.Qt.TopDockWidgetArea, self.setup_dock)
        # Setup tab to top
        self.setup_dock.raise_()
        self.setDockOptions(
            QtGui.QMainWindow.ForceTabbedDocks |
            QtGui.QMainWindow.VerticalTabs |
            QtGui.QMainWindow.AnimatedDocks)

    def set_setup_ui(self):
        '''
        Setup UI for input arguments.
        '''
        self.setup_ui = QtGui.QWidget()
        self.setup_ui_vbox_layout = QtGui.QVBoxLayout()
        # Add args dialog
        self.args_widget = CCPEMArgsWidget()
        self.setup_ui_vbox_layout.addWidget(self.args_widget)
        self.setup_ui.setLayout(self.setup_ui_vbox_layout)
        return self.setup_ui

    def set_pipeline_ui(self):
        self.pipeline_view = ccpem_pipeline_viewer.CCPEMProcessViewer(
            pipeline=None,
            parent=self)
        self.pipeline_dock = QtGui.QDockWidget('Pipeline',
                                               self,
                                               QtCore.Qt.Widget)
        self.pipeline_dock.setFeatures(
            QtGui.QDockWidget.DockWidgetMovable |
            QtGui.QDockWidget.DockWidgetFloatable)
        self.pipeline_dock.setToolTip('Monitor job process')
        self.pipeline_dock.setWidget(self.pipeline_view)
        self.pipeline_dock.setVisible(False)
        self.tabifyDockWidget(self.setup_dock, self.pipeline_dock)

    def set_launcher_ui(self):
        '''
        Show file view launcher.
        '''
        self.launcher_dock = QtGui.QDockWidget('Launcher',
                                               self,
                                               QtCore.Qt.Widget)
        self.launcher_dock.setFeatures(
            QtGui.QDockWidget.DockWidgetMovable |
            QtGui.QDockWidget.DockWidgetFloatable)
        self.launcher_dock.setToolTip('Launch job files')
        self.launcher_dock.setWidget(self.launcher)
        self.launcher_dock.setVisible(False)
        self.tabifyDockWidget(self.setup_dock, self.launcher_dock)

    def disable_args_widgets(self, disable=True):
        base_classes = self.args_widget.findChildren(CCPEMArgBaseWidget)
        if len(base_classes) > 0:
            for child in base_classes:
                if hasattr(child, 'disable'):
                    child.setDisabled(disable)
        else:
            self.args_widget.setDisabled(disable)

    def add_extension_frame_layout(
            self,
            args_layout,
            button_name,
            button_tooltip):
        '''
        Add extension frame for grouped args.
        '''
        def button_clicked():
            if button.isChecked():
                frame.show()
            else:
                frame.hide()
        button = QtGui.QCheckBox(button_name)
        button.clicked.connect(button_clicked)
        button.setToolTip(button_tooltip)
        args_layout.addWidget(button)
        frame = QtGui.QFrame()
        frame.setFrameStyle(
            QtGui.QFrame.StyledPanel | QtGui.QFrame.Sunken)
        layout = QtGui.QVBoxLayout()
        frame.setLayout(layout)
        args_layout.addWidget(frame)
        button_clicked()
        return layout

    def set_window_title(self):
        window_title = 'CCP-EM | ' + self.task.task_info.name
        if self.args.job_title.value is not None:
            if self.args.job_title.value != 'None':
                window_title += ' | '
                window_title += self.args.job_title.value
        if self.task.job_location is not None:
            window_title += ' | '
            window_title += os.path.basename(self.task.job_location)
        self.setWindowTitle(window_title)

    def set_toolbar(self):
        # Set toolbar and options
        self.tool_bar = QtGui.QToolBar('Tool bar')
        self.tool_bar.setContextMenuPolicy(QtCore.Qt.PreventContextMenu)
        self.tool_bar.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.tool_bar.setMovable(False)
        #
        self.status = 'ready'
        #
        run_icon = os.path.join(icon_utils.get_theme_icons_path(),
                                'actions/system-run.png')
        self.tb_run_button = self.tool_bar.addAction(
            QtGui.QIcon(run_icon),
            'Run',
            self.handle_toolbar_run)
        self.tb_run_button.setToolTip('Run task process')
        #
        new_icon = os.path.join(icon_utils.get_theme_icons_path(),
                                'actions/window-new.png')
        self.tb_new_button = self.tool_bar.addAction(
            QtGui.QIcon(new_icon),
            'New',
            self.create_child)
        self.tb_new_button.setToolTip('Clone into new job')

        #
        open_icon = os.path.join(icon_utils.get_theme_icons_path(),
                                 'actions/document-open-recent.png')
        self.tb_load_button = self.tool_bar.addAction(
            QtGui.QIcon(open_icon),
            'Load',
            self.handle_toolbar_load)
        self.tb_load_button.setToolTip('Load previous job')
        #
        if settings.which(program='coot') is not None:
            coot_icon = os.path.join(icon_utils.get_other_icons_path(),
                                     'coot_icon.png')
            self.tb_coot_button = self.tool_bar.addAction(
                QtGui.QIcon(coot_icon),
                'Coot',
                self.run_coot)
        #
        if settings.which(program='ccp4mg') is not None:
            ccp4mg_icon = os.path.join(icon_utils.get_other_icons_path(),
                                       'ccp4mg_icon.png')
            self.tb_ccp4mg_button = self.tool_bar.addAction(
                QtGui.QIcon(ccp4mg_icon),
                'CCP4mg',
                self.run_ccp4mg)
        #
        if settings.which(program='chimera') is not None:
            chimera_icon = os.path.join(icon_utils.get_other_icons_path(),
                                        'chimera_icon_no_iccp.png')
            self.tb_chimera_button = self.tool_bar.addAction(
                QtGui.QIcon(chimera_icon),
                'Chimera',
                self.run_chimera)
        #
        terminal_icon = os.path.join(icon_utils.get_theme_icons_path(),
                                     'apps/terminal.png')
        self.tb_terminal_button = self.tool_bar.addAction(
            QtGui.QIcon(terminal_icon),
            'Terminal',
            self.run_terminal)
        self.tb_terminal_button.setEnabled(False)
        #
        folder_icon = os.path.join(icon_utils.get_theme_icons_path(),
                                   'status/folder-open.png')
        self.tb_browser_button = self.tool_bar.addAction(
            QtGui.QIcon(folder_icon),
            'Output',
            self.run_file_browser)
        self.tb_browser_button.setEnabled(False)
        #
        info_icon = os.path.join(icon_utils.get_theme_icons_path(),
                                 'status/dialog-question.png')
        self.tb_info_button = self.tool_bar.addAction(
            QtGui.QIcon(info_icon),
            'Info',
            self.run_info_dialog)
        #
        # Add spacer to right align status
        spacer = QtGui.QWidget()
        spacer.setSizePolicy(QtGui.QSizePolicy.Expanding,
                             QtGui.QSizePolicy.Expanding)
        self.tool_bar.addWidget(spacer)
        #
        stop_icon = os.path.join(icon_utils.get_theme_icons_path(),
                                 'actions/process-stop.png')
        self.tb_stop_button = self.tool_bar.addAction(
            QtGui.QIcon(stop_icon),
            'Kill',
            self.handle_toolbar_stop)
        self.tb_stop_button.setEnabled(False)
        # Add status indicator
        self.status_widget = ccpem_widgets.CCPEMStatusWidget(parent=self)
        self.status_action = self.tool_bar.addWidget(self.status_widget)
        self.addToolBar(self.tool_bar)

    def check_error_logs(self):
        '''
        Check error log files and display.
        '''
        for jobs in self.task.pipeline.pipeline:
            for job in jobs:
                if job.stderr is not None:
                    statinfo = os.stat(job.stderr)
                    if statinfo.st_size > 0:
                        # Show as dock tab
                        dock_name = 'Error: ' + job.name
                        self.set_error_log_dock(log_path=job.stderr,
                                                dock_name=dock_name)
                        # Add to launcher
                        self.launcher.add_file(
                            arg_name=None,
                            file_type='standard',
                            path=job.stderr,
                            description='Error file',
                            display_from='running',
                            selected=True)

    def set_error_log_dock(self, log_path, dock_name):
        '''
        Dock to display stdout_dockempeak.txt.
        '''
        # Set peak browser
        log_widget = QtGui.QWidget()
        log_widget_layout = QtGui.QVBoxLayout()
        # Setup layout
        log_tb = ccpem_widgets.CCPEMTextBrowser(
            filepath=log_path,
            parent=self)
        log_widget_layout.addWidget(log_tb)
        log_widget.setLayout(log_widget_layout)
        # Set peak dock
        log_dock = QtGui.QDockWidget(dock_name,
                                     self,
                                     QtCore.Qt.Widget)
        log_dock.setWidget(log_widget)
        log_dock.setVisible(True)
        log_dock.setToolTip('View log file')
        self.tabifyDockWidget(self.launcher_dock, log_dock)

    def run_coot(self):
        '''
        Run coot viewer.
        '''
        if self.run_coot_custom() == NotImplemented:
            self.launcher.launch_mg_viewers(mg_viewer='Coot',
                                            launch_mg_without_file=True)

    def run_coot_custom(self):
        return NotImplemented

    def run_chimera(self):
        '''
        Run chimera viewer.
        '''
        if self.run_chimera_custom() == NotImplemented:
            self.launcher.launch_mg_viewers(mg_viewer='Chimera',
                                            launch_mg_without_file=True)

    def run_chimera_custom(self):
        return NotImplemented

    def run_ccp4mg(self):
        '''
        Run ccp4mg viewer.
        '''
        if self.run_chimera_custom() == NotImplemented:
            self.launcher.launch_mg_viewers(mg_viewer='CCP4mg',
                                            launch_mg_without_file=True)

    def run_ccp4mg_custom(self):
        return NotImplemented

    def run_relion(self):
        '''
        Run Relion
        '''
        if hasattr(self, 'run_relion_custom'):
            self.run_relion_custom()
        else:
            run_relion()

    def run_info_dialog(self):
        '''
        Show task info dialog
        '''
        info_str = '<p><b>{0}</b>: {1} </p> {2}'.format(
            self.task.task_info.name,
            self.task.task_info.description,
            self.task.task_info.author)
        if self.task.task_info.documentation_link is not None:
            doc_link = '<p><a href="{0}">Online documentation</a></p>'.format(
                self.task.task_info.documentation_link)
            info_str += doc_link
        QtGui.QMessageBox.about(self,
                                'Task info',
                                info_str)

    def run_file_browser(self):
        '''
        Run file browser for output files.
        '''
        if self.pipeline is None:
            path = 'file:///' + os.getcwd()
        else:
            path = 'file:///' + self.pipeline.location
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(path))

    def run_terminal(self):
        '''
        Run terminal and set job location as working directory.
        '''
        if hasattr(self, 'pipeline'):
            run_terminal(working_directory=self.pipeline.location)
        else:
            run_terminal()

    def handle_toolbar_run(self):
        '''
        Run task.
        '''
        if self.database_path is not None:
            db = sqlite_project_database.CCPEMDatabase(
                database_path=self.database_path)
            job_id = db.insert_new_job(program=self.task.task_info.name,
                                       job_location='None')
        else:
            job_id=None
        if self.check_input():
            self.run_detached_process(job_id=job_id)

    def check_input(self):
        '''
        Check required inputs are set, display warning message if not.
        '''  
        base_classes = self.findChildren(CCPEMArgBaseWidget)
        ready = True
        warnings = []
        for child in base_classes:
            if hasattr(child, 'is_ready'):
                if not child.is_ready():
                    ready = False
                    warnings.append(child.label)
        if not ready:
            text = 'Warning - set following arguments\n'
            for warning in warnings:
                text += '\n    {0}'.format(warning)
            QtGui.QMessageBox.warning(self,
                                      'Error',
                                      text)
        return ready

    def run_detached_process(self, job_id=None):
        '''
        Launch detached job.
        '''
        db_inject = None
        if self.database_path is not None:
            db_inject = sqlite_project_database.CCPEMInjector(
                database_path=self.database_path)
        self.task.run_task(job_id=job_id,
                           db_inject=db_inject)
        self.pipeline = self.task.pipeline
        self.set_on_job_running()

    def handle_toolbar_load(self):
        '''
        Open job pipeline and redraw widget.
        '''
        dialog_path = os.getcwd()
        q_settings = QtCore.QSettings()
        # Get job location from parent
        if self.parent is not None:
            if hasattr(self.parent, 'active_project_path'):
                dialog_path = self.parent.active_project_path
        # If not set get from previous location
        if dialog_path is None:
            dialog_path = q_settings.value('LOADTASK_DIR_KEY').toString()
        # If path doesn't exist go to cwd
        if not os.path.exists(dialog_path):
            dialog_path = os.getcwd()
        #
        task_file = QtGui.QFileDialog.getOpenFileName(
            None,
            'Open a File',
            dialog_path,
            '*.ccpem')

        if os.path.exists(path=task_file):
            q_settings.setValue('LOADTASK_DIR_KEY',
                                os.path.dirname(str(task_file)))
            relaunch_task_window(
                task_class=self.task.__class__,
                window_class=self.__class__,
                task_file=task_file,
                geometry=self.geometry())
            self.close()

    def handle_toolbar_stop(self):
        '''
        Stop pipeline.
        '''
        if self.pipeline is not None:
            self.pipeline.kill_jobs_and_terminate()

    def handle_title_set(self):
        self.set_window_title()

    def recreate_process(self):
        if hasattr(self.task, 'run_pipeline'):
            # Exception for old style pipeplines (see mrcallspacea), new
            # pipelines should be like refmac
            try:
                self.task.run_pipeline(run=False)
            except:
                TypeError
        self.check_job_status()

    def check_job_status(self):
        '''
        Detect if job completed.
        '''
        if self.pipeline is not None:
            status = process_manager.get_process_status(
                self.pipeline.json)
            if status == 'finished':
                self.set_on_job_running()
                self.set_on_job_finish()
            elif status == 'failed':
                self.set_on_job_running()
                self.set_on_job_failed()
            elif status == 'running':
                self.set_on_job_running()

    def set_on_job_running(self):
        self.set_window_title()
        if self.status != 'running':
            if self.pipeline is not None:
                self.status_widget.set_running()
                self.job_status_timer.start(500)
                self.tb_stop_button.setEnabled(True)
                self.tb_run_button.setEnabled(False)
                self.tb_load_button.setEnabled(False)
                self.tb_browser_button.setEnabled(True)
                self.tb_terminal_button.setEnabled(True)
                self.disable_args_widgets()
                self.status = 'running'
                if hasattr(self, 'pipeline_view'):
                    if hasattr(self.pipeline_view, 'pipeline_widget'):
                        self.pipeline_view.pipeline_widget.set_pipeline(
                            pipeline=self.task.pipeline)
                    self.pipeline_dock.setVisible(True)
                    self.pipeline_dock.raise_()
                if len(self.launcher.files) > 0:
                    self.launcher_dock.setVisible(True)
                    self.launcher.set_tree_view()
        self.set_on_job_running_custom()

    def set_on_job_running_custom(self):
        pass

    def set_on_job_finish(self, show_error_logs=True):
        '''
        Actions to run on job completion.
        '''
        self.status_widget.set_finished()
        self.tb_run_button.setEnabled(False)
        self.disable_args_widgets()
        self.tb_load_button.setEnabled(True)
        self.tb_browser_button.setEnabled(True)
        self.tb_terminal_button.setEnabled(True)
        self.tb_stop_button.setEnabled(False)
        self.job_status_timer.stop()
        self.status = 'finished'
        if hasattr(self, 'pipeline_view'):
            if hasattr(self.pipeline_view, 'pipeline_widget'):
                self.pipeline_view.pipeline_widget.set_pipeline(
                            pipeline=self.task.pipeline)
            self.pipeline_dock.setVisible(True)
            self.pipeline_dock.raise_()
        if len(self.launcher.files) > 0:
            self.launcher_dock.setVisible(True)
            self.launcher.set_tree_view()
        if show_error_logs:
            self.check_error_logs()
        self.set_on_job_finish_custom()

    def set_on_job_finish_custom(self):
        pass

    def set_on_job_failed(self):
        '''
        Actions to run on job fail.
        '''
        self.status_widget.set_failed()
        self.tb_run_button.setEnabled(False)
        self.tb_load_button.setEnabled(True)
        self.tb_browser_button.setEnabled(True)
        self.tb_stop_button.setEnabled(False)
        self.job_status_timer.stop()
        self.status = 'failed'
        if hasattr(self, 'pipeline_view'):
            self.pipeline_view.pipeline_widget.set_pipeline(
                pipeline=self.task.pipeline)
            self.pipeline_dock.setVisible(True)
            self.pipeline_dock.raise_()
        self.set_on_job_failed_custom()
        if len(self.launcher.files) > 0:
            self.launcher_dock.setVisible(True)
            self.launcher.set_tree_view()

    def set_on_job_failed_custom(self):
        pass


class CCPEMInfoLabel(QtGui.QLabel):
    def __init__(self,
                 text=''):
        super(CCPEMInfoLabel, self).__init__()
        self.setWordWrap(True)
        self.setFrameStyle(QtGui.QFrame.StyledPanel |
                           QtGui.QFrame.Sunken)
        self.setSizePolicy(QtGui.QSizePolicy.Minimum,
                           QtGui.QSizePolicy.Maximum)
        self.setText(text)


class CCPEMExtensionFrame(QtGui.QVBoxLayout):
    def __init__(self,
                 button_name,
                 button_tooltip,
                 button_dependant=False):
        super(CCPEMExtensionFrame, self).__init__()
        self.button = QtGui.QRadioButton(button_name)
        self.button_dependant = button_dependant
        self.button.clicked.connect(self.button_clicked)
        self.button.setToolTip(button_tooltip)
        self.button.setAutoExclusive(False)
        self.addWidget(self.button)
        self.frame = QtGui.QFrame()
        self.frame.setFrameStyle(
            QtGui.QFrame.StyledPanel | QtGui.QFrame.Sunken)
        self.frame_layout = QtGui.QVBoxLayout()
        self.frame.setLayout(self.frame_layout)
        self.addWidget(self.frame)
        self.button_clicked()
        self.active = True

    def hide(self):
        self.frame.hide()
        self.button.hide()

    def show(self):
        self.frame.show()
        self.button.show()
        self.button.setChecked(True)


    def add_extension_widget(self, widget):
        '''
        Convenience function to add widget to extension frame layout.
        '''
        self.frame_layout.addWidget(widget)
        if self.button_dependant:
            if hasattr(widget, 'set_active'):
                widget.set_active(self.button.isChecked())

    def add_extension_frame(self, layout):
        '''
        Convenience function to add layout to extension frame layout.
        '''
        self.frame_layout.addLayout(layout)

    def button_clicked(self):
        #
        if self.button.isChecked():
            self.frame.show()
            active = True
        else:
            self.frame.hide()
            active = False
        if self.button_dependant:
            # Set children active state
            # N.B. can't use findChildren with layouts so use get widget item
            children = self.frame_layout.count()
            for i in xrange(children):
                child = self.frame_layout.itemAt(i).widget()
                if hasattr(child, 'set_active'):
                    child.set_active(active)

    def disable(self):
        self.button.setDisabled(True)


class CCPEMArgBaseWidget(QtGui.QWidget):
    '''
    Base class for argument widgets.
    File types format = 'Type name (*.foo *.bar)'
    '''
    def __init__(self,
                 parent,
                 arg_name,
                 args,
                 label=None,
                 label_width=100,
                 second_width=100,
                 tooltip_text=None,
                 required=False,
                 multiple_args=False,
                 active=True):
        super(CCPEMArgBaseWidget, self).__init__(parent)
        self.setup_ui = parent
        self.args = args
        self.arg = arg_name
        self.action = getattr(self.args, self.arg)
        self.label = label
        self.second_width = second_width
        self.required = required
        self.warning = False
        self.active = active

        # Set tooltip
        self.tooltip_text = tooltip_text
        if self.tooltip_text is None:
            self.tooltip_text = self.action.help

        # Set multiple_values
        arg = getattr(self.args, self.arg)
        self.multiple_values = False
        if multiple_args:
            if arg.nargs == '*':
                self.multiple_values = True

        if required:
            self.tooltip_text += ' | Required'

        # If tooltip greater than max length insert returns
        max_len = 60
        if len(self.tooltip_text) > max_len:
            wrapped_text = textwrap.wrap(self.tooltip_text, max_len)
            self.tooltip_text = ''
            for line in wrapped_text:
                self.tooltip_text += line + '\n'
            self.tooltip_text = self.tooltip_text[:-1]
        self.setToolTip(self.tooltip_text)

        # Set label
        if self.label is None:
            if self.action.metavar is None:
                self.label = self.arg
            else:
                self.label = self.action.metavar
        if self.multiple_values:
            self.label += '(s)'

        self.value_label = QtGui.QLabel(self.label)
        if label_width is not None:
            self.value_label.setFixedWidth(label_width)

        # Set grid
        self.grid_layout = QtGui.QGridLayout()
        self.grid_layout.addWidget(self.value_label, 0, 0)
        self.grid_layout.setAlignment(QtCore.Qt.AlignLeft)
        self.setLayout(self.grid_layout)
        self.value_line = None

        # Set required shading
        if self.required and self.action.value is None:
            self.set_shading(shade=True)

    def on_edit_finished(self):
        '''
        Set argument value on edit finished.
        '''
        value = self.value_line.text()
        self.set_arg_value(value=value)

    def set_arg_value(self, value):
        '''
        Set argument value.  Apply warning shade to if arg is required and set
        to none or if path does not exist.
        '''
        if value in ['', 'None']:
            self.action.value = None
        else:
            self.action.value = self.action.type(value)
        #
        if hasattr(self.value_line, 'setText'):
            self.value_line.setText(str(value))
        if self.action.value is None:
            if self.required:
                self.set_shading(shade=True)
            else:
                self.set_shading(shade=False)
        else:
            self.set_shading(shade=False)

    def set_shading(self, shade=True):
        '''
        Shade text QLineEdit red to display warning.
        '''
        if shade:
            self.value_label.setStyleSheet('color: red')
            self.warning = True
        else:
            self.value_label.setStyleSheet('color: None')
            self.warning = False

    def open_file_dialog(self):
        '''
        Open file dialog to select input file.
        '''
        job_location = None
        if self.setup_ui is not None:
            if self.setup_ui.task.job_location is not None:
                job_location = self.setup_ui.task.job_location
        dialog_path = get_last_directory_browsed(job_location=job_location)

        # Run file dialog
        if self.multiple_values:
            filenames = QtGui.QFileDialog.getOpenFileNames(
                self,
                'Open a File',
                QtCore.QDir.path(QtCore.QDir(dialog_path)),
                self.file_types)
            filename = []
            for path in filenames:
                filename.append(str(path))
            if len(filename) == 0:
                filename = None
        else:
            filename = QtGui.QFileDialog.getOpenFileName(
                self,
                'Open a File',
                QtCore.QDir.path(QtCore.QDir(dialog_path)),
                self.file_types)
            filename = str(filename)
            if filename == '':
                filename = None

        # Save file location
        if filename is not None:
            if isinstance(filename, list):
                dialog_path = os.path.dirname(filename[0])
            else:
                dialog_path = os.path.dirname(filename)
            set_last_directory_browsed(path=dialog_path)
        return filename

    def disable(self):
        '''
        Disable widget functions.  For use when processes is running.
        '''
        if self.value_line is not None:
            self.value_line.setReadOnly(True)

    def set_active(self, active=True):
        '''
        Set active (i.e. if in extension frame).
        '''
        self.active = active
        self.set_arg_value(value=self.action.value)

    def is_ready(self):
        '''
        Check status, return true if ready, false if not.
        '''
        if self.required and self.active:
            if self.warning:
                return False
            else:
                return True
        else:
            return True


class TitleArgInput(CCPEMArgBaseWidget):
    def __init__(self,
                 parent,
                 arg_name,
                 args,
                 label=None,
                 label_width=150,
                 tooltip_text=None,
                 required=False):
        super(TitleArgInput, self).__init__(
            parent=parent,
            arg_name=arg_name,
            args=args,
            label=label,
            label_width=label_width,
            tooltip_text=tooltip_text,
            required=required)
        # Keep reference to task window
        task_window = self.parent()
        # Set value display
        self.value_line = QtGui.QLineEdit()
        self.grid_layout.addWidget(self.value_line, 0, 1)
        # Set value line
        self.value_line.editingFinished.connect(self.on_edit_finished)
        if hasattr(task_window, 'handle_title_set'):
            self.value_line.editingFinished.connect(
                task_window.handle_title_set)
        self.set_arg_value(value=self.action.value)

    def disable(self):
        self.value_line.setEnabled(False)

class ListArgInput(CCPEMArgBaseWidget):
    '''
    List arg input.  Takes list of values seperated by a space or comma
    and converts to list of string.
    '''
    def __init__(self,
                 parent,
                 arg_name,
                 args,
                 element_type=str,
                 label=None,
                 label_width=150,
                 tooltip_text=None,
                 required=False):
        super(ListArgInput, self).__init__(
            parent=parent,
            arg_name=arg_name,
            args=args,
            label=label,
            label_width=label_width,
            tooltip_text=tooltip_text,
            required=required)
        self.element_type = element_type
        # Set value display
        self.value_line = QtGui.QLineEdit()
        self.grid_layout.addWidget(self.value_line, 0, 1)
        # Set value line
        self.value_line.editingFinished.connect(self.on_edit_finished)
        self.set_arg_value(value=self.action.value)

    def on_edit_finished(self):
        '''
        Set argument value on edit finished.
        '''
        value = self.value_line.text()
        if value == '':
            value = '0'
        self.set_arg_value(value=value)

    def set_arg_value(self, value):
        '''
        Set argument value.  Apply warning shade to if arg is required and set
        to none or if path does not exist.
        '''
        # Set arg value
        if value in ['', 'None']:
            self.action.value = None
        else:
            if type(value) is list:
                self.action.value = self.action.type(value)
            else:
                value_list = []
                values = str(value)
                values = values.replace(',', ' ')
                values = values.split()
                for v in values:
                    try:
                        v = self.element_type(v)
                        value_list.append(v)
                    except:
                        TypeError
                self.action.value = self.action.type(value_list)
        # Set display value
        if hasattr(self.value_line, 'setText'):
            values = ''
            for v in self.action.value:
                try:
                    v = str(v)
                    values += (v + ', ')
                except:
                    TypeError
            self.value_line.setText(values)
        if self.action.value is None:
            if self.required:
                self.set_shading(shade=True)
            else:
                self.set_shading(shade=False)
        else:
            self.set_shading(shade=False)

    def disable(self):
        self.value_line.setEnabled(False)


class FileArgInput(CCPEMArgBaseWidget):
    def __init__(self,
                 parent,
                 arg_name,
                 args,
                 label=None,
                 label_width=150,
                 tooltip_text=None,
                 required=False,
                 file_types=None):
        super(FileArgInput, self).__init__(
            parent=parent,
            arg_name=arg_name,
            args=args,
            label=label,
            label_width=label_width,
            tooltip_text=tooltip_text,
            required=required)
        self.file_types = file_types
        if self.file_types is not None:
            self.file_types += ';;All files (*.*)'
        else:
            self.file_types = 'All files (*.*)'

        # Set input button
        self.select_button = QtGui.QPushButton('Select')
        if self.second_width is not None:
            self.select_button.setFixedWidth(self.second_width)
        self.grid_layout.addWidget(self.select_button, 0, 1)
        # Set value display
        if self.multiple_values:
            self.value_line = QtGui.QListWidget()
        else:
            self.value_line = QtGui.QLineEdit()
            self.value_line.editingFinished.connect(self.on_edit_finished)
        self.grid_layout.addWidget(self.value_line, 0, 2)
        # Connect select button to file dialog
        self.select_button.clicked.connect(
            self.select_button_clicked)
        # Set value line
        self.set_arg_value(value=self.action.value)

    def select_button_clicked(self):
        '''
        Select file using file dialog.
        '''
        filename = self.open_file_dialog()
        if filename is not None:
            self.set_arg_value(value=filename)

    def set_arg_value(self, value):
        '''
        Set argument value.  Apply warning shade to if arg is required and set
        to none or if path does not exist.
        '''
        if self.multiple_values:
            # Clear QlistWidget before adding new values
            self.value_line.clear()
            if isinstance(value, list):
                for path in value:
                    item = QtGui.QListWidgetItem(path)
                    self.value_line.addItem(item)
            else:
                item = QtGui.QListWidgetItem(value)
                self.value_line.addItem(item)

        else:
            if value in ['', None, 'None']:
                self.action.value = None
            else:
                self.action.value = self.action.type(value)
            self.value_line.setText(str(value))
            #
            if self.action.value is not None:
                if not os.path.exists(self.action.value):
                    self.set_shading(shade=True)
                else:
                    self.set_shading(shade=False)
            elif self.active and self.required:
                    self.set_shading(shade=True)
            else:
                self.set_shading(shade=False)

    def disable(self):
        self.value_line.setReadOnly(True)
        self.select_button.setEnabled(False)


class NumberArgInput(CCPEMArgBaseWidget):
    def __init__(self,
                 parent,
                 arg_name,
                 args,
                 decimals=2,
                 step=0.1,
                 minimum=0,
                 maximum=999,
                 label=None,
                 label_width=150,
                 tooltip_text=None,
                 set_none=True,
                 required=False):
        super(NumberArgInput, self).__init__(
            parent=parent,
            arg_name=arg_name,
            args=args,
            label=label,
            label_width=label_width,
            tooltip_text=tooltip_text,
            required=required)
        self.set_none = set_none
        if isinstance(self.action.value, list):
            set_value = self.action.value[0]
        else:
            set_value = self.action.value

        # Set value display
        if self.action.type == float:
            self.value_line = QtGui.QDoubleSpinBox()
            self.value_line.setDecimals(decimals)
            self.value_line.setSingleStep(step)
        else:
            self.value_line = QtGui.QSpinBox()
        if self.set_none:
            if self.action() is None:
                self.value_line.setValue(0)
                self.value_line.setSpecialValueText('None')
        self.value_line.setMaximum(maximum)
        self.value_line.setMinimum(minimum)
        if self.second_width is not None:
            self.value_line.setFixedWidth(self.second_width)
        self.grid_layout.addWidget(self.value_line, 0, 1)

        # Signal value change
        self.value_line.valueChanged.connect(self.on_edit_finished)

        # Set initial value
        self.set_arg_value(value=set_value)

    def set_arg_value(self, value):
        '''
        Set argument value.  Apply warning shade to if arg is required and set
        to none or if path does not exist.
        '''        
        if self.set_none and (
                value is None or
                    value == self.value_line.specialValueText()):
            value = self.value_line.minimum()
        else:
            self.action.value = self.action.type(value)
            
        self.value_line.setValue(self.action.type(value))
        if self.action.value is None:
            if self.required:
                self.set_shading(shade=True)
            else:
                self.set_shading(shade=False)
        else:
            self.set_shading(shade=False)

    def disable(self):
        self.value_line.setReadOnly(True)


class CCPEMQComboBox(QtGui.QComboBox):
    '''
    Sub class QComboBox to stop scroll wheel from changing contents.
    '''
    def __init__(self, parent):
        super(CCPEMQComboBox, self).__init__(parent)

    def wheelEvent(self, *args, **kwargs):
        return None


class CheckArgInput(CCPEMArgBaseWidget):
    '''
    Check argument input (for Bool arguments).
    '''
    def __init__(self,
                 parent,
                 arg_name,
                 args,
                 label=None,
                 label_width=150,
                 second_width=150,
                 tooltip_text=None,
                 required=False,
                 dependants=None):
        super(CheckArgInput, self).__init__(
            parent=parent,
            arg_name=arg_name,
            args=args,
            label=label,
            label_width=label_width,
            second_width=second_width,
            tooltip_text=tooltip_text,
            required=required)
        self.dependants = dependants
        # Set value display
        # Must be Boolean type
        assert self.action.type is types.BooleanType
        if self.action.value is None:
            self.action.value = False
        self.value_line = QtGui.QCheckBox(self)
        self.value_line.setTristate(False)
        self.value_line.setChecked(self.action.value)
        if self.second_width is not None:
            self.value_line.setFixedWidth(self.second_width)
        self.grid_layout.addWidget(self.value_line, 0, 1)
        self.value_line.stateChanged.connect(self.set_arg_value)
        if self.dependants is not None:
            self.value_line.stateChanged.connect(self.set_dependants_active)
            self.set_dependants_active(value=self.value_line.isChecked())

    @QtCore.pyqtSlot(int)
    def set_arg_value(self, value):
        '''
        Set argument value.
        '''
        if value > 0:
            self.action.value = True
        else:
            self.action.value = False

    def disable(self):
        self.value_line.setEnabled(False)

    @QtCore.pyqtSlot(int)
    def set_dependants_active(self, value):
        #
        if value > 0:
            value = True
        else:
            value = False
        for dependant in self.dependants:
            if hasattr(dependant, 'set_active'):
                dependant.set_active(value)
            if hasattr(dependant, 'required'):
                dependant.required = True


class ChoiceArgInput(CCPEMArgBaseWidget):
    '''
    Choice argument input (also for Boolean).
    '''
    def __init__(self,
                 parent,
                 arg_name,
                 args,
                 label=None,
                 label_width=150,
                 second_width=100,
                 tooltip_text=None,
                 required=False):
        super(ChoiceArgInput, self).__init__(
            parent=parent,
            arg_name=arg_name,
            args=args,
            label=label,
            label_width=label_width,
            second_width=second_width,
            tooltip_text=tooltip_text,
            required=required)

        # Set value display
        # Boolean must be set as string for PyQt list
        if self.action.type is types.BooleanType:
            self.choices = ['False', 'True']
            init_value = str(self.action.value)
        else:
            self.choices = self.action.choices
            init_value = self.action.value
        self.value_line = CCPEMQComboBox(self)
        self.value_line.addItems(self.choices)
        if self.second_width is not None:
            self.value_line.setFixedWidth(self.second_width)
        self.grid_layout.addWidget(self.value_line, 0, 1)
        self.value_line.currentIndexChanged.connect(self.set_arg_value)
        if init_value in self.choices:
            self.set_arg_value(value=self.choices.index(init_value))

    def set_arg_value(self, value):
        '''
        Set argument value.  Apply warning shade to if arg is required and set
        to none or if path does not exist.
        '''
        if self.action.type is types.BooleanType:
            if self.choices[value] == 'False':
                self.action.value = False
            else:
                self.action.value = True
        else:
            self.action.value = self.action.type(self.choices[value])
        self.value_line.setCurrentIndex(value)

    def disable(self):
        self.value_line.setEnabled(False)


class KeywordArgInput(FileArgInput):
    def __init__(self,
                 parent,
                 arg_name,
                 args,
                 label=None,
                 label_width=150,
                 tooltip_text=None,
                 required=False):
        self.keyword_edit = QtGui.QPlainTextEdit()
        self.keyword_edit.textChanged.connect(self.text_changed)
        super(KeywordArgInput, self).__init__(
            parent=parent,
            arg_name=arg_name,
            args=args,
            label=label,
            label_width=label_width,
            tooltip_text=tooltip_text,
            required=required)
        self.file_types = ccpem_file_types.text_ext
        #
        self.grid_layout.addWidget(self.keyword_edit, 1, 0, 1, 3)

    def text_changed(self):
        self.action.value = str(self.keyword_edit.toPlainText())

    def set_arg_value(self, value):
        '''
        If file
        '''
        if value is not None:
            if os.path.exists(value):
                f = open(value, 'r')
                self.action.value = f.read()
                f.close()
                self.keyword_edit.clear()
            else:
                self.action.value = value
            self.keyword_edit.insertPlainText(str(self.action.value))

#         if value in ['', None, 'None']:
#             self.action.value = None
#         else:
#             self.action.value = self.action.type(value)
#         self.value_line.setText(str(value))
#         #
#         if self.action.value is not None:
#             if not os.path.exists(self.action.value):
#                 self.set_shading(shade=True)
#             else:
#                 self.set_shading(shade=False)
#         elif self.active and self.required:
#                 self.set_shading(shade=True)
#         else:
#             self.set_shading(shade=False)

    def select_button_clicked(self):
        '''
        Select file using file dialog.
        '''
        filename = self.open_file_dialog()
        if filename is not None:
            filename = str(filename)
            if ccpem_file_types.check_text_file(filename):
                self.value_line.setText(str(filename))
                self.keywords_str = open(filename, 'r').read()
                self.keyword_edit.setPlainText(self.keywords_str)
                self.action.value = str(self.keyword_edit.toPlainText())
            else:
                text = 'Warning: {0} not text file'.format(filename)
                QtGui.QMessageBox.warning(self,
                                          'Error',
                                          text)

    def disable(self):
        '''
        Disable widget functions.  For use when processes is running.
        '''
        self.keyword_edit.setReadOnly(True)
        self.select_button.setEnabled(False)


class SequenceArgInput(FileArgInput):
    def __init__(self,
                 parent,
                 arg_name,
                 args,
                 label=None,
                 label_width=150,
                 tooltip_text=None,
                 required=False):
        self.keyword_edit = QtGui.QPlainTextEdit()
        self.file_button = QtGui.QRadioButton('File input')
        self.text_button = QtGui.QRadioButton('Text input')

        super(SequenceArgInput, self).__init__(
            parent=parent,
            arg_name=arg_name,
            args=args,
            label=label,
            label_width=label_width,
            tooltip_text=tooltip_text,
            required=required)
        self.file_types = ccpem_file_types.all_ext
        
        # Text edit
        self.keyword_edit.textChanged.connect(self.text_changed)
        self.grid_layout.addWidget(self.keyword_edit, 0, 1)
        self.keyword_edit.hide()

        # Text or file selection
        self.file_button.toggled.connect(
            self.set_file_or_text_input)
        self.text_button.toggled.connect(
            self.set_file_or_text_input)
        #
        self.file_button.setChecked(True)
        self.grid_layout.addWidget(self.file_button, 2, 1)
        self.grid_layout.addWidget(self.text_button, 3, 1)

    def set_file_or_text_input(self):
        if self.file_button.isChecked():
            self.keyword_edit.hide()
            self.value_line.show()
            self.select_button.show()
            value = str(self.value_line.text())
        else:
            self.keyword_edit.show()
            self.value_line.hide()
            self.select_button.hide()
            value = str(self.keyword_edit.toPlainText())
        
        self.set_arg_value(value=value)

    def text_changed(self):
        value = str(self.keyword_edit.toPlainText())
        self.set_arg_value(value=value)

    def select_button_clicked(self):
        '''
        Select file using file dialog.
        '''
        filename = self.open_file_dialog()
        if filename is not None:
            filename = str(filename)
            try:
                self.value_line.setText(str(filename))
            except IOError:
                text = 'Warning: {0} not text file'.format(filename)
                QtGui.QMessageBox.warning(self,
                                          'Error',
                                          text)
            self.set_arg_value(value=filename)

    def disable(self):
        '''
        Disable widget functions.  For use when processes is running.
        '''
        self.keyword_edit.setReadOnly(True)
        self.select_button.setEnabled(False)


    def set_arg_value(self, value):
        '''
        Set argument value.  Apply warning shade to if arg is required and set
        to none or if path does not exist.
        '''
        if value in ['', None, 'None']:
            self.action.value = None
        else:
            self.action.value = str(value)
        if self.action.value is not None:
            if self.text_button.isChecked():
                self.set_shading(shade=False)
            else:
                self.value_line.setText(str(value))
                if os.path.exists(self.action.value):
                    self.set_shading(shade=False)
                else:
                    self.set_shading(shade=True)
        else:
            self.set_shading(shade=True)

class MRCMapHeaderInfo(QtGui.QWidget):
    def __init__(self, parent=None, filename=None, width=100):
        super(MRCMapHeaderInfo, self).__init__(parent)
        self.width = width
        self.setToolTip('MRC map header information')
        # Set layout
        self.grid_layout = QtGui.QGridLayout()
        self.setLayout(self.grid_layout)
        self.map_data = None

        # Set col header
        x_label = QtGui.QLabel('x')
        y_label = QtGui.QLabel('y')
        z_label = QtGui.QLabel('z')
        self.grid_layout.addWidget(x_label, 0, 1)
        self.grid_layout.addWidget(y_label, 0, 2)
        self.grid_layout.addWidget(z_label, 0, 3)

        # Set row headers
        origin_label = QtGui.QLabel('Origin')
        origin_label.setToolTip('Map origin (grid points)')
        dim_label = QtGui.QLabel('Dimension')
        dim_label.setToolTip('Dimensions (grid points)')
        self.grid_layout.addWidget(dim_label, 1, 0)
        self.grid_layout.addWidget(origin_label, 2, 0)

        # Set dim values
        for n, name in enumerate(['dim_x', 'dim_y', 'dim_z']):
            value_label = self.set_value_label(value='-', name=name)
            self.grid_layout.addWidget(value_label, 1, n+1)
        # Set origin values
        for n, name in enumerate(['origin_x', 'origin_y', 'origin_z']):
            value_label = self.set_value_label(value='-', name=name)
            self.grid_layout.addWidget(value_label, 2, n+1)

        # Set voxel size
        voxel_label = QtGui.QLabel('Voxel')
        voxel_label.setToolTip('Voxel size (A^3)')
        self.grid_layout.addWidget(voxel_label, 1, 4)
        value_label = self.set_value_label(value='-', name='voxel')
        self.grid_layout.addWidget(value_label, 1, 5)
        # Set alignment of QLabel to right
        for label in self.findChildren(QtGui.QLabel):
            label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        #
        self.set_filename(filename)

    def set_value_label(self, value, name):
        value_label = QtGui.QLineEdit(self)
        value_label.setFixedWidth(self.width)
        value_label.setText(value)
        value_label.setReadOnly(True)
        value_label.setObjectName(name)
        return value_label

    def set_filename(self, filename):
        self.filename = filename
        self.set_header()

    def set_header(self):
        map_params = {'dim_x': '-',
                      'dim_y': '-',
                      'dim_z': '-',
                      'origin_x': '-',
                      'origin_y': '-',
                      'origin_z': '-',
                      'voxel': '-'}
        if self.filename is not None:
            if os.path.exists(path=self.filename):
                self.map_data = read_mrc_header.MRCMapHeaderData(self.filename)
                # checks both the extension and the header of the target mapfile
                if self.map_data.is_map_file:
                    # XXX 
                    map_params = {'dim_x': self.map_data.mx_my_mz[0],
                                  'dim_y': self.map_data.mx_my_mz[1],
                                  'dim_z': self.map_data.mx_my_mz[2],
                                  'origin_x': int(self.map_data.origin[0]),
                                  'origin_y': int(self.map_data.origin[1]),
                                  'origin_z': int(self.map_data.origin[2]),
                                  'voxel': round(self.map_data.voxx_voxy_voxz[2], 2)}

        for key, value in map_params.iteritems():
            value_label = self.findChild(
                QtGui.QLineEdit, key)
            value_label.setText(str(value))


def get_last_directory_browsed(job_location=None):
    '''
    Use Qt settings to get previously browsed location
    '''
    q_settings = QtCore.QSettings()
    # Get previous path
    path = q_settings.value('SETUP_DIR_KEY').toString()
    if not os.path.exists(path):
        path = os.getcwd()
        if job_location is not None:
            if not os.path.exists(job_location):
                path = job_location
    return path


def set_last_directory_browsed(path):
    '''
    Use Qt settings to save path as last browsed location
    '''
    q_settings = QtCore.QSettings()
    q_settings.setValue('SETUP_DIR_KEY',
                        path)


def relaunch_task_window(task_class,
                         window_class,
                         task_file,
                         geometry=None,
                         main_window=None):
    pipeline = process_manager.CCPEMPipeline(
        pipeline=None,
        import_json=task_file)
    database_path = None
    if hasattr(main_window, 'database_path'):
        database_path = main_window.database_path
    task = task_class(
        parent=main_window,
        pipeline=pipeline,
        database_path=database_path)
    window = window_class(parent=main_window,
                          task=task)
    if geometry is not None:
        window.setGeometry(geometry)
    window.show()


def get_ccpem_bin_location(attribute):
    args = settings.get_ccpem_settings()
    if getattr(args, attribute, None) is not None:
        return getattr(args, attribute, None).value
    else:
        return None
    return None


def get_linux_terminal_command():
    command = settings.which('gnome-terminal')
    if command is None:
        command = settings.which('konsole')
    if command is None:
        command = settings.which('xterm')
    return command


def run_terminal(working_directory=None):
    if sys.platform == 'linux2':
        command = get_linux_terminal_command()
        find_wd = False
        if working_directory is not None:
            if os.path.exists(working_directory):
                find_wd = True
        if command is not None:
            args = []
            if find_wd:
                if 'gnome-terminal' in command:
                    arg = '--working-directory={0}'.format(working_directory)
                    args = [arg]
                elif 'konsole' in command:
                    args = '--workdir {0}'.format(working_directory)
                elif 'xterm' in command:
                    # Can't reliably pass working directory to xterm
                    pass
            run_detatched_process(command=command,
                                  args=args)
    elif sys.platform == 'darwin':
        if working_directory is None:
                working_directory = ''
        osascript = '''osascript -e 'tell application "Terminal" \
                       to do script "cd {0}"' '''.format(
            working_directory)
        os.system(osascript)
    else:
        print 'Window terminal launch not supported yet...'

def run_coot(args=[]):
    '''
    Run coot with optional command line args e.g. :
    coot --pdb=foo.pdb --map=foo.mrc
    '''
    coot_command = settings.which('coot')
    if coot_command is None:
        text = 'Warning: Coot not found'
        ccpem_utils.print_error(message=text)
    else:
        run_detatched_process(command=coot_command,
                              args=args)


def run_ccp4mg(args=[]):
    '''
    Run ccp4mg with optional command line args e.g. :
    ccp4mg -pdb foo.pdb -map foo.map
    N.B. ccp4mg doesn't currently automatically recognise .mrc extension.
    '''
    ccp4mg_command = settings.which('ccp4mg')
    if ccp4mg_command is None:
        text = 'Warning: CCP4MG not found'
        ccpem_utils.print_error(message=text)
    else:
        run_detatched_process(command=ccp4mg_command,
                              args=args)


def run_chimera(args=[]):
    '''
    Run chimera with optional command line args e.g. :
    chimera foo.pdb foo.map
    '''
    chimera_command = settings.which('chimera')
    if chimera_command is None:
        text = 'Warning: Chimera not found'
        ccpem_utils.print_error(message=text)
    else:
        run_detatched_process(command=chimera_command,
                              args=args)


def run_pymol(args=[]):
    '''
    Run PyMOL with optional command line args e.g. :
    pymol foo.pdb foo.ccp4
    N.B. ccp4mg doesn't currently automatically recognise .mrc or .map
    extensions.
    '''
    pymol_command = settings.which('pymol')
    if pymol_command is None:
        text = 'Warning: PyMOL not found'
        ccpem_utils.print_error(message=text)
    else:
        run_detatched_process(command=pymol_command,
                              args=[])


def run_relion():
    '''
    Run relion GUI.
    '''
    relion_command = get_ccpem_bin_location(attribute='relion_bin')
    # Else try and find command
    if relion_command is None:
        relion_command = settings.which('relion')
    if relion_command is None:
        text = 'Warning: RELION not found'
        ccpem_utils.print_error(message=text)
    else:
        run_detatched_process(command=relion_command,
                              args=[])


def run_detatched_process(command, args=[], verbose=True):
    assert command is not None
    assert type(args) is type([])
    if verbose:
        command_str = ccpem_utils.print_header(message='CCP-EM toolbar launch',
                                               return_str=True)
        command_str += ccpem_utils.print_sub_sub_header(message='Command',
                                                        return_str=True)
        command_str += command
        for arg in args:
            command_str += ' '
            command_str += arg
        command_str += '\n'
        command_str += ccpem_utils.return_footer()
    process = QtCore.QProcess()
    process.startDetached(command, args)


def main():
    '''
    For testing.
    '''
    pass

if __name__ == '__main__':
    main()
