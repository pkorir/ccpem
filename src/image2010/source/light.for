*************LIGHT.FOR************************************************
*
*	ADDS ILLUMINATION CUEING TO A SURFACE IMAGE.
*	HAS FURTHER PROVISION FOR THE USE OF HIGHLIGHTS.
*
*	INPUTS:THETA,PHI,DEPTH,FDIF,NCOS,FSPEC
*
*		THETA,PHI are illumination angles. If THETA,PHI = 0,0
*		illumination is from viewing position. Theta positive
*		gives illumination from th right, Phi positive gives
*		illumination from above.
*		DEPTH gives ratio of lighting of back/front. If DEPTH = 0
*		Back is dark.
*		FDIF is fraction of reflected light that is diffuse.
*		NCOS is cosine power for specular reflection. Generally
*		NCOS = 6. A higher power makes the surface more highly
*		polished.
*		FSPEC is fraction of light in specular reflection. Any
*		other light is only depth queued.
*
*	Version 1.0 Written by GV, 28/5/1986.
*       Version 1.1 RAC Version for AL 7Jan93
*	Version 1.2 JMS Increased dimension
************************************************************************
*
	PARAMETER (MAP_S=2048)
c	PARAMETER (MAP_S=1024)
	COMMON //NX,NY,NZ
	LOGICAL FLAG
C
C*** jms 17.06.2010
	DIMENSION NXYZ(3),MXYZ(3),NXYZST(3),CELL(6),BRRAY(MAP_S,MAP_S),
     *         ARRAY(MAP_S,MAP_S),LABELS(20,20),TITLE(20)
CTSH++
	CHARACTER TMPTITLE*80
	EQUIVALENCE (TMPTITLE,TITLE)
	REAL*4 PI/3.141592654/

CTSH--
C
	EQUIVALENCE (NX,NXYZ)
C
	DATA NXYZST/0,0,0/
C*** initialization put in by jms 07.03.96
	do j=1,map_S
	 do i=1,map_s
	  brray(i,j) = 0.
	 end do
	end do
C
C	Open image files.
C
	CALL IMOPEN(1,'IN','RO')
	CALL IMOPEN(2,'OUT','NEW')
	CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
	CALL ITRHDR(2,1)
	CALL IALMOD(2,0)
C
	IF ((NX.GT.MAP_S).OR.(NY.GT.MAP_S)) THEN
	WRITE(6,91)
91	FORMAT(' INPUT IMAGE TOO LARGE. PROGRAM STOPS.')
	STOP
	ENDIF
C
	READ(5,*)THETA,PHI,DEPTH,FDIF,NCOS,FSPEC
	WRITE(6,295)THETA,PHI,DEPTH,FDIF,NCOS,FSPEC
C*** jms 17.06.2010
295	FORMAT(' Illumination angles Theta, Phi =',2F7.2,/,
     *         ' Depth cueing (back/front) =',F7.2,/,
     *         ' Fraction of diffuse illumination=',F7.4,/,
     *         ' Power of cosine for highlight =',I3,/,
     *         ' Fraction of specular illumination =',F7.4)
C
CTSH	ENCODE(80,301,TITLE)THETA,PHI,DEPTH,FDIF,NCOS,FSPEC
CTSH++
	WRITE(TMPTITLE,301)THETA,PHI,DEPTH,FDIF,NCOS,FSPEC
CTSH--
301	FORMAT('HILIGHT:',2F5.1,2F5.2,I3,F5.2)
C
C	Read in image.
C
	CALL IRDPAS(1,ARRAY,MAP_S,MAP_S,0,NX-1,0,NY-1,*99)
C
C	Set up normal of illumination as direction cosines.
C
CTSH	XIL=COSD(THETA)*COSD(PHI)	
CTSH	YIL=SIND(THETA)*COSD(PHI)	
CTSH	ZIL=SIND(PHI)	
CTSH++
	XIL=COS(THETA*PI/180.)*COS(PHI*PI/180.)
	YIL=SIN(THETA*PI/180.)*COS(PHI*PI/180.)
	ZIL=SIN(PHI*PI/180.)
CTSH--
C
	GLO=1.-FSPEC-FDIF
	GLO=MAX(GLO,0.)
C
C	Compute normals to the surface at each point, and take cosine
C	with illumination.
C
	DO 200 K=2,NY-1
	DO 200 J=2,NX-1
C
C	Find slopes of X in Y and Z
C
	X1=ARRAY(J,K)
C
C	Check if point is on a surface.
C
	IF (X1.LT.0.) THEN
	X1=-X1
	BRI=1.2
	ENDIF
C
	IF (X1.LT.2) GOTO 200
C
	X2=(ABS(ARRAY(J+1,K))-ABS(ARRAY(J-1,K)))*0.5
	X3=(ABS(ARRAY(J,K+1))-ABS(ARRAY(J,K-1)))*0.5
C
C	Deal with too rapid changes.
C
	IF (ABS(X2).LT.2) GOTO 201
C
	X2=ABS(ARRAY(J+1,K))-X1
C
201	CONTINUE
C	
	IF (ABS(X3).LT.2) GOTO 202
C
	X3=ABS(ARRAY(J,K+1))-X1
C
202	CONTINUE
C
C	Define normal.
C
	PN1=1.
	PN2=-X2
	PN3=-X3
C
C	Normalise to 1.
C
	PSSQ=PN1*PN1+PN2*PN2+PN3*PN3
	PSSQ=SQRT(PSSQ)
C
	PN1=PN1/PSSQ
	PN2=PN2/PSSQ
	PN3=PN3/PSSQ
C
C	Take cosine with illumination.
C
	C=PN1*XIL+PN2*YIL+PN3*ZIL
C
	C=MAX(C,0.)
C
C	Find cosine of angle between reflected light and direction of view.
C
	D=2.*PN1*C-XIL
C
	D=MAX(D,0.001)
	D=MIN(D,1.)
C
C	Raise to the power of NCOS
C
	D=LOG(D)
	D=D*FLOAT(NCOS)
	D=EXP(D)
C
C	Shade.
C
	DEN=C*FDIF+D*FSPEC+GLO
C
C	Depth queue.
C
	DEN=DEN*(DEPTH+(X1*(1.-DEPTH)/DMAX))
C
	BRRAY(J,K)=DEN
C
200	CONTINUE
C
C	Find out here just how much of the image array actually contains
C	picture
C
C	NX1 = MAP$
C	NX2 = 1
C	NY1 = MAP$
C	NY2 = 1
C	DO J = 1 , MAP$
C	  FLAG = .FALSE.
C	  DO I = 1 , MAP$
C	    IF(BRRAY(I,J).GT.0.0) THEN
C	      IF(I.LT.NX1) NX1 = I
C	      IF(I.GT.NX2) NX2 = I
C	      FLAG = .TRUE.
C	    END IF
C	  END DO
C	  IF(FLAG) THEN
C	    IF(J.LT.NY1) NY1 = J
C	    IF(J.GT.NY2) NY2 = J
C	  END IF
C	END DO
C	NX = NX2-NX1+1
C	NY = NY2-NY1+1
C
C	NNY = NY/2				!
C	NNY = NNY*2				!
C	IF(NNY.NE.NY) THEN			!
C	  NY = NY + 1				!
C	  IF(NY2.LT.MAP$) THEN			!This code needed only because
C	    NY2 = NY2 + 1			! of the idiosyncrosies
C	  ELSE IF(NY1.GT.1) THEN		! of AEDDSP which does funny
C	    NY1 = NY1 - 1			! things if NY is odd.
C	  ELSE					!
C	    NY2 = NY2 - 1			!
C	    NY = NY - 1				!
C	  END IF				!
C	END IF					!
C	
C
C	CALL IALSIZ(2,NXYZ,NXYZST)
C	DO I = 1 , 3
C	  MXYZ(I) = NXYZ(I)
C	END DO
C	CALL IALSAM(2,MXYZ)
C
	NX1=1
	NX2=NX
	NY1=1
	NY2=NY
C
	CALL ICLDEN(BRRAY,MAP_S,MAP_S,NX1,NX2,NY1,NY2,DMIN,DMAX,DMEAN)
C
C	Rescale so that output is 0 - 255.
C
	SCALE=255./(DMAX-DMIN)
C
	DO 400 J=1,MAP_S
	DO 400 I=1,MAP_S
C
	BRRAY(I,J)=(BRRAY(I,J)-DMIN)*SCALE
C
400	CONTINUE
C
	CALL ICLDEN(BRRAY,MAP_S,MAP_S,NX1,NX2,NY1,NY2,DMIN,DMAX,DMEAN)
C
	NX1 = NX1 -1
	NX2 = NX2 -1
	NY1 = NY1 -1
	NY2 = NY2 -1
C	
C	Write out the image.
C
C$	CALL IWRPAS(2,BRRAY,MAP$,MAP$,0,NX-1,0,NY-1)
C$	CALL ICLDEN(BRRAY,MAP$,MAP$,1,NX,1,NY,DMIN,DMAX,DMEAN)
	CALL IWRPAS(2,BRRAY,MAP_S,MAP_S,NX1,NX2,NY1,NY2)
C
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
	CALL IMCLOSE(2)
	CALL IMCLOSE(1)
C
	WRITE(6,93)
93	FORMAT(' PROGRAM EXECUTED TO END.')
	STOP
99	WRITE(6,92)
92	FORMAT(' END OF IMAGE WHILE READING')
	STOP
	END
