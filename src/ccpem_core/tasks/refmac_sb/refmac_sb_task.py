#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core.settings import which
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.refmac import refmac_task
from ccpem_core.map_tools import mean_amplitudes


class RefmacSB(task_utils.CCPEMTask):
    '''
    CCPEM Refmac map blur / sharpen utility.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='MRCtoMTZ',
        author='Brown A, Long F, Nicholls RA, Long F, Toots J, Murshudov G',
        version='5.8.0103',
        description=(
            'Convert MRC map to MTZ structure factors using Refmac.  '
            'Includes map blur / sharpen utility.  '
            'Macromolecular refinement program.  REFMAC is a program '
            'designed for REFinementof MACromolecular structures.  It uses '
            'maximum likelihood and some elements of Bayesian '
            'statistics.  N.B. requires CCP4.'),
        short_description=(
            'Convert MRC map to MTZ structure factors using Refmac.  '
            'Includes map blur / sharpen utility.'),
        references=None)

    def __init__(self,
                 task_info=task_info,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 verbose=False,
                 parent=None):
        command = which(program='refmac5')
        #
        super(RefmacSB, self).__init__(
            command=command,
            task_info=task_info,
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            verbose=verbose,
            parent=parent)
        self.atomsf = os.path.join(os.environ['CLIBD'],
                                   'atomsf_electron.lib')
        #
        assert os.path.exists(self.atomsf)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-mapin',
            '--input_map',
            help='''Target input map (mrc format)''',
            type=str,
            metavar='Input map',
            default=None)
        #
        parser.add_argument(
            '-resolution',
            '--resolution',
            help='''Resolution of input map (Angstrom)''',
            metavar='Resolution',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-mapref',
            '--reference_map',
            help='Reference map for sharpening (mrc format)',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-start_pdb',
            '--start_pdb',
            help='Include docked model if available (pdb format)',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-blur_array',
            '--blur_array',
            help='List of B factors to blur map',
            metavar='Blur',
            type=list,
            default=[50, 100, 150, 200])
        #
        parser.add_argument(
            '-sharp_array',
            '--sharp_array',
            help='List of B factors to sharpen map',
            metavar='Sharp',
            type=list,
            default=[50, 100, 150, 200])
        #
        return parser

    def run_pipeline(self, run=True, job_id=None, db_inject=None):
        '''
        Generate job classes and process.  Run=false for reloading.
        '''
        self.log_path = os.path.join(self.job_location,
                                     'stdout.txt')
        self.bin_results_path = os.path.join(
            self.job_location,
            'mean_sf.csv')

        # Convert map to mtz and blur/sharpen

        # On finish process data for plot
        hklout_path = os.path.join(self.job_location,
                                   'starting_map.mtz')
        #
        on_finish_custom = OnFinish(
            mtz_in_path=hklout_path,
            blur_array=self.args.blur_array.value,
            sharp_array=self.args.sharp_array.value,
            results_path=self.bin_results_path)
        #
        self.process_maptomtz = refmac_task.RefmacMapToMtz(
            command=self.command,
            name='Map to MTZ',
            resolution=self.args.resolution.value,
            mode='Global',
            job_location=self.job_location,
            map_path=self.args.input_map.value,
            blur_array=self.args.blur_array.value,
            sharp_array=self.args.sharp_array.value,
            atomsf_path=self.atomsf,
            on_finish_custom=on_finish_custom)

        # Set pipeline
        pl = [[self.process_maptomtz.process]]

        if run:
            self.pipeline = process_manager.CCPEMPipeline(
                pipeline=pl,
                job_id=job_id,
                args_path=self.args.jsonfile,
                location=self.job_location,
                database_path=self.database_path,
                db_inject=db_inject,
                taskname=self.task_info.name,
                title=self.args.job_title.value,
                verbose=self.verbose)
            self.pipeline.start()

    def validate_args(self):
        # Now do at gui level
        return True


class OnFinish(process_manager.CCPEMProcessCustomFinish):
    def __init__(self,
                 mtz_in_path,
                 blur_array,
                 sharp_array,
                 results_path):
        super(OnFinish, self).__init__()
        self.results_path = results_path
        self.blur_array = blur_array
        self.sharp_array = sharp_array
        self.mtz_in_path = mtz_in_path

    def on_finish(self, parent_process=None):
        mean_amplitudes.get_mean_amplitudes(
             mtz_in_path=self.mtz_in_path,
             blur_array=self.blur_array,
             sharp_array=self.sharp_array,
             results_path=self.results_path)

def main():
    '''
    Run task
    '''
    task_utils.command_line_task_launch(
        task=RefmacSB)

if __name__ == '__main__':
    main()
