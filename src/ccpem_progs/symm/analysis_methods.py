# coding: utf-8
import scipy.signal as signal
import numpy as np
import argparse
from numpy import float64
import sys

# the following import is necessary if the currently deprecated non-uniform FT should be reimplemented, see the
# commented-out code below
# import nufft


def spectral_analyze_data(data, missing_wedge=None, frequencies=None, precenter=False, normalize=False):

    # Calculate nyquist
    nyquist = data.shape[0]/2

    # Setting frequencies for the periodogram
    if frequencies:
        f = np.arange(1, frequencies, 1)
    else:
        f = np.arange(1, nyquist, 1)

    # Output array
    dim = [len(f), data.shape[1], data.shape[2]]
    output = np.zeros(dim)

    if missing_wedge is None:
        missing_wedge = np.ones(data.shape)

    # Compute Lomb-Scargle periodogram for each height and radius
    for y in range(data.shape[1]):

        for x in range(data.shape[2]):

            a = data[:, y, x]  # all e densities in a given radius and slice

            # Defining variable for the periodogram
            angles = np.linspace(0, 2*np.pi, len(a), dtype=float64)
            # angles = angular coordinates of the values in a

            # Remove voxels within the missing wedge
            mask = []
            for i in range(missing_wedge.shape[0]):  # iterates over indices of angles
                if missing_wedge[i, y, x] == 0:
                    mask.append(i)
                    # ie mask contains the positions along the radius
                    # that need to be removed as list

            a = np.delete(a, mask)       # a are the values without masked ones
            b = np.delete(angles, mask)  # b are the angles without masked ones

            a = float64(a)

            f = float64(f)  # frequency list

            # nyquist frequency is either the half the number of sampled pixels or half the circumference of the
            # sampling circle
            nyq = int(min((x + 1) * np.pi, max(f)))

            # Compute Lomb-Scargle periodogram
            if precenter:
                output[0:nyq, y, x] = signal.lombscargle(b, a - np.mean(a), f[0:nyq])
            else:
                output[0:nyq, y, x] = (signal.lombscargle(b, a, f[0:nyq]))

            if normalize:
                output[:, y, x] *= 2 / np.dot(a, a)

    return output


def fft_data(data, missing_wedge=None):

    # Calculates Fourier transform
    coefs = np.fft.rfft(data, axis=0)
    coefs = coefs[1:, :, :]
    return np.absolute(coefs)

# simple version of NUFFT, fortran based, rather old core algorithm, recent python wrap
# https://github.com/dfm/Python-nufft/
# python code is MIT licensed, Fortran code it is based on is BSD licensed
# def nufft_analysis(masked_extended_circumference):
#
#     mec = masked_extended_circumference  # masked with nans
#
#     # setting the the nodes (x), values (y) and frequencies
#     x = np.linspace(0, 2*np.pi, len(mec), dtype=np.float64)[np.logical_not(np.isnan(mec))]
#     y = mec[np.logical_not(np.isnan(mec))]
#     f = np.arange(1, len(mec)/2 + 1)
#
#     # computing the nufft
#     # coeffs_x = nufft.nufft1(x, y, len(mec)/2)
#     coeffs_x = nufft.nufft3(x, y, f)
#     out_vals_x = np.absolute(coeffs_x)
#
#     return out_vals_x
#
#
# # this function is called from the main programme
# def non_uni_FFT(data, mask):
#
#     # the repeated masking won't be necessary in most cases, but makes future proof in case
#     # the upstream masking is ever written differently
#     masked = np.where(mask == 0, np.nan, data)
#
#     output_matrix = np.zeros((int(data.shape[0]/2), data.shape[1], data.shape[2]))
#
#     # calculating the nfft, populating the output array
#     for i in range(data.shape[2]):
#         for j in range(data.shape[1]):
#             nfft = nufft_analysis(masked[:, j, i])
#             output_matrix[:, j, i] = nfft
#
#     return output_matrix

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("data", nargs='+')
    parser.add_argument("missing_wedge", nargs='+')
    parser.add_argument("--frequencies", type=int, default=None)  # #nargs='+', default=None)
    args = parser.parse_args()
    spectral_analyze_data(args.data, args.missing_wedge)
