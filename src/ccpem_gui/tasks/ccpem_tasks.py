#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.

import os
import inspect

class CCPEMTask(object):
    '''
    CCPEM Task.
    '''
    def __init__(self,
                 name,
                 task,
                 window,
                 test_data=None):
        self.name = name
        self.task = task
        self.window = window
        self.path = inspect.getfile(self.task)
        self.test_data = os.path.dirname(inspect.getfile(test_data))

class CCPEMTasks(object):
    '''
    Class of Tasks.  Relates program name to task function.
    '''
    def __init__(self,
                 verbose=False):
        '''
        Set up task dictionary
        '''
        self.errors = {}

        # Buccaneer
        self.buccaneer = None
        try:
            from ccpem_core.tasks.buccaneer import buccaneer_task
            from ccpem_gui.tasks.buccaneer import buccaneer_window
            from ccpem_core.test_data.tasks import buccaneer
            self.buccaneer = CCPEMTask(
                name=buccaneer_task.Buccaneer.task_info.name,
                task=buccaneer_task.Buccaneer,
                window=buccaneer_window.BuccaneerWindow,
                test_data=buccaneer)
        except Exception as e:
            self.errors['Buccaneer'] = e
        # Choyce
        self.choyce = None
        try:
            import modeller
            from ccpem_core.tasks.choyce import choyce_task
            from ccpem_gui.tasks.choyce import choyce_window
            from ccpem_core.test_data.tasks import choyce
            self.choyce = CCPEMTask(
                name=choyce_task.Choyce.task_info.name,
                task=choyce_task.Choyce,
                test_data=choyce,
                window=choyce_window.ChoyceWindow)
        except Exception as e:
            self.errors['Choyce'] = e

        # Dock-EM
        self.dock_em = None
        try:
            from ccpem_core.tasks.dock_em import dock_em_task
            from ccpem_gui.tasks.dock_em import dock_em_window
            from ccpem_core.test_data.tasks import dock_em
            self.dock_em = CCPEMTask(
                name=dock_em_task.DockEM.task_info.name,
                task=dock_em_task.DockEM,
                test_data=dock_em,
                window=dock_em_window.DockEMMainWindow)
        except Exception as e:
            self.errors['Dock EM'] = e 

        # Flex-EM
        self.flex_em = None
        try:
            import modeller
            from ccpem_core.tasks.flex_em import flexem_task
            from ccpem_gui.tasks.flex_em import flexem_window
            from ccpem_core.test_data.tasks import flex_em
            self.flex_em = CCPEMTask(
                name=flexem_task.FlexEM.task_info.name,
                task=flexem_task.FlexEM,
                test_data=flex_em,
                window=flexem_window.FlexEMWindow)
        except Exception as e:
            self.errors['Flex-EM'] = e

        # Molrep
        self.molrep = None
        try:
            from ccpem_core.tasks.molrep import molrep_task
            from ccpem_gui.tasks.molrep import molrep_window
            from ccpem_core.test_data.tasks import molrep
            self.molrep = CCPEMTask(
                name=molrep_task.MolRep.task_info.name,
                task=molrep_task.MolRep,
                test_data=molrep,
                window=molrep_window.MolrepWindow)
        except Exception as e:
            self.errors['Molrep'] = e

        # MRC to MTZ
        self.mrc_to_mtz = None
        try:
            from ccpem_core.tasks.refmac_sb import refmac_sb_task
            from ccpem_gui.tasks.refmac_sb import refmac_sb_window
            from ccpem_core.test_data.tasks import refmac_sb
            self.mrc_to_mtz = CCPEMTask(
                name=refmac_sb_task.RefmacSB.task_info.name,
                task=refmac_sb_task.RefmacSB,
                test_data=refmac_sb,
                window=refmac_sb_window.RefmacSBWindow)
        except Exception as e:
            self.errors['MRC to MTZ'] = e

        # MRC to tif
        self.mrc2tif = None
        try:
            from ccpem_core.tasks.mrc_mrc2tif import mrc_mrc2tif_task
            from ccpem_gui.tasks.mrc_mrc2tif import mrc_mrc2tif_window
            from ccpem_core.test_data.tasks import mrc_mrc2tif
            self.mrc2tif = CCPEMTask(
                name=mrc_mrc2tif_task.Mrc2Tif.task_info.name,
                task=mrc_mrc2tif_task.Mrc2Tif,
                test_data=mrc_mrc2tif,
                window=mrc_mrc2tif_window.Mrc2TifWindow)
        except Exception as e:
            self.errors['MRC2TIF'] = e

        # MRC Allspacea
        self.mrcallspacea = None
        try:
            from ccpem_core.tasks.mrc_allspacea import mrc_allspacea_task
            from ccpem_gui.tasks.mrc_allspacea import mrc_allspacea_window
            from ccpem_core.test_data.tasks import mrc_allspacea
            self.mrcallspacea = CCPEMTask(
                name=mrc_allspacea_task.MrcAllspacea.task_info.name,
                task=mrc_allspacea_task.MrcAllspacea,
                test_data=mrc_allspacea,
                window=mrc_allspacea_window.MrcAllspaceaWindow)
        except Exception as e:
            self.errors['MRCAllspacea '] = e

        # Nautilus
        self.nautilus = None
        try:
            from ccpem_core.tasks.nautilus import nautilus_task
            from ccpem_gui.tasks.nautilus import nautilus_window
            from ccpem_core.test_data.tasks import nautilus
            self.nautilus = CCPEMTask(
                name=nautilus_task.Nautilus.task_info.name,
                task=nautilus_task.Nautilus,
                test_data=nautilus,
                window=nautilus_window.NautilusWindow)
        except Exception as e:
            self.errors['Nautilus'] = e

        # ProSMART
        self.prosmart = None
        try:
            from ccpem_core.tasks.prosmart import prosmart_task
            from ccpem_gui.tasks.prosmart import prosmart_window
            from ccpem_core.test_data.tasks import prosmart
            self.prosmart = CCPEMTask(
                name=prosmart_task.ProSMART.task_info.name,
                task=prosmart_task.ProSMART,
                test_data=prosmart,
                window=prosmart_window.ProSMARTWindow)
        except Exception as e:
            self.errors['Prosmart'] = e

        # Refmac
        self.refmac = None
        try:
            from ccpem_core.tasks.refmac import refmac_task
            from ccpem_gui.tasks.refmac import refmac_window
            from ccpem_core.test_data.tasks import refmac
            self.refmac = CCPEMTask(
                name=refmac_task.Refmac.task_info.name,
                task=refmac_task.Refmac,
                test_data=refmac,
                window=refmac_window.Refmac5Window)
        except Exception as e:
            self.errors['Refmac'] = e

        # Ribfind
        self.ribfind = None
        try:
            from ccpem_core.tasks.ribfind import ribfind_task
            from ccpem_gui.tasks.ribfind import ribfind_window
            from ccpem_core.test_data.tasks import ribfind
            self.ribfind = CCPEMTask(
                name=ribfind_task.Ribfind.task_info.name,
                task=ribfind_task.Ribfind,
                test_data=ribfind,
                window=ribfind_window.RibfindWindow)
        except Exception as e:
            self.errors['Ribfind'] = e

        # Shake
        self.shake = None
        try:
            from ccpem_core.tasks.shake import shake_task
            from ccpem_gui.tasks.shake import shake_window
            from ccpem_core.test_data.tasks import shake
            self.shake = CCPEMTask(
                name=shake_task.Shake.task_info.name,
                task=shake_task.Shake,
                test_data=shake,
                window=shake_window.ShakeWindow)
        except Exception as e:
            self.errors['Shake'] = e

        # Tempy Diff Map
        self.tempy_diff_map = None
        try:
            from ccpem_core.tasks.tempy.difference_map import difference_map_task
            from ccpem_gui.tasks.tempy.difference_map import difference_map_window
            from ccpem_core.test_data.tasks.tempy import difference_map
            self.tempy_diff_map = CCPEMTask(
                name=difference_map_task.DifferenceMap.task_info.name,
                task=difference_map_task.DifferenceMap,
                test_data=difference_map,
                window=difference_map_window.DifferenceMapWindow)
        except Exception as e:
            self.errors['TEMPy Diff Map'] = e

        # Tempy SMOC
        self.tempy_smoc = None
        try:
            from ccpem_core.tasks.tempy.smoc import smoc_task
            from ccpem_gui.tasks.tempy.smoc import smoc_window
            from ccpem_core.test_data.tasks.tempy import smoc
            self.tempy_smoc = CCPEMTask(
                name=smoc_task.SMOC.task_info.name,
                task=smoc_task.SMOC,
                test_data=smoc,
                window=smoc_window.SMOCMapWindow)
        except Exception as e:
            self.errors['TEMPy SMOC'] = e

        # Tempy SCCC
        self.tempy_sccc = None
        try:
            from ccpem_core.tasks.tempy.sccc import sccc_task
            from ccpem_gui.tasks.tempy.sccc import sccc_window
            from ccpem_core.test_data.tasks.tempy import sccc
            self.tempy_sccc = CCPEMTask(
                name=sccc_task.SCCC.task_info.name,
                task=sccc_task.SCCC,
                test_data=sccc,
                window=sccc_window.SCCCMapWindow)
        except Exception as e:
            self.errors['TEMPy SCCC'] = e

        if verbose:
            if len(self.errors) > 0:
                print self.errors

    def get_task_class(self, program):
        '''
        Return task class from program name.
        '''
        for task in self.__dict__.values():
            if isinstance(task, CCPEMTask):
                if task.name == program:
                    return task.task
        return None

    def get_window_class(self, program):
        '''
        Return window class from program name.
        '''
        for task in self.__dict__.values():
            if isinstance(task, CCPEMTask):
                if task.name == program:
                    return task.window
        return None

def main():
    '''
    For testing.
    '''
    tasks = CCPEMTasks()
    print tasks.errors
    print tasks.refmac.task

if __name__ == '__main__':
    main()
