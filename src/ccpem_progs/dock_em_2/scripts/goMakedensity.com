#!/bin/csh -x
# $1 pdb filre root
# $2 map to take size from
# $3 sampling of map
# $4 lp resolution to filter map to. high res limit
# $5 hp resolution to filter map to. low res limit

../Makedensity2 << eot
$1.pdb
$2.mrc
$1.mrc
$3
0.
eot

../Mapfilter2 << eot
$1.mrc
$3
$4,$5
$1_ff$4.mrc
eot

../MaskConvolute << eot
$1.mrc
$3
10.
$1_maskcnv10.mrc
eot
