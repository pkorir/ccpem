#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
from clipper_tools.em import structure_factors
from ccpem_core import test_data

class Test(unittest.TestCase):
    '''
    Unit test for mmdb_coord_tools.
    '''
    def setUp(self):
        self.test_data = test_data.get_test_data_path()
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)

    def test_clipper_tools(self):
        mtz_in_path = os.path.join(self.test_data,
                                'mtz/starting_map.mtz')
        assert os.path.exists(mtz_in_path)
        mtz = structure_factors.ClipperMTZ(mtz_in_path=mtz_in_path)
        mtz.import_column_data(column_label='[Fout0, Pout0]',
                               get_resolution=True)
        assert mtz.cell.a() == 75.0
        reso = mtz.column_data['resolution_1/Ang^2']
        assert reso.min() == 0.0
        self.assertAlmostEqual(reso.mean(),
                               0.0296,
                               places=4)

if __name__ == '__main__':
    unittest.main()