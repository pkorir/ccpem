# from read_symmetries import which_symmetries
from helper_functions.nptomo import NumpyTom, MRCfile
import numpy as np

from math import pi, cos, sin
try:
    from scipy.ndimage import map_coordinates
    from scipy.signal import convolve2d
    # use fftconvolve instead of convolve2d to make it faster
except ImportError:
    print "\nScipy could not be imported.\nA slower function will be used instead.\n"
    from helper_functions.helper import convolve2d_hack
from normalise_nan import nan_norm


# simplifies handling of the real space average files to determine relevant areas of the tomograms
class RealSpaceAverage:

    def __init__(self, path, threshold):
        self.path = path
        self.ry_pairs = self.mass_average(cutoff=threshold)

    # identifies the y-slices and sampling radii in the averaged inverted real space image that carry density over a
    # threshold and therefore should be taken into consideration. Returns a list of (radius, slice) tupels.
    # note that the cutoff was chosen arbitrarily
    def mass_average(self, cutoff=0.3, normalise=True):

        sampling_matrix = self.make_sampling_matrix(self.path)

        if normalise:
            sampling_matrix = nan_norm(sampling_matrix)

        flattened_matrix = sampling_matrix.reshape(sampling_matrix.shape[0],
                                                   sampling_matrix.shape[1] * sampling_matrix.shape[2])

        # generate a matrix of means of each circle, where the indices correspond to y slice number
        # and circle number
        means = np.mean(flattened_matrix, axis=0)
        means = means.reshape(sampling_matrix.shape[1], sampling_matrix.shape[2])

        indices_array = np.indices(means.shape)  # dimensions: 2, y slices, radii
        radii = indices_array[1, :, :]
        y_slices = indices_array[0, :, :]

        # remove all coordinates where means under the cutoff
        flat_r = radii.flatten()[means.flatten() > cutoff]
        flat_y = y_slices.flatten()[means.flatten() > cutoff]

        # zip the 2 arrays into a list of coordinate tuples
        coordinates = zip(flat_r, flat_y)
        return coordinates

    # Truncates/pads a float f to n decimal places without rounding
    def truncate(self, f):
        s = '{}'.format(f)
        if 'e' in s or 'E' in s:
            return '{0:.{1}f}'.format(f, 0)
        i, p, d = s.partition('.')
        return int(''.join([i, (d + '0' * 0)[:0]]))

    # converts an mrc file into the required format for sampling, ie every row in 3D represents one circle
    def make_sampling_matrix(self, filepath, adjust_centre=False):

        if filepath[-4:].lower() == ".npy":  # sets the class NumpyTom or MRCfile to open_tom
            open_tom = NumpyTom
        elif filepath[-4:].lower() == ".mrc":
            open_tom = MRCfile
        else:
            print 'Error: Input file format not supported'
            return 0

        with open_tom(filepath) as t:

            # Clean the data (reformat etc)
            data = t.get_all_tom_density_values()

            # Variables we need from the tomogram
            z_dim_tomo = t.width
            y_dim_tomo = t.height
            x_dim_tomo = t.depth
            centre = adjust_centre if adjust_centre else self.truncate(x_dim_tomo / 2), self.truncate(y_dim_tomo / 2)

        # Sampling (ie. getting the necessary values in the right format)

        # Calculate variables we need for sampling matrix
        min_radius = 1
        max_radius = min(x_dim_tomo - centre[0], min(centre), y_dim_tomo - centre[1]) - 1
        sample_size = int(pi * max_radius)
        min_height = 1
        max_height = z_dim_tomo - 1

        # Building the sampling matrix
        radii = np.arange(min_radius, max_radius)  # is the max radius not supposed to be in there?
        angles = np.arange(sample_size)
        angles = 2 * pi * angles / sample_size
        dt = [('radius', int),
              ('angle', float)]
        dim = (len(radii), len(angles))
        p = np.zeros(dim, dtype=dt)  # sets a new array full of 0
        p['radius'] = radii[:, np.newaxis]
        p['angle'] = angles

        p = p.ravel()  # returns P as a 1dim array, ie an array of tuples in 1dim

        # convert from polar coordinates (radius, angle) to cartesian coordinates (x,z)
        ct = [('x', float),
              ('z', float)]
        c = map(lambda r, a: (cos(a) * r, sin(a) * r), p['radius'], p['angle'])
        c = np.array(c, dtype=ct).reshape(dim)

        # this step is necessary to center the cartesian coordinate system on centre
        c['x'] += centre[0]
        c['z'] += centre[1]
        # at this point, C is a 2d array of cartesian coordinate tuples, where every row corresponds to points on the
        # same radius, every column to ponts at the same angle

        # add z dimension to get the sampling matrix
        z = np.arange(min_height, max_height)
        z_ = z[np.newaxis, :, np.newaxis]
        c_ = np.swapaxes(c, 0, 1)
        c_ = c_[:, np.newaxis, :]

        # dim means that for every a, y and z, there is a sub-array with 3 cartesian coordinates
        # two dimensions are added compared to the C array since no more tuples are used on the lowest level
        dim = (len(angles), len(z), len(radii), 3)

        vp = voxel_positions = np.zeros(dim)
        vp[:, :, :, 0] = z_
        vp[:, :, :, 1] = c_['z']
        vp[:, :, :, 2] = c_['x']
        # VP at this point, 1st dim = same angle, 2nd = same Y slice, 3rd same radius, 4th coordinates

        np.set_printoptions(threshold=np.inf)  # I presume this is for diagnostics

        # map sampling matrix voxel positions to voxel data (ie in tomogram)
        v = vp.ravel().reshape(-1, 3).T

        try:
            vv = map_coordinates(data, v)  # reminder: data are the tomogram density values
        except NameError:
            print NameError
            vv = map(lambda z, y, x: data[z, y, x], v[0], v[1], v[2])  # is the order not y, z, x??
            vv = np.array(vv)
        vv = vv.reshape(dim[:-1])
        return vv