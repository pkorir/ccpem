sfftk.schema package
--------------------

.. automodule:: sfftk.schema
	:members: SFFType, SFFAttribute, SFFTypeError, SFFSegmentation, SFFSoftware, SFFTransformList, SFFTransform, SFFTransformationMatrix, SFFCanonicalEulerAngles, SFFViewVectorRotation, SFFBoundingBox, SFFGlobalExternalReferences, SFFBiologicalAnnotation, SFFExternalReferences, SFFExternalReference, SFFComplexesAndMacromolecules, SFFComplexes, SFFMacromolecules, SFFColour, SFFRGBA, SFFSegmentList, SFFSegment, SFFContourList, SFFContour, SFFContourPoint, SFFMeshList, SFFMesh, SFFVertexList, SFFVertex, SFFPolygonList, SFFPolygon, SFFShapePrimitiveList, SFFShape, SFFCone, SFFCuboid, SFFCylinder, SFFEllipsoid 
	:show-inheritance: