#
#     Copyright (C) 2014 Tom Burnley
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
# Interactive clickable gallery viewer
#
# Tom Burnley 04/2014

import sys
from PyQt4.QtCore import Qt, QSize
from PyQt4.QtGui import QApplication, QDialog, QGridLayout, QLabel, QPixmap
import click_button
from ccpem_core import ccpem_utils

class ImageGallery(QDialog):
  def __init__(self, parent = None,
                     max_col = 10,
                     window_title = 'Image gallery'):
    super(QDialog, self).__init__(parent)
    self.setWindowTitle(window_title)
    self.gl = QGridLayout(self)
    self.gl.setHorizontalSpacing(5)
    self.gl.setVerticalSpacing(5)
    self.setLayout(self.gl)
    self.row = 0
    self.col = 0
    self.max_col = max_col
    self.selected_images = []

  def update_window_size(self):
    resize_x = (self.col+1) * 150
    resize_y = (self.row+1) * 150
    if self.row == 0:
      self.resize(max(450, resize_x), 200)
    else:
      self.resize(self.max_col * 150, resize_y)

  def update_rol_col_position(self):
    if self.col + 1 < self.max_col:
      self.col += 1
    else:
      self.row += 1
      self.col = 0

  def add_cb(self, image_tag, image_name = None, pil_image = None):
    # Image button widget
    assert [image_name, pil_image].count(None) == 1
    if image_name is not None:
      cb = click_button.ClickButton(image_tag  = image_tag,
                                    image_name = image_name,
                                    parent     = self)
    else:
      cb = click_button.ClickButton(image_tag = image_tag,
                                    pil_image = pil_image,
                                    parent = self)
    self.layout().addWidget(cb, self.row, self.col)
    self.update_window_size()
    self.update_rol_col_position()

  def add_image(self, image_name):
    # Image widget
    label = QLabel()
    pixmap = QPixmap(image_name)
    pixmap = pixmap.scaled(QSize(130, 130), Qt.KeepAspectRatioByExpanding)
    label.setPixmap(pixmap)
    self.layout().addWidget(label, self.row, self.col)
    self.update_window_size()
    self.update_rol_col_position()

class gallery_click_buttons(object):
  def __init__(self, window_title = 'Gallery with clickable buttons',
                     image_file_list = None,
                     pil_image_list = None,
                     image_tag_list = None):
    assert [image_file_list, pil_image_list].count(None) == 1
    self.selected_images = []

    # Setup gallery
    ccpem_utils.print_header(message = 'Interactive gallery viewer')
    app = QApplication(sys.argv)
    image_gallery = ImageGallery(window_title = window_title)

    # Add buttons
    if image_file_list is not None:
      if image_tag_list is None or len(image_tag_list) != len(image_file_list):
        image_tag_list = list(xrange(len(image_file_list)))
      for n, image in enumerate(image_file_list):
        image_gallery.add_cb(image_name = image,
                             image_tag  = image_tag_list[n])
    else:
      if image_tag_list is None or len(image_tag_list) != len(pil_image_list):
        image_tag_list = list(xrange(len(pil_image_list)))
      for n, pil_image in enumerate(pil_image_list):
        image_gallery.add_cb(pil_image = pil_image,
                             image_tag = image_tag_list[n])
    image_gallery.show()
    print "\n  Click on image to select, reclick to deselect or close window to finish"
    
    # Exit GUI and store selected image list
    app.exec_()
    self.selected_images = image_gallery.selected_images
