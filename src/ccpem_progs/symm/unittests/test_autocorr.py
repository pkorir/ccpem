#!/usr/bin/env python
# coding: utf-8

import unittest
import autocorr
import numpy as np


class TestAnalysisMethods(unittest.TestCase):

    def setUp(self):
        np.seterrcall(lambda *args: None)  # floating point errors are normally passed to logger in make_autocorr_samples
        self.npz = np.load("unittests/testdata/autocorr_testdata.npz")
        self.in_data = self.npz["data"]
        self.small_mask = self.npz["mask"]
        self.large_mask = self.npz["warning_mask"]

    def test_autocorrelation_no_mask(self):
        output, new_mask, warning = autocorr.autocorrelation(in_data=self.in_data, old_mask=np.ones(self.in_data.shape))
        np.testing.assert_array_equal(output, self.npz["ac"], "result from non-masked autocorrelation wrong")
        np.testing.assert_array_equal(new_mask, self.npz["mask_after_ac"], "returned mask after non-masked "
                                                                         "autocorrelation wrong (i.e. not all 1s).")
        self.assertEqual(warning, 0, "wrong warning variable returned (should be 0)")

    def test_autocorrelation_small_mask(self):  # this mask can be bridged by autocorr
        output, new_mask, warning = autocorr.autocorrelation(in_data=self.in_data, old_mask=self.small_mask)
        np.testing.assert_array_equal(output, self.npz["acm"], "result from small-mask autocorrelation wrong")
        np.testing.assert_array_equal(new_mask, self.npz["mask_after_acm"], "returned mask after small-masked "
                                                                         "autocorrelation wrong (i.e. not all 1s).")
        self.assertEqual(warning, 0, "wrong warning variable returned (should be 0)")

    def test_autocorrelation_large_mask(self):  # this one can't --> warning raised
        output, new_mask, warning = autocorr.autocorrelation(in_data=self.in_data, old_mask=self.large_mask)
        np.testing.assert_array_equal(output, self.npz["acm2"], "result from large-mask autocorrelation wrong")
        np.testing.assert_array_equal(new_mask, self.npz["mask_after_acm2"], "returned mask after small-masked "
                                                                            "autocorrelation wrong")
        self.assertEqual(warning, 1, "wrong warning variable returned (should be 1)")

if __name__ == "__main__":
    unittest.main()