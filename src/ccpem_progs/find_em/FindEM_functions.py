import sys, os, subprocess
# various findem functions
class Functions():

  def make_args(self):
    arg_string = ""
    for i, each in enumerate(sys.argv[1:]):
      arg_string = arg_string + """{0}""".format(each)
      if (i < len(sys.argv)):
        arg_string = arg_string + "\n\n"
      else:
        arg_string = arg_STRING + "\n"
    return arg_string

  def runSubscript(self, script, arg_string):
  
    if(sys.platform == 'win32'):
      cmd = os.path.join(os.environ['CCPEMBIN'],script + '.exe')
    else:
      cmd = os.path.join(os.environ['CCPEMBIN'],script)
  
      p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
  
      stdout_data, stderr_data = p.communicate(arg_string)
      print stdout_data
   
      info_msgs = []
    
      if stderr_data is not None and stderr_data is not '':
        info_msgs.append(stderr_data)
      
      if (p.returncode != 0):
        info_msgs.append("It appears there may have been an error in running " + script + " - check your input")

      if(len(info_msgs) > 0):
        info_msgs.insert(0, self.err_count)
        self.err_count+=1
     
        self.errors[script] = info_msgs
