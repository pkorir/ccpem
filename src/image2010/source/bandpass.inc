C*** bandpass.inc
	real*4		bignum
	parameter	(bignum = 10000000.)
	real*4		denmax
	parameter	(denmax = 255.)
	real*4		drange
	parameter	(drange = 0.5)
        integer*4       idevin
        parameter       (idevin=1)
        integer*4       idevout
        parameter       (idevout=2)
        integer*4       maxsize
        parameter       (maxsize = 4096)
	real*4		peakdefault
	parameter	(peakdefault = 0.025)
	real*4		scalemin
	parameter	(scalemin = 0.1)
	real*4		scalemax
	parameter	(scalemax = 1.0)
	real*4		shapedefault
	parameter	(shapedefault = 3.0)
	real*4		shapemin
	parameter	(shapemin = 1.0)
	real*4		shapemax
	parameter	(shapemax = 5.0)
C***
        character       filin*256
        character       filout*256
	character	text*80
	character	title*80
C***
        real*4          mapbuf(maxsize * (maxsize+2))
	common 
     *         	nx, ny, nz, nxtrans, mode,
     *		dmin, dmax, dmean, bmin, bmax
