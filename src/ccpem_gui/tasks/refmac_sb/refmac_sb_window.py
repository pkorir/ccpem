#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Task window for Refmac sharpen / blur utility
'''
import os
import warnings
from PyQt4 import QtGui
from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.tasks.refmac_sb import refmac_sb_task
from ccpem_gui.tasks.refmac_sb import refmac_sb_plot


class RefmacSBWindow(window_utils.CCPEMTaskWindow):
    '''
    Refmac window.
    '''
    def __init__(self,
                 task,
                 parent=None):
        super(RefmacSBWindow, self).__init__(
            task=task,
            parent=parent)
        self.plot_widget = None

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input map
        map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_map',
            args=self.args,
            label='Input map',
            file_types=ccpem_file_types.mrc_ext,
            required=True)
        self.args_widget.args_layout.addWidget(map_input)

        # Input pdb
        pdb_input = window_utils.FileArgInput(
            parent=self,
            arg_name='start_pdb',
            args=self.args,
            label='Input PDB',
            file_types=ccpem_file_types.pdb_ext,
            required=False)
        self.args_widget.args_layout.addWidget(pdb_input)

        # Resolution
        resolution_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='resolution',
            args=self.args,
            required=True)
        self.args_widget.args_layout.addWidget(resolution_input)

        # Sharp array
        sharp_array_input = window_utils.ListArgInput(
            parent=self,
            arg_name='sharp_array',
            args=self.args,
            required=True,
            element_type=float)
        self.args_widget.args_layout.addWidget(sharp_array_input)

        # Blur array
        blur_array_input = window_utils.ListArgInput(
            parent=self,
            arg_name='blur_array',
            args=self.args,
            required=True,
            element_type=float)
        self.args_widget.args_layout.addWidget(blur_array_input)

        # Set inputs for launcher
        self.launcher.add_file(
            arg_name='input_map',
            file_type='map',
            description=self.args.input_map.help,
            selected=True)
        self.launcher.add_file(
            arg_name='start_pdb',
            file_type='pdb',
            description=self.args.start_pdb.help,
            selected=True)

    def set_plot_widget(self):
        '''
        Matplotlib results dock
        '''
        path = ''
        if hasattr(self.task, 'process_maptomtz'):
            path = self.task.process_maptomtz.hklout_path
        if os.path.exists(path):
            # Suppress following warning:
            # UserWarning: Unable to find pixel distance along axis for
            # interval padding of ticks; assuming no interval padding needed.
            warnings.simplefilter('ignore', UserWarning)
            # Set plot widget
            self.plot_widget = refmac_sb_plot.RefmacMapSharpPlotWidget(
                parent=self,
                results_path=self.task.bin_results_path,
                mtz_in_path=path,
                blur_array=self.args.blur_array.value,
                sharp_array=self.args.sharp_array.value,
                pdb_in_path=self.args.start_pdb.value)
            # Set dock and layout
            self.plot_dock = QtGui.QDockWidget(
                'Plot',
                self)
            self.plot_dock.setWidget(self.plot_widget)
            self.tabifyDockWidget(self.pipeline_dock, self.plot_dock)
            self.plot_dock.raise_()
            # Reassign coot button to launch coot with maps
            self.run_coot_custom = self.plot_widget.launch_coot

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.  For now show starting, refined
        pdb and experimental map.
        '''
        # Set launcher files
        # Processed mtz (w/ sharp / blurred arrays)
        if hasattr(self.task, 'process_maptomtz'):
            self.launcher.add_file(
                arg_name=None,
                path=self.task.process_maptomtz.hklout_path,
                file_type='mtz',
                description='Structure factors from input map',
                selected=True)
        self.launcher.set_tree_view()
        self.set_plot_widget()


def main(args=None):
    '''
    Launch stand alone window.
    '''
    window_utils.standalone_window_launch(
        task=refmac_sb_task.RefmacSB,
        window=RefmacSBWindow)


if __name__ == '__main__':
    main()
