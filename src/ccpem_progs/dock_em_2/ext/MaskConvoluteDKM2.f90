!*******************************************************************************
!=* MaskConvoluteDKM2p0.f90,  AMR April 2015      						       *
!=******************************************************************************
!=* DOCKEM2.0.f90                		   								       *
!=*                                    		  AUTHOR: A.ROSEMAN  	           *
!=*									    									   *
!=* DockEM2 - Molecular density matching program.		   					   *
!=* This program is part of a suite of programs that performs a molecular      *
!=* density matching a template based alignment of a template motif against    *
!=* a 3D target map, using the locally normalised correlation coefficient.     *
!=* It is for the purpose of docking domain structures into 3D maps	       	   *
!=*									      	 								   *
!=* Copyright (C) 2015 The University of Manchester 			      		   *
!=*                                                                   	       *
!=*                                                                            *
!=*    This program is free software: you can redistribute it and/or modify    *
!=*    it under the terms of the GNU General Public License as published by    *
!=*    the Free Software Foundation, either version 3 of the License, or       *
!=*    (at your option) any later version.                                     *  
!=*        								       								   *
!=*    This program is distributed in the hope that it will be useful,         *
!=*    but WITHOUT ANY WARRANTY; without even the implied warranty of          *
!=*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
!=*    GNU General Public License for more details.                            *
!=* 									      								   *
!=*    You should have received a copy of the GNU General Public License       *
!=*    along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
!=* 									     								   *
!=*    For enquiries contact:						    					   *
!=*    									  								       *
!=*    Alan Roseman  													       *
!=*    Faculty of Life Sciences											       *
!=*    University of Manchester											       *
!=*    The Michael Smith Building										       *
!=*    Oxford Road														       *
!=*    Manchester. M13 9PT												       *
!=*    Email:Alan.Roseman@manchester.ac.uk    							       *
!=*																		       *
!=*   																	       *
!=******************************************************************************
!from
!MaskConvolute.f90, AMR Mar 2015.

! from f90 program filter maps
! 
! uses f90 array features
!
! filter is a sharp truncation
!  
! AMR 4/10/99
! edit 7/6/2001 AMR added high pass filter
! V1.01 correct header for correct cell size, N. 
! though 0:N-1 21/9/01 AMR
! note maps must be "cubic".
! last edit dec 2002.
! add next_ft_size option 2/2003.
! v2.02 change c in filter sub

        program MaskConvolute
	use  image_arrays
 	Use  mrc_image

	real sampling,lp,hp
	integer next_ft_size,err
 
        real, dimension (:,:,:), allocatable :: cmap, sphere
	character*256 filename,outfile1
        DATA IFOR/0/,IBAK/1/,ZERO/0.0/
 
   
1     format(A45)      
      write(*,1) 'Mask Convolute Fourier filter program, for DockEM2.0 '
      write(*,1) '-----------------------------------------------------'
      print*,'last update 1/4/15'
    


! 1.  read in template filename, and threshold of data to use.

      write(*,*) 'Enter the map filename >'
      !read*,filename
      read (5,'(a)') filename
      print*,filename      
      
      write(*,*) 'Enter the sampling of the map (A/pixel) > '
      read*,sampling
      print*,sampling
            
      write(*,*) 'Enter the radius of the sphere to convolute (A) > '  
      read*,lp
      print*,'Radius in A, pixels :  ', lp,lp/sampling

      lp=lp/sampling

	print*,lp

      write(*,*) 'Enter the output map filename >'
      read (5,'(a)') outfile1
      print*,outfile1
                       
  
! 2. read in the map
  
      CALL IMOPEN(1,filename(1:lnblnk(filename)),'RO')
      CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
      
	print*, NX,NY,NZ
	
!	if (odd(nx).eq..true.) STOP 'MAP DIMENSION MUST BE EVEN'
	
	nxp2=nx+2
	nxp1=nx+1
	nym1=ny-1
	nzm1=nz-1
	
	if ((nx.ne.ny).and.(nx.ne.nz)) STOP "The map region must be a cube."
	
	mx=nx
	my=ny
	mz=nz
	mx=next_ft_size(mx)
	my=next_ft_size(my)
	mz=next_ft_size(mz)
	mxp2=mx+2
	mxp1=mx+1
	mym1=my-1
	mzm1=mz-1
	
	print*,mx,my,mz
	
	
        allocate (map(0:mXP1,0:mYM1,0:mZM1))	
        allocate (sphere(0:mXP1,0:mYM1,0:mZM1))
        sphere = 0
        allocate (cmap(0:mXP1,0:mYM1,0:mZM1))
        cmap = 0

 
      	call read_mrcimage(1,nx,ny,nz)
	call IMOPEN(4,outfile1(1:lnblnk(outfile1)),'NEW')
	call ITRHDR(4,1)
	call IWRHDR(4,title,ntflag,dmin,dmax,dmean)

     	call IMOPEN(2,'DkmSphereCnv.mrc','NEW')
	call ITRHDR(2,1)
	call IWRHDR(2,title,ntflag,dmin,dmax,dmean)
      
        CALL IMCLOSE(1)   



! 3. open input and output image stacks 





! 	4.5 filter image

	  print*,mxp2,my,mz
	
          CALL BCKW_FT(map,mXP2,mX,mY,mZ)
          
          print*, 'ft'
          
          CALL makesphere(sphere,mx,my,mz,sampling,lp)  !XXX
 !         call write_mrcimage(sphere,2,sampling) 
	  CALL WRITEMRCMAP(2,sphere,mx,my,mz,sampling,err) !open already, close it, subtract 2 from nx

          CALL BCKW_FT(sphere,mXP2,mX,mY,mZ)
          print*, 'ft-sphere'

	  print*,mx,my,mz

          call CONVOLUTION3D(map,sphere,cmap,mx,my,mz)    !XXX ! need to pre FT ??
          
        
	
	  print*, 'convolution done'
  
  
9999    continue
 !     	call write_mrcimage(cmap,4,sampling)       
	CALL WRITEMRCMAP(4,cmap,mx,my,mz,sampling,err) !open already, close it, subtract 2 from nx
	
	print*,'done, close files now.'

! 8.  close files and exit
	
!       CALL IMCLOSE(2) 
!       CALL IMCLOSE(4) 

       print*,'Program finished O.K.'
            
      STOP
997   STOP 'Error on file read.'
      
      
      CONTAINS



      subroutine read_mrcimage(stream,x,y,z)
 
		
        Use  image_arrays
        Use  mrc_image	
	INTEGER  stream, IX,IY,IZ
	integer x,y,z
 	     
!	nx=size(map,1)
!	ny=size(map,2)
!	nz=size(map,3)
  
  
!        nx=nx-2
	nxp2=nx+2
        nxm1=nx-1
        nym1=ny-1
        nzm1=nz-1

	print*,'read_mrcimage'	

	
      print*,NX,NY,NZ,x,y,z     	      	
!     read in file 
 	map=0
      DO 350 IZ = 0,z-1
    	  DO 350 IY = 0,y-1
            CALL IRDLIN(stream,ALINE,*998)
            DO 300 IX = 0,x-1
                map(ix,iy,iz) = ALINE(IX+1)
300         CONTINUE   
	  !  map(NX+0,iy,iz)=0
	   ! map(NX+1,iy,iz)=0
350   CONTINUE

      return
998   STOP 'Error on file read.'
      end subroutine 


      subroutine write_mrcimage(volume,stream,sampling)	
      	
      Use  image_arrays
      Use  mrc_image
  	
      INTEGER stream,IX,IY,IZ
      REAL origin
      real volume(nx+1,ny,nz)
   
 	     
!      nx=size(map,1)
!      ny=size(map,2)
!      nz=size(map,3)
      
!      nx=nx-2
      nxp2=nx+2
      nxm1=nx-1
      nym1=ny-1
      nzm1=nz-1
	
      print*,'write_mrcimage'	      	

      print*,NX,NY,NZ  	
!     write file 

      DMIN =  1.E10
      DMAX = -1.E10
      DOUBLMEAN = 0.0

      DO 450 IZ = 0,NZM1
      DO 450 IY = 0,NYM1
            DO 400 IX = 0,NXM1
            	B = map(IX,IY,IZ)
                ALINE(IX+1) = B
                
                DOUBLMEAN = DOUBLMEAN + B         
                IF (B .LT. DMIN) DMIN = B
                IF (B .GT. DMAX) DMAX = B

400         CONTINUE   

      CALL IWRLIN(stream,ALINE)
450   CONTINUE
      DMEAN = DOUBLMEAN/(NX*NY*NZ)
      
      cell(1) = sampling *NX
      cell(2) = sampling *NY
      cell(3) = sampling *NZ
      cell(4) = 90
      cell(5) = 90
      cell(6) = 90
	
      CALL IALCEL(STREAM,CELL)  
      CALL IWRHDR(stream,TITLE,-1,DMIN,DMAX,DMEAN)

      origin=0.0
   !   CALL IALCEL(STREAM,CELL)

      
      return
999   STOP 'Error on file write.'
      end subroutine 



  
!C***************************************************************************

        SUBROUTINE makesphere(sphere,sx,sy,sz,sampling,lp)
        Use  image_arrays


	real sampling,lp_cutoff,hp_cutoff,rad,r,hp,lp
	real cx,cy,cz,x,y,z
	integer ix,iy,iz
	integer nxm1,nym1,nzm1
	integer sx,sy,sz
        real, dimension (0:sx+1,0:sy-1,0:sz-1) :: sphere,sphere2

	print*,'lp=',lp
	     
!	nx=size(map,1)
!	ny=size(map,2)
!	nz=size(map,3)
	
!	nx=nx-2
	
	nxm1=nx-1
	nym1=ny-1	
	nzm1=nz-1
	
	nxp1=nx+1
        

		
	cx=int((nx+1)/2) 
	cy=int((ny+1)/2) 
	cz=int((nz+1)/2) 
	
	print*,nx,ny,nz,cx,cy,cz
	
	lp_cutoff=lp

	print*,'lp_cutoff=',lp
		
        sphere=0.0

 	DO 301 iz=0,nzm1
        DO 302 iy=0,nym1
        DO 303 ix=0,nxp1
        
        	x=ix-cx
        	y=iy-cy
        	z=iz-cz
        	
      
                r=sqrt((x*x) + (y*y)+  (z*z))
                if ((r.le.lp_cutoff)) then 
                			sphere(ix,iy,iz)=(1.0)		
                endif
                
		

303     CONTINUE
302     CONTINUE
301     CONTINUE
	print*,1
     		sphere2 = cshift(sphere,shift = int(cx),dim=1)
	print*,2
		sphere = cshift(sphere2,shift = int(cy),dim=2)
	print*,3
		sphere2 = cshift(sphere,shift = int(cz),dim=3)
	print*,4
		sphere = sphere2
	print*,5

        RETURN
        END  SUBROUTINE makesphere

  
  	LOGICAL FUNCTION ODD(N)
  	INTEGER N
  	REAL F,I ,T 	
  	
  	F=FLOAT(N)
  	I=INT(F/2)
  	R=I/2
  	T=(I-R)
  	
  	IF (T.GT.0.00001) THEN
  					ODD=.TRUE.
  				ELSE
  					ODD=.FALSE.
  				ENDIF
  	END FUNCTION ODD 
  


	subroutine CONVOLUTION3D(a,b,c,nx2,ny2,nz2)
        integer nx2,ny2,nz2,i,j,k
        real a(0:nx2+1,0:ny2-1,0:nz2-1),b(0:nx2+1,0:ny2-1,0:nz2-1),c(0:nx2+1,0:ny2-1,0:nz2-1)
        real n
        integer nxp2
	integer dx

        nxp2=nx2+2

        c=0
        
        do k=0,nz2-1
  	      do j=0,ny2-1
            	    do i=0,nx2+1,2
                
                        c(i,j,k)=a(i,j,k)*b(i,j,k)-a(i+1,j,k)*b(i+1,j,k)
                        c(i+1,j,k)=a(i,j,k)*b(i+1,j,k)+a(i+1,j,k)*b(i,j,k)
               
                enddo
            enddo
       enddo

        CALL FORW_FT(c,NXP2,NX2,NY2,NZ2)

        n=nx2*ny2*nz2		
        C=C*N

	return

        end subroutine CONVOLUTION3D



     SUBROUTINE WRITEMRCMAP(stream,map,x,y,z,sampling,err)
      Use  mrc_image
      REAL val,sampling
      
      integer x,y,z,err,stream,ix,iy,iz
      real map(0:x+1,0:y-1,0:z-1)

      print*,'write_mrcimage'         

      print*,NX,NY,NZ  
!     write file 

      DMIN =  1.E10
      DMAX = -1.E10
      DOUBLMEAN = 0.0

      DO 450 IZ = 0,NZ-1
      DO 450 IY = 0,NY-1
            
            DO 400 IX = 0,NX-1
     
                val = map(IX,IY,IZ)
                ALINE(IX+1) = val
                
                DOUBLMEAN = DOUBLMEAN + val         
                IF (val .LT. DMIN) DMIN = val
                IF (val .GT. DMAX) DMAX = val

400         CONTINUE   


      CALL IWRLIN(stream,ALINE)
450   CONTINUE
      DMEAN = DOUBLMEAN/(NX*NY*NZ)

      cell(1) = sampling *NX
      cell(2) = sampling *NY
      cell(3) = sampling *NZ
      cell(4) = 90
      cell(5) = 90
      cell(6) = 90

      CALL IALCEL(STREAM,CELL)  
      CALL IWRHDR(stream,TITLE,-1,DMIN,DMAX,DMEAN)
      CALL IMCLOSE(stream)
      
      return
999   STOP 'Error on file write.'
 
      END SUBROUTINE WRITEMRCMAP
	
	

  	end program MaskConvolute



