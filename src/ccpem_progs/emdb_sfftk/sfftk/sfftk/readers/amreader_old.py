#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
meshreader.py
========================

Read Amira Mesh model (.am) files.

This script is inspired by `surfreader.py`. It employs a two-pass strategy in unravelling .am files. First of all, all the data is read into memory.

In the first pass, the contents of the header are parsed. 

The second pass consists of reading the data using the `Bio-Formats` module.

Requirements (available from PyPi)
----------------------------------
- `simpleparser`
- `python-bioformats`
- `javabridge`

References
==========
The following article is useful as it exposes many internals of Amira Mesh files:
- http://www.mathworks.com/matlabcentral/fileexchange/39235-load-amira-5-2-2-files/content/LoadData_Amira.m

todo:
-----
- Find generic way to extract 'Materials' field of header (use XPath?)
- Reorganise the grammar and/or parser to output a dict of dicts
- Replace bioformats (seems to be extremely memory inefficient!)

Version history:
----------------
0.0.1, 2015-12-18, First working version for ad hoc .am binary filess
0.0.2, 2016-02-02, Incorporated fast method for generating contours (untested)
0.0.3, 2016-02-04, Refined fast method for generating contours
0.0.4, 2016-02-22, Minor changes: Model initialiser (__init__) arguments, renamed first_pass/second_pass to _*; assertions on .am header etc.
0.0.5, 2016-02-25, Removed demo code from main()
0.0.6, 2016-05-16, Added two new tokens 'number_string' and '<q>' for a string of numbers and the quote that delimits them to overcome some header issues

Parser
------
An `.am` file is specified by a VRML-like syntax
The declaration below defines an EBNF grammar for this syntax.
In brief, the file consists of a header composed of blocks
The blocks may have innerblocks.
Innerblocks contain useful parameters such as materials specifications.::

    mesh           :=    tsn, header, tsn, definitions*, tsn, block*, tsn, references*, tsn
    header         :=    "#", ts, propname, ts, hyphname, ts, number*
    definitions    :=    definition+
    definition     :=    "define", ts, propname, ts, values, ts, c*, tsn
    block          :=    propname*, ts, "{", tsn, innerblock1*, tsn, vallines*, tsn, "}", tsn
    innerblock1    :=    propname*, ts, "{", tsn, innerblock2*, tsn, vallines*, tsn, "}", tsn
    innerblock2    :=    propname*, ts, "{", tsn, innerblock3*, tsn, vallines*, tsn, "}", tsn
    innerblock3    :=    propname*, ts, "{", tsn, vallines*, tsn, "}", tsn
    vallines       :=    valline+
    valline        :=    ts, propname, ts, values, ts, c*, tsn
    references     :=    reference+
    reference      :=    propname, ts, "{", ts, type, ts, propname, ts, "}", ts, ref, tsn
    type           :=    propname, ("[", ts, number, ts, "]")*
    ref            :=    "@", number, ("("*, propname*, c*, number*, ")")*
    values         :=    number_seq / number_string / string
    number_seq     :=    number, (ts, number)*
    number_string  :=    q, number, (ts, number)*, q
    propname       :=    [A-Za-z], [A-Za-z0-9_]*
    hyphname       :=    [A-Za-a], [A-Za-z0-9_-]*
    <tsn>          :=    [ \\t\\n]*         # this will not be reported
    <ts>           :=    [ \\t]*
    <c>            :=    ","
    <eq>           :=    [=]
    <q>            :=    ['"]
    
Production rules identified with angle brackets e.g. `<tsn>` are non-printable.
"""
from __future__ import division

import re
import sys

import bioformats
import javabridge
from simpleparse.common import numbers, strings
from simpleparse.dispatchprocessor import DispatchProcessor, getString, dispatchList, dispatch
from simpleparse.parser import Parser
from skimage.measure import find_contours


__author__  = 'Paul K. Korir, PhD'
__email__   = 'pkorir@ebi.ac.uk, paul.korir@gmail.com'
__date__    = '2015-12-18'




# simpleparse

# An `.am` file is specified by a VRML-like syntax
# the declaration below defines an EBNF grammar for this syntax.
# In brief, the file consists of a header composed of blocks
# The blocks may have innerblocks.
# Innerblocks contain useful parameters such as materials specifications
declaration = r'''
mesh           :=    tsn, header, tsn, definitions*, tsn, block*, tsn, references*, tsn
header         :=    "#", ts, propname, ts, hyphname, ts, number*
definitions    :=    definition+
definition     :=    "define", ts, propname, ts, values, ts, c*, tsn
block          :=    propname*, ts, "{", tsn, innerblock1*, tsn, vallines*, tsn, "}", tsn
innerblock1    :=    propname*, ts, "{", tsn, innerblock2*, tsn, vallines*, tsn, "}", tsn
innerblock2    :=    propname*, ts, "{", tsn, innerblock3*, tsn, vallines*, tsn, "}", tsn
innerblock3    :=    propname*, ts, "{", tsn, vallines*, tsn, "}", tsn
vallines       :=    valline+
valline        :=    ts, propname, ts, values, ts, c*, tsn
references     :=    reference+
reference      :=    propname, ts, "{", ts, type, ts, propname, ts, "}", ts, ref, tsn
type           :=    propname, ("[", ts, number, ts, "]")*
ref            :=    "@", number, ("("*, propname*, c*, number*, ")")*
values         :=    number_seq / number_string / string
number_seq     :=    number, (ts, number)*
number_string  :=    q, number, (ts, number)*, q
propname       :=    [A-Za-z], [A-Za-z0-9_]*
hyphname       :=    [A-Za-a], [A-Za-z0-9_-]*
<tsn>          :=    [ \t\n]*            # this production rule will not be reported!
<ts>           :=    [ \t]*
<c>            :=    ","
<eq>           :=    [=]
<q>            :=    ['"]
'''
      

class MeshProcessor(DispatchProcessor):
    """
    Class that captures the results of parsing the grammar

    Once the data is parsed using the parser above it needs to be accessed.
    This is done using a processor. Processors must inherit from DispatchProcessor (DP).
    The DP contains methods (termed handlers) to the various production rules e.g. 'header'
    with the exception of the root production rule (in the example above 'mesh' is the root
    production rule). 
    
    If a production rule is a terminal rule containing data then a call to getString returns its content.
    
    If it is a container then the call depends on how it is structured:
    - if it is a unitary container then a call to dispatch returns child production rule results
    - if it is a list-like container then a call to dispatchList returns a list-like dataset of child production results
    
    Unfortunately, the implementation of EBNF grammar does not allow recursion.
    """
    #--------------------------------------------------------------------
    #: Primitives
    #: values
    def values(self, (tag, left, right, taglist), _buffer):
        return dispatch(self, taglist[0], _buffer)

    # number_seq
    def number_seq(self, (tag, left, right, taglist), _buffer):
        return dispatchList(self, taglist, _buffer)
    
    # number_string
    def number_string(self, (tag, left, right, taglist), _buffer):
        return dispatchList(self, taglist, _buffer)
    
    # number
    def number(self, (tag, left, right, taglist), _buffer):
        return getString((tag, left, right, taglist), _buffer)
    
    # propname
    def propname(self, (tag, left, right, taglist), _buffer):
        return getString((tag, left, right, taglist), _buffer)
    
    # hyphname (an extension of propname by '-')
    def hyphname(self, (tag, left, right, taglist), _buffer):
        return getString((tag, left, right, taglist), _buffer)

    # string
    def string(self, (tag, left, right, taglist), _buffer):
        return getString((tag, left, right, taglist), _buffer)

    # superblock
    def superblock(self, (tag, left, right, taglist), _buffer):
        return dispatchList(self, taglist, _buffer)
    
    #--------------------------------------------------------------------
    # production rules
    # header
    def header(self, (tag, left, right, taglist), _buffer):
        return dispatchList(self, taglist, _buffer)
#         return {'Header': dispatchList(self, taglist, _buffer)}

    # definitions
    def definitions(self, (tag, left, right, taglist), _buffer):
        return dispatchList(self, taglist, _buffer)
#         return {'Definitions': dispatchList(self, taglist, _buffer)[0]}
    
    # definition
    def definition(self, (tag, left, right, taglist), _buffer):
#         this_block = dispatchList(self, taglist, _buffer)
#         key, value = this_block[0], this_block[1:]
#         return {'Definition': {key: value}}
        return dispatchList(self, taglist, _buffer)
    
    # block
    def block(self, (tag, left, right, taglist), _buffer):
#         this_block = dispatchList(self, taglist, _buffer)
#         key, value = this_block[0], this_block[1:]
#         return {key: value}
        return dispatchList(self, taglist, _buffer)

    # innerblock1
    def innerblock1(self, (tag, left, right, taglist), _buffer):
#         this_block = dispatchList(self, taglist, _buffer)
#         key, value = this_block[0], this_block[1:]
#         return {key: value}
        return dispatchList(self, taglist, _buffer)

    # innerblock2
    def innerblock2(self, (tag, left, right, taglist), _buffer):
#         this_block = dispatchList(self, taglist, _buffer)
#         key, value = this_block[0], this_block[1:]
#         return {key: value}
        return dispatchList(self, taglist, _buffer)

    # innerblock3
    def innerblock3(self, (tag, left, right, taglist), _buffer):
#         this_block = dispatchList(self, taglist, _buffer)
#         key, value = this_block[0], this_block[1:]
#         return {key: value}
        return dispatchList(self, taglist, _buffer)

    # vallines
    def vallines(self, (tag, left, right, taglist), _buffer):
#         this_block = dispatchList(self, taglist, _buffer)
#         key, value = this_block[0], this_block[1:]
#         return {key: value}
        return dispatchList(self, taglist, _buffer)

    # valline
    def valline(self, (tag, left, right, taglist), _buffer):
#         this_block = dispatchList(self, taglist, _buffer)
#         key, value = this_block[0], this_block[1:]
#         return {key: value}
        return dispatchList(self, taglist, _buffer)

    # references
    def references(self, (tag, left, right, taglist), _buffer):
        return dispatchList(self, taglist, _buffer)
    
    # reference
    def reference(self, (tag, left, right, taglist), _buffer):
        return dispatchList(self, taglist, _buffer)
    
    # type
    def type(self, (tag, left, right, taglist), _buffer):
        return dispatchList(self, taglist, _buffer)
    
    # ref
    def ref(self, (tag, left, right, taglist), _buffer):
        return dispatchList(self, taglist, _buffer)

    # vertices
    def vertices(self, (tag, left, right, taglist), _buffer):
        return dispatchList(self, taglist, _buffer)  


class Model(object):
    """
    Class that encapsulates data from a .am file
    """    
    def __init__(self, filetype, _format, version):
        self.filetype = filetype
        self.format = _format
        self.version = version
        
        # flags
        self.is_populated = False
        
        # definitions
        self.Definitions = None
        
        # materials
        self.Materials = None
        self.colour_to_material = dict()
        
        # image data
        self.Mesh               = dict()
        
        # vectorised data
        self.Contours           = dict()
        
        # converted vectorised data to segments
        self.Segments           = dict()
    
    def __repr__(self):
        materials = ""
        for M in self.Materials.itervalues():
            materials += "%s\n" % M
        x, y, c = self.Mesh[0].shape 
        string = """
            \r*******************************************************************************
            \rAmira Mesh file
            \rVersion:              %s
            \rFormat:               %s
            \r*******************************************************************************
            \rMaterials:
            \r%s
            \r
            \rMesh                  %s images each of %s X %s pixels (%s colors)
            \r
            \r*******************************************************************************
            """ % (
               self.version,
               self.format,
               materials,
               len(self.Mesh), x, y, c,
               )
        return string
    
    def __getitem__(self, key):
        return self.Mesh[key]

    def generate_contours(self, tol=0.01, verbose=False):
        """
        Generate contours for each image in self.Mesh
        """
        segments = dict()
        for Z,img in self.Mesh.items():
#         for Z in xrange(10): # for testing purposes
            img = self.Mesh[Z]
            if verbose:
                print >> sys.stderr, "Generating contours for Z = %s..." % Z
                
            segments[Z] = dict()
            colour_to_material = dict()

            for M in self.Materials.itervalues():
                if M.Color is None:
                    continue

                colour = tuple(M.Color)
                colour_to_material[colour] = M.name
                
                # binarise
                mask_0 = ((img[:,:,0] >= (colour[0] - tol)) & (img[:,:,0] <= (colour[0] + tol)))*1
                mask_1 = ((img[:,:,1] >= (colour[1] - tol)) & (img[:,:,1] <= (colour[1] + tol)))*1
                mask_2 = ((img[:,:,2] >= (colour[2] - tol)) & (img[:,:,2] <= (colour[2] + tol)))*1               
                mask_sum = mask_0 + mask_1 + mask_2 # add them to get 3 for perfect matches
                mask = (mask_sum == 3)*255
                
                # find contours
                contours = find_contours(mask, 254, fully_connected='high')
                                
                if colour not in segments[Z]:
                    segments[Z][colour] = contours
                else:
                    segments[Z][colour] += contours
                
                if verbose:
                    print >> sys.stderr, "Generated %s contours for colour=(%s)..." % (len(contours), ",".join(map(str, M.Color)))
                
            if verbose:
                print >> sys.stderr, "Generated %s contours..." % len(segments[Z][colour])
        
        self.colour_to_material = colour_to_material
        
        # collate contours by colour
        segments2 = dict()
        for z in segments:
            for colour in segments[z]:
                if colour not in segments2:
                    segments2[colour] = dict()
#                 if z not in segments2[colour]:
                segments2[colour][z] = dict()
                contour_id = 0
                for contour in segments[z][colour]:
                    segments2[colour][z][contour_id] = [(x,y,z) for x,y in contour]
                    contour_id += 1
        
        if verbose:
            total_p = 0
            for z in segments:
                for colour in segments[z]:
                    for contours in segments[z][colour]:
                        total_p += len(contours)
                    
            print >> sys.stderr, 'Points in Model.Contours = %s' % (total_p)
            
            total_p = 0
            for colour in segments2:
                for z in segments2[colour]:
                    for contour_id in segments2[colour][z]:
                        total_p += len(segments2[colour][z][contour_id])
            
            print >> sys.stderr, 'Points in Model.Segments = %s' % (total_p)
        
        self.Contours = segments
        self.Segments = segments2
        

class Definition(object):
    """
    Class that encapsulates header definitions
    """
    def __init__(self, name):
        self.name = name
        self.value = None
    
    def set_value(self, name, value):
        if name == 'Lattice':
            self.value = tuple(map(int, value))
        # add other definitions here with 'elif'
        else:
            raise ValueError('Unknown definition')
    
    def __repr__(self):
        return "%s                    %s" % (self.name, self.value)


class Material(object):
    """
    Class that encapsulates a material
    """
    def __init__(self, name):
        self.name = name
        self.Id = None
        self.Color = None
        self.Name = None
        self.Group = None
    
    def set_param(self, param_name, param_value):
        if param_name == 'Id':
            self.Id = param_value[0]
        elif param_name == 'Color':
            self.Color = map(float, param_value)
        elif param_name == 'Name':
            self.Name = param_value.strip('"')
        elif param_name == 'Group':
            self.Group = param_value
        # add other params here
        else:
            raise ValueError('Unknown material parameter')
    
    def __repr__(self):
        string = """
            \r%s
            \rId                    %s""" % (self.name, self.Id)
        if self.Name is not None:
            string += """
                \rName                  %s""" % (self.Name)
        if self.Color is not None:
            string += """
                \rColor                 %s""" % (self.Color)
        if self.Group is not None:
            string += """
                \rGroup                 %s""" % (self.Group)
        return string


def _first_pass(file_object, model_object):
    """
    Determine the offsets for binary data in stream.
    
    Return a model_object with offsets populated.
    
    Arguments:
    :param file file_object:
        an open file object to a .am file
        
    :param `Model` model_object:
        an empty object of class Model
    """
    data = file_object.read()
    
    # parse the header for Materials
    parser = Parser(declaration)
    # the processor to handle the data from the parser
    mesh_processor = MeshProcessor()
    # find all the ASCII before the Vertices
    # set the re.S flag to allow . to include \n in the search
    header_match = re.search(r'(?P<data>.*)\n@1', data, flags=re.S)
    # the header is the 'data' group
    header = header_match.group('data')    
    
    # parse the header with our parser and our processor
    output = parser.parse(header, production='mesh', processor=mesh_processor)
    
#     print output
    
#     sys.exit(0)
#     import pprint
#     pp = pprint.PrettyPrinter()
#     pp.pprint(output)
    
    # get definitions
    definitions = output[1][1] # likely to break if 'define' statements are removed from the root
    
    # get definitions from header
    definitions_dict = dict()
    i = 0
    for definition in definitions:
        name = definition[0]
        D = Definition(name)
        try:
            value = definition[1]
        except IndexError: # no value
            definitions_dict[name] = D
            i += 1
            continue
        D.set_value(name, value)
        definitions_dict[name] = D
        i += 1
            
    # get materials
    materials = output[1][2][1][1:] # likely to break if 'Materials' is restructured
    
    # get materials from header
    materials_dict = dict()
    i = 0
    for mat in materials:
        name = mat[0]
        M = Material(name)
        try:
            params = mat[1]
        except IndexError: # no params
            materials_dict[name] = M
            i += 1
            continue
        for param in params:
            param_name = param[0]
            param_value = param[1]
            M.set_param(param_name, param_value)
        materials_dict[name] = M
        i += 1
    
    model_object.Definitions = definitions_dict
    model_object.Materials = materials_dict
      
    # set the is_populated flag
    model_object.is_populated           = True
        
    return model_object


def _second_pass(fn, model_object, *args, **kwargs):
    """
    Parse the stream in file_object with offsets defined in model_object.
    
    Return a model_object with all available data present.
    
    Arguments:
    @file_object
        an open file object to a .am file
        
    @model_object
        an empty object of class Model
    """
    # check to ensure that the model_object is appropriately populated
    if not model_object.is_populated:
        raise ValueError("Unpopulated model object. Run first_pass() first.")

    
    z_range = model_object.Definitions['Lattice'].value[2]
            
    with bioformats.ImageReader(fn) as reader:
        for Z in xrange(z_range):
            model_object.Mesh[Z] = reader.read(z=Z)
    
    model_object.generate_contours(*args, **kwargs)
        
    return model_object


def get_data(fn, verbose=False, read_data=True, *args, **kwargs):
    """
    Main function that returns a MeshModel object given the file name
    
    :param str fn: filename
    :param bool verbose: verbosity (default `False`)
    :param bool read_data: get the data (default) or only header
    :return MeshModel: encapsulated data
    :rtype MeshModel: `MeshModel` object
    """
    f = open(fn, 'rb')
    design = f.readline().strip().split(' ')
    filetype = design[1]
    
    # check the filetype
    try:
        assert filetype == 'AmiraMesh'
    except:
        raise TypeError('Not an AmiraMesh file')
    
    _format = design[2]
    
    # check the _format
    try:
        assert _format == 'BINARY-LITTLE-ENDIAN'
    except:
        raise TypeError("Not a file of _format 'BINARY-LITTLE-ENDIAN': %s" % _format)
    
    version = design[-1]
    
    # check the version
    try:
        assert version == '2.1'
    except:
        raise TypeError("Not version 2.1: %s" % version)
    
    # rewind the file handle
    f.seek(0)
    
    MeshModel = Model(filetype, _format, version)
        
    # set offsets
    MeshModel = _first_pass(f, MeshModel)
        
    f.close()

    if read_data:
        if __name__ == "__main__":
            # obtain data
            MeshModel = _second_pass(fn, MeshModel, *args, **kwargs)
        elif __name__ != "__main__":
            javabridge.start_vm(class_path=bioformats.JARS, max_heap_size='10G', run_headless=True)
            try:
                # running the following line removes the following output from appearing:
                # log4j:WARN No appenders could be found for logger (loci.common.NIOByteBufferProvider).
                # log4j:WARN Please initialize the log4j system properly.
                # log4j:WARN See http://logging.apache.org/log4j/1.2/faq.html#noconfig for more info.
                bioformats.init_logger()

                # obtain data
                MeshModel = _second_pass(fn, MeshModel, *args, **kwargs)
            finally:
                javabridge.kill_vm()
    
    return MeshModel


def parse_args(args):
    """
    Parse commandline arguments.
    """
    import optparse

    usage = "usage: %prog [options] file.am"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option(
                      '-o', '--output',
                      help='file to write output to'
                      )
    parser.add_option(
                      '-v', '--verbose',
                      default=False,
                      action="store_true",
                      help='verbose output True [default: false]'
                      )
    
    if len(args) == 0:
        if __name__ == '__main__':
            parser.print_help()
        raise TypeError('Missing arguments.')
    else:
        options, pos_args = parser.parse_args(args)
    
    if len(pos_args) == 0:
        raise TypeError('Missing .am file.')
    
    # the .am file has to be the first positional argument
    if not re.search(r'.am$', pos_args[0]):
        raise TypeError('Non-mesh file. Retry with .am file.')
    
    return options, pos_args


def main():
    """
    Parse an .am file.
    """
    options, pos_args = parse_args(sys.argv[1:])
    
    mesh_file = pos_args[0]
    
    javabridge.start_vm(class_path=bioformats.JARS, max_heap_size='10G', run_headless=True)

    try:
        # running the following line removes the following output from appearing:
        # log4j:WARN No appenders could be found for logger (loci.common.NIOByteBufferProvider).
        # log4j:WARN Please initialize the log4j system properly.
        # log4j:WARN See http://logging.apache.org/log4j/1.2/faq.html#noconfig for more info.
        bioformats.init_logger()

        MeshModel = get_data(mesh_file, verbose=options.verbose)
        print >> sys.stderr, MeshModel
            
    finally:
        javabridge.kill_vm()
    
    return 0 # everything went well


if __name__ == "__main__":
    sys.exit(main())
