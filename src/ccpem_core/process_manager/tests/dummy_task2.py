#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
from __future__ import print_function
import sys
import os

def main():
    files = sys.argv[1:]
    for f in files:
        assert os.path.exists(f)
        print('Found file : ', f)

    # If files ok
    f = open('check.log', 'w')
    f.write('All files present')
    f.close()

if __name__ == '__main__':
    main()