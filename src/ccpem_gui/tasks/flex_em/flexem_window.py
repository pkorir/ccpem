#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from PyQt4 import QtGui, QtCore, QtWebKit
from ccpem_core.tasks.flex_em import flexem_task
from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_gui.jsmol_viewer.molecular_viewer \
    import MolecularBrowserNoServer
try:
    import modeller
    modeller_available = True
except ImportError:
    modeller_available = False


class FlexEMWindow(window_utils.CCPEMTaskWindow):
    '''
    FlexEM window.
    '''
    def __init__(self,
                 task,
                 parent=None):
        super(FlexEMWindow, self).__init__(task=task,
                                           parent=parent)
        self.output_pdb = None

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        # Mode
        self.mode_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='mode',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.mode_input)
        # Input map
        self.map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_map',
            file_types=ccpem_file_types.mrc_ext,
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_input)
        # Input pdb
        pdb_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_pdb',
            file_types=ccpem_file_types.pdb_ext,
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(pdb_input)
        pdb_input.value_line.textChanged.connect(
            self.set_pdb_preview)
        # Input rigid body file
        rigid_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_rigid',
            required=True,
            file_types=ccpem_file_types.text_ext,
            args=self.args)
        self.args_widget.args_layout.addWidget(rigid_input)
        # Input other args
        layout = QtGui.QVBoxLayout()
        for arg in ['map_resolution', 'iterations', 'max_atom_disp']:
            arg_menu = window_utils.NumberArgInput(parent=self,
                                                   arg_name=arg,
                                                   required=True,
                                                   args=self.args)
            layout.addWidget(arg_menu)
        self.args_widget.args_layout.addLayout(layout)

        # Extended options
        options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Extended options',
            button_tooltip='Extended Flex-EM options')
        self.args_widget.args_layout.addLayout(options_frame)
        # -> Phi-Psi restraints
        phi_psi_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='phi_psi_restraints',
            args=self.args)
        options_frame.add_extension_widget(phi_psi_input)

        # Alpha test... refine B's; calc FSC; HM validation
        if True:
            refine_b_input = window_utils.ChoiceArgInput(
                parent=self,
                arg_name='refine_b',
                args=self.args)
            options_frame.add_extension_widget(refine_b_input)
            #
            fsc_calc_input = window_utils.ChoiceArgInput(
                parent=self,
                arg_name='fsc_calc',
                args=self.args)
            options_frame.add_extension_widget(fsc_calc_input)
            #
            self.half_map_1_input = window_utils.FileArgInput(
                parent=self,
                arg_name='half_map_1',
                args=self.args,
                required=True)

            self.half_map_2_input = window_utils.FileArgInput(
                parent=self,
                arg_name='half_map_2',
                args=self.args,
                required=True)

            # -> Validation on
            self.validation_button = window_utils.CheckArgInput(
                parent=self,
                arg_name='validate',
                dependants=[self.half_map_1_input, self.half_map_2_input],
                args=self.args)
            options_frame.add_extension_widget(self.validation_button)

            # -> Half map 1
            options_frame.add_extension_widget(self.half_map_1_input)

            # -> Half map 2
            options_frame.add_extension_widget(self.half_map_2_input)

        # Display pdb
        self.pdb_preview = MolecularBrowserNoServer()
        self.pdb_preview.setMinimumSize(QtCore.QSize(404, 404))
        self.pdb_preview.setMaximumSize(QtCore.QSize(404, 404))
        self.pdb_preview.setUrl(QtCore.QUrl('about:blank'))
        self.args_widget.args_layout.addWidget(self.pdb_preview)
        self.args_widget.args_layout.setAlignment(self.pdb_preview,
                                                  QtCore.Qt.AlignCenter)
        if self.args.input_pdb.value is not None:
            self.set_pdb_preview(filename=self.args.input_pdb.value)

        # Add files to launcher
        self.launcher.add_file(
            arg_name='input_rigid',
            file_type='standard',
            description='Rigid body cluster',
            selected=True)
        self.launcher.add_file(
            arg_name='input_pdb',
            file_type='pdb',
            description=self.args.input_pdb.help,
            selected=True)
        self.launcher.add_file(
            arg_name='input_map',
            file_type='map',
            description=self.args.input_map.help,
            selected=True)

    @QtCore.pyqtSlot(QtCore.QString)
    def set_pdb_preview(self, filename):
        filename = str(filename)
        filename = os.path.abspath(filename)
        if '.pdb' in filename:
            if os.path.exists(filename):
                filename = os.path.abspath(filename)
                self.pdb_preview.load_file(program='preview',
                                           model1=filename)

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.  For now show starting, refined
        pdb and experimental map.
        '''
        md_output_pdb = os.path.join(self.args.job_location.value,
                                     '1_MD/final1_mdcg.pdb')
        cg_output_pdb = os.path.join(self.args.job_location.value,
                                     '1_CG/final1_cg.pdb')
        if os.path.exists(md_output_pdb):
            self.output_pdb = md_output_pdb
        elif os.path.exists(cg_output_pdb):
            self.output_pdb = cg_output_pdb
        if self.output_pdb is not None:
            self.launcher.add_file(
                arg_name=None,
                file_type='pdb',
                path=self.output_pdb,
                description='Output PDB',
                display_from='finished',
                selected=True)
        self.set_results_jsmol()
        self.set_rv_ui()

    def set_results_jsmol(self):
        '''
        Show results with jsmol viewer
        '''
        self.pdb_results = MolecularBrowserNoServer()
        self.pdb_results.setUrl(QtCore.QUrl('about:blank'))
        assert os.path.isfile(self.args.input_pdb.value)
        assert os.path.isfile(self.output_pdb)
        self.pdb_results.load_file(program='flexem',
                                   model1=self.args.input_pdb.value,
                                   model2=self.output_pdb)
        self.launcher_dock = QtGui.QDockWidget('Structures',
                                               self,
                                               QtCore.Qt.Widget)
        self.launcher_dock.setWidget(self.pdb_results)
        self.tabifyDockWidget(self.pipeline_dock, self.launcher_dock)

    def set_rv_ui(self):
        '''
        RVAPI results viewer.
        '''
        rv_index = os.path.join(
            self.task.job_location,
            'rvapi_data/index.html')
        if os.path.exists(rv_index):
            self.web_view = QtWebKit.QWebView()
            self.web_view.load(QtCore.QUrl(rv_index))
            self.launcher_dock = QtGui.QDockWidget('Results',
                                                   self,
                                                   QtCore.Qt.Widget)
            self.launcher_dock.setWidget(self.web_view)
            self.tabifyDockWidget(self.pipeline_dock, self.launcher_dock)

def main():
    '''
    Launch stand alone window.
    '''
    if modeller_available:
        window_utils.standalone_window_launch(
            task=flexem_task.FlexEM,
            window=FlexEMWindow)
    else:
        print ('Modeller required and not available'
               '\nFor installation details please see: '
               '\n    https://salilab.org/modeller/')

if __name__ == '__main__':
    main()
