#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.refmac import refmac_task

class LocScale(task_utils.CCPEMTask):
    '''
    CCPEM LocScale Wrappr.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='LocScale',
        author='A. J. Jakobi, M. Wilmanns, C. Sachse',
        version='1.1',
        description=(
            'Local amplitude sharpening based target structure'),
        short_description=(
            '[BETA TEST] Local amplitude sharpening based target structure'
            'Coefficient'),
        documentation_link='http://www.biorxiv.org/content/biorxiv/early/2017/03/29/121913.full.pdf',
        references=None)

    def __init__(self,
                 task_info=task_info,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        # XXX to be set
        command = 'loc-scale'
        #
        super(LocScale, self).__init__(
           command=command,
           task_info=task_info,
           database_path=database_path,
           args=args,
           args_json=args_json,
           pipeline=pipeline,
           job_location=job_location,
           parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        map_path = parser.add_argument_group()
        map_path.add_argument(
            '-target_map',
            '--target_map',
            help='Target map to be auto sharpened (mrc format)',
            metavar='Target map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-resolution',
            '--resolution',
            help='Resolution of target map (Angstrom)',
            metavar='Resolution',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-pixel_size',
            '--pixel_size',
            help='Pixel size in Angstrom',
            metavar='Pixel size',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-reference_model',
            '--reference_model',
            help='Reference model (PDB format)',
            metavar='Reference model',
            type=str,
            default=None)
        # 
        parser.add_argument(
            '-refine_bfactors',
            '--refine_bfactors',
            help='Refine reference structure B-factors using Refmac',
            metavar='Refine B-factors',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-reference_map',
            '--reference_map',
            help='Reference map',
            metavar='Reference map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-window_size',
            '--window_size',
            help='Window size in pixel',
            metavar='Window size',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-use_mpi',
            '--use_mpi',
            help='Use mpi for parallel processing',
            metavar='Use MPI',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-n_mpi',
            '--n_mpi',
            help='Number of mpi nodes',
            metavar='MPI nodes',
            type=float,
            default=3)
        #
        return parser

    def run_pipeline(self, job_id=None, db_inject=None):
        pl = [[]]
#         # Run Refmac to refine reference structure B-factors
#         if self.args.refine_bfactors():
#             self.refmac_process = refmac_task.RefmacRefine(
#                 command=self.command,
#                 job_location=self.job_location,
#                 pdb_path=self.args.reference_model(),
#                 mtz_path=self.process_free_r_flags.hklout,
#                 resolution=self.args.resolution.value,
#                 mode='Global',
#                 name='Refmac refine (global)',
#                 sharp=None,
#                 ncycle=20,
#                 output_hkl=True)
#             pl.append([self.refmac_process.process])
#             # XXX TODO - get refmac structure
#             loc_scale_structure = ''
#         else:
#             loc_scale_structure = self.args.reference_model

        # Calculate Fcalc
        self.refmac_sfcalc_crd_process = refmac_task.RefmacSfcalcCrd(
            job_location=self.job_location,
            pdb_path=self.args.reference_model(),
            resolution=self.args.resolution())
        self.refmac_sfcalc_mtz = os.path.join(
            self.job_location,
            'sfcalc_from_crd.mtz')

        # Generate process
        self.loc_scale_process = LocScaleWrapper(
            command=self.command,
            target_map=self.args.reference_map(),
            target_resolution=self.args.resolution(),
            reference_map=self.args.reference_map(),
            reference_structure=loc_scale_structure,
            job_location=self.job_location,
            name='Loc Scale')

        pl.append([self.loc_scale_process.process])
        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            db_inject=db_inject,
            database_path=self.database_path,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

class LocScaleWrapper(object):
    '''
    Wrapper for LocScale process.
    '''
    def __init__(self,
                 command,
                 job_location,
                 target_map,
                 target_resolution,
                 reference_map,
                 reference_structure,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__

        # Set args
        # XXX Todo

        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)

def main():
    '''
    Run task
    '''
    task_utils.command_line_task_launch(
        task=LocScale)

if __name__ == '__main__':
    main()
