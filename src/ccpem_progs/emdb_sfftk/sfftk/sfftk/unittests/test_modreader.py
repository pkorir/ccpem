#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division

"""
test_modreader.py

Unit tests for modreader module

Run as:
python -m unittest test_modreader

TODO:
- 

Version history:
0.1.0, 2015-10-19, basic, chunk and data tests

"""

__author__ = 'Paul K. Korir, PhD'
__email__ = 'pkorir@ebi.ac.uk'
__date__ = '2015-10-19'


import sys
import os.path
import glob
import unittest
import tests
from readers.modreader import get_data, parse_args


class TestModreader_basic(unittest.TestCase):
    """
    Unit tests on basic read of .mod file
    """
    def test_read_pass(self):
        """
        Test that valid file passes
        """
        self.mod = get_data(open(tests.TEST_DATA_PATH + 'test_data/test_data.mod', 'rb'))
        self.assertEqual(self.mod.version[0], 'V')
    
    def test_read_fail1(self):
        """
        Test that file missing 'IMOD' at beginning fails
        """
        f = open(tests.TEST_DATA_PATH + 'test_data/test_bad_data1.mod', 'rb')
        with self.assertRaises(ValueError):
            get_data(f) # missing 'IMOD' start
        
    def test_read_fail2(self):
        """
        Test that file missing 'IEOF' at end fails
        """
        f = open(tests.TEST_DATA_PATH + 'test_data/test_bad_data2.mod', 'rb')
        with self.assertRaises(ValueError):
            get_data(f) # missing 'IEOF' end

class TestModreader_chunks(unittest.TestCase):
    """
    Unit tests on chunks
    """
    def setUp(self):
        """
        Setup a .mod file
        """
        self.mod = get_data(open(tests.TEST_DATA_PATH + 'test_data/test_data.mod', 'rb'))
        
    def test_IMOD_pass(self):
        """
        Test that IMOD chunk read
        """
        self.assertTrue(self.mod.isset)
    
    def test_OBJT_pass(self):
        """
        Test that OBJT chunk read
        """
        for o,O in self.mod.objts.iteritems():
            self.assertTrue(O.isset)
    
    def test_CONT_pass(self):
        """
        Test that CONT chunk read
        """
        for o,O in self.mod.objts.iteritems():
            for c,C in O.conts.iteritems():
                self.assertTrue(C.isset)
    
    def test_MESH_pass(self):
        """
        Test that MESH chunk read
        """
        for o,O in self.mod.objts.iteritems():
            for m,M in O.meshes.iteritems():
                self.assertTrue(M.isset)
    
    def test_IMAT_pass(self):
        """
        Test that IMAT chunk read
        """
        for o,O in self.mod.objts.iteritems():
            self.assertTrue(O.imat.isset)
    
    def test_VIEW_pass(self):
        """
        Test that VIEW chunk read
        """
        for v,V in self.mod.views.iteritems():
            self.assertTrue(V.isset)
    
    def test_MINX_pass(self):
        """
        Test that MINX chunk read
        """
        self.assertTrue(self.mod.minx.isset)
    
    def test_MEPA_pass(self):
        """
        Test that MEPA chunk read
        """
        for o,O in self.mod.objts.iteritems():
            try:
                self.assertTrue(O.mepa.isset)
            except AttributeError:
                self.assertEqual(O.mepa, None)
    
    def test_CLIP_pass(self):
        """
        Test that CLIP chunk read
        """
        for o,O in self.mod.objts.iteritems():
            try:
                self.assertTrue(O.clip.isset)
            except AttributeError:
                self.assertEqual(O.clip, None)


class TestModreader_data(unittest.TestCase):
    """
    Unit tests to validate data in file
    """
    def setUp(self):
        """
        Setup a .mod file
        """
        self.mod = get_data(open(tests.TEST_DATA_PATH + 'test_data/test_data.mod', 'rb'))
    
    def test_number_of_OBJT_chunks(self):
        """
        Test that compares declared and found OBJT chunks
        """
        self.assertEqual(self.mod.objsize, len(self.mod.objts))
    
    def test_number_of_CONT_chunks(self):
        """
        Test that compares declared and found CONT chunks
        """
        for o,O in self.mod.objts.iteritems():
            self.assertEqual(O.contsize, len(O.conts))
    
    def test_number_of_MESH_chunks(self):
        """
        Test that compares declared and found MESH chunks
        """
        for o,O in self.mod.objts.iteritems():
            self.assertEqual(O.meshsize, len(O.meshes))
    
    def test_number_of_surface_objects(self):
        """
        Test that compares declared and found surface objects
        """
        for o,O in self.mod.objts.iteritems():
            no_of_surfaces = 0
            for c,C in O.conts.iteritems():
                if C.surf != 0:
                    no_of_surfaces += 1
                    
            self.assertEqual(O.surfsize, no_of_surfaces)
        
    
    def test_number_of_points_in_CONT_chunk(self):
        """
        Test that compares declared an found points in CONT chunks
        """
        for o,O in self.mod.objts.iteritems():
            for c,C in O.conts.iteritems():
                self.assertEqual(C.psize, len(C.pt))
    
    def test_number_of_vertex_elements_in_MESH_chunk(self):
        """
        Test that compares declared an found vertices in MESH chunks
        """
        for o,O in self.mod.objts.iteritems():
            for m,M in O.meshes.iteritems():
                self.assertEqual(M.vsize, len(M.vert))
    
    def test_number_of_list_elements_in_MESH_chunk(self):
        """
        Test that compares declared an found indices in MESH chunks
        """
        for o,O in self.mod.objts.iteritems():
            for m,M in O.meshes.iteritems():
                self.assertEqual(M.lsize, len(M.list))


class TestModreader_commandlineoptions(unittest.TestCase):
    def test_missing_arguments(self):
        """
        Test that missing arguments raise TypeError
        """
        self.assertRaises(TypeError, parse_args, [])
    
    def test_missing_imod_file(self):
        """
        Test that missing IMOD model file raises TypeError
        """
        self.assertRaises(TypeError, parse_args, ["-o", 'sys.stdout'])
    
    def test_output_missing_becomes_stdout(self):
        """
        Test that undefined output resolves to stdout
        """
        options, args = parse_args(["somefile.mod"])
        self.assertEqual(options.output, sys.stdout)
    
    def test_output_present(self):
        """
        Test that defined output is a string e.g. 'sys.stdout' (special case)
        """
        options, args = parse_args(["-o", "sys.stdout", "somefile.mod", ])
        self.assertEqual(options.output, 'sys.stdout')
        
    def test_show_chunks_True(self):
        """
        Test for show_chunks == True
        """
        options, args = parse_args(["-s", "somefile.mod"])
        self.assertTrue(options.show_chunks)
    
    def test_show_chunks_False(self):
        """
        Test for show_chunks == False
        """
        options, args = parse_args(["somefile.mod"])
        self.assertFalse(options.show_chunks)
        options, args = parse_args(["-o", "sys.stdout", "somefile.mod"])
        self.assertFalse(options.show_chunks)


if __name__ == "__main__":
    unittest.main()