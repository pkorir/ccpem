cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

set(PACKAGE_NAME "src")

if(COMMAND add_python_paths)
    # Path to ccpem/src/ (i.e. this directory)
    add_python_paths(":${CMAKE_CURRENT_SOURCE_DIR}")
endif()
