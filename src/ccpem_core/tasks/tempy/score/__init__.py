# See examples
'''
Calculate_scores.py
* Input
    1) map
    2) pdb
        -> Need functionality to compare scores for multiple PDB inputs
            -> i.e. to decide which is best fit from multiple model inputs.
            -> Run in scoring in parallel
                -> set number of CPUs
    3) resolution
    4) Contour map in absolute vale (optional, default is 1.5 sigma)

* Output
    1) Overall metrics:
        - Percentage overlap (model vs map)
        - CC (local cross-corr.):
            - Local single value for overlapping pixels
            - Global single value for entire unit cell
        - Mutual infomation score (bin maps, calculate if bins are in regs, entropy based score)
            (ranges from 0 to high positive, higher is better, one is good, only scored on overlapping region)
            - Local and global (normalized) as CC
        - Normal vector score (angular difference between surface normals, between 0-1, 1 best)
            - Global
        - Surface distance (Normalized distance between surface points)
            - Global

    * Maya:
    Several cats:
        - Density based scores
        - Surface based scores
        - Combined scores
        - Cons.
            -> borda
                -> Generates cons. by ranking
                -> see Examples/Consensus_ScoredEnsemble.py

    Given multiple fits:
        -> Run borda
        -> Run clustering by RMSD
            -> Make heat map by score metric, can be CCC, MI, NV, NV-S
            -> Score_and_Cluster_Plot.py
                -> makes matplotlib object
                    -> Figure 2 of TEMPy j. app. xtal. Farabella
                    -> Figure 3
                        -> Output top cluster (Agnel to write).

    * Run as pipeline
        -> Show globals results seper. as they are calculated before...

    2) Local (segment) CC
        SCCC - Seg. based cross correlation (better for lower res maps)
            -> Based on rigid bodies
                -> Use SSE if no rigid body file is added
                -> calculate_SCCC_RBfile.py
                    
        #
        SMOC (better high res maps for <5Ang)
            - Scores overlapping residue window (parameter for n residues; 
                default 9, could use ~5 for high res e.g. 3Ang)
            - score_smoc.py
            - Agnel will generate SMOC plot per chain, tile in GUI
            - Generates PDB with scoring in Q col. 
                - Agnel will make a chimera script to colour by Q
                - Colour range set to min/max as dep. on resolution
                    -> Make sure to note this in tool tips

    * Example script

# Global scores
ccpem-python  ~/Code/devtools/checkout/ccpem/src/ccpem_core/TEMPy/Examples/Calculate_scores.py -m ~/Code/devtools/checkout/ccpem/example_data/tempy/Examples/Test_Files/1akeA_10A.mrc -p ~/Code/devtools/checkout/ccpem/example_data/tempy/Examples/Test_Files/1ake_mdl1.pdb -r 10 

-> Get from stdout / Agnel to make json output of info below
Percent overlap: 0.922600619195
Local correlation score:  0.975722056375
Local Mutual information score:  0.303642272949
Normalized Mutual information score: 1.24150276184
Surface distance score:  0.839393233078
Normal vector score:  0.917336248693

# Local scoring
ccpem-python  ~/Code/devtools/checkout/ccpem/src/ccpem_core/TEMPy/Examples/score_smoc.py ~/Code/devtools/checkout/ccpem/example_data/tempy/Examples/Test_Files/1akeA_10A.mrc ~/Code/devtools/checkout/ccpem/example_data/tempy/Examples/Test_Files/1ake_mdl1.pdb 10

-> plots drawn in score_smoc -> from line ~67.
    -> Take as base and make new plotting that takes raw smoc data and plots for GUI
    -> Output dict_chains_scores as json object
        -> Will contain metadata info for plots i.e.:
            a: res: score


XXX - Add above to Flex-EM pipeline i.e. to be run automatically after Flex-EM

# Make as seperate interface
    -> Heatmap SCCC
        -> see figure 1/2 supp figure 2
            -> 'Local assessment of local quality' eLIFE
