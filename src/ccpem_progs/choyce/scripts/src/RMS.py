'''
RMS -   module calculating th RMS between a reference crystal structure
        and the density fitted models from CHOYCE
'''

import sys

from modeller import *

if len(sys.argv) != 2:
    print 'Usage: script.py [reference structure]'
    sys.exit()

# read in reference structure name from keyboard
reference = sys.argv[1]
# load data from 'Superposed_models_ALL.txt' file
try:
    IN = open('Superposed_models_All.txt')
    header  = IN.readline()                           # skip header line
    lines   = IN.readlines()
    IN.close()
except:
    print 'Could not open Superposed_models_All.txt file'
    sys.exit()


env = environ()

# open file for output 'All.txt'
OUT = open('RMSD.txt','w')
OUT.write('%s %s %s RMS\n' %(header.split()[3], header.split()[4], header.split()[2]))
#OUT.write('%s %s %s RMS\n' %(header.split()[3], header.split()[1], header.split()[2]))

# loop through lines
for line in lines:

    aln = alignment(env)
    # load in reference structure
    mdl  = model(env, file=reference)
    aln.append_model(mdl=mdl, align_codes='1', atom_files='1')

    
    superposed = line.split()[3]
    print superposed

    mdl2 = model(env, file=superposed)
    aln.append_model(mdl=mdl2, align_codes='2', atom_files='2')
#    aln.align(gap_penalties_1d=(-600, -400))
    aln.salign(rr_file='$(LIB)/as1.sim.mat',  # Substitution matrix used
               output='',
               max_gap_length=20,
               gap_function=False, #True,              # If False then align2d not done
               feature_weights=(1., 0., 0., 0., 0., 0.),
               gap_penalties_1d=(-600, -400), #(-100, 0),
               gap_penalties_2d=(3.5, 3.5, 3.5, 0.2, 4.0, 6.5, 2.0, 0.0, 0.0),
               # d.p. score matrix
               #write_weights=True, output_weights_file='salign.mtx'
               similarity_flag=True)   # Ensuring that the dynamic programming
                                       # matrix is not scaled to a difference matrix
    aln.write('reference_'+superposed+'.ali')

    #---align and create alignment file (ALIGN2D here)
#    aln.salign(rr_file='$(LIB)/as1.sim.mat',  # Substitution matrix used
#               max_gap_length=20,
#               gap_function=True,#False, #True,              # If False then align2d not done
#               feature_weights=(1., 0., 0., 0., 0., 0.),
#               gap_penalties_1d=(-600, -400), #(-100, 0),
#               gap_penalties_2d=(3.5, 3.5, 3.5, 0.2, 4.0, 6.5, 2.0, 0.0, 0.0),
#               gap_penalties_3d=(0, 2.0),
#               similarity_flag=True)   # Ensuring that the dynamic programming
#                                       # matrix is not scaled to a difference matrix

    #aln = alignment(env, file='tmp.pir', align_codes=('actin_yao', 'actin_A'))
    # Genereate and save structure=structure alignment
    atmsel = selection(mdl).only_atom_types('CA')
    r = atmsel.superpose(mdl2, aln,fit=True)
    rms = r.rms
    drms = r.drms  

    # write out rmsd and equivalent posisitions according to the alignment
##    print "initial rms= %.2f" % r.initial_rms
##    print "rms= %.2f" % r.rms
##    print "%d equivalent posisitons" % r.num_equiv_pos 
    OUT.write('%s %s %s %f\n' %(superposed, line.split()[4], line.split()[2], rms))
    #OUT.write('%s %s %s %f\n' %(superposed, line.split()[1], line.split()[2], rms))
OUT.close()
