import sys
import re
import os
import time
import pyrvapi
import pyrvapi_ext
from pyrvapi_ext import parsers

#have not modified anything checking on what to be shown in results from nautilus

def fix_fsc_table(line):
    return re.sub('sigmaSig ZZ TT cor(\|F1\|,\|F2\|) \$\$', 'sigmaSig ZZ TT cor(\|F1\|,\|F2\|) H1 H2 \$\$', line)
    '''
    error_line = ' 2sin(th)/l 2sin(th)/l NREF sigma  FSC PHdiff cos(PHdiff) sigmaSig ZZ TT cor(|F1|,|F2|) $$'
    fixed_line = ' 2sin(th)/l 2sin(th)/l NREF sigma  FSC PHdiff cos(PHdiff) sigmaSig ZZ TT cor(|F1|,|F2|) H1 H2 $$'
    '''

def fix_stars(line):
    return re.sub('0.0\*\*\*\*\*\*  0.00', '0.0  0.00  0.00', line)

def main(stdin):
    doc = pyrvapi_ext.document.newdoc(wintitle='Nautilus Pipeline Output',
                                      xml='output.xml',
                                      layout=pyrvapi.RVAPI_LAYOUT_Tabs)
    doc.add_header('Parsed output')
    pyrvapi_ext.flush()
    log_parser = parsers.generic_parser

    tab = pyrvapi_ext.pyrvapi_tab('Nautilus Pipeline Results')
    # Wait until stdin path exists before opening
    while not os.path.exists(stdin):
        time.sleep(1)
    istream = open(stdin, 'r')
    ostream = sys.stdout
    kwargs = dict(verbose=True, pause=0, patches=[fix_fsc_table, fix_stars])
    log_parser(tab.id, True, job_params=None, xmlout=None).parse_stream(istream, ostream, **kwargs)

if __name__ == '__main__':
    '''
    usage:
        ccpem-python nautilus_results.py <path_to_nautiluspipeline_stdout>
    or:
        ccpem-python -m ccpem_core.gui_ext.tasks.nautilus.nautilus_results
            ./test_data/nautiluspipeline_stdout.txt
    '''
    if len(sys.argv) == 2:
        stdin = sys.argv[1]
        main(stdin=stdin)
