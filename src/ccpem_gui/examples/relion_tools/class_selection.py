#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
# Class selection tools for RELION

import sys
from ccpem_core import utils
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core.relion_tools.gallery_viewer import click_gallery_viewer
from textwrap import dedent
from ccpem_core.star_io_mmdb.ext import star_io_mmdb as star_io_mmdb_ext
from ccpem_core.mrc_map_io_clipper import map_tools


def option_parser():
    parser = ccpem_argparser.ccpemArgParser(
        fromfile_prefix_chars='@',
        description='Relion class selection tools',
        epilog=dedent(
            'To import parameters from argument text file use e.g. :'
            'python class_selections.py @my_args.txt'))

    # Positional arg (required)
    parser.add_argument(
        '--particle_star_filename',
        help = 'Name of star file containing particle metadata to parse ')
    # Star output
    star_out_grp = parser.add_argument_group()
    star_out_grp.add_argument(
        '-os',
        '--output_star_file',
        help='Output star file containing selected class metadata',
        type=bool,
        default=True)
    star_out_grp.add_argument(
        '-osn',
        '--output_star_filename',
        help='Output star filename',
        default=None,
        type=str)
    # Awk output
    awk_out_grp = parser.add_argument_group()
    awk_out_grp.add_argument(
        '-oa',
        '--output_awk_file',
        help='Output awk file to manually parse star file (as per RELION manual)',
        nargs='?',
        type=bool,
        default=False)
    awk_out_grp.add_argument(
        '-oan',
        '--output_awk_filename',
        help='Output awk filename',
        default=None,
        type=str)
    #
    # Class selection options
    selection_method_grp = parser.add_argument_group()
    selection_method_grp.add_argument(
        '-ss',
        '--select_from_mrc_file',
        help='File name of mrc file containing template classes, allows gallery selection',
        default=None,
        type=str)
    selection_method_grp.add_argument(
        '-sl',
        '--select_from_list',
        help=('Enter list of class number to select separated by a space e.g. '
              '"1 2 3 5 18"'),
        default=None,
        nargs='+',
        type=int)
    selection_method_grp.add_argument(
        '-se',
        '--select_manual',
        help='Manually enter class number when prompted at the command line',
        default=False,
        type=bool)
    #
    return parser


# Write old style awk script for parsing class selections (as RELION tutorial)
def write_class_selection_awk_script(
        class_selection_list,
        awk_script_filename='tst_awk_star_for_selected_particles.sh',
        particle_star_filename='./tst_data/run1_it025_data.star',
        selected_star_filename='particles_selected_class2d.star'):
    awk_line = "awk '{if (NF < 3) {print} else {if ("
    col_rlnClassNumber = '20'
    for cs in class_selection_list:
        cs_awk = '$' + col_rlnClassNumber + '==' + str(cs) + ' || '
        awk_line += cs_awk
    awk_line = awk_line[0:-4] + " ) print }}' < " \
        + particle_star_filename + ' > ' + selected_star_filename
    if True:
        print awk_line
    else:
        awk_script = open(awk_script_filename, 'w')
        awk_script.write(awk_line)


# Get command line class selection
def get_command_line_class_selection():
    class_selection = raw_input(
        'Please enter class numbers to select : \n (e.g. 1 2 4 10; '
        'enter to finish)\n')
    class_selection = class_selection.replace(',', ' ')
    class_selection = class_selection.split()
    return class_selection

# Check class selection, remove duplicates and sort
def process_class_selections(class_selection, number_classes):
    utils.print_sub_header(message='Final class selection')
    class_selection_parsed = []
    warning_cntr = 0
    for selection in class_selection:
        try:
            sel = int(selection)
        except ValueError:
            continue
        if (sel <= number_classes) and (sel not in class_selection_parsed):
            class_selection_parsed.append(sel)
        else:
            print ('  WARNING - following class not assigned to any particles '
                   'so removed : {0:3d}').format(sel)
            warning_cntr += 1
    class_selection = sorted(class_selection_parsed)
    utils.print_sub_sub_header(message='Selected classes')
    print '    n    Class'
    for n, selected_image in enumerate(class_selection):
        print '   {0:2d} {1:8d} '.format(n+1, selected_image)
    if warning_cntr > 0:
        print '  Number of WARNINGS : {0:3d}'.format(warning_cntr)
    if len(class_selection) < 1:
        print "\nSorry: No suitable classes selected"
        utils.print_footer()
        sys.exit()
    return class_selection


def star_obj_keep_selected_classes(star_obj,
                                   class_assignments,
                                   class_selection):
    del_images = []
    for n, ca in enumerate(class_assignments):
        if ca not in class_selection:
            del_images.append(n)
    star_obj.DeleteLoopRows('images', del_images)


def main(parser=None):
    utils.print_header(message='RELION class selection')
    if parser == None:
        parser = option_parser()
        parser.print_argument_current_status()
    args = parser.parse_args()
    if args.particle_star_filename in ['None', None]:
        print '\nSorry: Particle star filename is not defined in arguments'
        utils.print_footer()
        sys.exit()

    # Get star data and class assignment
    particle_star = star_io_mmdb_ext.MMDBStarObj(args.particle_star_filename,
                                                 'r',
                                                 False)
    if particle_star.rc != 0:
        print 'Error reading star file'
    class_assignments = particle_star.GetLoopRealVector('images',
                                                        '_rlnClassNumber')
    min_class_num = int(min(class_assignments))
    max_class_num = int(max(class_assignments))
    utils.print_sub_header(message = 'Import star metadata')
    print '  Star filename       : ', args.particle_star_filename
    print '  Number of particles : ', len(class_assignments)
    print '  Number of classes   : ', max_class_num

    # Various class selection methods
    #
    # Click gallery for visual class selection
    if args.select_from_mrc_file not in ['None', None]:
        # Import images directly from mrc image stack
        utils.print_sub_header(message = 'Import mrc stack')
        nx_imagestack = map_tools.ClipperNXMap(filename = args.select_from_mrc_file)
        if not nx_imagestack.is_image_stack:
            print '\nSorry: supplied mrc file is not an image stack'
            utils.print_footer()
            sys.exit()
        nx_imagestack.print_stats()
        # Convert stack to pil images
        pil_image_list = nx_imagestack.get_pil_image_array_from_stack(auto_contrast = True)
        # Start click gallery viewer
        # Check if class numbering starts at 0 or 1
        if min_class_num == 0:
            image_tag_list = list(xrange(0, len(pil_image_list)))
        else:
            image_tag_list = list(xrange(1, len(pil_image_list)+1))
        cgv = click_gallery_viewer.gallery_click_buttons(
            window_title='RELION classes',
            pil_image_list=pil_image_list,
            image_tag_list=image_tag_list)
        class_selection = cgv.selected_images

    # TODO
    # Convert pil array to pixmap
    if False:
        for image in pil_image_list:
            print type(image)
            pixmap = utils.convert_pil_image_to_pyqt_pixmap(image)
            print type(pixmap)
            assert 0

    elif args.select_from_list not in ['None', None]:
        print args.select_from_list
        class_selection = args.select_from_list

    elif args.select_manual:
        class_selection = get_command_line_class_selection()

    else:
        print '\nSorry: No class selection method defined in arguments'
        utils.print_footer()
        sys.exit()

    # Process star file and keep selected particles
    class_selection = process_class_selections(class_selection=class_selection,
                                               number_classes=max_class_num)
    star_obj_keep_selected_classes(star_obj=particle_star,
                                   class_assignments=class_assignments,
                                   class_selection=class_selection)

    # Output new star file with selected classes
    utils.print_sub_header(
        message='Export star metadata for selected particles')
    class_assignments = particle_star.GetLoopRealVector('images',
                                                        '_rlnClassNumber')
    if args.output_star_filename in ['None', None]:
        star_output_filename = \
            args.particle_star_filename + '_class_parsed.star'
    else:
        star_output_filename = args.output_star_filename
    print '  Star filename       : ', star_output_filename
    print '  Number of particles : ', len(class_assignments)
    #
    particle_star.WriteMMCIFFile(star_output_filename)
    if particle_star.rc != 0:
        print 'Error writing star file'
    print '\n  New star file containing selected classes ready for next round of RELION'

    if args.output_awk_file:
        if args.output_star_filename in ['None', None]:
            star_output_filename = args.particle_star_filename + '_class_parsed.star'
        else:
            star_output_filename = args.output_star_filename
        awk_filename = './tst_data_output/tst_awk_star_for_selected_particles.sh'
        write_class_selection_awk_script(
            class_selection_list=class_selection,
            awk_script_filename=awk_filename)
    # End
    utils.print_footer()

if __name__ == '__main__':
    main()
