import sys
import os
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4 import QtWebKit


class WebViewWidget(QtWebKit.QWebView):
    def __init__(self,
                 html_path,
                 parent=None):
        super(WebViewWidget, self).__init__(parent)
        if os.path.exists(html_path):
            self.load(QtCore.QUrl(html_path))
        else:
            print 'html path not found'

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    web_viewer = WebViewWidget(html_path='example.html')
    web_viewer.show()
    sys.exit(app.exec_())
